-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2019 at 01:37 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_balioz`
--

-- --------------------------------------------------------

--
-- Table structure for table `forget_request`
--

CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forget_request`
--

INSERT INTO `forget_request` (`forget_request_id`, `forget_request_email`, `code`, `timeout`) VALUES
(1, 'astra.danta@gmail.com', '7XelGtFuOqyrgAbWU2dwV6Nmp', 1556425949),
(2, 'astra.danta@gmail.com', 'odxYVKEMO4XWF9u7ySwL5tZ2f', 1556426062),
(3, 'astra.danta@gmail.com', 'vsAVNol12QtXhD7gEpHbJO8wn', 1556426280),
(4, 'astra.danta@gmail.com', 'IAigrShfm8lE1qysVeK5NCLRz', 1556495904),
(5, 'astra.danta@gmail.com', 'N9GfLVvkzsKuDE25ciPwYFBMg', 1556496180),
(6, 'astra.danta@gmail.com', 'waK4Mkb6RjNL59nrBW8odPmtl', 1556496180),
(7, 'astra.danta@gmail.com', 'f4rwtEoWyeY6B28nj5RgJFuV7', 1556504113),
(8, 'astra.danta@gmail.com', 'bxGBvHiely5kAPLaVYIt1zWE9', 1556505367),
(9, 'astra.danta@gmail.com', 'p5fUBMbcC64tonjhduDaOsLSv', 1556792065);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_tanggal_lahir` varchar(100) DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `nik`, `staff_nama`, `tempat_tanggal_lahir`, `staff_alamat`, `staff_status`, `staff_kelamin`, `mulai_bekerja`, `staff_email`, `staff_phone_number`, `staff_keterangan`, `created_at`, `updated_at`) VALUES
(1, '1909807889237', 'Astradanta', 'Denpasar, 13 Februari 1997', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_lama_kerja`
-- (See below for the actual view)
--
CREATE TABLE `staff_lama_kerja` (
`staff_id` int(11)
,`lama_bekerja` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_staff_id`, `password`, `user_role_id`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$IGCxpiEKZsNN0VOZIFopjOvnTeOmTwuSdRR7zRzmLoMTi9Z.Mytgi', 1, 'assets/media/users/1556714265JXOGF.jpg', '2019-05-01 18:06:31', '2019-05-01 20:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2019-04-26 00:00:00', NULL),
(2, 'Admin', '2019-04-30 06:43:26', NULL),
(3, 'POS Officer', '2019-04-30 06:44:21', NULL);

-- --------------------------------------------------------

--
-- Structure for view `staff_lama_kerja`
--
DROP TABLE IF EXISTS `staff_lama_kerja`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_lama_kerja`  AS  select `staff`.`staff_id` AS `staff_id`,if(((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12) >= 1),concat(round((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12),1),' Tahun'),if((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) = 0),'Kurang dari sebulan',concat(timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()),' Bulan'))) AS `lama_bekerja` from `staff` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forget_request`
--
ALTER TABLE `forget_request`
  ADD PRIMARY KEY (`forget_request_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forget_request`
--
ALTER TABLE `forget_request`
  MODIFY `forget_request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
