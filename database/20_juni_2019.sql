/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_balioz

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-06-20 11:18:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for arus_stock_bahan
-- ----------------------------
DROP TABLE IF EXISTS `arus_stock_bahan`;
CREATE TABLE `arus_stock_bahan` (
  `arus_stock_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `po_bahan_detail_id` int(11) DEFAULT NULL,
  `po_bahan` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT 'insert',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`arus_stock_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of arus_stock_bahan
-- ----------------------------
INSERT INTO `arus_stock_bahan` VALUES ('90', '2019-06-06', 'stock_bahan', '23', null, null, '5', '0', '2', '198', '2068', 'Transfer stok bahan', 'update', '2019-06-06 13:54:33', '2019-06-06 13:54:33');
INSERT INTO `arus_stock_bahan` VALUES ('91', '2019-06-06', 'stock_bahan', '19', null, null, '5', '1', '0', '200', '2070', 'Penerimaan transfer stok', 'update', '2019-06-06 13:54:43', '2019-06-06 13:54:43');
INSERT INTO `arus_stock_bahan` VALUES ('92', '2019-06-06', 'stock_bahan', '23', null, null, '5', '1', '0', '200', '2070', 'Pengembalian sebagian stok', 'update', '2019-06-06 13:54:43', '2019-06-06 13:54:43');
INSERT INTO `arus_stock_bahan` VALUES ('93', '2019-06-06', 'stock_bahan', '23', null, null, '5', '0', '4', '196', '2066', 'Transfer stok bahan', 'update', '2019-06-06 14:12:53', '2019-06-06 14:12:53');
INSERT INTO `arus_stock_bahan` VALUES ('94', '2019-06-06', 'stock_bahan', '19', null, null, '5', '4', '0', '200', '2070', 'Penerimaan transfer stok', 'update', '2019-06-06 14:13:10', '2019-06-06 14:13:10');
INSERT INTO `arus_stock_bahan` VALUES ('95', '2019-06-06', 'stock_bahan', '23', null, null, '5', '0', '5', '195', '2065', 'Penyesuaian stok bahan', 'update', '2019-06-06 14:14:15', '2019-06-06 14:14:15');
INSERT INTO `arus_stock_bahan` VALUES ('96', '2019-06-06', 'stock_bahan', '23', null, null, '5', '0', '15', '180', '2065', 'Delete stok bahan', 'delete', '2019-06-06 14:21:44', '2019-06-06 14:21:44');
INSERT INTO `arus_stock_bahan` VALUES ('97', '2019-06-07', 'stock_bahan', '24', null, null, '4', '200', '0', '1750', '2265', 'Insert stok bahan', 'insert', '2019-06-07 11:23:40', '2019-06-07 11:23:40');
INSERT INTO `arus_stock_bahan` VALUES ('98', '2019-06-07', 'stock_bahan', '19', null, null, '5', '0', '170', '10', '2095', 'Penyesuaian stok bahan', 'update', '2019-06-07 14:07:06', '2019-06-07 14:07:06');
INSERT INTO `arus_stock_bahan` VALUES ('99', '2019-06-07', 'stock_bahan', '12', null, null, '4', '0', '200', '1550', '1895', 'Transfer stok bahan', 'update', '2019-06-07 15:55:07', '2019-06-07 15:55:07');
INSERT INTO `arus_stock_bahan` VALUES ('100', '2019-06-07', 'stock_bahan', '25', null, null, null, '45', '0', '45', '1940', 'Insert stok bahan', 'insert', '2019-06-07 23:32:02', '2019-06-07 23:32:02');
INSERT INTO `arus_stock_bahan` VALUES ('101', '2019-06-07', 'stock_bahan', '26', null, null, null, '45', '0', '90', '1985', 'Insert stok bahan', 'insert', '2019-06-07 23:32:24', '2019-06-07 23:32:24');
INSERT INTO `arus_stock_bahan` VALUES ('102', '2019-06-07', 'stock_bahan', '27', null, null, null, '23', '0', '113', '2008', 'Insert stok bahan', 'insert', '2019-06-07 23:33:28', '2019-06-07 23:33:28');
INSERT INTO `arus_stock_bahan` VALUES ('103', '2019-06-07', 'stock_bahan', '28', null, null, null, '20', '0', '133', '2028', 'Insert stok bahan', 'insert', '2019-06-07 23:35:27', '2019-06-07 23:35:27');
INSERT INTO `arus_stock_bahan` VALUES ('104', '2019-06-07', 'stock_bahan', '29', null, null, '5', '24', '0', '34', '1919', 'Insert stok bahan', 'insert', '2019-06-07 23:40:19', '2019-06-07 23:40:19');
INSERT INTO `arus_stock_bahan` VALUES ('105', '2019-06-07', 'stock_bahan', '29', null, null, '5', '0', '4', '30', '1915', 'Edit stok bahan', 'update', '2019-06-07 23:40:25', '2019-06-07 23:40:25');

-- ----------------------------
-- Table structure for arus_stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `arus_stock_produk`;
CREATE TABLE `arus_stock_produk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` float DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of arus_stock_produk
-- ----------------------------
INSERT INTO `arus_stock_produk` VALUES ('44', '2019-06-06', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '0', '10', '160', '36650', 'update', 'Penyesuaian stok produk', '2019-06-06 14:15:20', '2019-06-06 14:15:20');
INSERT INTO `arus_stock_produk` VALUES ('45', '2019-06-06', 'stock_produk', null, null, null, null, null, null, '14', '8', null, '0', '60', '100', '36650', 'delete', 'Delete stok produk', '2019-06-06 14:22:18', '2019-06-06 14:22:18');
INSERT INTO `arus_stock_produk` VALUES ('46', '2019-06-06', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '0', '20', '80', '36630', 'update', 'Transfer stok produk', '2019-06-06 14:23:06', '2019-06-06 14:23:06');
INSERT INTO `arus_stock_produk` VALUES ('47', '2019-06-06', 'stock_produk', null, null, null, null, null, null, '14', '8', null, '10', '0', '90', '36650', 'update', 'Penerimaan transfer stok', '2019-06-06 14:23:38', '2019-06-06 14:23:38');
INSERT INTO `arus_stock_produk` VALUES ('48', '2019-06-06', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '10', '0', '90', '36650', 'update', 'Pengembalian sebagian stok', '2019-06-06 14:23:38', '2019-06-06 14:23:38');
INSERT INTO `arus_stock_produk` VALUES ('49', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '0', '15', '75', '36635', 'update', 'Transfer stok produk', '2019-06-07 20:50:36', '2019-06-07 20:50:36');
INSERT INTO `arus_stock_produk` VALUES ('50', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '0', '5', '70', '36630', 'update', 'Penyesuaian stok produk', '2019-06-07 20:51:15', '2019-06-07 20:51:15');
INSERT INTO `arus_stock_produk` VALUES ('51', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '14', '8', null, '1', '0', '84', '36645', 'update', 'Penerimaan transfer stok', '2019-06-07 20:52:49', '2019-06-07 20:52:49');
INSERT INTO `arus_stock_produk` VALUES ('52', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '12', '8', null, '14', '0', '84', '36645', 'update', 'Pengembalian sebagian stok', '2019-06-07 20:52:49', '2019-06-07 20:52:49');
INSERT INTO `arus_stock_produk` VALUES ('53', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '15', '8', null, '30', '0', '114', '36675', 'insert', 'Insert stok produk', '2019-06-07 23:46:43', '2019-06-07 23:46:43');
INSERT INTO `arus_stock_produk` VALUES ('54', '2019-06-07', 'stock_produk', null, null, null, null, null, null, '15', '8', null, '5', '0', '119', '36680', 'update', 'Edit stok produk', '2019-06-07 23:47:04', '2019-06-07 23:47:04');

-- ----------------------------
-- Table structure for bahan
-- ----------------------------
DROP TABLE IF EXISTS `bahan`;
CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bahan
-- ----------------------------
INSERT INTO `bahan` VALUES ('4', '1', '1', '1', '01', 'Kain wol', '1000', '0', null, null, '', '2019-05-09 08:26:23', '2019-05-16 10:23:06');
INSERT INTO `bahan` VALUES ('5', '1', '1', '1', '02', 'Kain wol', '250', '0', null, null, '', '2019-05-16 09:38:45', '2019-05-26 16:10:39');

-- ----------------------------
-- Table structure for forget_request
-- ----------------------------
DROP TABLE IF EXISTS `forget_request`;
CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  PRIMARY KEY (`forget_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forget_request
-- ----------------------------
INSERT INTO `forget_request` VALUES ('1', 'astra.danta@gmail.com', '7XelGtFuOqyrgAbWU2dwV6Nmp', '1556425949');
INSERT INTO `forget_request` VALUES ('2', 'astra.danta@gmail.com', 'odxYVKEMO4XWF9u7ySwL5tZ2f', '1556426062');
INSERT INTO `forget_request` VALUES ('3', 'astra.danta@gmail.com', 'vsAVNol12QtXhD7gEpHbJO8wn', '1556426280');
INSERT INTO `forget_request` VALUES ('4', 'astra.danta@gmail.com', 'IAigrShfm8lE1qysVeK5NCLRz', '1556495904');
INSERT INTO `forget_request` VALUES ('5', 'astra.danta@gmail.com', 'N9GfLVvkzsKuDE25ciPwYFBMg', '1556496180');
INSERT INTO `forget_request` VALUES ('6', 'astra.danta@gmail.com', 'waK4Mkb6RjNL59nrBW8odPmtl', '1556496180');
INSERT INTO `forget_request` VALUES ('7', 'astra.danta@gmail.com', 'f4rwtEoWyeY6B28nj5RgJFuV7', '1556504113');
INSERT INTO `forget_request` VALUES ('8', 'astra.danta@gmail.com', 'bxGBvHiely5kAPLaVYIt1zWE9', '1556505367');
INSERT INTO `forget_request` VALUES ('9', 'astra.danta@gmail.com', 'p5fUBMbcC64tonjhduDaOsLSv', '1556792065');

-- ----------------------------
-- Table structure for guest
-- ----------------------------
DROP TABLE IF EXISTS `guest`;
CREATE TABLE `guest` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_nama` varchar(50) DEFAULT NULL,
  `guest_alamat` varchar(50) DEFAULT NULL,
  `guest_telepon` varchar(50) DEFAULT NULL,
  `kewarganegaraan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of guest
-- ----------------------------
INSERT INTO `guest` VALUES ('1', 'Flori Balducci', '82 Helena Road', '(967) 8049009', 'Greece', 'Dietrich-Prosacco', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('2', 'Mariejeanne Harriot', '820 Old Gate Lane', '(840) 1314554', 'China', 'Lang, Cronin and Renner', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('3', 'Georgeanne Itscovitz', '70 Monterey Center', '(806) 1026968', 'Hungary', 'Lind-Fritsch', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('4', 'Ross Melby', '76513 Kim Road', '(736) 7638117', 'China', 'Bins, Schoen and Feil', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('5', 'Fiona Mecozzi', '143 Fairfield Place', '(607) 5194734', 'Brazil', 'Gleichner, Daniel and Schiller', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('6', 'Sterne Willcott', '81 Walton Lane', '(631) 4504037', 'Brazil', 'Jakubowski-Armstrong', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('7', 'Gwenny Lesley', '5261 Vidon Junction', '(567) 9002636', 'Belarus', 'Schuster LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('8', 'Rowena Doel', '51485 Myrtle Avenue', '(137) 9375177', 'China', 'Batz, Schinner and Kulas', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('9', 'Blayne Madders', '189 Fieldstone Way', '(289) 4087619', 'China', 'Hickle and Sons', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('10', 'Dorree Trillow', '61766 Comanche Plaza', '(991) 7787514', 'Indonesia', 'Breitenberg-Kessler', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('11', 'Christyna Kopfer', '2765 Sachtjen Terrace', '(747) 4778906', 'Greece', 'Shanahan-Leuschke', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('12', 'Rori MacGovern', '4 Golf View Center', '(286) 8270254', 'Brazil', 'MacGyver, Cormier and Littel', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('13', 'Joline Lightwood', '782 Emmet Center', '(647) 9604352', 'China', 'Kihn, Keebler and Kilback', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('14', 'Rachael Stonman', '33 Ohio Crossing', '(341) 5744399', 'China', 'Carroll-Wintheiser', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('15', 'Koren Carnilian', '73039 Columbus Crossing', '(546) 6273654', 'Sweden', 'Balistreri-Fahey', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('16', 'Editha Izachik', '7 Atwood Way', '(520) 6817544', 'China', 'Huels, Friesen and Ferry', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('17', 'Arlen Stansby', '9791 Westerfield Trail', '(351) 2298993', 'Poland', 'O\'Connell, Kunze and Hirthe', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('18', 'Chantalle Setter', '09 Corben Terrace', '(116) 2822172', 'China', 'O\'Hara-Christiansen', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('19', 'Carline Carlet', '627 Dovetail Court', '(309) 5413634', 'Philippines', 'Hagenes-Bogan', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('20', 'Aldon Whelpton', '0316 Laurel Trail', '(158) 9670811', 'Cuba', 'Schumm LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('21', 'Alley De Filippis', '4 Meadow Vale Street', '(575) 3822356', 'Chile', 'Halvorson, Mills and Feeney', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('22', 'Ingaborg Bleddon', '18 Brentwood Terrace', '(144) 9812785', 'Brazil', 'Kuvalis, Yost and Blanda', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('23', 'Melisande Struss', '328 Westridge Junction', '(455) 8958254', 'Czech Republic', 'Kautzer Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('24', 'Deonne Wetherill', '56 Knutson Pass', '(777) 5645475', 'South Korea', 'White, Grimes and Towne', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('25', 'Georgie Wreak', '9 Hermina Point', '(794) 7951065', 'Honduras', 'Kuhn and Sons', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('26', 'Wilhelm Bouchier', '4741 Anthes Court', '(357) 8218855', 'Malawi', 'McKenzie, Cummerata and Hodkiewicz', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('27', 'Obidiah Whitehead', '402 Dapin Center', '(678) 5066556', 'Philippines', 'Halvorson LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('28', 'Thomasa Chelley', '1488 Graceland Center', '(277) 6701861', 'Ukraine', 'Berge Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('29', 'Ddene Babinski', '85 Manitowish Street', '(144) 3075190', 'Indonesia', 'Buckridge Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('30', 'Celka Grimwade', '73 Portage Park', '(750) 7341786', 'Philippines', 'Konopelski-Barrows', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('31', 'Corey McGuckin', '68558 Brown Alley', '(704) 8965991', 'China', 'Predovic-Von', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('32', 'Leonerd Tyce', '86513 Caliangt Lane', '(774) 5073494', 'Philippines', 'Adams, Macejkovic and Jerde', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('33', 'Orella Wermerling', '205 Manley Street', '(991) 1726918', 'Indonesia', 'Schimmel, Kilback and Jast', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('34', 'Howey Fursey', '7237 8th Point', '(460) 6261833', 'Sweden', 'Mosciski and Sons', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('35', 'Karlik McPartling', '00 Lighthouse Bay Park', '(924) 6981473', 'Portugal', 'Larson-Gleichner', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('36', 'Murielle Styan', '88 Barnett Drive', '(286) 7016636', 'Nigeria', 'Kuhic LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('37', 'Lincoln Bemand', '3410 Birchwood Lane', '(148) 2052074', 'Thailand', 'Romaguera-Zboncak', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('38', 'Jephthah Kochel', '4 Cardinal Crossing', '(808) 4465890', 'China', 'Graham-Little', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('39', 'Darla Greenin', '64181 Petterle Hill', '(924) 5168867', 'China', 'Terry, Ritchie and Orn', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('40', 'Clerissa Jerromes', '2 Thompson Avenue', '(415) 5647053', 'Brazil', 'Rosenbaum Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('41', 'Hilario Neat', '916 Village Green Court', '(739) 4699363', 'Pakistan', 'Schmidt, Hane and Sawayn', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('42', 'Kinna Mullard', '2774 Dayton Center', '(953) 6429361', 'Honduras', 'Adams, Blanda and Flatley', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('43', 'Upton Robison', '19 Vermont Court', '(608) 9879498', 'Finland', 'Jaskolski-Rau', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('44', 'Georgetta Teasdale-Markie', '47131 Calypso Parkway', '(885) 7202888', 'Ukraine', 'Grady Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('45', 'Von McAllaster', '90 Merry Court', '(912) 2537166', 'Israel', 'Schowalter, Marvin and Collins', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('46', 'Teodoor Arney', '775 Lake View Park', '(973) 2752314', 'United States', 'Gleason, Abernathy and Keebler', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('47', 'Derrik Timlett', '37914 Westridge Park', '(839) 1675731', 'Paraguay', 'Kassulke, Torp and Heller', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('48', 'Shayne Fulford', '0834 Schmedeman Terrace', '(585) 8752388', 'Central African Republic', 'Yundt, Wiegand and Gorczany', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('49', 'Niven Sings', '5 Kropf Point', '(490) 4277003', 'Indonesia', 'Baumbach, McGlynn and Dibbert', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('50', 'Tawsha Southernwood', '7 Banding Place', '(519) 9495006', 'Indonesia', 'Christiansen Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('51', 'Cletis Nizet', '762 Sunnyside Pass', '(833) 2292874', 'Germany', 'DuBuque-Tromp', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('52', 'Whitby Colegrove', '96 Ridgeway Hill', '(473) 3100076', 'China', 'West-Boyer', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('53', 'Boote Jugging', '05 Manley Trail', '(172) 4262253', 'Indonesia', 'Hermann-Mertz', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('54', 'Darin Hadigate', '1897 Jenna Point', '(762) 9746580', 'Portugal', 'Sanford-Flatley', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('55', 'Colleen Cowperthwaite', '5914 Scofield Parkway', '(499) 1846250', 'Botswana', 'Walsh-Collier', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('56', 'Kalinda Reeves', '87938 Scoville Park', '(945) 5416752', 'China', 'Crooks-Littel', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('57', 'Stanly Simmon', '593 Sunfield Trail', '(856) 3117924', 'Czech Republic', 'Runte LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('58', 'Jonie Tyrie', '4666 Dexter Drive', '(512) 8017799', 'United States', 'Spinka-Koss', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('59', 'Marjy Floyed', '86411 Elgar Pass', '(438) 6993824', 'Malta', 'Doyle, Hilll and Armstrong', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('60', 'Cameron Fruish', '31 Huxley Hill', '(625) 9131431', 'Portugal', 'Crist-Upton', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('61', 'Wittie Rosenbush', '379 Vidon Terrace', '(565) 9084044', 'Indonesia', 'Kirlin-Greenholt', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('62', 'Joly Dowling', '8 Briar Crest Point', '(379) 4323593', 'Palestinian Territory', 'Bashirian LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('63', 'Cari Bragg', '12 Cambridge Lane', '(940) 7537696', 'United States', 'Hermiston-Bradtke', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('64', 'Danita McElrea', '1 Hermina Park', '(105) 4872946', 'Sweden', 'Witting, Thompson and Ortiz', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('65', 'Salvador Drane', '95129 Lillian Avenue', '(321) 3837144', 'Indonesia', 'Homenick, Bernhard and MacGyver', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('66', 'Hazel Gorke', '87 Laurel Pass', '(947) 9506094', 'Indonesia', 'Stracke, Gusikowski and Dach', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('67', 'Currie Weavill', '72 Anhalt Center', '(215) 1696509', 'Colombia', 'Herman, Marks and Stokes', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('68', 'Colly Pash', '10 Kings Point', '(614) 1631411', 'Japan', 'Lakin, Fadel and Harber', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('69', 'Rodge Silbert', '6 Kensington Center', '(750) 2485107', 'China', 'Metz LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('70', 'Lyman Dunleavy', '294 Susan Road', '(704) 4534444', 'Argentina', 'Stark Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('71', 'Alaine Gorelli', '001 Bultman Trail', '(609) 9114154', 'Turkey', 'Crona-Hartmann', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('72', 'Reamonn Kenningham', '783 Bunker Hill Alley', '(537) 9225267', 'Tanzania', 'Kautzer LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('73', 'Jorie Firmin', '705 Debra Trail', '(823) 1853766', 'Indonesia', 'Gerlach-Harvey', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('74', 'Jarrett Ervin', '69 Dwight Place', '(361) 1886538', 'China', 'Hauck, Kirlin and Mills', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('75', 'Durant Bayly', '476 Lakeland Junction', '(140) 9405382', 'Poland', 'Kreiger, Watsica and Schuppe', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('76', 'Sherlocke Larmuth', '008 Vidon Plaza', '(961) 9844397', 'Greece', 'Leuschke, Gulgowski and Rippin', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('77', 'Ondrea Edlington', '7100 Jenifer Parkway', '(260) 8231999', 'Indonesia', 'Jones, Lueilwitz and Heidenreich', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('78', 'Bride Barthelet', '43688 Iowa Avenue', '(480) 4812764', 'Sweden', 'Borer-Weber', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('79', 'Jewelle Wroath', '879 Grayhawk Terrace', '(106) 2197686', 'Tajikistan', 'Langosh, Bogisich and Zulauf', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('80', 'Ottilie Baal', '96827 Eliot Pass', '(766) 1242389', 'Costa Rica', 'Kirlin, Bashirian and Friesen', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('81', 'Glyn Davidsson', '2 Namekagon Plaza', '(724) 9164172', 'China', 'Lang-Bogisich', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('82', 'Bronson Jancso', '183 6th Drive', '(118) 8895411', 'Japan', 'Ernser LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('83', 'Rupert Allerton', '26 Mcbride Road', '(789) 6625900', 'China', 'Dickens Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('84', 'Fidelia Waugh', '6 Service Center', '(623) 3426204', 'China', 'Turcotte Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('85', 'Dane Dmitrienko', '38462 Kings Place', '(484) 4276472', 'China', 'Hoeger-O\'Connell', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('86', 'Katha Gallety', '787 Riverside Court', '(949) 7127484', 'Russia', 'Ankunding Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('87', 'Larissa Behn', '8 Lawn Point', '(138) 5307361', 'Belarus', 'Skiles, Okuneva and Cronin', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('88', 'Lou Roches', '7 Garrison Circle', '(800) 2182310', 'Spain', 'Wisoky, Rippin and Hagenes', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('89', 'Carolee Caltera', '48875 Derek Court', '(391) 7691920', 'China', 'Senger LLC', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('90', 'Kenna Lyver', '68 Dennis Road', '(852) 3122101', 'Indonesia', 'Dicki-Rath', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('91', 'Gilberte Carnow', '99536 Hansons Circle', '(101) 3082973', 'Indonesia', 'Tremblay, Parker and Zemlak', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('92', 'Obidiah Leydon', '85 Corscot Point', '(257) 6534971', 'Indonesia', 'Leannon-Lubowitz', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('93', 'Martguerita Queree', '3 Blaine Hill', '(727) 9083304', 'Indonesia', 'Schuppe, Powlowski and Pagac', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('94', 'Lianne Brach', '211 Waubesa Court', '(337) 4584332', 'Indonesia', 'Leuschke Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('95', 'Jodie Cicculini', '998 Kim Circle', '(469) 7889966', 'Chile', 'Cummings Inc', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('96', 'Alonzo Bonnyson', '8281 Morning Trail', '(588) 9729628', 'Panama', 'Durgan-Wilkinson', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('97', 'Parrnell Linklater', '0 Vera Center', '(913) 7749031', 'Poland', 'Rath, Herzog and Stoltenberg', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('98', 'Lane Caplin', '3219 Browning Junction', '(946) 8908441', 'China', 'Grant Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('99', 'Branden Dainton', '8383 Shoshone Lane', '(908) 8388498', 'China', 'Beer, Prosacco and Mann', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('100', 'Carson Pinching', '57 Sugar Place', '(779) 3037037', 'Bosnia and Herzegovina', 'Gleichner Group', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('102', 'Astradanta', 'Gang nuri 2 no 19 ', '085792078364', 'Indonesia', 'okok', '2', '2019-06-07 10:04:20');
INSERT INTO `guest` VALUES ('103', 'Astradanta', 'Sample alamat', '098392830', 'Indonesia', '--', '2', '2019-06-07 10:33:12');

-- ----------------------------
-- Table structure for harga_produk
-- ----------------------------
DROP TABLE IF EXISTS `harga_produk`;
CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `rentang_awal` int(11) DEFAULT NULL,
  `rentang_akhir` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`harga_produk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of harga_produk
-- ----------------------------

-- ----------------------------
-- Table structure for history_penyesuaian_bahan
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_bahan`;
CREATE TABLE `history_penyesuaian_bahan` (
  `history_penyesuaian_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jenis_bahan_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_penyesuaian_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of history_penyesuaian_bahan
-- ----------------------------
INSERT INTO `history_penyesuaian_bahan` VALUES ('1', '2019-05-13', '4', '1', '1', '10', '01', 'Kain wol', 'Kain', 'Gudang', '1500', '1490', 'Rusak', '2019-05-13 17:01:15', '2019-05-13 17:01:15');
INSERT INTO `history_penyesuaian_bahan` VALUES ('2', '2019-05-13', '4', '1', '2', '11', '01', 'Kain wol', 'Kain', 'Toko', '1700', '1710', 'Salah input', '2019-05-13 17:03:04', '2019-05-13 17:03:04');
INSERT INTO `history_penyesuaian_bahan` VALUES ('3', '2019-06-03', '4', '1', '2', '12', '01', 'Kain wol', 'Kain', 'Toko', '1600', '1550', 'Rusak', '2019-06-03 16:34:58', '2019-06-03 16:34:58');
INSERT INTO `history_penyesuaian_bahan` VALUES ('4', '2019-06-06', '5', '1', '1', '23', '02', 'Kain wol', 'Kain', 'Gudang', '20', '15', 'Rusak', '2019-06-06 14:14:15', '2019-06-06 14:14:15');
INSERT INTO `history_penyesuaian_bahan` VALUES ('5', '2019-06-07', '5', '1', '2', '19', '02', 'Kain wol', 'Kain', 'Toko', '180', '10', 'Sample', '2019-06-07 14:07:06', '2019-06-07 14:07:06');

-- ----------------------------
-- Table structure for history_penyesuaian_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_produk`;
CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_penyesuaian_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of history_penyesuaian_produk
-- ----------------------------
INSERT INTO `history_penyesuaian_produk` VALUES ('1', '3', '2', '1', '6', '2019-05-16', '01', 'Handuk', null, 'Handuk', 'Gudang', '2700', '2000', 'testing', '2019-05-16 14:41:31', '2019-05-16 14:41:31');
INSERT INTO `history_penyesuaian_produk` VALUES ('2', '3', '2', '1', '6', '2019-06-03', '01', 'Handuk', null, 'Towel', 'Gudang', '2000', '1990', 'Rusak', '2019-06-03 16:43:19', '2019-06-03 16:43:19');
INSERT INTO `history_penyesuaian_produk` VALUES ('3', '8', '6', '2', '12', '2019-06-06', '06', 'Medium Napkin', null, 'Napkin', 'Toko', '110', '100', 'Rusak', '2019-06-06 14:15:20', '2019-06-06 14:15:20');
INSERT INTO `history_penyesuaian_produk` VALUES ('4', '8', '6', '2', '12', '2019-06-07', '06', 'Medium Napkin', null, 'Napkin', 'Toko', '75', '70', 'Sample', '2019-06-07 20:51:15', '2019-06-07 20:51:15');

-- ----------------------------
-- Table structure for history_transfer_bahan
-- ----------------------------
DROP TABLE IF EXISTS `history_transfer_bahan`;
CREATE TABLE `history_transfer_bahan` (
  `history_transfer_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` text COLLATE utf8mb4_unicode_ci,
  `bahan_seri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_transfer_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of history_transfer_bahan
-- ----------------------------
INSERT INTO `history_transfer_bahan` VALUES ('43', '2019-06-06', '2019-06-06', '5', '23', '02', 'Kain wol', '44352', '2', '1', '1', '2', 'Gudang', 'Toko', '198', 'Diterima Sebagian', 'sample', '2019-06-06 13:54:33', '2019-06-06 13:54:33');
INSERT INTO `history_transfer_bahan` VALUES ('44', '2019-06-06', '2019-06-06', '5', '23', '02', 'Kain wol', '44352', '4', '4', '1', '2', 'Gudang', 'Toko', '196', 'Diterima Semua', 'okok', '2019-06-06 14:12:53', '2019-06-06 14:12:53');
INSERT INTO `history_transfer_bahan` VALUES ('45', '2019-06-07', null, '4', '12', '01', 'Kain wol', '1234568', '200', '0', '2', '1', 'Toko', 'Gudang', '1550', 'Menunggu Konfirmasi', '', '2019-06-07 15:55:07', '2019-06-07 15:55:07');

-- ----------------------------
-- Table structure for history_transfer_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_transfer_produk`;
CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_transfer_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of history_transfer_produk
-- ----------------------------
INSERT INTO `history_transfer_produk` VALUES ('14', '2019-06-06', '2019-06-06', '8', '12', '06', 'Medium Napkin', '051905011', '20', '10', '2', '1', 'Toko', 'Gudang', '80', 'Diterima Sebagian', 'Sample', '2019-06-06 14:23:06', '2019-06-06 14:23:06');
INSERT INTO `history_transfer_produk` VALUES ('15', '2019-06-07', '2019-06-07', '8', '12', '06', 'Medium Napkin', '051905011', '15', '1', '2', '1', 'Toko', 'Gudang', '75', 'Diterima Sebagian', 'sample', '2019-06-07 20:50:36', '2019-06-07 20:50:36');

-- ----------------------------
-- Table structure for jenis_bahan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_bahan`;
CREATE TABLE `jenis_bahan` (
  `jenis_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_bahan_kode` varchar(10) DEFAULT NULL,
  `jenis_bahan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_bahan
-- ----------------------------
INSERT INTO `jenis_bahan` VALUES ('1', '02', 'Kain', '2019-05-05 17:32:27', '2019-05-05 17:36:23');

-- ----------------------------
-- Table structure for jenis_produk
-- ----------------------------
DROP TABLE IF EXISTS `jenis_produk`;
CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_produk
-- ----------------------------
INSERT INTO `jenis_produk` VALUES ('2', '01', 'Towel', '2019-05-05 17:47:43', '2019-05-27 12:48:37');
INSERT INTO `jenis_produk` VALUES ('3', '02', 'Bedsheets', '2019-05-05 17:47:56', null);
INSERT INTO `jenis_produk` VALUES ('4', '03', 'Cushion', '2019-05-31 14:59:46', null);
INSERT INTO `jenis_produk` VALUES ('5', '04', 'Duvet', '2019-05-31 14:59:46', null);
INSERT INTO `jenis_produk` VALUES ('6', '05', 'Napkin', '2019-05-31 15:05:32', null);

-- ----------------------------
-- Table structure for komposisi_produk
-- ----------------------------
DROP TABLE IF EXISTS `komposisi_produk`;
CREATE TABLE `komposisi_produk` (
  `komposisi_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `komposisi_qty` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`komposisi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of komposisi_produk
-- ----------------------------
INSERT INTO `komposisi_produk` VALUES ('2', '3', '4', '1', '30', '2019-05-17 15:04:16', null);
INSERT INTO `komposisi_produk` VALUES ('3', '3', '4', '1', '5', '2019-05-25 23:46:57', null);
INSERT INTO `komposisi_produk` VALUES ('4', '8', '5', '2', '4', '2019-06-06 14:42:29', null);

-- ----------------------------
-- Table structure for lokasi
-- ----------------------------
DROP TABLE IF EXISTS `lokasi`;
CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`),
  UNIQUE KEY `lokasi_kode` (`lokasi_kode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lokasi
-- ----------------------------
INSERT INTO `lokasi` VALUES ('1', '02', 'Gudang', 'Gudang', 'Gang nusa kambanagan', '0361 223456', '2019-05-05 15:58:24', null);
INSERT INTO `lokasi` VALUES ('2', '01', 'Toko', 'Toko', 'Jalan ke toko ', '0361 332564', '2019-05-09 16:46:53', null);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_kode` varchar(50) DEFAULT NULL,
  `menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'pos', 'POS', null, 'pos', null, 'cart', '1', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('2', 'master_data', 'Master Data', null, '#', null, 'master', '3', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('3', 'guest', 'Guest', 'list|add|edit|delete', 'guest', null, 'address-book', '4', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('4', 'inventori', 'Inventori', '', '#', null, 'commode', '5', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('5', 'produksi', 'produksi', 'list|add|edit|delete', 'produksi', null, 'hammers', '6', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('6', 'laporan', 'Laporan', null, '#', null, 'clipboards', '7', '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES ('7', 'dashboard', 'Dashboard', null, 'home', null, 'layers', '2', '2019-06-18 04:12:56', '2019-06-18 04:12:56');

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ekspedisi_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `nama_pelanggan` varchar(50) DEFAULT NULL,
  `alamat_pelanggan` varchar(100) DEFAULT NULL,
  `telepon_pelanggan` varchar(20) DEFAULT NULL,
  `urutan` bigint(20) DEFAULT NULL,
  `no_faktur` varchar(100) DEFAULT NULL,
  `status_distribusi` enum('not_yet','done') DEFAULT 'not_yet',
  `tanggal` date DEFAULT NULL,
  `jenis_transaksi` enum('retail','distributor') DEFAULT 'distributor',
  `pengiriman` enum('ya','tidak') DEFAULT NULL,
  `total` float DEFAULT NULL,
  `biaya_tambahan` float DEFAULT NULL,
  `potongan_akhir` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `terbayar` float DEFAULT NULL,
  `sisa_pembayaran` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `status_distribusi_done_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan
-- ----------------------------
INSERT INTO `penjualan` VALUES ('21', null, '2', 'Fiona Mecozzi', '143 Fairfield Place', '(607) 5194734', null, '-', 'not_yet', '2019-06-01', 'distributor', 'tidak', '210000', '0', '0', '210000', '220000', '0', '10000', null, '2019-06-01 16:42:06', '2019-06-01 16:42:06');
INSERT INTO `penjualan` VALUES ('22', null, '2', 'Gwenny Lesley', '5261 Vidon Junction', '(567) 9002636', null, '-', 'not_yet', '2019-06-01', 'distributor', 'tidak', '210000', '0', '0', '210000', '220000', '0', '10000', null, '2019-06-01 16:46:25', '2019-06-01 16:46:25');
INSERT INTO `penjualan` VALUES ('23', null, '2', 'Guest', '', '', null, '-', 'not_yet', '2019-06-01', 'distributor', 'tidak', '420000', '25000', '10000', '435000', '450000', '0', '15000', null, '2019-06-01 16:47:28', '2019-06-01 16:47:28');
INSERT INTO `penjualan` VALUES ('24', null, null, 'Guest', '', '', null, '-', 'not_yet', '2019-06-07', 'distributor', 'tidak', '100000', '0', '0', '100000', '100000', '0', '0', null, '2019-06-07 23:58:24', '2019-06-07 23:58:24');
INSERT INTO `penjualan` VALUES ('25', null, null, 'Guest', '', '', null, '-', 'not_yet', '2019-06-08', 'distributor', 'tidak', '210000', '0', '0', '210000', '220000', '0', '10000', null, '2019-06-08 00:02:58', '2019-06-08 00:02:58');
INSERT INTO `penjualan` VALUES ('26', null, '2', 'Guest', '', '', null, '-', 'not_yet', '2019-06-08', 'distributor', 'tidak', '160000', '0', '0', '160000', '160000', '0', '0', null, '2019-06-08 00:15:02', '2019-06-08 00:15:02');
INSERT INTO `penjualan` VALUES ('27', null, '1', 'Guest', '', '', null, '-', 'not_yet', '2019-06-08', 'distributor', 'tidak', '80000', '0', '0', '80000', '80000', '0', '0', null, '2019-06-08 00:18:02', '2019-06-08 00:18:02');
INSERT INTO `penjualan` VALUES ('28', null, '1', 'Guest', '', '', '1', 'PL-190608/1/1', 'not_yet', '2019-06-08', 'distributor', 'tidak', '80000', '0', '0', '80000', '100000', '0', '20000', null, '2019-06-08 00:46:20', '2019-06-08 00:46:20');

-- ----------------------------
-- Table structure for penjualan_produk
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_produk`;
CREATE TABLE `penjualan_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `hpp` float DEFAULT NULL,
  `potongan` float DEFAULT NULL,
  `ppn_persen` float DEFAULT NULL,
  `ppn_nominal` float DEFAULT NULL,
  `harga_net` float DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan_produk
-- ----------------------------
INSERT INTO `penjualan_produk` VALUES ('11', '12', '5', '5', '79000', '60000', null, '0.1', null, null, '395000', '2019-06-01 15:27:04', '2019-06-01 15:27:04');
INSERT INTO `penjualan_produk` VALUES ('12', '12', '8', '3', '40000', '10000', null, '0.1', null, null, '120000', '2019-06-01 15:27:04', '2019-06-01 15:27:04');
INSERT INTO `penjualan_produk` VALUES ('13', '13', '5', '5', '79000', '60000', null, '0.1', null, null, '395000', '2019-06-01 15:27:25', '2019-06-01 15:27:25');
INSERT INTO `penjualan_produk` VALUES ('14', '13', '8', '3', '40000', '10000', null, '0.1', null, null, '120000', '2019-06-01 15:27:25', '2019-06-01 15:27:25');
INSERT INTO `penjualan_produk` VALUES ('15', '14', '5', '2', '80000', '60000', null, '0.1', null, null, '160000', '2019-06-01 15:32:01', '2019-06-01 15:32:01');
INSERT INTO `penjualan_produk` VALUES ('16', '15', '7', '5', '105000', '80000', null, '0.1', null, null, '525000', '2019-06-01 15:40:54', '2019-06-01 15:40:54');
INSERT INTO `penjualan_produk` VALUES ('17', '16', '4', '2', '50000', '40000', null, '0.1', null, null, '100000', '2019-06-01 15:48:41', '2019-06-01 15:48:41');
INSERT INTO `penjualan_produk` VALUES ('18', '16', '6', '6', '80000', '50000', null, '0.1', null, null, '480000', '2019-06-01 15:48:41', '2019-06-01 15:48:41');
INSERT INTO `penjualan_produk` VALUES ('19', '17', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-01 15:50:53', '2019-06-01 15:50:53');
INSERT INTO `penjualan_produk` VALUES ('20', '18', '7', '3', '105000', '80000', null, '0.1', null, null, '315000', '2019-06-01 16:37:14', '2019-06-01 16:37:14');
INSERT INTO `penjualan_produk` VALUES ('21', '19', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-01 16:40:32', '2019-06-01 16:40:32');
INSERT INTO `penjualan_produk` VALUES ('22', '20', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-01 16:41:27', '2019-06-01 16:41:27');
INSERT INTO `penjualan_produk` VALUES ('23', '21', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-01 16:42:06', '2019-06-01 16:42:06');
INSERT INTO `penjualan_produk` VALUES ('24', '22', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-01 16:46:25', '2019-06-01 16:46:25');
INSERT INTO `penjualan_produk` VALUES ('25', '23', '7', '4', '105000', '80000', null, '0.1', null, null, '420000', '2019-06-01 16:47:28', '2019-06-01 16:47:28');
INSERT INTO `penjualan_produk` VALUES ('26', '24', '4', '2', '50000', '40000', null, '0.1', null, null, '100000', '2019-06-07 23:58:24', '2019-06-07 23:58:24');
INSERT INTO `penjualan_produk` VALUES ('27', '25', '7', '2', '105000', '80000', null, '0.1', null, null, '210000', '2019-06-08 00:02:58', '2019-06-08 00:02:58');
INSERT INTO `penjualan_produk` VALUES ('28', '26', '6', '2', '80000', '50000', null, '0.1', null, null, '160000', '2019-06-08 00:15:02', '2019-06-08 00:15:02');
INSERT INTO `penjualan_produk` VALUES ('29', '27', '8', '2', '40000', '0', null, '0.1', null, null, '80000', '2019-06-08 00:18:02', '2019-06-08 00:18:02');
INSERT INTO `penjualan_produk` VALUES ('30', '28', '8', '2', '40000', '0', null, '0.1', null, null, '80000', '2019-06-08 00:46:20', '2019-06-08 00:46:20');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `produk_nama` varchar(100) NOT NULL,
  `harga_eceran` double DEFAULT '0',
  `harga_grosir` double DEFAULT '0',
  `harga_konsinyasi` double DEFAULT '0',
  `produk_minimal_stock` bigint(20) NOT NULL,
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('3', '2', '01', 'Handuk', '0', '0', '0', '6000', '0', '0', '2019-05-16 11:13:30', '2019-05-26 16:15:32');
INSERT INTO `produk` VALUES ('4', '2', '02', 'Face Towel', '50000', '48000', '49000', '30', '0', '0', '2019-05-31 15:11:43', '2019-05-31 15:18:52');
INSERT INTO `produk` VALUES ('5', '3', '03', 'Medium Bedsheets', '80000', '79000', '79500', '20', '0', '0', '2019-05-31 15:12:33', '2019-05-31 15:19:00');
INSERT INTO `produk` VALUES ('6', '4', '04', 'Cushion dacron', '80000', '75000', '0', '30', '0', '0', '2019-05-31 15:14:18', '2019-05-31 15:19:06');
INSERT INTO `produk` VALUES ('7', '5', '05', 'Inner Duvet', '105000', '102000', '0', '10', '0', '0', '2019-05-31 15:15:01', '2019-05-31 15:19:15');
INSERT INTO `produk` VALUES ('8', '6', '06', 'Medium Napkin', '40000', '35000', '0', '30', '0', '0', '2019-05-31 15:15:45', '2019-05-31 15:19:21');

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of satuan
-- ----------------------------
INSERT INTO `satuan` VALUES ('1', 'kg', 'kilo gram', '2019-05-06 06:50:49', '2019-05-06 06:51:35');
INSERT INTO `satuan` VALUES ('2', 'LT', 'Liter', '2019-05-27 10:06:32', null);

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_tanggal_lahir` varchar(100) DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', '1909807889237', 'Astradanta', 'Denpasar, 13 Februari 1997', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', null);
INSERT INTO `staff` VALUES ('2', '1115051039', 'Smaple', 'okok ', 'okok', 'Staff', 'Laki-laki', '2019-05-01', 'sample@ok.co', '073927936887', '', '2019-05-30 16:01:58', null);
INSERT INTO `staff` VALUES ('3', '90263567839', 'Gudang Admin', 'Gudang', 'Gudang', 'Staff', 'Laki-laki', '2019-05-10', 'sample@gudang.com', '2039820398203', '', '2019-06-07 10:34:10', null);

-- ----------------------------
-- Table structure for stock_bahan
-- ----------------------------
DROP TABLE IF EXISTS `stock_bahan`;
CREATE TABLE `stock_bahan` (
  `stock_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_seri` varchar(50) DEFAULT NULL,
  `stock_bahan_qty` bigint(20) DEFAULT NULL,
  `stock_bahan_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_bahan_id`),
  KEY `stock_bahan_constraint` (`bahan_id`),
  CONSTRAINT `stock_bahan_constraint` FOREIGN KEY (`bahan_id`) REFERENCES `bahan` (`bahan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stock_bahan
-- ----------------------------
INSERT INTO `stock_bahan` VALUES ('12', '4', '2', '1234568', '1350', null, '0', '2019-05-13 07:59:09', null);
INSERT INTO `stock_bahan` VALUES ('19', '5', '2', '44352', '10', null, '0', '2019-05-25 12:52:54', null);
INSERT INTO `stock_bahan` VALUES ('21', '5', '1', '454545', '120', null, '1', '2019-06-05 15:55:18', null);
INSERT INTO `stock_bahan` VALUES ('22', '5', '2', '123455777', '200', null, '1', '2019-06-05 16:19:05', null);
INSERT INTO `stock_bahan` VALUES ('23', '5', '1', '44352', '15', null, '1', '2019-06-06 12:53:35', null);
INSERT INTO `stock_bahan` VALUES ('24', '4', '1', '3423523532525', '200', null, '0', '2019-06-07 11:23:40', null);
INSERT INTO `stock_bahan` VALUES ('29', '5', '2', '53543423', '20', null, '0', '2019-06-07 23:40:19', null);

-- ----------------------------
-- Table structure for stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `stock_produk`;
CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` bigint(20) NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` bigint(20) DEFAULT NULL,
  `stock_produk_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_produk_id`),
  KEY `stock_produk_constraint` (`produk_id`),
  CONSTRAINT `stock_produk_constraint` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stock_produk
-- ----------------------------
INSERT INTO `stock_produk` VALUES ('6', '3', '1', '05', '19', '1', '051901021', '1990', '40000', null, '', '0', '2019-05-16 11:19:12', null);
INSERT INTO `stock_produk` VALUES ('7', '3', '2', '05', '19', '1', '051901011', '4000', '45000', null, '', '0', '2019-05-16 16:31:00', null);
INSERT INTO `stock_produk` VALUES ('8', '4', '2', '05', '19', '2', '051901012', '120', '40000', null, '', '0', '2019-05-31 15:16:14', null);
INSERT INTO `stock_produk` VALUES ('9', '5', '2', '05', '19', '1', '051902011', '100', '60000', null, '', '0', '2019-05-31 15:19:50', null);
INSERT INTO `stock_produk` VALUES ('10', '6', '2', '05', '19', '1', '051903011', '80', '50000', null, '', '0', '2019-05-31 15:20:20', null);
INSERT INTO `stock_produk` VALUES ('11', '7', '2', '05', '19', '1', '051904011', '200', '80000', null, '', '0', '2019-05-31 15:20:46', null);
INSERT INTO `stock_produk` VALUES ('12', '8', '2', '05', '19', '1', '051905011', '84', '10000', null, '', '0', '2019-05-31 15:21:12', null);
INSERT INTO `stock_produk` VALUES ('13', '8', '2', '06', '19', '1', '061905011', '30000', '30000', null, '', '1', '2019-06-05 16:12:46', null);
INSERT INTO `stock_produk` VALUES ('14', '8', '1', null, null, null, '051905011', '71', null, null, null, '1', '2019-06-06 11:45:55', null);
INSERT INTO `stock_produk` VALUES ('15', '8', '2', '06', '19', '2', '061905012', '35', '20000', null, '', '0', '2019-06-07 23:46:43', null);

-- ----------------------------
-- Table structure for sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `sub_menu`;
CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `sub_menu_kode` varchar(50) DEFAULT NULL,
  `sub_menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sub_menu
-- ----------------------------
INSERT INTO `sub_menu` VALUES ('1', '1', 'penjualan', 'Penjualan', null, 'pos', null, '2019-06-18 03:17:08', '2019-06-18 03:17:08');
INSERT INTO `sub_menu` VALUES ('2', '1', 'rekapitulasi-pos', 'Rekapitulasi POS', null, 'rekapitulasi-pos', null, '2019-06-18 03:17:08', '2019-06-18 03:17:08');
INSERT INTO `sub_menu` VALUES ('3', '2', 'suplier', 'Suplier', 'list|add|edit|delete|view', 'suplier', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('4', '2', 'satuan', 'Satuan', 'list|add|edit|delete|view', 'satuan', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('5', '2', 'jenis-produk', 'Jenis Produk', 'list|add|edit|delete|view', 'jenis-produk', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('6', '2', 'jenis-bahan', 'Jenis Bahan', 'list|add|edit|delete|view', 'jenis-bahan', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('7', '2', 'lokasi', 'Lokasi', 'list|add|edit|delete|view', 'lokasi', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('8', '2', 'user', 'User', 'list|add|edit|delete|view', 'user', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('9', '2', 'user-role', 'User Role', '', 'user-role', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('10', '2', 'tipe-pembayaran', 'Tipe Pembayaran', 'list|add|edit|delete|view', 'tipe-pembayaran', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('11', '2', 'staff', 'Staf', 'list|add|edit|delete|view', 'staff', null, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES ('12', '4', 'bahan', 'Bahan', 'list|add|edit|delete|view|stock', 'bahan', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('13', '4', 'produk', 'Produk', 'list|add|edit|delete|view|stock', 'produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('14', '4', 'produk-by-location', 'Produk per Lokasi', 'list', 'produk-by-location', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('15', '4', 'koposisi-produk', 'Komposisi Produk', 'list|add|edit|delete|view', 'komposisi-produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('16', '4', 'transfer-bahan', 'Transfer Stok Bahan', 'list|transfer_bahan|konfirmasi_transfer_bahan|history_transfer_bahan', 'transfer-bahan', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('17', '4', 'transfer-produk', 'Transfer Stok Produk', 'list|transfer_produk|konfirmasi_transfer_produk|history_transfer_produk', 'transfer-produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('18', '4', 'penyesuaian-bahan', 'Penyesuaian Stok Bahan', 'list|pesnyesuaian_bahan|history_penyesuaian_bahan', 'penyesuaian-bahan', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('19', '4', 'penyesuaian-produk', 'Penyesuaian Stok Produk', 'list|pesnyesuaian_produk|history_penyesuaian_produk', 'penyesuaian-produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('20', '4', 'low-stock-bahan', 'Low Stock Bahan', 'list', 'low-stock-bahan', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('21', '4', 'low-stock-produk', 'Low Stock Produk', 'list', 'low-stock-produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('22', '4', 'kartu-stock-bahan', 'Kartu Stok Bahan', 'list', 'kartu-stock-bahan', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('23', '4', 'kartu-stock-produk', 'Kartu Stok Produk', 'list', 'kartu-stock-produk', null, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES ('24', '6', 'laporan-stock-bahan', 'Laporan Stok Bahan', 'list', 'laporan-stock-bahan', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('25', '6', 'laporan-stock-produk', 'Laporan Stok Produk', 'list', 'laporan-stok-produk', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('26', '6', 'laporan-income', 'Laporan Income', 'list', 'laporan-income', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('27', '6', 'laporan-pendapatan', 'Laporan Pendapatan', 'list', 'laporan-pendapatan', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('28', '6', 'laporan-penjualan', 'Laporan Penjualan', 'list', 'laporan-penjualan', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('29', '6', 'laporan-pengeluaran', 'laporan-pengeluaran', 'list', 'laporan-pengeluaran', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('30', '6', 'laporan-produksi', 'Laporan Produksi', 'list', 'laporan-produksi', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('31', '6', 'laporan-hutang', 'Laporan Hutang', 'list', 'laporan-hutang', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES ('32', '6', 'laporan-piutang', 'laporan-piutang', 'list', 'laporan-piutang', null, '2019-06-18 04:00:08', '2019-06-18 04:00:08');

-- ----------------------------
-- Table structure for suplier
-- ----------------------------
DROP TABLE IF EXISTS `suplier`;
CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`suplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of suplier
-- ----------------------------
INSERT INTO `suplier` VALUES ('1', 'GS', 'PT Giro Perkasa', 'Jalan hayamwuruk no 3 denpasar', '0361 225678', 'okok@okok.com', '2019-05-06 07:19:38', '2019-05-15 16:58:15');
INSERT INTO `suplier` VALUES ('2', 'OK', 'Ok', 'ok', '2321323', 'wd@dwdw.ddw', '2019-05-27 10:06:19', null);

-- ----------------------------
-- Table structure for tipe_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `tipe_pembayaran`;
CREATE TABLE `tipe_pembayaran` (
  `tipe_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_pembayaran_kode` varchar(20) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tipe_pembayaran_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipe_pembayaran
-- ----------------------------
INSERT INTO `tipe_pembayaran` VALUES ('1', '01', 'Cash', '2019-06-07 21:49:31', '2019-06-07 21:49:31');
INSERT INTO `tipe_pembayaran` VALUES ('2', '02', 'EDC', '2019-06-07 21:50:06', '2019-06-07 21:50:06');
INSERT INTO `tipe_pembayaran` VALUES ('3', '03', 'Check', '2019-06-07 21:50:29', '2019-06-07 21:50:29');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '$2y$10$sIfZ5LEefpTjmSN9B0Wl6.gZ44N.km9zp34lIaTt8CIdIBs4g4uAS', '1', 'assets/media/users/1556714265JXOGF.jpg', null, '2019-05-01 18:06:31', '2019-05-01 20:37:45');
INSERT INTO `user` VALUES ('3', '2', '$2y$10$Ow35eK65.YHQiQHF6Zv3BOs4oYfTSe4qB2RdOqlvc6eg/Mllpjote', '2', 'assets/media/users/1559203558njGi5.png', '2', '2019-05-30 16:05:58', null);
INSERT INTO `user` VALUES ('4', '3', '$2y$10$BHIevtB0Q2.4iZ/61Ctrwu6XUG4pet/ih1EhokaQPEhlyZkf92nM.', '2', 'assets/media/users/1559874966o9JE8.png', '1', '2019-06-07 10:36:06', null);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_akses` text,
  `keterangan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', 'Super Admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true},\"rekapitulasi-pos\":{\"akses_menu\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":true},\"tipe-pembayaran\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true},\"produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true},\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"koposisi-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"transfer-bahan\":{\"akses_menu\":true,\"list\":true,\"transfer_bahan\":true,\"konfirmasi_transfer_bahan\":true,\"history_transfer_bahan\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-bahan\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_bahan\":true,\"history_penyesuaian_bahan\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"produksi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"laporan\":{\"akses_menu\":true,\"laporan-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-income\":{\"akses_menu\":true,\"list\":true},\"laporan-pendapatan\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-pengeluaran\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true}}}', null, '2019-04-26 00:00:00', null);
INSERT INTO `user_role` VALUES ('2', 'Admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true},\"rekapitulasi-pos\":{\"akses_menu\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":false,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":true,\"view\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true},\"jenis-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":true},\"tipe-pembayaran\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":true,\"view\":true},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":false,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"bahan\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true,\"stock\":true},\"produk\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true,\"stock\":true},\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"koposisi-produk\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":true,\"delete\":true,\"view\":false},\"transfer-bahan\":{\"akses_menu\":true,\"list\":true,\"transfer_bahan\":true,\"konfirmasi_transfer_bahan\":true,\"history_transfer_bahan\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-bahan\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_bahan\":true,\"history_penyesuaian_bahan\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"produksi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"laporan\":{\"akses_menu\":true,\"laporan-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-income\":{\"akses_menu\":true,\"list\":true},\"laporan-pendapatan\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-pengeluaran\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true}}}', null, '2019-04-30 06:43:26', null);
INSERT INTO `user_role` VALUES ('3', 'POS Officer', '{\"pos\":{\"akses_menu\":false,\"penjualan\":{\"akses_menu\":false},\"rekapitulasi-pos\":{\"akses_menu\":false}},\"dashboard\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":false,\"suplier\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"satuan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"lokasi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false}},\"guest\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false},\"inventori\":{\"akses_menu\":false,\"bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"stock\":false},\"produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"stock\":false},\"produk-by-location\":{\"akses_menu\":false,\"list\":false},\"koposisi-produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"transfer-bahan\":{\"akses_menu\":false,\"list\":false,\"transfer_bahan\":false,\"konfirmasi_transfer_bahan\":false,\"history_transfer_bahan\":false},\"transfer-produk\":{\"akses_menu\":false,\"list\":false,\"transfer_produk\":false,\"konfirmasi_transfer_produk\":false,\"history_transfer_produk\":false},\"penyesuaian-bahan\":{\"akses_menu\":false,\"list\":false,\"pesnyesuaian_bahan\":false,\"history_penyesuaian_bahan\":false},\"penyesuaian-produk\":{\"akses_menu\":false,\"list\":false,\"pesnyesuaian_produk\":false,\"history_penyesuaian_produk\":false},\"low-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"low-stock-produk\":{\"akses_menu\":false,\"list\":false},\"kartu-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"kartu-stock-produk\":{\"akses_menu\":false,\"list\":false}},\"produksi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false},\"laporan\":{\"akses_menu\":false,\"laporan-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":false,\"list\":false},\"laporan-income\":{\"akses_menu\":false,\"list\":false},\"laporan-pendapatan\":{\"akses_menu\":false,\"list\":false},\"laporan-penjualan\":{\"akses_menu\":false,\"list\":false},\"laporan-pengeluaran\":{\"akses_menu\":false,\"list\":false},\"laporan-produksi\":{\"akses_menu\":false,\"list\":false},\"laporan-hutang\":{\"akses_menu\":false,\"list\":false},\"laporan-piutang\":{\"akses_menu\":false,\"list\":false}}}', '', '2019-04-30 06:44:21', null);

-- ----------------------------
-- View structure for display_stock_bahan
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `display_stock_bahan` AS select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah` from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by `bahan`.`bahan_id` ;

-- ----------------------------
-- View structure for display_stock_bahan_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `display_stock_bahan_lokasi` AS select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah`,stock_bahan.stock_bahan_lokasi_id from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by stock_bahan.stock_bahan_lokasi_id,stock_bahan.bahan_id ;

-- ----------------------------
-- View structure for display_stock_produk
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `display_stock_produk` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where stock_produk.delete_flag = 0 group by `produk`.`produk_id` ;

-- ----------------------------
-- View structure for display_stock_produk_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `display_stock_produk_lokasi` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah`,stock_produk.stock_produk_lokasi_id from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by stock_produk.stock_produk_lokasi_id,stock_produk.produk_id ;

-- ----------------------------
-- View structure for staff_lama_kerja
-- ----------------------------
DROP VIEW IF EXISTS `staff_lama_kerja`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `staff_lama_kerja` AS  ;
