/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_balioz

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-04-29 15:27:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for forget_request
-- ----------------------------
DROP TABLE IF EXISTS `forget_request`;
CREATE TABLE `forget_request` (
`forget_request_id`  int(11) NOT NULL AUTO_INCREMENT ,
`forget_request_email`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`code`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`timeout`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`forget_request_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=9

;

-- ----------------------------
-- Records of forget_request
-- ----------------------------
BEGIN;
INSERT INTO `forget_request` VALUES ('1', 'astra.danta@gmail.com', '7XelGtFuOqyrgAbWU2dwV6Nmp', '1556425949'), ('2', 'astra.danta@gmail.com', 'odxYVKEMO4XWF9u7ySwL5tZ2f', '1556426062'), ('3', 'astra.danta@gmail.com', 'vsAVNol12QtXhD7gEpHbJO8wn', '1556426280'), ('4', 'astra.danta@gmail.com', 'IAigrShfm8lE1qysVeK5NCLRz', '1556495904'), ('5', 'astra.danta@gmail.com', 'N9GfLVvkzsKuDE25ciPwYFBMg', '1556496180'), ('6', 'astra.danta@gmail.com', 'waK4Mkb6RjNL59nrBW8odPmtl', '1556496180'), ('7', 'astra.danta@gmail.com', 'f4rwtEoWyeY6B28nj5RgJFuV7', '1556504113'), ('8', 'astra.danta@gmail.com', 'bxGBvHiely5kAPLaVYIt1zWE9', '1556505367');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
`user_id`  int(11) NOT NULL AUTO_INCREMENT ,
`user_name`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`email`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`password`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`user_role_id`  int(11) NOT NULL ,
`avatar`  varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`phone_number`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`created_at`  datetime NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`user_id`),
UNIQUE INDEX `user_email` (`email`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'Astradanta', 'astra.danta@gmail.com', '$2y$10$1z0YZpwE1hLdvaXviaUdQOtEBcFDKAeY67nELG.zv4EjJge7gCncq', '1', 'assets/media/users/1556521696UoypK.jpg', '085792078364', '2019-04-26 20:49:58', '2019-04-29 15:08:16');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
`role_id`  int(11) NOT NULL AUTO_INCREMENT ,
`role_name`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`created_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`role_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES ('1', 'Super Admin', '2019-04-26 00:00:00', null);
COMMIT;

-- ----------------------------
-- Auto increment value for forget_request
-- ----------------------------
ALTER TABLE `forget_request` AUTO_INCREMENT=9;

-- ----------------------------
-- Auto increment value for user
-- ----------------------------
ALTER TABLE `user` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for user_role
-- ----------------------------
ALTER TABLE `user_role` AUTO_INCREMENT=2;
