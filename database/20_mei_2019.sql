-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2019 at 05:56 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_balioz`
--

-- --------------------------------------------------------

--
-- Table structure for table `arus_stock_bahan`
--

CREATE TABLE `arus_stock_bahan` (
  `arus_stock_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `po_bahan_detail_id` int(11) DEFAULT NULL,
  `po_bahan` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT 'insert',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arus_stock_bahan`
--

INSERT INTO `arus_stock_bahan` (`arus_stock_bahan_id`, `tanggal`, `table_name`, `stock_bahan_id`, `po_bahan_detail_id`, `po_bahan`, `bahan_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `keterangan`, `method`, `created_at`, `updated_at`) VALUES
(21, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 3000, 0, 3000, 3000, 'Insert stok bahan', 'insert', NULL, NULL),
(22, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 2000, 0, 5000, 5000, 'Insert stok bahan', 'insert', NULL, NULL),
(23, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(24, '2019-05-13', 'stock_bahan', 12, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'insert', NULL, NULL),
(25, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(26, '2019-05-13', 'stock_bahan', 0, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(27, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 300, 4700, 4700, 'Tranfer stok bahan', 'update', NULL, NULL),
(28, '2019-05-13', 'stock_bahan', NULL, NULL, NULL, 4, 300, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(29, '2019-05-13', 'stock_bahan', NULL, NULL, NULL, 4, 300, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(30, '2019-05-13', 'stock_bahan', NULL, NULL, NULL, 4, 300, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(31, '2019-05-13', 'stock_bahan', NULL, NULL, NULL, 4, 300, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(32, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(33, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(34, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(35, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(36, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(37, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(38, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(39, '2019-05-13', 'stock_bahan', NULL, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(40, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 500, 4500, 4500, 'Tranfer stok bahan', 'update', NULL, NULL),
(41, '2019-05-13', 'stock_bahan', 12, NULL, NULL, 4, 500, 0, 5000, 5000, 'Konfrimasi transfer stok', 'update', NULL, NULL),
(42, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 0, 300, 4700, 4700, 'Tranfer stok bahan', 'update', NULL, NULL),
(43, '2019-05-13', 'stock_bahan', 13, NULL, NULL, 4, 300, 0, 5000, 5000, 'Konfirmasi transfer stok', 'insert', NULL, NULL),
(44, '2019-05-13', 'stock_bahan', 10, NULL, NULL, 4, 0, 10, 4990, 4990, 'Penyesuaian stok bahan', 'update', NULL, NULL),
(45, '2019-05-13', 'stock_bahan', 11, NULL, NULL, 4, 10, 0, 5000, 5000, 'Penyesuaian stok bahan', 'update', NULL, NULL),
(46, '2019-05-15', 'stock_bahan', 14, NULL, NULL, 4, 40000, 0, 45000, 45000, 'Insert stok bahan', 'insert', NULL, NULL),
(47, '2019-05-15', 'stock_bahan', 15, NULL, NULL, 4, 4, 0, 45004, 45004, 'Insert stok bahan', 'insert', NULL, NULL),
(48, '2019-05-16', 'stock_bahan', 16, NULL, NULL, 4, 30, 0, 45034, 45034, 'Insert stok bahan', 'insert', NULL, NULL),
(49, '2019-05-16', 'stock_bahan', 10, NULL, NULL, 4, 0, 1, 43544, 43544, 'Tranfer stok bahan', 'update', NULL, NULL),
(50, '2019-05-16', 'stock_bahan', 10, NULL, NULL, 4, 1, 0, 43545, 43545, 'Konfirmasi transfer stok', 'update', NULL, NULL),
(51, '2019-05-16', 'stock_bahan', 14, NULL, NULL, 4, 0, 1, 3584, 3584, 'Tranfer stok bahan', 'update', NULL, NULL),
(52, '2019-05-16', 'stock_bahan', 11, NULL, NULL, 4, 0, 1000, 2584, 2584, 'Tranfer stok bahan', 'update', NULL, NULL),
(53, '2019-05-16', 'stock_bahan', 13, NULL, NULL, 4, 1000, 0, 3584, 3584, 'Konfirmasi transfer stok', 'update', NULL, NULL),
(54, '2019-05-16', 'stock_bahan', 17, NULL, NULL, 4, 1, 0, 3585, 3585, 'Konfirmasi transfer stok', 'insert', NULL, NULL),
(55, '2019-05-16', 'stock_bahan', 11, NULL, NULL, 4, 0, 300, 3285, 3285, 'Tranfer stok bahan', 'update', NULL, NULL),
(56, '2019-05-17', 'stock_bahan', 18, NULL, NULL, 4, 0, 0, 3285, 3285, 'Insert stok bahan', 'insert', '2019-05-17 06:59:15', '2019-05-17 06:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `arus_stock_produk`
--

CREATE TABLE `arus_stock_produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` float DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arus_stock_produk`
--

INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(12, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2, NULL, 200, 0, 200, 200, 'insert', 'Insert stok produk', '2019-05-13 02:41:13', '2019-05-13 02:41:13'),
(13, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2, NULL, 0, 30, 170, 170, 'update', 'Tranfer stok produk', '2019-05-13 02:41:30', '2019-05-13 02:41:30'),
(14, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2, NULL, 30, 0, 200, 200, 'insert', 'Konfrimasi transfer stok', '2019-05-13 02:41:37', '2019-05-13 02:41:37'),
(15, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2, NULL, 0, 50, 150, 150, 'update', 'Tranfer stok produk', '2019-05-13 02:41:53', '2019-05-13 02:41:53'),
(16, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2, NULL, 50, 0, 200, 200, 'update', 'Konfrimasi transfer stok', '2019-05-13 02:42:09', '2019-05-13 02:42:09'),
(17, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2, NULL, 0, 20, 180, 180, 'update', 'Tranfer stok produk', '2019-05-13 02:51:35', '2019-05-13 02:51:35'),
(18, '2019-05-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2, NULL, 20, 0, 200, 200, 'update', 'Konfirmasi transfer stok', '2019-05-13 02:51:43', '2019-05-13 02:51:43'),
(19, '2019-05-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 3, NULL, 3000, 0, 3000, 3000, 'insert', 'Insert stok produk', '2019-05-16 03:19:12', '2019-05-16 03:19:12'),
(20, '2019-05-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 3, NULL, 0, 300, 2700, 2700, 'update', 'Tranfer stok produk', '2019-05-16 04:44:55', '2019-05-16 04:44:55'),
(21, '2019-05-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 3, NULL, 0, 700, 2000, 2000, 'update', 'Penyesuaian stok produk', '2019-05-16 06:41:31', '2019-05-16 06:41:31'),
(22, '2019-05-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 7, 3, NULL, 4000, 0, 6000, 6000, 'insert', 'Insert stok produk', '2019-05-16 08:31:00', '2019-05-16 08:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`bahan_id`, `bahan_jenis_id`, `bahan_satuan_id`, `bahan_suplier_id`, `bahan_kode`, `bahan_nama`, `bahan_minimal_stock`, `bahan_qty`, `bahan_last_stock`, `bahan_harga`, `bahan_keterangan`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 1, '01', 'Kain wol', 1000, 0, NULL, NULL, '', '2019-05-09 08:26:23', '2019-05-16 10:23:06'),
(5, 1, 1, 1, '02', 'Kain wol', 30, 0, NULL, NULL, '', '2019-05-16 09:38:45', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `display_stock_bahan`
-- (See below for the actual view)
--
CREATE TABLE `display_stock_bahan` (
`bahan_id` int(11)
,`jumlah` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `display_stock_produk`
-- (See below for the actual view)
--
CREATE TABLE `display_stock_produk` (
`produk_id` int(11)
,`jumlah` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `forget_request`
--

CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forget_request`
--

INSERT INTO `forget_request` (`forget_request_id`, `forget_request_email`, `code`, `timeout`) VALUES
(1, 'astra.danta@gmail.com', '7XelGtFuOqyrgAbWU2dwV6Nmp', 1556425949),
(2, 'astra.danta@gmail.com', 'odxYVKEMO4XWF9u7ySwL5tZ2f', 1556426062),
(3, 'astra.danta@gmail.com', 'vsAVNol12QtXhD7gEpHbJO8wn', 1556426280),
(4, 'astra.danta@gmail.com', 'IAigrShfm8lE1qysVeK5NCLRz', 1556495904),
(5, 'astra.danta@gmail.com', 'N9GfLVvkzsKuDE25ciPwYFBMg', 1556496180),
(6, 'astra.danta@gmail.com', 'waK4Mkb6RjNL59nrBW8odPmtl', 1556496180),
(7, 'astra.danta@gmail.com', 'f4rwtEoWyeY6B28nj5RgJFuV7', 1556504113),
(8, 'astra.danta@gmail.com', 'bxGBvHiely5kAPLaVYIt1zWE9', 1556505367),
(9, 'astra.danta@gmail.com', 'p5fUBMbcC64tonjhduDaOsLSv', 1556792065);

-- --------------------------------------------------------

--
-- Table structure for table `harga_produk`
--

CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `rentang_awal` int(11) DEFAULT NULL,
  `rentang_akhir` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_penyesuaian_bahan`
--

CREATE TABLE `history_penyesuaian_bahan` (
  `history_penyesuaian_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jenis_bahan_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_penyesuaian_bahan`
--

INSERT INTO `history_penyesuaian_bahan` (`history_penyesuaian_bahan_id`, `tanggal`, `bahan_id`, `jenis_bahan_id`, `lokasi_id`, `stock_bahan_id`, `bahan_kode`, `bahan_nama`, `jenis_bahan_nama`, `lokasi_nama`, `qty_awal`, `qty_akhir`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '2019-05-13', 4, 1, 1, 10, '01', 'Kain wol', 'Kain', 'Gudang', 1500, 1490, 'Rusak', '2019-05-13 09:01:15', '2019-05-13 09:01:15'),
(2, '2019-05-13', 4, 1, 2, 11, '01', 'Kain wol', 'Kain', 'Toko', 1700, 1710, 'Salah input', '2019-05-13 09:03:04', '2019-05-13 09:03:04');

-- --------------------------------------------------------

--
-- Table structure for table `history_penyesuaian_produk`
--

CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_penyesuaian_produk`
--

INSERT INTO `history_penyesuaian_produk` (`history_penyesuaian_produk_id`, `produk_id`, `jenis_produk_id`, `lokasi_id`, `stock_produk_id`, `tanggal`, `produk_kode`, `produk_nama`, `produk_seri`, `jenis_produk_nama`, `lokasi_nama`, `qty_awal`, `qty_akhir`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 1, 6, '2019-05-16 00:00:00', '01', 'Handuk', NULL, 'Handuk', 'Gudang', 2700, 2000, 'testing', '2019-05-16 06:41:31', '2019-05-16 06:41:31');

-- --------------------------------------------------------

--
-- Table structure for table `history_transfer_bahan`
--

CREATE TABLE `history_transfer_bahan` (
  `history_transfer_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` text COLLATE utf8mb4_unicode_ci,
  `bahan_seri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_transfer_bahan`
--

INSERT INTO `history_transfer_bahan` (`history_transfer_bahan_id`, `tanggal`, `tanggal_konfirmasi`, `bahan_id`, `stock_bahan_id`, `bahan_kode`, `bahan_nama`, `bahan_seri`, `history_transfer_qty`, `histori_lokasi_awal_id`, `histori_lokasi_tujuan_id`, `dari`, `tujuan`, `last_stock`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(24, '2019-05-13', NULL, 4, 10, '01', 'Kain wol', '123456', 500, 1, 2, 'Gudang', 'Toko', 4500, 'Diterima', '', '2019-05-12 23:58:51', '2019-05-12 23:58:51'),
(25, '2019-05-13', NULL, 4, 11, '01', 'Kain wol', '23456', 500, 2, 1, 'Toko', 'Gudang', 4500, 'Ditolak', '', '2019-05-12 23:59:37', '2019-05-12 23:59:37'),
(27, '2019-05-13', NULL, 4, 10, '01', 'Kain wol', '123456', 500, 2, 1, 'Toko', 'Gudang', 4500, 'Ditolak', '', '2019-05-13 00:33:44', '2019-05-13 00:33:44'),
(28, '2019-05-13', NULL, 4, 11, '01', 'Kain wol', '23456', 500, 1, 2, 'Gudang', 'Toko', 4500, 'Ditolak', '', '2019-05-13 00:35:55', '2019-05-13 00:35:55'),
(29, '2019-05-13', NULL, 4, 11, '01', 'Kain wol', '23456', 500, 2, 1, 'Toko', 'Gudang', 4500, 'Ditolak', '', '2019-05-13 00:38:19', '2019-05-13 00:38:19'),
(30, '2019-05-13', NULL, 4, 10, '01', 'Kain wol', '123456', 500, 1, 2, 'Gudang', 'Toko', 4500, 'Diterima', '', '2019-05-13 00:39:18', '2019-05-13 00:39:18'),
(31, '2019-05-13', NULL, 4, 10, '01', 'Kain wol', '123456', 500, 1, 2, 'Gudang', 'Toko', 4500, 'Diterima', '', '2019-05-13 00:42:52', '2019-05-13 00:42:52'),
(32, '2019-05-13', '2019-05-13', 4, 11, '01', 'Kain wol', '23456', 300, 2, 1, 'Toko', 'Gudang', 4700, 'Diterima', '', '2019-05-13 02:48:58', '2019-05-13 02:48:58'),
(33, '2019-05-16', '2019-05-16', 4, 10, '01', 'Kain wol', '123456', 1, 1, 2, 'Gudang', 'Toko', 43544, 'Ditolak', '', '2019-05-16 02:57:29', '2019-05-16 02:57:29'),
(34, '2019-05-16', '2019-05-16', 4, 14, '01', 'Kain wol', '211234124', 1, 1, 2, 'Gudang', 'Toko', 3584, 'Diterima', 'bahan diterima', '2019-05-16 02:59:37', '2019-05-16 02:59:37'),
(35, '2019-05-16', '2019-05-16', 4, 11, '01', 'Kain wol', '23456', 1000, 2, 1, 'Toko', 'Gudang', 2584, 'Diterima', '', '2019-05-16 03:03:24', '2019-05-16 03:03:24'),
(36, '2019-05-16', NULL, 4, 11, '01', 'Kain wol', '23456', 300, 2, 1, 'Toko', 'Gudang', 3285, 'Menunggu Konfirmasi', '', '2019-05-16 04:36:53', '2019-05-16 04:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `history_transfer_produk`
--

CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_transfer_produk`
--

INSERT INTO `history_transfer_produk` (`history_transfer_produk_id`, `tanggal`, `tanggal_konfirmasi`, `produk_id`, `stock_produk_id`, `produk_kode`, `produk_nama`, `produk_seri`, `history_transfer_qty`, `histori_lokasi_awal_id`, `histori_lokasi_tujuan_id`, `dari`, `tujuan`, `last_stock`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(4, '2019-05-13', NULL, 2, 4, '02', 'Handuk large size', '051902021', 30, 1, 2, 'Gudang', 'Toko', 170, 'Diterima', '', '2019-05-13 02:41:31', '2019-05-13 02:41:31'),
(5, '2019-05-13', NULL, 2, 4, '02', 'Handuk large size', '051902021', 50, 1, 2, 'Gudang', 'Toko', 150, 'Ditolak', '', '2019-05-13 02:41:53', '2019-05-13 02:41:53'),
(6, '2019-05-13', '2019-05-13', 2, 4, '02', 'Handuk large size', '051902021', 20, 1, 2, 'Gudang', 'Toko', 180, 'Diterima', '', '2019-05-13 02:51:35', '2019-05-13 02:51:35'),
(7, '2019-05-16', NULL, 3, 6, '01', 'Handuk', '051901021', 300, 1, 2, 'Gudang', 'Toko', 2700, 'Menunggu Konfirmasi', NULL, '2019-05-16 04:44:55', '2019-05-16 04:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_bahan`
--

CREATE TABLE `jenis_bahan` (
  `jenis_bahan_id` int(11) NOT NULL,
  `jenis_bahan_kode` varchar(10) DEFAULT NULL,
  `jenis_bahan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_bahan`
--

INSERT INTO `jenis_bahan` (`jenis_bahan_id`, `jenis_bahan_kode`, `jenis_bahan_nama`, `created_at`, `updated_at`) VALUES
(1, '02', 'Kain', '2019-05-05 17:32:27', '2019-05-05 17:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_produk`
--

INSERT INTO `jenis_produk` (`jenis_produk_id`, `jenis_produk_kode`, `jenis_produk_nama`, `created_at`, `updated_at`) VALUES
(2, '01', 'Handuk', '2019-05-05 17:47:43', NULL),
(3, '02', 'Bed Cover', '2019-05-05 17:47:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `komposisi_produk`
--

CREATE TABLE `komposisi_produk` (
  `komposisi_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `komposisi_qty` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komposisi_produk`
--

INSERT INTO `komposisi_produk` (`komposisi_id`, `produk_id`, `bahan_id`, `satuan_id`, `komposisi_qty`, `created_at`, `updated_at`) VALUES
(2, 3, 4, 1, 30, '2019-05-17 15:04:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`lokasi_id`, `lokasi_kode`, `lokasi_nama`, `lokasi_tipe`, `lokasi_alamat`, `lokasi_telepon`, `created_at`, `updated_at`) VALUES
(1, '02', 'Gudang', 'Gudang', 'Gang nusa kambanagan', '0361 223456', '2019-05-05 15:58:24', NULL),
(2, '01', 'Toko', 'Toko', 'Jalan ke toko ', '0361 332564', '2019-05-09 16:46:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `produk_nama` varchar(100) NOT NULL,
  `harga_eceran` double DEFAULT NULL,
  `harga_grosir` double DEFAULT '0',
  `harga_kosinyasi` double DEFAULT NULL,
  `produk_minimal_stock` bigint(20) NOT NULL,
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `produk_jenis_id`, `produk_kode`, `produk_nama`, `harga_eceran`, `harga_grosir`, `harga_kosinyasi`, `produk_minimal_stock`, `disc_persen`, `disc_nominal`, `created_at`, `updated_at`) VALUES
(3, 2, '01', 'Handuk', NULL, 0, NULL, 3000, 0, 0, '2019-05-16 11:13:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`satuan_id`, `satuan_kode`, `satuan_nama`, `created_at`, `updated_at`) VALUES
(1, 'kg', 'kilo gram', '2019-05-06 06:50:49', '2019-05-06 06:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_tanggal_lahir` varchar(100) DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `nik`, `staff_nama`, `tempat_tanggal_lahir`, `staff_alamat`, `staff_status`, `staff_kelamin`, `mulai_bekerja`, `staff_email`, `staff_phone_number`, `staff_keterangan`, `created_at`, `updated_at`) VALUES
(1, '1909807889237', 'Astradanta', 'Denpasar, 13 Februari 1997', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_lama_kerja`
-- (See below for the actual view)
--
CREATE TABLE `staff_lama_kerja` (
`staff_id` int(11)
,`lama_bekerja` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `stock_bahan`
--

CREATE TABLE `stock_bahan` (
  `stock_bahan_id` int(11) NOT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_seri` varchar(50) DEFAULT NULL,
  `stock_bahan_qty` bigint(20) DEFAULT NULL,
  `stock_bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_bahan`
--

INSERT INTO `stock_bahan` (`stock_bahan_id`, `bahan_id`, `stock_bahan_lokasi_id`, `stock_bahan_seri`, `stock_bahan_qty`, `stock_bahan_keterangan`, `created_at`, `updated_at`) VALUES
(10, 4, 1, '123456', 1, NULL, '2019-05-13 07:57:56', NULL),
(11, 4, 2, '23456', 410, NULL, '2019-05-13 07:58:31', NULL),
(12, 4, 2, '123456', 1500, NULL, '2019-05-13 07:59:09', NULL),
(13, 4, 1, '23456', 1300, NULL, '2019-05-13 10:51:13', NULL),
(14, 4, 1, '211234124', 39, NULL, '2019-05-15 16:19:24', NULL),
(15, 4, 1, '5235235325', 4, NULL, '2019-05-15 16:19:44', NULL),
(16, 4, 1, '66666', 30, NULL, '2019-05-16 10:07:15', NULL),
(17, 4, 2, '211234124', 1, NULL, '2019-05-16 11:24:23', NULL),
(18, 4, NULL, NULL, 0, NULL, '2019-05-17 14:59:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_produk`
--

CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` bigint(20) NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` bigint(20) DEFAULT NULL,
  `stock_produk_keterangan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_produk`
--

INSERT INTO `stock_produk` (`stock_produk_id`, `produk_id`, `stock_produk_lokasi_id`, `month`, `year`, `urutan`, `stock_produk_seri`, `stock_produk_qty`, `hpp`, `last_stok`, `stock_produk_keterangan`, `created_at`, `updated_at`) VALUES
(6, 3, 1, '05', '19', 1, '051901021', 2000, 40000, NULL, '', '2019-05-16 11:19:12', NULL),
(7, 3, 2, '05', '19', 1, '051901011', 4000, 40000, NULL, '', '2019-05-16 16:31:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`suplier_id`, `suplier_kode`, `suplier_nama`, `suplier_alamat`, `suplier_telepon`, `suplier_email`, `created_at`, `updated_at`) VALUES
(1, 'GS', 'PT Giro Perkasa', 'Jalan hayamwuruk no 3 denpasar', '0361 225678', 'okok@okok.com', '2019-05-06 07:19:38', '2019-05-15 16:58:15');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_staff_id`, `password`, `user_role_id`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$r87JvwVqPDb67b30360IauSMhfXX9IE0qUbXAfnsxKUPFUH6lT4f.', 1, 'assets/media/users/1556714265JXOGF.jpg', '2019-05-01 18:06:31', '2019-05-01 20:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2019-04-26 00:00:00', NULL),
(2, 'Admin', '2019-04-30 06:43:26', NULL),
(3, 'POS Officer', '2019-04-30 06:44:21', NULL);

-- --------------------------------------------------------

--
-- Structure for view `display_stock_bahan`
--
DROP TABLE IF EXISTS `display_stock_bahan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_bahan`  AS  select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah` from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) group by `bahan`.`bahan_id` ;

-- --------------------------------------------------------

--
-- Structure for view `display_stock_produk`
--
DROP TABLE IF EXISTS `display_stock_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_produk`  AS  select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) group by `produk`.`produk_id` ;

-- --------------------------------------------------------

--
-- Structure for view `staff_lama_kerja`
--
DROP TABLE IF EXISTS `staff_lama_kerja`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_lama_kerja`  AS  select `staff`.`staff_id` AS `staff_id`,if(((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12) >= 1),concat(round((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12),1),' Tahun'),if((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) = 0),'Kurang dari sebulan',concat(timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()),' Bulan'))) AS `lama_bekerja` from `staff` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arus_stock_bahan`
--
ALTER TABLE `arus_stock_bahan`
  ADD PRIMARY KEY (`arus_stock_bahan_id`);

--
-- Indexes for table `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`bahan_id`);

--
-- Indexes for table `forget_request`
--
ALTER TABLE `forget_request`
  ADD PRIMARY KEY (`forget_request_id`);

--
-- Indexes for table `harga_produk`
--
ALTER TABLE `harga_produk`
  ADD PRIMARY KEY (`harga_produk_id`);

--
-- Indexes for table `history_penyesuaian_bahan`
--
ALTER TABLE `history_penyesuaian_bahan`
  ADD PRIMARY KEY (`history_penyesuaian_bahan_id`);

--
-- Indexes for table `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  ADD PRIMARY KEY (`history_penyesuaian_produk_id`);

--
-- Indexes for table `history_transfer_bahan`
--
ALTER TABLE `history_transfer_bahan`
  ADD PRIMARY KEY (`history_transfer_bahan_id`);

--
-- Indexes for table `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  ADD PRIMARY KEY (`history_transfer_produk_id`);

--
-- Indexes for table `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  ADD PRIMARY KEY (`jenis_bahan_id`);

--
-- Indexes for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  ADD PRIMARY KEY (`jenis_produk_id`);

--
-- Indexes for table `komposisi_produk`
--
ALTER TABLE `komposisi_produk`
  ADD PRIMARY KEY (`komposisi_id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`lokasi_id`),
  ADD UNIQUE KEY `lokasi_kode` (`lokasi_kode`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`satuan_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `stock_bahan`
--
ALTER TABLE `stock_bahan`
  ADD PRIMARY KEY (`stock_bahan_id`),
  ADD KEY `stock_bahan_constraint` (`bahan_id`);

--
-- Indexes for table `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD PRIMARY KEY (`stock_produk_id`),
  ADD KEY `stock_produk_constraint` (`produk_id`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`suplier_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arus_stock_bahan`
--
ALTER TABLE `arus_stock_bahan`
  MODIFY `arus_stock_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `forget_request`
--
ALTER TABLE `forget_request`
  MODIFY `forget_request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `harga_produk`
--
ALTER TABLE `harga_produk`
  MODIFY `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_penyesuaian_bahan`
--
ALTER TABLE `history_penyesuaian_bahan`
  MODIFY `history_penyesuaian_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  MODIFY `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `history_transfer_bahan`
--
ALTER TABLE `history_transfer_bahan`
  MODIFY `history_transfer_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  MODIFY `history_transfer_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  MODIFY `jenis_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  MODIFY `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `komposisi_produk`
--
ALTER TABLE `komposisi_produk`
  MODIFY `komposisi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `lokasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_bahan`
--
ALTER TABLE `stock_bahan`
  MODIFY `stock_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stock_produk`
--
ALTER TABLE `stock_produk`
  MODIFY `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `suplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `stock_bahan`
--
ALTER TABLE `stock_bahan`
  ADD CONSTRAINT `stock_bahan_constraint` FOREIGN KEY (`bahan_id`) REFERENCES `bahan` (`bahan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD CONSTRAINT `stock_produk_constraint` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
