/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_balioz

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-07-02 19:25:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for arus_stock_bahan
-- ----------------------------
DROP TABLE IF EXISTS `arus_stock_bahan`;
CREATE TABLE `arus_stock_bahan` (
  `arus_stock_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `po_bahan_detail_id` int(11) DEFAULT NULL,
  `po_bahan` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT 'insert',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`arus_stock_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for arus_stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `arus_stock_produk`;
CREATE TABLE `arus_stock_produk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` float DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bahan
-- ----------------------------
DROP TABLE IF EXISTS `bahan`;
CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for forget_request
-- ----------------------------
DROP TABLE IF EXISTS `forget_request`;
CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  PRIMARY KEY (`forget_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for guest
-- ----------------------------
DROP TABLE IF EXISTS `guest`;
CREATE TABLE `guest` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_nama` varchar(50) DEFAULT NULL,
  `guest_alamat` varchar(50) DEFAULT NULL,
  `guest_telepon` varchar(50) DEFAULT NULL,
  `kewarganegaraan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for harga_produk
-- ----------------------------
DROP TABLE IF EXISTS `harga_produk`;
CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `minimal_pembelian` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`harga_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for history_penyesuaian_bahan
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_bahan`;
CREATE TABLE `history_penyesuaian_bahan` (
  `history_penyesuaian_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jenis_bahan_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_penyesuaian_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for history_penyesuaian_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_produk`;
CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_penyesuaian_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for history_transfer_bahan
-- ----------------------------
DROP TABLE IF EXISTS `history_transfer_bahan`;
CREATE TABLE `history_transfer_bahan` (
  `history_transfer_bahan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` text COLLATE utf8mb4_unicode_ci,
  `bahan_seri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_transfer_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for history_transfer_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_transfer_produk`;
CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_transfer_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jenis_bahan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_bahan`;
CREATE TABLE `jenis_bahan` (
  `jenis_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_bahan_kode` varchar(10) DEFAULT NULL,
  `jenis_bahan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for jenis_produk
-- ----------------------------
DROP TABLE IF EXISTS `jenis_produk`;
CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for komposisi_produk
-- ----------------------------
DROP TABLE IF EXISTS `komposisi_produk`;
CREATE TABLE `komposisi_produk` (
  `komposisi_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `komposisi_qty` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`komposisi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for lokasi
-- ----------------------------
DROP TABLE IF EXISTS `lokasi`;
CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`),
  UNIQUE KEY `lokasi_kode` (`lokasi_kode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_kode` varchar(50) DEFAULT NULL,
  `menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ekspedisi_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `nama_pelanggan` varchar(50) DEFAULT NULL,
  `alamat_pelanggan` varchar(100) DEFAULT NULL,
  `telepon_pelanggan` varchar(20) DEFAULT NULL,
  `urutan` bigint(20) DEFAULT NULL,
  `no_faktur` varchar(100) DEFAULT NULL,
  `status_distribusi` enum('not_yet','done') DEFAULT 'not_yet',
  `tanggal` date DEFAULT NULL,
  `jenis_transaksi` enum('retail','distributor') DEFAULT 'distributor',
  `pengiriman` enum('ya','tidak') DEFAULT NULL,
  `total` float DEFAULT NULL,
  `biaya_tambahan` float DEFAULT NULL,
  `potongan_akhir` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `terbayar` float DEFAULT NULL,
  `sisa_pembayaran` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `status_distribusi_done_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for penjualan_produk
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_produk`;
CREATE TABLE `penjualan_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `hpp` float DEFAULT NULL,
  `potongan` float DEFAULT NULL,
  `ppn_persen` float DEFAULT NULL,
  `ppn_nominal` float DEFAULT NULL,
  `harga_net` float DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for po_bahan
-- ----------------------------
DROP TABLE IF EXISTS `po_bahan`;
CREATE TABLE `po_bahan` (
  `po_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_bahan_no` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `suplier_id` int(11) DEFAULT NULL,
  `tipe_pembayaran` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_no` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_keterangan` varchar(100) DEFAULT NULL,
  `pengiriman` varchar(50) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `tanggal_pemesanan` date DEFAULT NULL,
  `total` bigint(20) DEFAULT '0',
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `grand_total` bigint(20) DEFAULT '0',
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `status_penerimaan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`po_bahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for po_bahan_detail
-- ----------------------------
DROP TABLE IF EXISTS `po_bahan_detail`;
CREATE TABLE `po_bahan_detail` (
  `po_bahan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_bahan_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `sub_total` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`po_bahan_detail_id`),
  KEY `po_bahan_constraint` (`po_bahan_id`),
  CONSTRAINT `po_bahan_constraint` FOREIGN KEY (`po_bahan_id`) REFERENCES `po_bahan` (`po_bahan_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_satuan_id` int(11) DEFAULT NULL,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `produk_nama` varchar(100) NOT NULL,
  `harga_eceran` double DEFAULT '0',
  `harga_grosir` double DEFAULT '0',
  `harga_konsinyasi` double DEFAULT '0',
  `produk_minimal_stock` bigint(20) NOT NULL,
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produksi
-- ----------------------------
DROP TABLE IF EXISTS `produksi`;
CREATE TABLE `produksi` (
  `produksi_id` int(11) NOT NULL AUTO_INCREMENT,
  `urutan` int(11) DEFAULT NULL,
  `produksi_kode` varchar(255) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `estimasi_selesai` date DEFAULT NULL,
  `status_produksi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tanggal_selesai` date DEFAULT NULL,
  `status_penerimaan` varchar(255) DEFAULT NULL,
  `lokasi_bahan_id` int(11) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`produksi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produksi_item
-- ----------------------------
DROP TABLE IF EXISTS `produksi_item`;
CREATE TABLE `produksi_item` (
  `produksi_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `produksi_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`produksi_item_id`),
  KEY `produksi_item_relation` (`produksi_id`),
  CONSTRAINT `produksi_item_relation` FOREIGN KEY (`produksi_id`) REFERENCES `produksi` (`produksi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produksi_item_bahan
-- ----------------------------
DROP TABLE IF EXISTS `produksi_item_bahan`;
CREATE TABLE `produksi_item_bahan` (
  `produksi_item_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `produksi_item_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`produksi_item_bahan_id`),
  KEY `produksi_item_bahan_relation` (`produksi_item_id`),
  CONSTRAINT `produksi_item_bahan_relation` FOREIGN KEY (`produksi_item_id`) REFERENCES `produksi_item` (`produksi_item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for stock_bahan
-- ----------------------------
DROP TABLE IF EXISTS `stock_bahan`;
CREATE TABLE `stock_bahan` (
  `stock_bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_seri` varchar(50) DEFAULT NULL,
  `stock_bahan_qty` bigint(20) DEFAULT NULL,
  `stock_bahan_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_bahan_id`),
  KEY `stock_bahan_constraint` (`bahan_id`),
  CONSTRAINT `stock_bahan_constraint` FOREIGN KEY (`bahan_id`) REFERENCES `bahan` (`bahan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `stock_produk`;
CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` bigint(20) NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` bigint(20) DEFAULT NULL,
  `stock_produk_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_produk_id`),
  KEY `stock_produk_constraint` (`produk_id`),
  CONSTRAINT `stock_produk_constraint` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `sub_menu`;
CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `sub_menu_kode` varchar(50) DEFAULT NULL,
  `sub_menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for suplier
-- ----------------------------
DROP TABLE IF EXISTS `suplier`;
CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`suplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tipe_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `tipe_pembayaran`;
CREATE TABLE `tipe_pembayaran` (
  `tipe_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_pembayaran_kode` varchar(20) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `no_akun` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `kembalian` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tipe_pembayaran_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_akses` text,
  `keterangan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- View structure for display_stock_bahan
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `display_stock_bahan` AS select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah` from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by `bahan`.`bahan_id` ;

-- ----------------------------
-- View structure for display_stock_bahan_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `display_stock_bahan_lokasi` AS select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah`,stock_bahan.stock_bahan_lokasi_id from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by stock_bahan.stock_bahan_lokasi_id,stock_bahan.bahan_id ;

-- ----------------------------
-- View structure for display_stock_produk
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `display_stock_produk` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where stock_produk.delete_flag = 0 group by `produk`.`produk_id` ;

-- ----------------------------
-- View structure for display_stock_produk_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `display_stock_produk_lokasi` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah`,stock_produk.stock_produk_lokasi_id from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by stock_produk.stock_produk_lokasi_id,stock_produk.produk_id ;

-- ----------------------------
-- View structure for staff_lama_kerja
-- ----------------------------
DROP VIEW IF EXISTS `staff_lama_kerja`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `staff_lama_kerja` AS  ;
