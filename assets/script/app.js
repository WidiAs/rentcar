"use strict";

// Class Definition
var KTGeneral = function () {
	var base_url = $("#base_url").val();
	var list_url = $("#list_url").val();
	var page = (($("#page").val() != undefined) ? $("#page").val() : "");
	var current_page = (($("#current_page").val() != undefined) ? $("#current_page").val() : "");
	var showErrorMsg = function (form, type, msg) {
		var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

		form.find('.alert').remove();
		alert.prependTo(form);
		//alert.animateClass('fadeIn animated');
		KTUtil.animateClass(alert[0], 'fadeIn animated');
		alert.find('span').html(msg);
	}
	var generalForm = function () {
		if ($.fn.datepicker) {
			$('#mulai_bekerja').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'yyyy-mm-dd',
			});
			$('.tanggal').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'yyyy-mm-dd',
			});
			$('.input-daterange').datepicker({
				todayHighlight: true,
				autoclose: true,
				format: 'yyyy-mm-dd',
				orientation: "bottom left",
				templates: {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>',
				},
			});
		}
		if ($.fn.select2) {
			console.log(123)
			$('#kt_select2_1, #kt_select2_2, .kt_select_2').select2({
				width: '100%'
			});
		}

	}

	var KTDataTable = function () {

		$(".input-numeral").val(0);
		$(".modal").on('hidden.bs.modal', function () {
			$(".input-numeral").val(0);

		})
		var actionJSON = $("#table_action").html() != "" && $("#table_action").length ? jQuery.parseJSON($("#table_action").html()) : "";
		var actionWidth = ($("#table_action").data("width") != undefined) ? $("#table_action").data("width") : 110;
		var sumColumn = []
		if ($("#sumColumn").length > 0 && $("#sumColumn").html() != "") {
			sumColumn = jQuery.parseJSON($("#sumColumn").html());
		}
		var defAction = {
			targets: -1,
			responsivePriority: 1,
			title: 'Actions',
			orderable: false,
			render: function (data, type, full, meta) {
				var header = '<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">'
				var headerDrop = '<span class="dropdown btn-group m-btn-group m-btn-group--pill btn-group-sm">' +
					'<a href="javascript:;" class="btn btn-sm btn-primary" data-toggle="dropdown" aria-expanded="true">' +
					'<i class="la la-ellipsis-h"></i> Menu' +
					'</a>' +
					'<div class="dropdown-menu dropdown-menu-right">';
				var style = (($("#table_action").data("style") != undefined && $("#table_action").data("style") == "dropdown") ? "dropdown" : 'btn')
				var body = '<textarea style="display:none">' + JSON.stringify(data) + '</textarea>\
                        '
				if (actionJSON.pay != undefined) body += '<a href="' + ((data.pay_url != undefined) ? data.pay_url : "javascript:;") + '" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-icon btn-icon-sm stock-btn" title="Pembayaran" ' + (((actionJSON.pay != undefined) && (actionJSON.pay)) ? '' : 'style="display:none"') + '>\
                            <i class="flaticon-notepad"></i>&nbsp; Pembayaran\
                        </a>\
                        '
				if (data.menu_url != undefined) body += '<a href="' + ((data.menu_url != undefined) ? data.menu_url : "javascript:;") + '" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-icon btn-icon-sm stock-btn" title="Menu Action" ' + (((actionJSON.menu != undefined) && (actionJSON.menu)) ? '' : 'style="display:none"') + '>\
                            <i class="la la-key"></i>&nbsp; Menu Akses\
                        </a>\
                        '
				if (actionJSON.adjust != undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-primary') + ' btn-icon btn-icon-sm ' + ((actionJSON.adjust != undefined) ? 'adjust-btn' : '') + '" title="Penyesuaian" ' + (((actionJSON.adjust != undefined) && (actionJSON.adjust)) ? '' : 'style="display:none"') + '>\
                            <i class="fa fa-adjust"></i>&nbsp;Sesuaikan\
                        </a>\
                        '
				if (data.price_url != undefined) body += '<a href="' + ((data.price_url != undefined) ? data.price_url : "javascript:;") + '" class="dropdown-item btn-icon btn-icon-sm ' + ((actionJSON.price != undefined) ? 'price-btn' : '') + '" title="Rentang Harga" ' + (((actionJSON.price != undefined) && (actionJSON.price)) ? '' : 'style="display:none"') + '>\
                            <i class="flaticon-price-tag"></i>&nbsp;Rentang Harga\
                        </a>\
                        '
				if (actionJSON.confirmation != undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-primary') + ' btn-icon btn-icon-sm ' + ((actionJSON.confirmation != undefined) ? 'confirmation-btn' : '') + '" title="Konfirmasi" ' + (((actionJSON.confirmation != undefined) && (actionJSON.confirmation)) ? '' : 'style="display:none"') + '>\
                            <i class="fa fa-calendar-check"></i>&nbsp; Konfirmasi\
                            </a>\
                        '
				if (actionJSON.transfer != undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-sm btn-icon btn-icon-sm ' + ((actionJSON.transfer != undefined) ? 'transfer-btn' : '') + '" title="Transfer" ' + (((actionJSON.transfer != undefined) && (actionJSON.transfer)) ? '' : 'style="display:none"') + '>\
                            <i class="flaticon-truck"></i>&nbsp; Tranfer\
                        </a>\
                        '
				if (data.stok_url != undefined) body += '<a href="' + ((data.stok_url != undefined) ? data.stok_url : "javascript:;") + '" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-icon btn-icon-sm stock-btn" title="Stok" ' + (((actionJSON.stock != undefined) && (actionJSON.stock)) ? '' : 'style="display:none"') + '>\
                            <i class="flaticon-open-box"></i>&nbsp; Stok\
                        </a>\
                        '
				if (actionJSON.view != undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-info') + ' btn-icon btn-icon-sm ' + ((actionJSON.view != undefined) ? ((data.class_detail != undefined) ? data.class_detail : 'view-btn') : '') + '" title="Details" ' + (((actionJSON.view != undefined) && (actionJSON.view)) ? '' : 'style="display:none"') + '>\
                            <i class="flaticon-visible"></i>&nbsp; Detail\
                        </a>\
                        '
				if (actionJSON.edit != undefined && data.deny_edit == undefined) body += '<a href="' + ((data.edit_url != undefined) ? data.edit_url : "javascript:;") + '" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-warning') + ' btn-icon btn-icon-sm ' + ((actionJSON.edit != undefined) ? 'edit-btn' : '') + '" title="Edit" ' + (((actionJSON.edit != undefined) && (actionJSON.edit)) ? ((data.hide_edit != undefined) ? 'style="display:none"' : '') : 'style="display:none"') + '>\
                            <i class="flaticon2-edit"></i>&nbsp; Edit\
                        </a>\
                        '
				if (actionJSON.delete != undefined && data.deny_delete == undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-danger') + ' btn-icon btn-icon-sm ' + ((actionJSON.delete != undefined) ? 'delete-btn' : '') + '" data-route="' + ((data.delete_url != undefined) ? data.delete_url : '') + '" title="Delete" ' + (((actionJSON.delete != undefined) && (actionJSON.delete)) ? ((data.hide_delete != undefined) ? 'style="display:none"' : '') : 'style="display:none"') + '>\
                            <i class="flaticon2-trash"></i>&nbsp; Hapus\
                        </a>\
                       '
				if (actionJSON.barcode != undefined) body += '<a href="javascript:;" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-primary') + ' btn-icon btn-icon-sm ' + ((actionJSON.barcode != undefined) ? 'barcode-btn' : '') + '"  title="Print Barcode" ' + (((actionJSON.barcode != undefined) && (actionJSON.barcode)) ? '' : 'style="display:none"') + '>\
                            &nbsp;<i class="fa fa-barcode"></i>&nbsp; Barcode\
                        </a>\
                       '
				if (data.composition_url != undefined) body += '<a href="' + ((data.composition_url != undefined) ? data.composition_url : "javascript:;") + '" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-warning') + ' btn-icon btn-icon-sm ' + ((actionJSON.composition != undefined) ? 'composition-btn' : '') + '" title="Penyesuaian" ' + (((actionJSON.composition != undefined) && (actionJSON.composition)) ? '' : 'style="display:none"') + '>\
                            <i class="fa fa-cubes"></i>\
                        </a>\
                        '
				if (page == "produksi" && data.produksi_status_btn != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm produksi-status-btn" title="Selesai Produksi">\
                            <i class="flaticon2-checkmark"></i>&nbsp; Selesai Produksi\
                        </a>\
                        '
				if (page == "custom-produksi" && data.produksi_status_btn != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm produksi-status-btn" title="Selesai Produksi">\
                            <i class="flaticon2-checkmark"></i>&nbsp; Selesai Produksi\
                        </a>\
                        '
				if (page == "custom-produksi" && data.start_produksi_btn != undefined) body += '<a href="' + ((data.start_url != undefined) ? data.start_url : "javascript:;") + '" class="dropdown-item btn-icon btn-icon-sm produksi-custom-status-btn" title="Mulai Produksi">\
                            <i class="flaticon2-checkmark"></i>&nbsp; Mulai Produksi\
                        </a>\
                        '
				if (actionJSON.print_pos != undefined) body += '<a href="' + ((data.print_pos_url != undefined) ? data.print_pos_url : "javascript:;") + '" target="_blank" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-icon btn-icon-sm posting-btn" title="Print Struk" ' + (((actionJSON.print_pos != undefined) && (actionJSON.print_pos)) ? '' : 'style="display:none"') + '>\
                            <i class="fa fa-print"></i>&nbsp; Print Struct\
                        </a>\
                        '
				if (actionJSON.print != undefined) body += '<a href="' + ((data.print_url != undefined) ? data.print_url : "javascript:;") + '" target="_blank" class="dropdown-item ' + ((style == "dropdown") ? "" : 'btn btn-success') + ' btn-icon btn-icon-sm print-btn" title="Print" ' + (((actionJSON.print != undefined) && (actionJSON.print)) ? '' : 'style="display:none"') + '>\
                            <i class="fa fa-print"></i>&nbsp; Print\
                        </a>\
                        '
				if (page == "custom-produksi" && data.penerimaan_status_btn != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" title="Penerimaan Produksi">\
                            <i class="flaticon-tool"></i>&nbsp; Penerimaan Produksi\
                        </a>\
                        '
				if (page == "produksi" && data.penerimaan_status_btn != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" title="Penerimaan Produksi">\
                            <i class="flaticon-tool"></i>&nbsp; Penerimaan Produksi\
                        </a>\
                        '
				if (page == "order_bahan" && data.penerimaan_status_btn != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" title="Penerimaan Produksi">\
                            <i class="flaticon-tool"></i>&nbsp; Penerimaan Order Bahan\
                        </a>\
                        '
				if (actionJSON.surat_jalan != undefined) body += '<a href="' + ((data.surat_jalan_url != undefined) ? data.surat_jalan_url : "javascript:;") + '" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" title="Surat Jalan">\
                            <i class="flaticon-tool"></i>&nbsp; Buat Surat Jalan\
                        </a>\
                        '
				if (actionJSON.penerimaan_po != undefined) body += '<a href="javascript:;" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" title="Penerimaan PO">\
                            <i class="flaticon-tool"></i>&nbsp; Penerimaan PO\
                        </a>\
                        '
				if (actionJSON.invoice != undefined) body += '<a href="' + ((data.invoice_url != undefined) ? data.invoice_url : "javascript:;") + '" class="dropdown-item btn-icon btn-icon-sm penerimaan-status-btn" '+ (((data.invoice_hide != undefined) && (data.invoice_hide == true)) ? 'style="display:none"' : '') +' title="Invoice">\
                            <i class="flaticon-tool"></i>&nbsp; Invoice\
                        </a>\
                        '
				var footer = '</div>'
				var footerDrop = '</div></span>';
				var temp = header + body + footer
				if (style == "dropdown") {
					temp = headerDrop + body + footerDrop
				}
				return temp;
			},
		}
		var columnDef = [];
		var columnsTemp = [];
		var bagdeObject = {};

		var badgeTemplate = function (target) {
			return {
				targets: target,
				responsivePriority: 1,
				render: function (data, type, full, meta) {
					var status = {
						"Diterima Sebagian": {'title': 'Diterima Sebagian', 'class': 'kt-badge--warning'},
						"Diterima Semua": {'title': 'Diterima Semua', 'class': 'kt-badge--success'},
						"Ditolak": {'title': 'Ditolak', 'class': ' kt-badge--danger'},
						"Menunggu Konfirmasi": {'title': 'Menunggu Konfirmasi', 'class': ' kt-badge--info'},
						"Hutang": {'title': 'Hutang', 'class': ' kt-badge--danger'},
						"Lunas": {'title': 'Lunas', 'class': ' kt-badge--success'},
						"Belum Diterima": {'title': 'Belum Diterima', 'class': ' kt-badge--danger'},
						"Diterima": {'title': 'Diterima', 'class': ' kt-badge--success'},
						"Dikerjakan": {'title': 'Dikerjakan', 'class': ' kt-badge--warning'},
						"Selesai": {'title': 'Selesai', 'class': ' kt-badge--info'},
					};
					return '<span class="kt-badge ' + status[full[bagdeObject["field_" + target]]].class + ' kt-badge--inline kt-badge--pill">' + status[full[bagdeObject["field_" + target]]].title + '</span>';
				}
			}
		}
		if ($("#table_columnDef").html() != null || $("#table_columnDef").html() != "" && $("#table_columnDef").length) {
			var temp = jQuery.parseJSON($("#table_columnDef").html());
			columnDef.push(temp);
		}
		if ($("#table_column").html() != null || $("#table_column").html() != "" && $("#table_column").length) {
			columnsTemp = jQuery.parseJSON($("#table_column").html());
			if (actionJSON != "") {
				if ($('.kt-datatable').length > 0) {
					columnsTemp.push(action);
				}
				if ($('.datatable').length > 0) {
					var actionColumn = {data: null, mData: null}
					columnsTemp.push(actionColumn);
					columnDef.push(defAction);
				}

			}

		}
		$.each(columnsTemp, function (key, value) {
			if (value.template != undefined) {
				if (value.template == "badgeTemplate") {
					columnDef.push(badgeTemplate(key));
					bagdeObject["field_" + key] = value.data;
				}
			}
		});
		if ($('.kt-datatable').length > 0) {
		}
		if ($('.datatable').length > 0) {
			var table = $('.datatable').DataTable({
				responsive: true,
				searchDelay: 500,
				processing: true,
				serverSide: true,
				ordering: false,
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
				pageLength: 10,
				dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
				ajax: list_url,
				columns: columnsTemp,
				columnDefs: columnDef,
				footerCallback: function (row, data, start, end, display) {
					var api = this.api(), data;

					// Remove the formatting to get integer data for summation
					var intVal = function (i) {
						return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
					};

					$.each(sumColumn, function (i, value) {
						var column = value;
						// Total over all pages
						var total = api.column(column).data().reduce(function (a, b) {
							return intVal(a) + intVal(b);
						}, 0);

						// Total over this page
						var pageTotal = api.column(column, {page: 'current'}).data().reduce(function (a, b) {
							return intVal(a) + intVal(b);
						}, 0);

						// Update footer
						$(api.column(column).footer()).html(
							KTUtil.numberString(pageTotal.toFixed(0)),
						);
					})

				},

			});
			$("#generalSearch").on('keyup', function (e) {
				table.search($("#generalSearch").val());
				var params = {};
				$("#akses-pdf").attr('href', current_page + "pdf?key=" + $("#generalSearch").val());
				$("#akses-excel").attr('href', current_page + "excel?key=" + $("#generalSearch").val());
				$('.searchInput').each(function () {

					var i = $(this).data('col-index');
					if (params[i]) {
						params[i] += '|' + $(this).val();
					} else {
						params[i] = $(this).val();
					}
					var url = $("#akses-pdf").attr('href');
					$("#akses-pdf").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())
					url = $("#akses-excel").attr('href');
					$("#akses-excel").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())

				});
				$.each(params, function (i, val) {
					// apply search params to datatable
					table.column(i).search(val ? val : '', false, false);
				});
				table.table().draw();
			})
			$(".textSearch").keyup(function () {
				$(this).trigger('change');
			})
			$(".searchInput").change(function () {
				table.search($("#generalSearch").val());
				var params = {};
				$("#akses-pdf").attr('href', current_page + "pdf?key=" + $("#generalSearch").val());
				$("#akses-excel").attr('href', current_page + "excel?key=" + $("#generalSearch").val());
				$('.searchInput').each(function () {

					var i = $(this).data('col-index');
					if (params[i]) {
						params[i] += '|' + $(this).val();
					} else {
						params[i] = $(this).val();
					}
					var url = $("#akses-pdf").attr('href');
					$("#akses-pdf").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())
					url = $("#akses-excel").attr('href');
					$("#akses-excel").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())

				});
				$.each(params, function (i, val) {
					// apply search params to datatable
					table.column(i).search(val ? val : '', false, false);
				});
				table.table().draw();
			})
		}

		$("#child_data_ajax").on('click', '.view-btn', function () {

			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				$('#kt_modal_detail label[name="' + key + '"]').html(value);
				$('#kt_modal_detail [name="' + key + '"]').val(value);
				var field = $('#kt_modal_detail .img-preview').data("field");
				if (field == key) {
					$('#kt_modal_detail .img-preview').attr("src", base_url + value);
				}
			})

			$("#kt_modal_detail").modal("show");
		})
		$("#child_data_ajax").on('click', '.confirmation-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				$('#kt_modal_confirmation [name="' + key + '"]').val(value);
				$('#kt_modal_confirmation [name="' + key + '"]').html(value);
			})

			$("#kt_modal_confirmation").modal("show");
			$("#qty_terima").val(1);
		})
		$("#child_data_ajax").on('click', '.adjust-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				$('#kt_modal_adjust [name="' + key + '"]').val(value);
				$('#kt_modal_adjust label[name="' + key + '"]').html(value);
				var field = $('#kt_modal_adjust .img-preview').data("field");
				if (field == key) {
					$('#kt_modal_adjust .img-preview').attr("src", base_url + value);
				}
			})

			$("#kt_modal_adjust").modal("show");
		})
		$("#child_data_ajax").on('click', '.edit-btn', function () {

			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				var type = $('[name="' + key + '"]').attr('type');
				if (type == 'file') {
					$('#kt_modal_edit .img-preview').attr('src', base_url + value);
				} else if (type == 'radio') {
					$('#kt_modal_edit [name="' + key + '"][value=' + value + ']').prop("checked", true);

				} else if (type == 'checkbox') {
					if (value == true || value == 1) {
						$('#kt_modal_edit [name="' + key + '"]').prop("checked", true);
					} else {
						$('#kt_modal_edit [name="' + key + '"]').prop("checked", false);
					}
				} else {
					if (key != "password") {
						$('#kt_modal_edit [name="' + key + '"]').val(value);
						$('#kt_modal_edit [name="' + key + '"]').trigger('change');
						$(".input-numeral").trigger('change');
					}

				}
			})
			$("#kt_modal_edit").modal("show");
		})
		$("#child_data_ajax").on('click', '.transfer-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$('#kt_modal_transfer .lokasi_option').removeAttr('style');
			$.each(object, function (key, value) {
				var type = $('[name="' + key + '"]').attr('type');
				if (type == 'file') {
					$('#kt_modal_transfer .img-preview').attr('src', base_url + value);
				} else if (type == 'radio') {
					$('#kt_modal_transfer [name="' + key + '"][value=' + value + ']').attr('checked', 'checked');
				} else {
					if (key != "password") {
						if (key == "stock_bahan_lokasi_id" || key == "stock_produk_lokasi_id") {
							$('#kt_modal_transfer option[value="' + value + '"]').css('display', 'none');
						}
						$('#kt_modal_transfer label[name="' + key + '"]').html(value);
						$('#kt_modal_transfer [name="' + key + '"]').val(value);
						$('#kt_modal_transfer [name="' + key + '"]').trigger('change');
						$('#kt_modal_transfer select').val($("#kt_modal_transfer select option:first").val());
					}

				}
			})
			$("#kt_modal_transfer").modal("show");
		})
		$("#child_data_ajax").on('click', '.delete-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				$('#kt_modal_delete [name="' + key + '"]').val(value);
			})
			var route = object.delete_url;
			swal.fire({
				title: "Perhatian ...",
				text: "Yakin hapus data ini ?",
				type: "warning",
				showCancelButton: !0,
				confirmButtonColor: "#0abb87",
				confirmButtonText: "Ya, yakin",
				cancelButtonText: "Batal",
			}).then(function (e) {
				if (e.value) {

					$.ajax({
						url: route,
						type: "delete",
						data: {"id": object.row_id},
						beforeSend: function () {
							$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
						},
						success: function (response) {
							$('.wrapper-loading').fadeOut().addClass('hidden');
							var data = jQuery.parseJSON(response);
							if (data.success) {
								if ($('.kt-datatable').length > 0) {
									datatable.reload()
								}
								if ($('.datatable').length > 0) {
									table.ajax.reload();
								}
							} else {
								swal.fire({
									type: 'error',
									text: data.message,
									showConfirmButton: false,
									timer: 1500
								});
							}
						},
						error: function (request) {
							$('.wrapper-loading').fadeOut().addClass('hidden');
							swal.fire({
								title: "Ada yang Salah",
								html: request.responseJSON.message,
								type: "warning"
							});
						}
					});
				}
			});
		})
		$('#kt_add_submit').click(function (e) {
			e.preventDefault();
			var btn = $(this);
			var form = $(this).closest('form');
			form.validate({
				rules: {
					password: {
						required: true,
						minlength: 5,
					},
					re_password: {
						required: true,
						minlength: 5,
						equalTo: '#password'
					}
				}
			});
			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						form.clearForm();
						form.validate().resetForm();
						if ($.fn.typeahead) {
							loadTypeahead();
						}
						$('.modal').modal('hide');
						if ($('.kt-datatable').length > 0) {
							datatable.reload()
						}
						if ($('.datatable').length > 0) {
							table.ajax.reload();
						}
						if (btn.attr('data-reload') != undefined){
							location.reload();
						}
						$("input[type=number]").val(0);
						if (btn.data('page') != undefined && btn.data('page') == "transfer-stock") {
							if (data.sjl){
								window.open(data.sjl, '_blank');
							}
							swal.fire({
								type: 'success',
								title: 'Dalam proses tranfer',
								text: 'Menunggu konfirmasi penerimaan'
							});
						}
					} else {
						swal.fire({
							type: 'error',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		});
		$("#kt_modal_transfer select").change(function () {
			$("#kt_modal_transfer [name='lokasi_tujuan_nama']").val(($("#kt_modal_transfer select option[value='" + $(this).val() + "']").html()));
		})
		$('#kt_edit_submit').click(function (e) {
			e.preventDefault();
			var btn = $(this);
			var form = $(this).closest('form');
			form.validate({
				rules: {
					password: {
						minlength: 5,
					},
					re_password: {
						minlength: 5,
						equalTo: '#edit_password'
					}
				}
			});
			if (!form.valid()) {
				return;
			}
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						form.clearForm();
						form.validate().resetForm();
						$(".modal").modal("hide");
						$("input[type=number]").val(0);
						if ($('.kt-datatable').length > 0) {
							datatable.reload()
						}
						if ($('.datatable').length > 0) {
							table.ajax.reload();
						}
						if ($.fn.typeahead) {
							loadTypeahead();
						}
					} else {
						swal.fire({
							type: 'error',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			});
		});
		var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
			'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
			'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
			'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
			'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
			'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
			'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
			'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
			'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
		];
		var substringMatcher = function (strs) {
			return function findMatches(q, cb) {
				var matches, substrRegex;

				// an array that will be populated with substring matches
				matches = [];

				// regex used to determine if a string contains the substring `q`
				substrRegex = new RegExp(q, 'i');

				// iterate through the pool of strings and for any string that
				// contains the substring `q`, add it to the `matches` array
				$.each(strs, function (i, str) {
					if (substrRegex.test(str)) {
						matches.push(str);
					}
				});

				cb(matches);
			};
		};

		if ($.fn.typeahead) {
			var loadTypeahead = function () {
				$.ajax({
					url: base_url + "suplier/option",
					type: "post",
					success: function (response) {
						states = jQuery.parseJSON(response);
						$('#kt_typeahead_1').typeahead({
							hint: true,
							highlight: true,
							minLength: 1
						}, {
							name: 'states',
							source: substringMatcher(states)
						});
						$('#kt_typeahead_2').typeahead({
							hint: true,
							highlight: true,
							minLength: 1
						}, {
							name: 'states',
							source: substringMatcher(states)
						});
					},
					error: function (request) {
					}
				});
			}
			loadTypeahead()
		}
	}
	var handleAvatarChange = function () {
		$(".img-input").change(function () {
			var display = $(this).attr('data-display');
			if (this.files && this.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#' + display).attr('src', e.target.result);
				}
				reader.readAsDataURL(this.files[0]);
			}
		});

	}


	// Public Functions
	return {
		// public functions
		init: function () {
			generalForm();
			KTDataTable();
			handleAvatarChange();
		}
	};
}();

// Class Initialization
jQuery(document).ready(function () {
	KTGeneral.init();
	return_home();
});

function return_home(){
	if ($('#table_action').length) {
		var action = JSON.parse($('#table_action').html());
		if (action['list'] != true){
			$('#kt_content').css('display', 'none')
		}
	};
	if ($('#action').length) {
		var action = JSON.parse($('#action').val());
		if (action['list'] != true){
			// var home = $('#back-home').val();
			// window.location.href = home;
			$('#konten-utama').css('display', 'none')
		}
	};
}
document.addEventListener('DOMContentLoaded', () => {
	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})

	});
	$('.input-angka').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})

	});
	$('.input-decimal').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralDecimalScale: 4
		})

	});
});
