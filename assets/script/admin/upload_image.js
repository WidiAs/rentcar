jQuery(document).ready(function () {
	image_change();
});


function image_change() {
	$(".img-input").change(function () {
		var display = $(this).data('display');
		var name = $(this).attr("name");
		upload_image(this, name)
		// readURL(this, display);
		$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
		$('.error-message').remove();
	});
}

function upload_image(input, name) {
	var test = $('[name="' + name + '"]')[0].files[0];
	console.log('image0');
	console.log(test);
	console.log(test.size);
	var label = $('[name="' + name + '"]').attr('data-display');
	var bagi = 2;

	if (parseInt(test.size) >= 1700000) {
		bagi = 8;
	} else if (1700000 > parseInt(test.size) > 1000000) {
		bagi = 4;
	}
	console.log(bagi);
	console.log(parseInt(bagi));
	var fileReader = new FileReader();
	var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

	fileReader.onload = function (event) {
		var image = new Image();

		image.onload = function () {
			// document.getElementById("original-Img").src=image.src;
			var canvas = document.createElement("canvas");
			var img_h = image.height;
			var img_w = image.width;

			if (bagi > 2) {
				img_h = image.height / bagi;
				img_w = image.width / bagi;
			}
			var context = canvas.getContext("2d");
			canvas.width = img_w;
			canvas.height = img_h;
			context.drawImage(image,
				0,
				0,
				image.width,
				image.height,
				0,
				0,
				canvas.width,
				canvas.height
			);

			document.getElementById(label).src = canvas.toDataURL();


			new_upload(name, canvas.toDataURL());
		}
		image.src = event.target.result;

	};

	var uploadImage = $('[name=' + name + ']');

	//check and retuns the length of uploded file.
	if (uploadImage[0].files.length === 0) {
		return;
	}

	//Is Used for validate a valid file.
	var uploadFile = $('[name=' + name + ']')[0].files[0];
	fileReader.readAsDataURL(uploadFile);


}


function new_upload(name, url) {
	var base_url = $('#home-url').val();
	console.log('image_resized');
	console.log(url);
	var blobBin = atob(url.split(',')[1]);
	var array = [];
	for (var i = 0; i < blobBin.length; i++) {
		array.push(blobBin.charCodeAt(i));
	}

	// console.log('file')
	// console.log(file)
	var data = new FormData();
	$.each($('[name=' + name + ']')[0].files, function (i, file) {

		// var file_new = new Blob([new Uint8Array(array)], {type: 'image/jpg'});
		// var file_new = new File([url], file['name']);
		var file_new = url.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
		console.log('file')
		// console.log(file_new)
		console.log(file_new)
		console.log(file)

		data.append('gambar', file_new);
		data.append('gambar_nama', file['name']);
		data.append('gambar_base', file);
	});
	console.log(data);
	$.ajax({
		url: base_url + "/upload-image",
		type: "post",
		data: data,
		contentType: false,
		cache: false,
		processData: false,
		beforeSend: function () {
			$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
			$('.error-message').remove();
		},
		success: function (response, status, xhr, $form) {
			$('.wrapper-loading').fadeOut().addClass('hidden');
			var data = JSON.parse(response);
			if (data.result == 'sukses') {
				console.log(data.url)
				$('[name=input_' + name + ']').val(data.url)
				$('[name=' + name + ']').siblings('label').html(data.url);
			} else {
				console.log(data.url)
				swal.fire({
					type: 'error',
					title: 'Peringatan',
					html: data.url,
					showConfirmButton: false,
					timer: 1500
				});
			}

		}, error: function (xhr, ajaxOptions, thrownError) {
			$('.wrapper-loading').fadeOut().addClass('hidden');
			console.log(xhr.status);
			console.log(xhr.responseText);
			console.log(thrownError);
		}
	});
}
