jQuery(document).ready(function () {
	input_numeral();
	input_numeral_change();
	tanggal();
	save();
});

function input_numeral() {
	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})
	});
}

function input_numeral_change() {
	$('.input-numeral').on('change, keyup', function () {
		var id = $(this).data('id');
		var max = parseFloat($('[name="max_kirim[' + id + ']"]').val());
		var nilai = intVal($(this).val());
		// console.log('jumlah');
		// console.log($(this).val());
		// console.log(max);
		if (nilai == "") {
			$(this).val("0");
		}

		if (nilai > max) {
			$(this).val(numstr(max));
		}
	});
}

function numstr(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function intVal(i) {
	return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
};

function tanggal() {
	$('.tanggal').datepicker({
		rtl: KTUtil.isRTL(),
		todayHighlight: true,
		orientation: "bottom left",
		autoclose: true,
		format: 'dd-mm-yyyy',
	});
}

function save() {
	$("#btn-save").click(function () {
		var btn = $(this);
		var form = $("#save-form");
		var url = $('#url').val();

		form.validate({});
		if (!form.valid()) {
			return false;
		}
		btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
		form.ajaxSubmit({
			url: form.attr("action"),
			beforeSend: function () {
				$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
			},
			success: function (response, status, xhr, $form) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				var data = jQuery.parseJSON(response);
				btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
				if (data.success) {
					console.log(data)
					Swal.fire({
						type: 'success',
						title: 'Data Berhasil Disimpan',
					}).then(function () {
						var url_print = url + '/print/' + data.id;
						window.open(url_print, "_blank");
						window.location.href = url;
					});
				} else {
					swal.fire({
						type: 'error',
						title: 'Peringatan',
						text: data.message,
						showConfirmButton: false,
						timer: 1500
					});
				}
			}, error: function (xhr, ajaxOptions, thrownError) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	})
}
