jQuery(document).ready(function () {
	$('.tanggal').datepicker({
		rtl: KTUtil.isRTL(),
		todayHighlight: true,
		orientation: "bottom left",
		autoclose: true,
		format: 'dd-mm-yyyy',
	});

	guest_datatable();
	penawaran_datatable();
	mobil_datatable();
	driver_datatable();
	general_search();

});

function guest_datatable() {
	if ($('#guest-table').length) {
		var guest_url = $("#guest_url").val();
		$("#guest-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: guest_url,
			columns: [{data: "no"}, {data: 'guest_nama'}, {data: 'guest_alamat'}, {data: 'guest_telepon'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
	}
}

function mobil_datatable() {
	if ($('#mobil-table').length) {
		var mobil_url = $("#mobil_url").val();
		var table = $("#mobil-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: mobil_url,
			columns: [{data: "no"}, {data: 'jenis_mobil_nama'}, {data: 'warna'}, {data: 'plat_mobil'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});

		$(".searchInput").change(function () {
			table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function () {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				} else {
					params[i] = $(this).val();
				}
				var url = $("#akses-pdf").attr('href');
				$("#akses-pdf").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())
				url = $("#akses-excel").attr('href');
				$("#akses-excel").attr('href', url + "&" + $(this).data('field') + "=" + $(this).val())

			});
			$.each(params, function (i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});
			table.table().draw();
		})
	}
}

function driver_datatable() {
	if ($('#driver-table').length) {
		var driver_url = $("#driver_url").val();
		$("#driver-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: driver_url,
			columns: [{data: "no"}, {data: 'staff_nama'}, {data: 'staff_alamat'}, {data: 'staff_phone_number'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
	}
}

function penawaran_datatable() {
	if ($('#penawaran-table').length) {
		var penawaran_url = $("#penawaran_url").val();
		$("#penawaran-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: penawaran_url,
			columns: [{data: "no"}, {data: 'no_penawaran'}, {data: 'pelanggan_nama'}, {data: 'tanggal_order_lbl'}, {data: 'grand_total_lbl'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
	}
}


function str_to_number(i) {
	return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
}

function format_number(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function input_numeral() {
	$('.input-numeral').on('change keyup', function () {
		var nilai = $(this).val();
		if (!nilai){
			$(this).val(0);
		}
	})
}

function input_numeral_harga() {
	$('.input-numeral-harga').on('change keyup', function () {
		var nilai = $(this).val();
		if (!nilai){
			$(this).val(0);
		}
	})
}
