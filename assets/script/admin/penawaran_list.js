jQuery(document).ready(function () {
	$("#child_data_ajax").on('click','.view-btn',function(){

		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		getDetail(object.penawaran_id);
	});

	detail_penyewaan();
});

function getDetail(id){
	$.ajax({
		type: "POST",
		url: $("#detail_url").val()+"/"+id,
		cache: false,
		success: function(response){
			var res = jQuery.parseJSON(response);
			parseDetail(res.data);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
			console.log(thrownError);
		}
	});
}
function parseDetail(data){
	$("#item_child").html('');
	$.each(data,function(i,value){
		var temp = "<tr>"+
			"<td>"+value.jenis_mobil_nama+"</td>"+
			"<td>"+value.plat_mobil+"</td>"+
			"<td>"+value.jenis+"</td>"+
			"<td>"+value.driver_nama+"</td>"+
			"<td>"+value.tanggal_ambil_lbl+"</td>"+
			"<td>"+value.hari+"</td>"+
			"<td>"+value.harga+"</td>"+
			"<td>"+value.sub_total+"</td>"+
			+"</tr>";
		$("#item_child").append(temp);
	})
}
function parseDetailPenyewaan(data){
	$("#item_child").html('');
	$.each(data,function(i,value){
		var temp = "<tr>"+
			"<td>"+value.jenis_mobil_nama+"</td>"+
			"<td>"+value.plat_mobil+"</td>"+
			"<td>"+value.jenis+"</td>"+
			"<td>"+value.driver_nama+"</td>"+
			"<td>"+value.tanggal_ambil_lbl+"</td>"+
			"<td>"+value.hari+"</td>"+
			"<td>"+value.harga+"</td>"+
			"<td>"+value.biaya_tambahan+"</td>"+
			"<td>"+value.sub_total+"</td>"+
			+"</tr>";
		$("#item_child").append(temp);
	})
}

function detail_penyewaan() {
	$(".table-penyewaan").on('click', '.detail-penyewaan-btn', function () {
		var base_url = $('#home-url').val();
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$.each(object, function (key, value) {
			$('#kt_modal_detail label[name="' + key + '"]').html(value);
			$('#kt_modal_detail [name="' + key + '"]').val(value);
		})

		var data = new FormData();
		data.append('penyewaan_id', object.penyewaan_id);

		$.ajax({
			type: "POST",
			url: base_url+'/penyewaan/detail/'+object.penyewaan_id,
			cache: false,
			success: function(response){
				var res = jQuery.parseJSON(response);
				parseDetailPenyewaan(res.data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});

		$("#kt_modal_detail").modal("show");
	});
}
