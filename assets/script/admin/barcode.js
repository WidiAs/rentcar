$(document).ready(function () {
	modal_open();

	cetak();

});

function modal_open() {
	$(".datatable").on('click', '.barcode-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		// console.log(object.barcode)
		var number = object.barcode;
		$('[name="number"]').val(number);
		$('[name="jumlah"]').val(1);
		$('#kt_modal_barcode').modal('show');
	});
}

function cetak() {
	$('#cetak').on('click', function () {
		var base_link = $('#link-print').val();
		var number = $('[name="number"]').val();
		var jumlah = $('[name="jumlah"]').val();

		if (!jumlah){
			swal.fire({
				title: "Perhatian ...",
				text: "Jumlah Cetak wajib di isi",
				type: "warning"
			});
		}else {
			var link_print = base_link+'/'+number+'/'+jumlah;
			$('#kt_modal_barcode').modal('hide');

			window.open(link_print, '_blank');
		}

	})

}
