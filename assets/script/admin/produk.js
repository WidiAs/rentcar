"use strict";

// Class Definition
var KTProduk = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        $("#child_data_ajax").on('click','.view-btn',function(){

            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_detail_produk label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_produk [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_produk .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_produk .img-preview').attr("src",base_url+value);
                }
            })
            getdetail(object.produk_id)
            
        })
        function getdetail(id){
            $.ajax({
                type: "POST",
                url: base_url+'produk/harga/'+id,
                cache: false,
                success: function(response){
                    parseHarga(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function parseHarga(response){
            var object = JSON.parse(response);
            $("#view_child_data").html("");
            if(object.length > 0){
                $.each(object,function(i,value){
                    var text = "<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+(value.minimal_pembelian)+"</td>"+
                                    "<td>"+(value.harga)+"</td>"+
                               "</tr>";
                    $("#view_child_data").append(text);
                })
            } else {
                $("#view_child_data").append('<tr><td colspan="3" style="text-align: center;">Tidak ada data</td></tr>');
            }
            $("#kt_modal_detail_produk").modal("show");
        }        
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTProduk.init();
});