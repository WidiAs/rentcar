"use strict";

// Class Definition
var KTHome = function() {
	var general = function(){
		if ($.fn.datepicker) {
			console.log(123);
			$('.tanggal').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'yyyy-mm-dd',
			});
		}
		var url_piutang_akan = $("#piutang_akan").val();
		var piutang_akan_table = $("#piutang-akan-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
			pageLength: 5,
			ajax: url_piutang_akan,
			columns:[{data:'no'},{data:'nama_pelanggan'},{data:'no_faktur'},{data:'sisa'},{data:'tenggat_pelunasan'}]
		});
		var url_piutang_jatuh = $("#piutang_jatuh").val();
		var piutang_jatuh_table = $("#piutang-jatuh-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
			pageLength: 5,
			ajax: url_piutang_jatuh,
			columns:[{data:'no'},{data:'nama_pelanggan'},{data:'no_faktur'},{data:'sisa'},{data:'tenggat_pelunasan'}]
		});
		var url_hutang_akan = $("#hutang_akan").val();
		var hutang_akan_table = $("#hutang-akan-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
			pageLength: 5,
			ajax: url_hutang_akan,
			columns:[{data:'no'},{data:'suplier_nama'},{data:'po_bahan_no'},{data:'sisa'},{data:'tenggat_pelunasan'}]
		});
		var url_hutang_jatuh = $("#hutang_jatuh").val();
		var hutang_jatuh_table = $("#hutang-jatuh-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
			pageLength: 5,
			ajax: url_hutang_jatuh,
			columns:[{data:'no'},{data:'suplier_nama'},{data:'po_bahan_no'},{data:'sisa'},{data:'tenggat_pelunasan'}]
		});
		var url_guest = $("#country_url").val();
		var guest_table = $("#guest-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
			pageLength: 5,
			ajax: url_guest,
			columns:[{data:'kewarganegaraan'},{data:'qty'}]
		});
		var url_most = $("#most_url").val();
		var most_table = $("#most-table").DataTable({
			responsive: true,
			searching: false,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthChange: false,
			bPaginate: false,
			info: false,
			pageLength: 5,
			ajax: url_most,
			columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'qty'},{data:'lokasi_nama'}]
		});
		var url_least = $("#least_url").val();
		var least_table = $("#least-table").DataTable({
			responsive: true,
			searching: false,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthChange: false,
			bPaginate: false,
			info: false,
			pageLength: 5,
			ajax: url_least,
			columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'qty'},{data:'lokasi_nama'}]
		});
		$(".akses-filter_data").click(function () {
			window.location.href = $(base_url).val()+"?start_date="+$("#start_date").val()+"&end_date="+$("#end_date").val()+"&lokasi_id="+$("#lokasi_id").val();
		})
		var chart = function() {
			// LINE CHART
			new Morris.Line({
				// ID of the element in which to draw the chart.
				element: 'kt_morris_1',
				// Chart data records -- each entry in this array corresponds to a point on
				// the chart.
				data: jQuery.parseJSON($("#chart-data").val()),
				// The name of the data record attribute that contains x-values.
				xkey: 'y',
				parseTime: false,
				ykeys: ['a'],
				labels: ['Sales'],
				lineColors: ['#a0d0e0'],
				hideHover: 'auto'
			});

		}
		chart();
	}
	// Public Functions
	return {
		// public functions
		init: function() {
			general();
		}
	};
}();

// Class Initialization
jQuery(document).ready(function() {
	KTHome.init();
});
