jQuery(document).ready(function () {
	reset_btn();
});


function reset_btn(){
	$('.tombol-reset').on('click', function () {
		var route = $(this).data('route');
		swal.fire({
			title: "Perhatian ...",
			text: "Yakin melakukan reset ?",
			type: "warning",
			showCancelButton: !0,
			confirmButtonColor: "#0abb87",
			confirmButtonText: "Ya, yakin",
			cancelButtonText: "Batal",
		}).then(function (e) {
			if (e.value) {

				$.ajax({
					url: route,
					type: "get",
					beforeSend: function () {
						$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
					},
					success: function (response) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						var data = jQuery.parseJSON(response);
						if (data.success) {
							swal.fire({
								title: "Ada yang Salah",
								html: "Data Berhasil Di Reset",
								type: "success"
							});
						} else {
							swal.fire({
								type: 'error',
								text: data.message,
								showConfirmButton: false,
								timer: 1500
							});
						}
					},
					error: function (request) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						swal.fire({
							title: "Ada yang Salah",
							html: "Gagal Mereset Data",
							type: "warning"
						});
					}
				});
			}
		});
	})
}
