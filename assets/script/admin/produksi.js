"use strict";

// Class Definition
var KTProduksi = function() {
    var general = function(){
        var no = 0;
        var no_bahan = 0;
        var produk_no = null;
        var bahan_no = null;
        var base_url = $("#base_url").val();
        if($("#no_produk").length > 0 ){
            no = $("#no_produk").val() + 1;
        }
        if($("#no_bahan").length > 0 ){
            no_bahan = $("#no_bahan").val() + 1;
        }
        $('.input-numeral').keyup(function(){
            if($(this).val()==""){
                $(this).val(0);
            }
        })
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $("#add_item").click(function(){
            var text = '<div class="item-produksi" id="item_'+no+'">'+
            '<div class="row">'+
            '<div class="col-md-6">'+
            '<div class="form-group row">'+
            '<label for="example-text-input" class="col-3 col-form-label">Produk <b class="label--required">*</b></label>'+
            '<div class="col-9">'+
            '<div class="input-group col-12">'+
            '<input type="text" class="form-control readonly" name="produk_nama_'+no+'" id="produk_nama_'+no+'" required="" autocomplete="off">'+
            '<input type="hidden" class="produk_id" id="produk_id_'+no+'" name="'+'item[produk_'+no+"][produk_id]"+'" readonly="">'+
            '<div class="input-group-append"><button class="btn btn-primary produk-search" type="button" data-no="'+no+'"><i class="flaticon-search"></i></button></div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="form-group row">'+
            '<label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>'+
            '<div class="col-6">'+
            '<input type="text" class="form-control input-numeral produk-jumlah" autocomplete="off" data-no="'+no+'" id="produk_jumlah_'+no+'" name="'+'item['+"produk_"+no+"][jumlah]"+'" value="0" required="">'+
            '</div>'+
            '<label class="col-3 col-form-label" id="produk_satuan_'+no+'"></label>'+
            '</div>'+                                        
            '</div>'+
            '<div class="col-md-6">'+
            '<div class="form-group row">'+
            '<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>'+
            '<div class="col-9">'+
            '<textarea class="form-control" rows="3" id="produk_keterangan_'+no+'" name="'+'item['+"produk_"+no+"][keterangan]"+'"></textarea>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="col-md-12"><h5><strong>List Bahan</strong></h5></div>'+
			'<div id="bahan_container_'+no+'" class="col-12"></div>'+
            '</div>'+
            '</div>';
            $("#item-container").append(text);
            new Cleave($("#produk_jumlah_"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            no++;
        });
        $(".lokasi-bahan").change(function(){
            $(".bahan-container").html('');
            if($(this).val() != ""){
                sess_lokasi($(this).val());
            }
        })
        $("#item-container").on('click','.delete-produk',function(){
            var index = $(this).data('container');
            $("#"+index).remove();
        })

        $('#item-container').on('keyup','.input-numeral',function(){
            if($(this).val() == ""){
                $(this).val("0");
            }
        });
        $('#item-container').on('keydown','.readonly',function(e){
            e.preventDefault();
        });
		$('#item-container').on('keyup','.produk-jumlah',function(){
			var index = $(this).data('no');
			var jumlah = intVal($(this).val());
			$.each( $("#bahan_container_"+index+" .jumlah_bahan"), function( key, value ) {
				var potong = parseFloat($("#produk_"+index+"_potong_"+key).val())
				var total = ""+(jumlah * potong);
				if(total.indexOf(".")!=-1){
					total = parseFloat(total).toFixed(2);
				}
				$(value).val(total)
				$("#produk_"+index+"_bahan_"+key).html(total)
			})
		});
        $('#item-container').on('click','.produk-search',function(){ 
            produk_no = $(this).data('no');
            $("#kt_modal_produk").modal("show");
        });
        $('#produk_child').on('click','.chose-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
			if(object.bahan_nama == null){
				swal.fire({
					type: 'error',
					text:'Produk ini belum memiliki data konversi bahan',
					showConfirmButton: false,
					timer: 1500
				});
			} else {
				$("#produk_nama_"+produk_no).val(object.produk_nama);
				$("#produk_id_"+produk_no).val(object.produk_id);
				$("#produk_satuan_"+produk_no).html(object.satuan_nama);
				$("#bahan_container_"+produk_no).html('');
				$("#produk_jumlah_"+produk_no).val(1);
				$("#produk_keterangan_"+produk_no).val("");
				var bahan = object.bahan_nama.split("|");
				var bahan_id = object.bahan_id.split("|");
				var jumlah = object.potong.split("|");
				$.each( bahan, function( key, value ) {
					var text = '<div class="col-12 row">'+
						'<div class="col-md-6">'+
							'<div class="form-group row">'+
								'<input type="hidden" name="'+'item['+"produk_"+produk_no+"][item_bahan][bahan_"+key+"][bahan_id]"+'" id="bahan_id_'+key+'" value="'+bahan_id[key]+'">'+
								'<label class="col-3 col-form-label">Bahan</label>'+
								'<label class="col-1 col-form-label">:</label>'+
								'<label class="col-8 col-form-label" id="bahan_nama_'+key+'">'+value+'</label>'+
							'</div>'+
						'</div>'+
						'<div class="col-md-6">'+
							'<div class="form-group row">'+
								'<input type="hidden" name="'+'item['+"produk_"+produk_no+"][item_bahan][bahan_"+key+"][jumlah_potong]"+'" id="produk_'+produk_no+'_potong_'+key+'" value="'+jumlah[key]+'">'+
								'<input type="hidden" class="jumlah_bahan" name="'+'item['+"produk_"+produk_no+"][item_bahan][bahan_"+key+"][jumlah]"+'" id="jumlah_bahan_'+key+'">'+
								'<label class="col-3 col-form-label">Jumlah</label>'+
								'<label class="col-1 col-form-label">:</label>'+
								'<label class="col-8 col-form-label" id="produk_'+produk_no+'_bahan_'+key+'">'+jumlah[key]+'</label>'+
							'</div>'+
						'</div>'+
					'</div>';
					$("#bahan_container_"+produk_no).append(text);

				});
			}
            $("#kt_modal_produk").modal("hide");
        });
        $('#child_data_ajax').on('click','.view-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.produksi_id;
            get_detail(id)
        });
        $('#child_data_ajax').on('click','.produksi-status-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.produksi_id;
            $("#produksi_id").val(id);
            $("#lokasi_bahan_id").val(object.lokasi_bahan_id);
            $("#kt_status_produksi").modal("show");
        });
        $('#child_data_ajax').on('click','.penerimaan-status-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.produksi_id;
            $("#produksi_id_penerimaan").val(id);
            get_detail(id,1);            
        });
        function display_penerimaan(response){
            var object = JSON.parse(response);
            $("#hpp_container").html("");
            $.each(object.produksi.item,function(key,value){
                var text = '<div class="form-group row">'+
                '<label for="example-text-input" class="col-3 col-form-label">HPP '+value.produk_nama+'</label>'+
                '<label for="example-text-input" class="col-1 col-form-label">:</label>'+
                '<div class="col-8">'+
                                    // '<input type="hidden" name="jumlah[produk_'+value.produk_id+']" value="'+value.jumlah+'" />'+
                                    '<input type="text" class="form-control kt-input input-numeral" id="hpp_'+value.produk_id+'" name="hpp[produk_'+value.produk_id+']" autocomplete="off" data-col-index="5" value="0" required="" />'+
                                    '</div>'+
                                    '</div>';
                                    $("#hpp_container").append(text);
                                    new Cleave($("#hpp_"+value.produk_id), {
                                        numeral: true,
                                        numeralThousandsGroupStyle: 'thousand'
                                    })                

                                })
            $("#kt_penerimaan").modal("show");
        }                   
        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'produksi/detail',
                data:{'produksi_id':id},
                cache: false,
                success: function(response){
                    if(type==0){
                        parseDetail(response);    
                    } else {
                        display_penerimaan(response)
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object.produksi,function(key,value){
                $('#kt_modal_detail_produksi label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_produksi [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_produksi .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_produksi .img-preview').attr("src",base_url+value);
                }
            })
            $("#view_child_data").html("");
            $.each(object.produksi.item,function(key,value){
                var text = '<tr>'+
                '<td rowspan="'+Object.keys(value.item_bahan).length+'">'+value.produk_nama+'</td>'+
                '<td rowspan="'+Object.keys(value.item_bahan).length+'">'+value.jumlah+'</td>'+
                '<td rowspan="'+Object.keys(value.item_bahan).length+'"><'+value.keterangan+'/td>';
                var a = 0;
                $.each(value.item_bahan,function(key2,value2){
                    if(a == 0){
                        text +='<td>'+value2.bahan_nama+'</td><td>'+value2.jumlah+'</td></tr>';
                        a = 1;
                    } else {
                        text +='<tr><td>'+value2.bahan_nama+'</td><td>'+value2.jumlah+'</td></tr>';
                    }
                });
                $("#view_child_data").append(text);
            })
            $("#kt_modal_detail_produksi").modal("show");            
        }        
        function sess_add_bahan(key,bahan_id){
            $.ajax({
                type: "POST",
                url: base_url+'produksi/utility/sess-bahan-add',
                data:{'key':key,'bahan_id':bahan_id},
                cache: false,
                success: function(response){
                    bahan_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function sess_change_bahan(key,jumlah){
            $.ajax({
                type: "POST",
                url: base_url+'produksi/utility/sess-bahan-change',
                data:{'key':key,'jumlah':jumlah},
                cache: false,
                success: function(response){
                    bahan_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function sess_delete_bahan(key){
            $.ajax({
                type: "POST",
                url: base_url+'produksi/utility/sess-bahan-delete',
                data:{'key':key},
                cache: false,
                success: function(response){
                    bahan_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function sess_lokasi(key){
            $.ajax({
                type: "POST",
                url: base_url+'produksi/utility/sess-lokasi',
                data:{'lokasi_id':key},
                cache: false,
                success: function(response){
                    bahan_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }        
        function intVal(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };                       
        var url_produk = $("#list_produk").val();
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_produk,
            columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'spec'},{data:'stock'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        var url_produk = $("#list_bahan").val();
        var bahan_table = $("#bahan-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_produk,
            columns:[{data:'bahan_kode'},{data:'bahan_nama'},{data:'jumlah_lokasi'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#kt_add_submit").click(function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            var theres_no_zero = true;
            var  no_empty_bahan = true;
            $( "#item-container .input-numeral" ).each(function() {
                if($(this).val() == "0"){
                    theres_no_zero = false;
                }
            });
            if($("#item-container").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    text:'Harus ada produk yang di produksi',
                    showConfirmButton: false,
                    timer: 1500
                });                 
                return;                
            }
            if(!theres_no_zero){
                swal.fire({
                    type: 'error',
                    text:'Tidak boleh ada jumlah yang bernilai 0',
                    showConfirmButton: false,
                    timer: 1500
                });                 
                return;
            }            
            form.validate({});
            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"produksi";
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });                        
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        $("#kt_produksi_status_submit").click(function(){
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin ? \nMengubah status menjadi selesai akan mengurangi stok bahan dan data ini tidak dapat dirubah maupun dihapus dikemudian hari",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87", 
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    var form = $("#kt_selesai_produksi");
                    form.ajaxSubmit({
                        url: form.attr("action"),
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function(response, status, xhr, $form) {
                            console.log(response);
                            var data = jQuery.parseJSON(response);
                            if(data.success){
                                window.location.reload();
                            } else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });                        
                            }
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                        },error: function (xhr, ajaxOptions, thrownError) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });                    
                }
            });            
        })
        $("#kt_produksi_penerimaan_submit").click(function(){
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin ? \n Setelah Status menjadi diterima Produk akan otomatis ditambahkan ke lokasi yang di tuju",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87", 
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    var form = $("#kt_penerimaan_produksi");
                    form.validate({});
                    if (!form.valid()) {
                        return;
                    }
                    form.ajaxSubmit({
                        url: form.attr("action"),
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function(response, status, xhr, $form) {
                            console.log(response);
                            var data = jQuery.parseJSON(response);
                            if(data.success){
                                window.location.reload();
                            } else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });                        
                            }
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                        },error: function (xhr, ajaxOptions, thrownError) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });                    
                }
            });
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTProduksi.init();
});
document.addEventListener('DOMContentLoaded', () => {
	$('.input-numeral').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})

	});
	$('.input-decimal').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralDecimalScale: 4
		})

	});
});
