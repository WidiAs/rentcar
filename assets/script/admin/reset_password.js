"use strict";

// Class Definition
var KTLoginGeneral = function() {

    var login = $('#kt_reset');

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var handleFormSubmit = function() {
        $('#ket_reset_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    new_pass: {
                        required: true,
                        minlength : 5
                    },
                    re_pass: {
                        required: true,
                        minlength : 5,
                        equalTo : "#new_pass"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        showErrorMsg(form, 'success', data.message);
                        // similate 2s delay
                        setTimeout(function() {
                            window.location.href = baseUrl;
                        }, 1000);
                    } else {
                        showErrorMsg(form, 'danger', data.message);
                    }
                    
                	
                }
            });
        });
    }
    return {
        // public functions
        init: function() {
            handleFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLoginGeneral.init();
});