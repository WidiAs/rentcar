jQuery(document).ready(function () {
	select_penawaran();
	select_guest();
	select_mobil();
	mobil_add();
	simpan();
	tambah_biaya();
	select_driver();
	simpan_biaya();

	mobil_edit();
	tipe_sewa();
	harga();
	hari();
	modal_driver();
	input_numeral();
	hapus_mobil();
	biaya_tambahan();
	input_numeral_harga();
	tanggal_change();

	$('.tanggal').datepicker({
		rtl: KTUtil.isRTL(),
		todayHighlight: true,
		orientation: "bottom left",
		autoclose: true,
		format: 'dd-mm-yyyy',
	});

	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		});
	});
});
var noCount = 0;
var DriverNo = 0;
var date = $('#today').val();

function mobil_add() {
	$('#button_add_trans').on('click', function () {
		noCount = parseInt($(this).attr('data-count'));
		console.log(noCount);
	});
}

function tanggal_change() {
	$('.tanggal-mobil').on('change', function () {
		var no = $(this).attr('data-no');
		get_hari(no);
	});
}

function get_hari(no) {
	var tanggal_ambil = $('[name="tanggal_ambil['+no+']"]').val();
	var tanggal_selesai = $('[name="tanggal_selesai['+no+']"]').val();
	var date1 = new Date(tanggal_ambil);
	var date2 = new Date(tanggal_selesai);

	var Difference_In_Time = date2.getTime() - date1.getTime();

	var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	console.log('get hari');
	console.log(Difference_In_Days);
	var hari = 1;

	if (Difference_In_Days){
		hari = parseInt(Difference_In_Days) + 1;
	}

//To display the final no. of days (result)
	$('[name="hari['+no+']"]').val(hari);

	sub_total(no);
}

function mobil_edit() {
	$('.change-mobil').on('click', function () {
		noCount = parseInt($(this).attr('data-no'));
		console.log(noCount);
	});
}

function select_guest() {
	$("#guest_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$.each(object, function (key, value) {
			$('#guest-form [name="' + key + '"]').val(value);
		})
		$("#kt_modal_search_pelanggan").modal("hide");
	});
}

function select_mobil() {
	$("#mobil_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		var biaya_array = Array();
		if ($('#row_' + noCount)[0]) {
			$('[name = "tanggal_ambil[' + noCount + ']"]').val(date);
			$('[name = "mobil_id[' + noCount + ']"]').val(object.mobil_id);
			$('[name = "jenis_mobil_nama[' + noCount + ']"]').val(object.jenis_mobil_nama);
			$('[name = "driver_id[' + noCount + ']"]').val('');
			$('#driver_' + noCount).val('');
			$('#biaya_tambahan_' + noCount).html(0);
			$('#jenis_mobil_' + noCount).html(object.jenis_mobil_nama);
			$('#plat_' + noCount).val(object.plat_mobil);
			$('[name = "hari[' + noCount + ']"]').val(0);
			$('[name = "harga[' + noCount + ']"]').val(0);
			$('[name = "subtotal[' + noCount + ']"]').val(0);
			$('[name = "biaya_tambahan[' + noCount + ']"]').val(0);
			$('[name = "biaya_array[' + noCount + ']"]').val(JSON.stringify(biaya_array));
			$('[name = "jenis_sewa[' + noCount + ']"]').val('Lepas Kunci').trigger('change');

			sub_total(noCount);
			cek_tipe(noCount);
		} else {
			var text = '<tr id="row_' + noCount + '">';
			text += '<td style="display:none">' +
				'<input type="hidden" name="mobil_id[' + noCount + ']" value="' + object.mobil_id + '">' +
				'<input type="hidden" name="driver_id[' + noCount + ']" value="">' +
				'<input type="hidden" class="subtotal" name="subtotal[' + noCount + ']" value="">' +
				'<input type="hidden" name="jenis_mobil_nama[' + noCount + ']" value="' + object.jenis_mobil_nama + '">' +
				'<input type="hidden" name="driver_id[' + noCount + ']" value="">' +
				'<input type="hidden" name="biaya_tambahan[' + noCount + ']" value="0">' +
				'<input type="hidden" name="hari[' + noCount + ']" value="0">' +
				'<textarea type="hidden" name="biaya_array[' + noCount + ']" >'+JSON.stringify(biaya_array)+'</textarea>' +
				'</td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" id="plat_' + noCount + '" readonly="" value="' + object.plat_mobil + '" name="plat_mobil[' + noCount + ']">' +
				'<div class="input-group-append"><button class="btn btn-primary change-mobil" data-toggle="modal"' +
				'data-target="#kt_modal_search_mobil" type="button" data-no="' + noCount + '"><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="jenis_mobil_' + noCount + '">' + object.jenis_mobil_nama + '</span></td>';
			text += '<td><select class="form-control tipe-sewa" name="jenis_sewa[' + noCount + ']" data-no="' + noCount + '">' +
				'<option value="Lepas Kunci">Lepas Kunci</option>' +
				'<option value="All In">All In</option>' +
				'</select></td>';
			text += '<td><input type="datetime-local" class="form-control tanggal-mobil" name="tanggal_ambil[' + noCount + ']" value="" required data-no="'+noCount+'"></td>';
			text += '<td><input type="datetime-local" class="form-control tanggal-mobil" name="tanggal_selesai[' + noCount + ']" value="" required data-no="'+noCount+'"></td>';
			// text += '<td><input type="text" class="form-control input-numeral hari" value="0" name="hari[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><input type="text" class="form-control input-numeral-harga harga" value="0" name="harga[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" name="driver_nama[' + noCount + ']" id="driver_' + noCount + '" value="" readonly>' +
				'<div class="input-group-append"><button class="btn btn-primary driver-search" type="button" id="btn_driver_' + noCount + '" data-no="' + noCount + '" disabled><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="biaya_tambahan_' + noCount + '">0</span></td>';
			text += '<td><span id="subtotal_' + noCount + '">0</span></td>';
			text += '<td><textarea class="form-control" name="keterangan[' + noCount + ']"></textarea></td>';
			text += '<td>' +
				'<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group">' +
				'<button class="btn btn-warning biaya-btn" type="button" data-no="' + noCount + '"><i class="fas fa-dollar-sign"></i></button>' +
				'<button class="btn btn-danger delete-btn" data-no="' + noCount + '"><i class="flaticon2-trash"></i></button>' +
				'</div>' +
				'</td>';
			text += '</tr>';

			$('#item_child').append(text);
			noCount = parseInt(noCount) + 1;
			$('#button_add_trans').attr('data-count', noCount);

			// $('.tanggal-ambil').datetimepicker({
			// 	rtl: KTUtil.isRTL(),
			// 	todayHighlight: true,
			// 	orientation: "bottom left",
			// 	autoclose: true,
			// 	format: 'dd-mm-yyyy HH:mm',
			// });

			$('.input-numeral-harga').toArray().forEach(function (field) {
				new Cleave(field, {
					numeral: true,
					numeralThousandsGroupStyle: 'thousand'
				});
			});

			mobil_edit();
			tipe_sewa();
			harga();
			hari();
			modal_driver();
			input_numeral_harga();
			hapus_mobil();
			biaya_tambahan();
			tanggal_change();
		}


		$("#kt_modal_search_mobil").modal("hide");
	});
}

function select_penawaran() {
	$("#penawaran_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$('[name = "guest_id"]').val(object.pelanggan_id);
		$('[name = "guest_nama"]').val(object.pelanggan_nama);
		$('[name = "guest_alamat"]').val(object.pelanggan_alamat);
		$('[name = "guest_telepon"]').val(object.pelanggan_kontak);
		$('[name = "penawaran_id"]').val(object.penawaran_id);
		$('[name = "no_penawaran"]').val(object.no_penawaran);
		$('[name = "grand_total"]').val(object.grand_total);

		$('#item_child').html('');

		$.each(object.item, function (index, value) {
			var biaya = 0;
			var biaya_array = Array();

			if (value.driver_harga > 0){
				biaya = biaya + str_to_number(value.driver_harga);
				var baru = {
					nama_kegiatan: 'Driver',
					jumlah: value.driver_harga
				};

				biaya_array.push(baru);
			}
			if (value.bbm > 0){
				biaya = biaya + str_to_number(value.bbm);
				var baru = {
					nama_kegiatan: 'BBM',
					jumlah: value.bbm
				};

				biaya_array.push(baru);
			}
			var text = '<tr id="row_' + index + '">';
			text += '<td style="display:none">' +
				'<input type="hidden" name="mobil_id[' + index + ']" value="' + value.mobil_id + '">' +
				'<input type="hidden" name="driver_id[' + index + ']" value="">' +
				'<input type="hidden" class="subtotal" name="subtotal[' + index + ']" value="'+value.sub_total+'">' +
				'<input type="hidden" name="jenis_mobil_nama[' + index + ']" value="' + value.jenis_mobil_nama + '">' +
				'<input type="hidden" name="driver_id[' + index + ']" value="">' +
				'<input type="hidden" name="biaya_tambahan[' + index + ']" value="' + biaya + '">' +
				'<textarea type="hidden" name="biaya_array[' + index + ']" >'+JSON.stringify(biaya_array)+'</textarea>' +
				'</td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" id="plat_' + index + '" readonly="" value="' + value.plat_mobil + '" name="plat_mobil[' + index + ']">' +
				'<div class="input-group-append"><button class="btn btn-primary change-mobil" data-toggle="modal"' +
				'data-target="#kt_modal_search_mobil" type="button" data-no="' + index + '"><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="jenis_mobil_' + index + '">' + value.jenis_mobil_nama + '</span></td>';
			text += '<td><select class="form-control tipe-sewa" name="jenis_sewa[' + index + ']" data-no="' + index + '">' +
				'<option value="Lepas Kunci" '+ ((value.jenis == 'Lepas Kunci') ? 'selected' : '') +'>Lepas Kunci</option>' +
				'<option value="All In" '+ ((value.jenis == 'All In') ? 'selected' : '') +'>All In</option>' +
				'</select></td>';
			text += '<td><input type="datetime-local" class="form-control tanggal-ambil" name="tanggal_ambil[' + index + ']" value="" required></td>';
			text += '<td><input type="text" class="form-control input-numeral hari" value="1" name="hari[' + index + ']" data-no="' + index + '"></td>';
			text += '<td><input type="text" class="form-control input-numeral harga" value="'+format_number(value.harga)+'" name="harga[' + index + ']" data-no="' + index + '"></td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" name="driver_nama[' + index + ']" id="driver_' + index + '" value="" '+ ((value.jenis == 'Lepas Kunci') ? 'readonly' : '') +'>' +
				'<div class="input-group-append"><button class="btn btn-primary driver-search" type="button" id="btn_driver_' + index + '" data-no="' + index + '" '+ ((value.jenis == 'Lepas Kunci') ? 'disabled' : '') +'><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="biaya_tambahan_' + index + '">'+format_number(biaya)+'</span></td>';
			text += '<td><span id="subtotal_' + index + '">'+format_number(value.sub_total)+'</span></td>';
			text += '<td><textarea class="form-control" name="keterangan[' + index + ']"></textarea></td>';
			// text += '<td><button class="btn btn-danger delete-btn" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>';
			text += '<td>' +
				'<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group">' +
				// '<button class="btn btn-success btn-kegiatan" data-no="' + index + '" style="display:none"><i class="la la-key"></i></button>' +
				'<button class="btn btn-warning biaya-btn" type="button" data-no="' + index + '"><i class="fas fa-dollar-sign"></i></button>' +
				'<button class="btn btn-danger delete-btn" data-no="' + index + '"><i class="flaticon2-trash"></i></button>' +
				'</div>' +
				'</td>';
			text += '</tr>';

			$('#item_child').append(text);
			noCount = parseInt(index) + 1;
			$('#button_add_trans').attr('data-count', noCount);

			$('.tanggal').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'dd-mm-yyyy hh-mm-ss',
			});

			$('.input-numeral').toArray().forEach(function (field) {
				new Cleave(field, {
					numeral: true,
					numeralThousandsGroupStyle: 'thousand'
				});
			});
		});

		mobil_edit();
		tipe_sewa();
		harga();
		hari();
		input_numeral();
		hapus_mobil();
		biaya_tambahan();
		modal_driver();
		grand_total();
		$("#kt_modal_search_penawaran").modal("hide");


		// if ($('#row_' + noCount)[0]) {
		//
		//
		// } else {
		// 	var text = '<tr id="row_' + noCount + '">';
		// 	text += '<td style="display:none">' +
		// 		'<input type="hidden" name="mobil_id[' + noCount + ']" value="' + object.mobil_id + '">' +
		// 		'<input type="hidden" name="driver_id[' + noCount + ']" value="">' +
		// 		'<input type="hidden" class="subtotal" name="subtotal[' + noCount + ']" value="">' +
		// 		'<input type="hidden" name="jenis_mobil_nama[' + noCount + ']" value="' + object.jenis_mobil_nama + '">' +
		// 		'</td>';
		// 	text += '<td><div class="input-group col-12">' +
		// 		'<input type="text" class="form-control" id="plat_' + noCount + '" readonly="" value="' + object.plat_mobil + '" name="plat_mobil[' + noCount + ']">' +
		// 		'<div class="input-group-append"><button class="btn btn-primary change-mobil" data-toggle="modal"' +
		// 		'data-target="#kt_modal_search_mobil" type="button" data-no="' + noCount + '"><i class="flaticon-search"></i></button></div>' +
		// 		'</div></td>';
		// 	text += '<td><span id="jenis_mobil_' + noCount + '">' + object.jenis_mobil_nama + '</span></td>';
		// 	text += '<td><select class="form-control tipe-sewa" name="jenis_sewa[' + noCount + ']" data-no="' + noCount + '">' +
		// 		'<option value="Lepas Kunci">Lepas Kunci</option>' +
		// 		'<option value="All In">All In</option>' +
		// 		'</select></td>';
		// 	text += '<td><input type="text" class="form-control tanggal" name="tanggal_ambil[' + noCount + ']" value="' + date + '" readonly></td>';
		// 	text += '<td><input type="text" class="form-control input-numeral hari" value="0" name="hari[' + noCount + ']" data-no="' + noCount + '"></td>';
		// 	text += '<td><input type="text" class="form-control input-numeral harga" value="0" name="harga[' + noCount + ']" data-no="' + noCount + '"></td>';
		// 	text += '<td><div class="input-group col-12">' +
		// 		'<input type="text" class="form-control" name="driver_nama[' + noCount + ']" id="driver_' + noCount + '" value="" readonly>' +
		// 		'<div class="input-group-append"><button class="btn btn-primary driver-search" type="button" id="btn_driver_' + noCount + '" data-no="' + noCount + '" disabled><i class="flaticon-search"></i></button></div>' +
		// 		'</div></td>';
		// 	text += '<td><span id="subtotal_' + noCount + '">0</span></td>';
		// 	text += '<td><button class="btn btn-danger delete-btn" data-no="' + noCount + '"><i class="flaticon2-trash"></i></button></td>';
		// 	text += '</tr>';
		//
		// 	$('#item_child').append(text);
		// 	noCount = parseInt(noCount) + 1;
		// 	$('#button_add_trans').attr('data-count', noCount);
		//
		// 	$('.tanggal').datepicker({
		// 		rtl: KTUtil.isRTL(),
		// 		todayHighlight: true,
		// 		orientation: "bottom left",
		// 		autoclose: true,
		// 		format: 'dd-mm-yyyy',
		// 	});
		//
		// 	$('.input-numeral').toArray().forEach(function (field) {
		// 		new Cleave(field, {
		// 			numeral: true,
		// 			numeralThousandsGroupStyle: 'thousand'
		// 		});
		// 	});
		//
		// 	mobil_edit();
		// 	harga();
		// 	hari();
		// 	tipe_sewa();
		// 	modal_driver();
		// 	select_driver();
		// }


		$("#kt_modal_search_mobil").modal("hide");
	});
}

function modal_driver() {
	$('.driver-search').on('click', function () {
		DriverNo = $(this).attr('data-no');

		$('#kt_modal_search_driver').modal('show');
	});
}


function select_driver() {
	$("#driver_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$('[name="driver_id[' + DriverNo + ']"]').val(object.user_id);
		$('[name="driver_nama[' + DriverNo + ']"]').val(object.staff_nama).attr('readonly', true);
		$("#kt_modal_search_driver").modal("hide");
	});
}

function hapus_mobil() {
	$('#item_child').on('click', '.delete-btn', function () {
		var no = $(this).data("no");
		$("#row_" + no).remove();
	});
}

function biaya_tambahan() {
	$('#item_child').on('click', '.biaya-btn', function () {
		var no = $(this).attr('data-no');
		var biaya = $('[name="biaya_array[' + no + ']"]').val();
		$('#biaya-list').html('');
		$('#nomor-biaya').val(no);

		var count = 0;
		var total = 0;
		if (biaya) {
			biaya = JSON.parse(biaya);
			$.each(biaya, function (index, value) {
				count = parseInt(count) + 1;
				total = total + parseFloat(value.jumlah);
				var list = '<tr id="row_biaya_' + index + '">' +
					'<td style="display:none">' +
					'<input type="hidden" class="row_biaya" value="'+index+'">' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="nama_kegiatan[' + index + ']" class="form-control" value="'+value.nama_kegiatan+'" required="">' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="jumlah[' + index + ']" class="form-control input-numeral jumlah-biaya" value="'+value.jumlah+'" required="">' +
					'</td>'+
					'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
					'</tr>';
				$('#biaya-list').append(list);
			})
		}

		$('#biaya-count').val(count);
		$('#total-biaya').html(format_number(total));

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});

		delete_biaya();
		jumlah_biaya();

		$('#modal-biaya').modal('show');
	});
}

function tambah_biaya() {
	$('#tambah-biaya-list').on('click', function () {
		var count = $('#biaya-count').val();
		var index = parseInt(count) + 1;
		var list = '<tr id="row_biaya_' + index + '"> ' +
			'<td style="display:none">' +
			'<input type="hidden" class="row_biaya" value="'+index+'">' +
			'</td>' +
			'<td>' +
			'<input type="text" placeholder="" name="nama_kegiatan[' + index + ']" class="form-control" value="" required="">' +
			'</td>' +

			'<td>' +
			'<input type="text" placeholder="" name="jumlah[' + index + ']" class="form-control input-numeral jumlah-biaya" value="0" required="">' +
			'</td>' +
			'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
			'</tr>';
		$('#biaya-list').append(list);
		$('#biaya-count').val(index);

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});


		delete_biaya();
		jumlah_biaya();
	})
}

function harga() {
	$('.harga').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function hari() {
	$('.hari').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function sub_total(no) {
	var harga = str_to_number($('[name="harga[' + no + ']"]').val());
	var hari = str_to_number($('[name="hari[' + no + ']"]').val());
	var biaya_tambahan = str_to_number($('[name="biaya_tambahan[' + no + ']"]').val());

	var total = (harga + biaya_tambahan) * hari;

	$('[name="subtotal[' + no + ']"]').val(total);
	$('#subtotal_' + no).html(format_number(total));

	grand_total();
}

function grand_total() {
	var total = 0;
	$('.subtotal').each(function () {

		total += str_to_number($(this).val());

	});
	$('[name="grand_total"]').val(total);
	$('#total-item').html(format_number(total));
}

function tipe_sewa() {
	$('.tipe-sewa').on('change', function () {
		var no = $(this).attr('data-no');
		cek_tipe(no);
	});
}

function cek_tipe(no) {
	var tipe = $('[name="jenis_sewa[' + no + ']"]').val();

	if (tipe == 'Lepas Kunci') {
		$('#driver_' + no).val(null).attr('readonly', true);
		$('#btn_driver_' + no).attr('disabled', true);
	} else {
		$('#driver_' + no).val(null).attr('readonly', false);
		$('#btn_driver_' + no).attr('disabled', false);
	}

	console.log('tipe')
}

function modal_driver() {
	$('.driver-search').on('click', function () {
		DriverNo = $(this).attr('data-no');
		console.log(DriverNo)

		$('#kt_modal_search_driver').modal('show');
	});
}


function select_driver() {
	$("#driver_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$('[name="driver_id[' + DriverNo + ']"]').val(object.user_id);
		console.log(object.user_id)
		$('[name="driver_nama[' + DriverNo + ']"]').val(object.staff_nama).attr('readonly', true);
		$("#kt_modal_search_driver").modal("hide");
	});
}

function simpan() {

	$("#btn-save").click(function () {
		var btn = $(this);
		var form = $("#save-form");
		var home = $("#home-url").val();
		var grand_total = $('[name="grand_total"]').val();
		var jenis_sewa = $('[name="jenis_penyewaan"]').val();
		if (jenis_sewa == 'Endors' && grand_total == 0){
			grand_total = 1;
		}
		var tanggal = true;
		$('.tanggal-mobil').each(function () {

			if ($(this).val() == ''){
				tanggal = false;
			}

		});
		var enough = true;
		if (grand_total > 0){
			if (tanggal == true){

				btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
				form.ajaxSubmit({
					url: form.attr("action"),
					beforeSend: function () {
						$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
					},
					success: function (response, status, xhr, $form) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						var data = jQuery.parseJSON(response);
						btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
						if (data.success) {
							console.log(data)
							$("#kt_modal_pembayaran").modal("hide");
							Swal.fire({
								type: 'success',
								title: 'Transaksi Berhasil',
							}).then(function () {
								form.clearForm();
								form.validate().resetForm();
								if (data.url != undefined){
									var url = data.url;
									window.open(url, "_blank");
								}
								// location.reload();
								window.location.href = home;
							});
						} else {
							console.log(data);
							swal.fire({
								type: 'error',
								title: 'Peringatan',
								text: data.message,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}, error: function (xhr, ajaxOptions, thrownError) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}else{
				swal.fire({
					type: 'error',
					title: 'Peringatan',
					text: 'Tanggal Ambil masih kosong',
					showConfirmButton: false,
					timer: 1500
				});
			}


		}else{
			swal.fire({
				type: 'error',
				title: 'Peringatan',
				text: 'Nilai Transaksi Tidak Boleh 0',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})
}

function delete_biaya() {
	$('#biaya-list').on('click', '.delete-biaya', function () {
		var no = $(this).data("no");
		$("#row_biaya_" + no).remove();
		total_biaya();
	});
}

function jumlah_biaya() {

	$('.jumlah-biaya').on('change keyup', function () {
		total_biaya();
	})
}

function total_biaya() {
	var total = 0;
	$('.jumlah-biaya').each(function () {

		total += str_to_number($(this).val());

	});
	$('#total-biaya').html(format_number(total));
}

function simpan_biaya() {
	$('#simpan-biaya').on('click', function () {
		var biaya_array = Array();
		var no = $('#nomor-biaya').val();
		var total = 0;
		$('.row_biaya').each(function () {
			var index = $(this).val();
			var nama_kegiatan = $('[name="nama_kegiatan['+index+']"]').val();
			var jumlah = str_to_number($('[name="jumlah['+index+']"]').val());
			total = total + jumlah;

			if (nama_kegiatan && jumlah > 0){
				var baru = {
					nama_kegiatan : nama_kegiatan,
					jumlah : jumlah
				};

				biaya_array.push(baru);
			}
		});
		$('#biaya_tambahan_'+no).html(format_number(total));
		$('[name="biaya_array[' + no + ']"]').val(JSON.stringify(biaya_array));
		$('[name="biaya_tambahan[' + no + ']"]').val(total);
		$('#modal-biaya').modal('hide');
		sub_total(no)
	})
}
