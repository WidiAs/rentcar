"use strict";

// Class Definition
var KTProduksi = function () {
	var general = function () {
		var no = 0;
		var no_bahan = 0;
		var bahan_no = null;
		var base_url = $("#base_url").val();
		if ($("#no_produk").length > 0) {
			no = $("#no_produk").val() + 1;
		}
		if ($("#no_bahan").length > 0) {
			no_bahan = intVal($("#no_bahan").val() + 1);
		}
		$('.input-numeral').keyup(function () {
			if ($(this).val() == "") {
				$(this).val(0);
			}
		})
		$('.tanggal').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			autoclose: true,
			format: 'yyyy-mm-dd',
		});

		$(".lokasi-bahan").change(function () {
			$(".bahan-container").html('');
			if ($(this).val() != "") {
				sess_lokasi($(this).val());
			}
		})
		$(".add-bahan").click(function () {
			if ($(".lokasi-bahan").val() == "") {
				swal.fire({
					type: 'error',
					text: 'Lokasi bahan harus diisi sebelum menambahkan bahan',
					showConfirmButton: false,
					timer: 1500
				});
			} else {
				var no = $(this).data('no');
				var index_bahan = $(this).data('bahan-container');

				var text = '<div class="row" id="bahan_item_' + no_bahan + '">' +
					'<div class="col-md-5">' +
					'<div class="form-group row">' +
					'<label for="example-text-input" class="col-3 col-form-label">Bahan <b class="label--required">*</b></label>' +
					'<div class="col-9">' +
					'<div class="input-group col-12">' +
					'<input type="text" class="form-control readonly" name="item_bahan_nama_' + no_bahan + '" id="item_bahan_nama_' + no_bahan + '"  required="" autocomplete="off">' +
					'<input type="hidden" class="form-control" id="item_bahan_id_' + no_bahan + '" name="' + 'item' + "[bahan_" + no_bahan + "][bahan_id]" + '" >' +
					'<div class="input-group-append">' +
					'<button class="btn btn-primary bahan-search" type="button" data-toggle="modal" data-no="' + no_bahan + '"><i class="flaticon-search"></i></button>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<div class="col-md-5">' +
					'<div class="form-group row">' +
					'<label for="example-text-input" class="col-3 col-form-label">Jumlah <b class="label--required">*</b></label>' +
					'<div class="col-6">' +
					'<input type="text" class="form-control input-numeral jumlah-bahan" autocomplete="off" data-no="' + no_bahan + '" " value="0" id="item_bahan_jumlah_' + no_bahan + '" name="' + 'item' + "[bahan_" + no_bahan + "][jumlah]" + '">' +
					'</div>' +
					'<label class="col-3 col-form-label" id="item_bahan_satuan_' + no_bahan + '"></label>' +
					'</div>' +
					'</div>' +
					'<div class="col-md-2"><button type="button" class="btn btn-danger btn-sm delete-bahan" data-no="' + no_bahan + '" data-item-bahan="bahan_item_' + no_bahan + '" ><i class="flaticon2-trash"></i></button></div>'
				'</div>';
				$("#bahan_container").append(text);
				new Cleave($("#item_bahan_jumlah_" + no_bahan), {
					numeral: true,
					numeralDecimalScale: 4
				})
				no_bahan++;
			}
		})
		$("#bahan_container").on('click', '.delete-bahan', function () {
			var index_bahan = $(this).data('item-bahan');
			$("#" + index_bahan).remove();
			var tempIndex = $(this).data('no');
			sess_delete_bahan(tempIndex);
		})
		$('#item-container').on('keydown', '.readonly', function (e) {
			e.preventDefault();
		});
		$('#bahan_container').on('click', '.bahan-search', function () {
			bahan_no = $(this).data('no');
			$("#kt_modal_bahan").modal("show");
		});
		$('#bahan_container').on('keyup', '.jumlah-bahan', function () {
			var tempIndex = $(this).data('no');
			var value = intVal($(this).val());
			sess_change_bahan(tempIndex, value);
		});
		$('#bahan_child').on('click', '.chose-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$("#item_bahan_nama_" + bahan_no).val(object.bahan_nama);
			if ($("#bahan_id_" + bahan_no).val() != object.bahan_id) {
				sess_add_bahan(bahan_no, object.bahan_id)
				$("#item_bahan_jumlah_" + bahan_no).val("0");
			}
			$("#item_bahan_id_" + bahan_no).val(object.bahan_id);
			$("#item_bahan_satuan_" + bahan_no).html(object.satuan_nama);
			$("#kt_modal_bahan").modal("hide");
		});
		$('#child_data_ajax').on('click', '.view-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			var id = object.produk_custom_id;
			console.log('object');
			console.log(object);
			console.log(id);
			get_detail(id)
		});
		$('#child_data_ajax').on('click', '.produksi-status-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			var id = object.produk_custom_id;
			$("#produksi_id").val(id);
			$("#lokasi_bahan_id").val(object.lokasi_bahan_id);
			$("#kt_status_produksi").modal("show");
		});
		$('#child_data_ajax').on('click', '.penerimaan-status-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			var id = object.produk_custom_id;
			$("#produksi_id_penerimaan").val(id);
			get_detail(id, 1);
		});

		function display_penerimaan(response) {
			var object = JSON.parse(response);
			$("#hpp_container").html("");
			$.each(object.item, function (key, value) {
				var text = '<div class="form-group row">' +
					'<label for="example-text-input" class="col-3 col-form-label">HPP ' + value.produk_nama + '</label>' +
					'<label for="example-text-input" class="col-1 col-form-label">:</label>' +
					'<div class="col-8">' +
					// '<input type="hidden" name="jumlah[produk_'+value.produk_id+']" value="'+value.jumlah+'" />'+
					'<input type="text" class="form-control kt-input input-numeral" id="hpp_' + value.produk_id + '" name="hpp[produk_' + value.produk_id + ']" autocomplete="off" data-col-index="5" value="0" required="" />' +
					'</div>' +
					'</div>';
				// $("#hpp_container").append(text);
				// new Cleave($("#hpp_" + value.produk_id), {
				// 	numeral: true,
				// 	numeralThousandsGroupStyle: 'thousand'
				// })

			})
			$("#kt_penerimaan").modal("show");
		}

		function get_detail(id, type = 0) {
			$.ajax({
				type: "POST",
				url: base_url + 'custom-produksi/detail',
				data: {'produksi_id': id},
				cache: false,
				success: function (response) {
					if (type == 0) {
						console.log(response)
						parseDetail(response);
					} else {
						display_penerimaan(response)
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}

		function parseDetail(response) {
			var object = JSON.parse(response);
			var data_key;
			$.each(object, function (key, value) {
				$('#kt_modal_detail_produksi label[name="' + key + '"]').html(value);
				$('#kt_modal_detail_produksi [name="' + key + '"]').val(value);
				var field = $('#kt_modal_detail_produksi .img-preview').data("field");
				if (field == key) {
					$('#kt_modal_detail_produksi .img-preview').attr("src", base_url + value);
				}
			})
			console.log('key');
			console.log(object.item);
			$("#view_child_data").html("");

			var text = '';
			$.each(object.item, function (key, value) {
				text += '<td>' + value.bahan_nama + '</td><td>' + value.jumlah + '</td></tr>';

			})

			$("#view_child_data").append(text);
			$("#kt_modal_detail_produksi").modal("show");
		}

		function sess_add_bahan(key, bahan_id) {
			$.ajax({
				type: "POST",
				url: base_url + 'custom-produksi/utility/sess-bahan-add',
				data: {'key': key, 'bahan_id': bahan_id},
				cache: false,
				success: function (response) {
					bahan_table.ajax.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}

		function sess_change_bahan(key, jumlah) {
			$.ajax({
				type: "POST",
				url: base_url + 'custom-produksi/utility/sess-bahan-change',
				data: {'key': key, 'jumlah': jumlah},
				cache: false,
				success: function (response) {
					bahan_table.ajax.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}

		function sess_delete_bahan(key) {
			$.ajax({
				type: "POST",
				url: base_url + 'custom-produksi/utility/sess-bahan-delete',
				data: {'key': key},
				cache: false,
				success: function (response) {
					bahan_table.ajax.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}

		function sess_lokasi(key) {
			$.ajax({
				type: "POST",
				url: base_url + 'custom-produksi/utility/sess-lokasi',
				data: {'lokasi_id': key},
				cache: false,
				success: function (response) {
					bahan_table.ajax.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}

		function intVal(i) {
			return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
		};

		var url_produk = $("#list_bahan").val();
		var bahan_table = $("#bahan-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: url_produk,
			columns: [{data: 'bahan_kode'}, {data: 'bahan_nama'}, {data: 'jumlah_lokasi'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
		$("#kt_add_submit").click(function (e) {
			e.preventDefault();
			var btn = $(this);
			var form = $(this).closest('form');
			var theres_no_zero = true;
			var no_empty_bahan = true;
			if ($("#bahan_container").html() == "") {
				no_empty_bahan = false;
			}
			$("#bahan_container .input-numeral").each(function () {
				if ($(this).val() == "0") {
					theres_no_zero = false;
				}
			});
			if (!theres_no_zero) {
				swal.fire({
					type: 'error',
					text: 'Tidak boleh ada jumlah yang bernilai 0',
					showConfirmButton: false,
					timer: 1500
				});
				return;
			}
			if (!no_empty_bahan) {
				swal.fire({
					type: 'error',
					text: 'Setiap produk harus memiliki bahan',
					showConfirmButton: false,
					timer: 1500
				});
				return;
			}
			form.validate({});
			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.success) {
						window.location.href = base_url + "custom-produksi";
					} else {
						swal.fire({
							type: 'error',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
					$('.wrapper-loading').fadeOut().addClass('hidden');
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});

		})
		$("#kt_produksi_status_submit").click(function () {
			swal.fire({
				title: "Perhatian ...",
				text: "Apakah anda yakin ? \nMengubah status menjadi selesai akan mengurangi stok bahan dan data ini tidak dapat dirubah maupun dihapus dikemudian hari",
				type: "warning",
				showCancelButton: !0,
				confirmButtonColor: "#0abb87",
				confirmButtonText: "Ya, yakin",
				cancelButtonText: "Batal",
			}).then(function (e) {
				if (e.value) {
					var form = $("#kt_selesai_produksi");
					form.ajaxSubmit({
						url: form.attr("action"),
						beforeSend: function () {
							$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
						},
						success: function (response, status, xhr, $form) {
							console.log(response);
							var data = jQuery.parseJSON(response);
							if (data.success) {
								window.location.reload();
							} else {
								swal.fire({
									type: 'error',
									text: data.message,
									showConfirmButton: false,
									timer: 1500
								});
							}
							$('.wrapper-loading').fadeOut().addClass('hidden');
						}, error: function (xhr, ajaxOptions, thrownError) {
							$('.wrapper-loading').fadeOut().addClass('hidden');
							console.log(xhr.status);
							console.log(xhr.responseText);
							console.log(thrownError);
						}
					});
				}
			});
		})
		$("#kt_produksi_penerimaan_submit").click(function () {
			swal.fire({
				title: "Perhatian ...",
				text: "Apakah anda yakin ? \n Setelah Status menjadi diterima Produk akan otomatis ditambahkan ke lokasi yang di tuju",
				type: "warning",
				showCancelButton: !0,
				confirmButtonColor: "#0abb87",
				confirmButtonText: "Ya, yakin",
				cancelButtonText: "Batal",
			}).then(function (e) {
				if (e.value) {
					var form = $("#kt_penerimaan_produksi");
					form.validate({});
					if (!form.valid()) {
						return;
					}
					form.ajaxSubmit({
						url: form.attr("action"),
						beforeSend: function () {
							$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
						},
						success: function (response, status, xhr, $form) {
							console.log(response);
							var data = jQuery.parseJSON(response);
							if (data.success) {
								window.location.reload();
							} else {
								swal.fire({
									type: 'error',
									text: data.message,
									showConfirmButton: false,
									timer: 1500
								});
							}
							$('.wrapper-loading').fadeOut().addClass('hidden');
						}, error: function (xhr, ajaxOptions, thrownError) {
							$('.wrapper-loading').fadeOut().addClass('hidden');
							console.log(xhr.status);
							console.log(xhr.responseText);
							console.log(thrownError);
						}
					});
				}
			});
		})
	}
	// Public Functions
	return {
		// public functions
		init: function () {
			general();
		}
	};
}();

// Class Initialization
jQuery(document).ready(function () {
	KTProduksi.init();
});
