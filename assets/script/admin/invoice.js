jQuery(document).ready(function () {
	select_mobil();
	mobil_add();
	mobil_edit();
	simpan();
	tambah_biaya();
	select_driver();
	simpan_biaya();
	tipe_sewa();
	harga();
	hari();
	modal_driver();
	input_numeral();
	hapus_mobil();
	biaya_tambahan();
	modal_jadwal();
	tambah_jadwal();
	simpan_jadwal();

	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		});
	});
});
var noCount = 0;
var DriverNo = 0;
var date = $('#today').val();

function mobil_add() {
	$('#button_add_trans').on('click', function () {
		noCount = parseInt($(this).attr('data-count'));
		console.log(noCount);
	});
}

function mobil_edit() {
	$('.change-mobil').on('click', function () {
		noCount = parseInt($(this).attr('data-no'));
		console.log(noCount);
	});
}

function select_mobil() {
	$("#mobil_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		var biaya_array = Array();
		if ($('#row_' + noCount)[0]) {
			$('#driver_' + noCount).val('');
			$('#biaya_tambahan_' + noCount).html(0);
			$('#jenis_mobil_' + noCount).val(object.mobil_jenis_nama);
			$('#plat_' + noCount).val(object.plat_mobil);

			$('[name = "mobil_id[' + noCount + ']"]').val(object.mobil_id);
			$('[name = "driver_id[' + noCount + ']"]').val('');
			$('[name = "hari[' + noCount + ']"]').val(0);
			$('[name = "hari_luar_kota[' + noCount + ']"]').val(0);
			$('[name = "total_jadwal[' + noCount + ']"]').val(0);
			$('[name = "total_overtime[' + noCount + ']"]').val(0);
			$('[name = "subtotal[' + noCount + ']"]').val(0);
			$('[name = "jenis_mobil_nama[' + noCount + ']"]').val(object.jenis_mobil_nama);
			$('[name = "biaya_tambahan[' + noCount + ']"]').val(0);
			$('[name = "biaya_array[' + noCount + ']"]').val(JSON.stringify(biaya_array));
			$('[name = "jadwal_mobil[' + noCount + ']"]').val(JSON.stringify(Array()));

			$('[name = "harga[' + noCount + ']"]').val(0);
			$('[name = "harga_luar_kota[' + noCount + ']"]').val(0);
			$('[name = "jenis_sewa[' + noCount + ']"]').val('Lepas Kunci').trigger('change');
			$('[name = "tanggal_ambil[' + noCount + ']"]').val(date);

			sub_total(noCount);
			cek_tipe(noCount);
		} else {
			var text = '<tr id="row_' + noCount + '">';
			text += '<td style="display:none">' +
				'<input type="hidden" name="mobil_id[' + noCount + ']" value="' + object.mobil_id + '">' +
				'<input type="hidden" name="driver_id[' + noCount + ']" value="">' +
				'<input type="hidden" class="hari" name="hari[' + noCount + ']" value="0">' +
				'<input type="hidden" name="hari_luar_kota[' + noCount + ']" value="0">' +
				'<input type="hidden" name="total_jadwal[' + noCount + ']" value="0">' +
				'<input type="hidden" name="total_overtime[' + noCount + ']" value="0">' +
				'<input type="hidden" class="subtotal" name="subtotal[' + noCount + ']" value="">' +
				'<input type="hidden" name="jenis_mobil_nama[' + noCount + ']" value="' + object.jenis_mobil_nama + '">' +
				'<input type="hidden" name="biaya_tambahan[' + noCount + ']" value="0">' +
				'<input type="hidden" name="biaya_array[' + noCount + ']" value="[]">' +
				'<input type="hidden" name="jadwal_mobil[' + noCount + ']" value="[]">' +
				'</td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" id="plat_' + noCount + '" readonly="" value="' + object.plat_mobil + '" name="plat_mobil[' + noCount + ']">' +
				'<div class="input-group-append"><button class="btn btn-primary change-mobil" data-toggle="modal"' +
				'data-target="#kt_modal_search_mobil" type="button" data-no="' + noCount + '"><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="jenis_mobil_' + noCount + '">' + object.jenis_mobil_nama + '</span></td>';
			text += '<td><select class="form-control tipe-sewa" name="jenis_sewa[' + noCount + ']" data-no="' + noCount + '">' +
				'<option value="Lepas Kunci">Lepas Kunci</option>' +
				'<option value="All In">All In</option>' +
				'</select></td>';
			text += '<td><input type="text" class="form-control tanggal" name="tanggal_ambil[' + noCount + ']" value="' + date + '" readonly></td>';
			text += '<td><span id="hari-label-' + noCount + '">0</span></td>';
			text += '<td><input type="text" class="form-control input-numeral harga" value="0" name="harga[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><input type="text" class="form-control input-numeral harga-luar-kota" value="0" name="harga_luar_kota[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" name="driver_nama[' + noCount + ']" id="driver_' + noCount + '" value="" readonly>' +
				'<div class="input-group-append"><button class="btn btn-primary driver-search" type="button" id="btn_driver_' + noCount + '" data-no="' + noCount + '" disabled><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="biaya_tambahan_' + noCount + '">0</span></td>';
			text += '<td><span id="subtotal_' + noCount + '">0</span></td>';
			text += '<td><textarea class="form-control" name="keterangan[' + noCount + ']"></textarea></td>';
			text += '<td>' +
				'<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group">' +
				'<button class="btn btn-success jadwal-mobil" type="button" data-no="' + noCount + '"><i class="fa fa-calendar-check"></i></button>' +
				'<button class="btn btn-warning biaya-btn" type="button" data-no="' + noCount + '"><i class="fas fa-dollar-sign"></i></button>' +
				'<button class="btn btn-danger delete-btn" data-no="' + noCount + '"><i class="flaticon2-trash"></i></button>' +
				'</div>' +
				'</td>';
			text += '</tr>';

			$('#item_child').append(text);
			noCount = parseInt(noCount) + 1;
			$('#button_add_trans').attr('data-count', noCount);

			$('.tanggal').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'dd-mm-yyyy',
			});

			$('.input-numeral').toArray().forEach(function (field) {
				new Cleave(field, {
					numeral: true,
					numeralThousandsGroupStyle: 'thousand'
				});
			});

			mobil_edit();
			tipe_sewa();
			harga();
			hari();
			modal_driver();
			input_numeral();
			hapus_mobil();
			biaya_tambahan();
			modal_jadwal();
		}


		$("#kt_modal_search_mobil").modal("hide");
	});
}

function hapus_mobil() {
	$('#item_child').on('click', '.delete-btn', function () {
		var no = $(this).data("no");
		$("#row_" + no).remove();
	});
}

function biaya_tambahan() {
	$('#item_child').on('click', '.biaya-btn', function () {
		var no = $(this).attr('data-no');
		var biaya = $('[name="biaya_array[' + no + ']"]').val();
		$('#biaya-list').html('');
		$('#nomor-biaya').val(no);

		var count = 0;
		var total = 0;
		if (biaya) {
			biaya = JSON.parse(biaya);
			$.each(biaya, function (index, value) {
				count = parseInt(count) + 1;
				total = total + parseFloat(value.jumlah);
				var list = '<tr id="row_biaya_' + index + '">' +
					'<td style="display:none">' +
					'<input type="hidden" class="row_biaya" value="' + index + '">' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="nama_kegiatan[' + index + ']" class="form-control" value="' + value.nama_kegiatan + '" required="">' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="jumlah[' + index + ']" class="form-control input-numeral jumlah-biaya" value="' + value.jumlah + '" required="">' +
					'</td>' +
					'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
					'</tr>';
				$('#biaya-list').append(list);
			})
		}

		$('#biaya-count').val(count);
		$('#total-biaya').html(format_number(total));

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});

		delete_biaya();
		jumlah_biaya();

		$('#modal-biaya').modal('show');
	});
}

function tambah_biaya() {
	$('#tambah-biaya-list').on('click', function () {
		var count = $('#biaya-count').val();
		var index = parseInt(count) + 1;
		var list = '<tr id="row_biaya_' + index + '"> ' +
			'<td style="display:none">' +
			'<input type="hidden" class="row_biaya" value="' + index + '">' +
			'</td>' +
			'<td>' +
			'<input type="text" placeholder="" name="nama_kegiatan[' + index + ']" class="form-control" value="" required="">' +
			'</td>' +

			'<td>' +
			'<input type="text" placeholder="" name="jumlah[' + index + ']" class="form-control input-numeral jumlah-biaya" value="0" required="">' +
			'</td>' +
			'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
			'</tr>';
		$('#biaya-list').append(list);

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});


		delete_biaya();
		jumlah_biaya();
	})
}

function harga() {
	$('.harga').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function hari() {
	$('.hari').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function sub_total(no) {
	var hari = str_to_number($('[name="hari[' + no + ']"]').val());
	var hari_luar_kota = str_to_number($('[name="hari_luar_kota[' + no + ']"]').val());
	var harga = str_to_number($('[name="harga[' + no + ']"]').val());
	var harga_luar_kota = str_to_number($('[name="harga_luar_kota[' + no + ']"]').val());
	var total_overtime = str_to_number($('[name="total_overtime[' + no + ']"]').val());
	var biaya_tambahan = str_to_number($('[name="biaya_tambahan[' + no + ']"]').val()) * (hari + hari_luar_kota);
	var total_jadwal = (hari * harga) + (hari_luar_kota * harga_luar_kota);

	var total = total_jadwal + biaya_tambahan + total_overtime;

	$('[name="subtotal[' + no + ']"]').val(total);
	$('#subtotal_' + no).html(format_number(total));

	grand_total();
}

function grand_total() {
	var total = 0;
	$('.subtotal').each(function () {

		total += str_to_number($(this).val());

	});
	$('[name="grand_total"]').val(total);
	$('#total-item').html(format_number(total));
}

function tipe_sewa() {
	$('.tipe-sewa').on('change', function () {
		var no = $(this).attr('data-no');
		cek_tipe(no);
	});
}

function cek_tipe(no) {
	var tipe = $('[name="jenis_sewa[' + no + ']"]').val();

	if (tipe == 'Lepas Kunci') {
		$('#driver_' + no).val(null).attr('readonly', true);
		$('#btn_driver_' + no).attr('disabled', true);
	} else {
		$('#driver_' + no).val(null).attr('readonly', false);
		$('#btn_driver_' + no).attr('disabled', false);
	}

	console.log('tipe')
}

function modal_driver() {
	$('.driver-search').on('click', function () {
		DriverNo = $(this).attr('data-no');
		console.log(DriverNo)

		$('#kt_modal_search_driver').modal('show');
	});
}


function select_driver() {
	$("#driver_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$('[name="driver_id[' + DriverNo + ']"]').val(object.user_id);
		console.log(object.user_id)
		$('[name="driver_nama[' + DriverNo + ']"]').val(object.staff_nama).attr('readonly', true);
		$("#kt_modal_search_driver").modal("hide");
	});
}

function simpan() {

	$("#btn-save").click(function () {
		var btn = $(this);
		var form = $("#save-form");
		var enough = true;
		btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
		form.ajaxSubmit({
			url: form.attr("action"),
			beforeSend: function () {
				$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
			},
			success: function (response, status, xhr, $form) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				var data = jQuery.parseJSON(response);
				btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
				if (data.success) {
					console.log(data)
					$("#kt_modal_pembayaran").modal("hide");
					Swal.fire({
						type: 'success',
						title: 'Transaksi Berhasil',
					}).then(function () {
						form.clearForm();
						form.validate().resetForm();
						if (data.url) {
							var url = data.url;
							window.open(url, "_blank");
						}
						location.reload();
					});
				} else {
					console.log(data);
					swal.fire({
						type: 'error',
						title: 'Peringatan',
						text: data.message,
						showConfirmButton: false,
						timer: 1500
					});
				}
			}, error: function (xhr, ajaxOptions, thrownError) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	})
}

function delete_biaya() {
	$('#biaya-list').on('click', '.delete-biaya', function () {
		var no = $(this).data("no");
		$("#row_biaya_" + no).remove();
		total_biaya();
	});
}

function jumlah_biaya() {

	$('.jumlah-biaya').on('change keyup', function () {
		total_biaya();
	})
}

function total_biaya() {
	var total = 0;
	$('.jumlah-biaya').each(function () {

		total += str_to_number($(this).val());

	});
	$('#total-biaya').html(format_number(total));
}

function simpan_biaya() {
	$('#simpan-biaya').on('click', function () {
		var biaya_array = Array();
		var no = $('#nomor-biaya').val();
		var total = 0;
		$('.row_biaya').each(function () {
			var index = $(this).val();
			var nama_kegiatan = $('[name="nama_kegiatan[' + index + ']"]').val();
			var jumlah = str_to_number($('[name="jumlah[' + index + ']"]').val());
			total = total + jumlah;

			if (nama_kegiatan && jumlah > 0) {
				var baru = {
					nama_kegiatan: nama_kegiatan,
					jumlah: jumlah
				};

				biaya_array.push(baru);
			}
		});
		$('#biaya_tambahan_' + no).html(format_number(total));
		$('[name="biaya_array[' + no + ']"]').val(JSON.stringify(biaya_array));
		$('[name="biaya_tambahan[' + no + ']"]').val(total);
		sub_total(no)
		$('#modal-biaya').modal('hide');
	})
}

function input_numeral_null() {
	$('.input-numeral').on('change keyup', function () {
		var val = $(this).val();
		if (val < 0 || val == '') {
			$(this).val(0);
		}
	})
}

function modal_jadwal() {
	$('.jadwal-mobil').on('click', function () {
		$('#jadwal-list').html('');
		var no = $(this).attr('data-no');
		var harga_normal = $('[name="harga[' + no + ']"]').val();
		var harga_luar = $('[name="harga_luar_kota[' + no + ']"]').val();
		$('#harga-normal').val(str_to_number(harga_normal));
		$('#harga-luar').val(str_to_number(harga_luar));
		var jadwal = $('[name="jadwal_mobil[' + no + ']"]').val();
		var total_jadwal = 0;
		var total_over = 0;
		$('#nomor-jadwal').val(no);
		var count = 0;

		if (jadwal) {
			jadwal = JSON.parse(jadwal);

			$.each(jadwal, function (index, value) {
				if (value.area === "Luar Kota") {
					total_jadwal = parseInt(total_jadwal) + parseInt(str_to_number(harga_luar));
				} else {
					total_jadwal = parseInt(total_jadwal) + parseInt(str_to_number(harga_normal));
				}
				total_over = parseInt(total_over) + str_to_number(value.biaya_overtime);
				var list = '<tr id="row_jadwal_' + index + '">' +
					'<td style="display:none">' +
					'<input type="hidden" class="row_jadwal" value="' + index + '">' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="tanggal[' + index + ']" class="form-control tanggal tanggal-jadwal" value="' + value.tanggal + '" required="" readonly>' +
					'</td>' +
					'<td>' +
					'<select class="form-control tipe-sewa-jadwal" name="area[' + index + ']" data-no="' + index + '">' +
					'<option value="Dalam Kota" ' + ((value.area == 'Dalam Kota') ? 'selected"' : '') + '>Dalam Kota</option>' +
					'<option value="Luar Kota" ' + ((value.area == 'Luar Kota') ? 'selected"' : '') + '>Luar Kota</option>' +
					'</select>' +
					'</td>' +
					'<td>' +
					'<input type="text" placeholder="" name="biaya_overtime[' + index + ']" class="form-control input-numeral biaya-overtime" value="' + value.biaya_overtime + '" required="">' +
					'</td>' +
					'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
					'</tr>';
				$('#jadwal-list').append(list);
				$('[name="area[' + index + ']"]').val(value.area).change();
				count = parseInt(index) + 1;
			});
		}

		$('#jadwal-count').val(count);

		$('#total-nominal-jadwal').val(total_jadwal);
		$('#total-nominal-over').val(total_over);
		$('#total-jadwal').html(format_number(total_jadwal));
		$('#total-over').html(format_number(total_over));

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});

		$('.tanggal').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			autoclose: true,
			format: 'dd-mm-yyyy',
		});

		input_numeral_null();

		tipe_sewa_jadwal();
		change_overtime();
		change_tanggal_jadwal();

		$('#modal-jadwal').modal('show');

	})
}

function tambah_jadwal() {
	$('#tambah-jadwal-list').on('click', function () {
		var index = $('#jadwal-count').val();
		var list = '<tr id="row_jadwal_' + index + '">' +
			'<td style="display:none">' +
			'<input type="hidden" class="row_jadwal" value="' + index + '">' +
			'</td>' +
			'<td>' +
			'<input type="text" placeholder="" name="tanggal[' + index + ']" class="form-control tanggal tanggal-jadwal" value="" required="" readonly>' +
			'</td>' +
			'<td>' +
			'<select class="form-control tipe-sewa-jadwal" name="area[' + index + ']" data-no="' + index + '">' +
			'<option value="Dalam Kota">Dalam Kota</option>' +
			'<option value="Luar Kota">Luar Kota</option>' +
			'</select>' +
			'</td>' +
			'<td>' +
			'<input type="text" placeholder="" name="biaya_overtime[' + index + ']" class="form-control input-numeral biaya-overtime" value="0" required="">' +
			'</td>' +
			'<td><button class="btn btn-danger delete-biaya" data-no="' + index + '"><i class="flaticon2-trash"></i></button></td>' +
			'</tr>';
		$('#jadwal-list').append(list);
		var count = parseInt(index) + 1;
		$('#jadwal-count').val(count);


		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});

		$('.tanggal').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			autoclose: true,
			format: 'dd-mm-yyyy',
		});



		tipe_sewa_jadwal();
		change_overtime();
		change_tanggal_jadwal();
	});

}

function change_tanggal_jadwal() {
	$('.tanggal-jadwal').on('change', function () {
		total_jadwal();
	})
}

function tipe_sewa_jadwal() {
	$('.tipe-sewa-jadwal').on('change', function () {
		total_jadwal();
	})
}

function change_overtime() {
	$('.biaya-overtime').on('change keyup', function () {
		total_jadwal();
	})
}

function total_jadwal() {
	var total_biaya = 0;
	var total_overtime = 0;
	var harga_normal = $('#harga-normal').val();
	var harga_luar = $('#harga-luar').val();
	var total_hari = 0;
	var total_hari_luar = 0;
	var total_hari_global = 0;

	$('.tipe-sewa-jadwal').each(function () {
		var index = $(this).attr('data-no');
		var area = $(this).val();

		total_hari_global = total_hari_global + 1;

		if (area === "Luar Kota") {
			total_hari_luar = total_hari_luar + 1;
			total_biaya = parseInt(total_biaya) + parseInt(harga_luar);
		} else {
			total_hari = total_hari + 1;
			total_biaya = parseInt(total_biaya) + parseInt(harga_normal);
		}

		total_overtime = parseInt(total_overtime) + str_to_number($('[name="biaya_overtime[' + index + ']"]').val());
		total_biaya = parseInt(total_biaya) + str_to_number($('[name="biaya_overtime[' + index + ']"]').val());
	});
	$('#total-nominal-jadwal').val(total_biaya);
	$('#total-nominal-over').val(total_overtime);
	$('#total-hari').val(total_hari);
	$('#total-hari-global').val(total_hari_global);
	$('#total-hari-luar').val(total_hari_luar);
	$('#total-jadwal').html(format_number(total_biaya));
	$('#total-over').html(format_number(total_overtime));
}

function simpan_jadwal() {
	$('#simpan-jadwal').on('click', function () {
		var total_jadwal = $('#total-nominal-jadwal').val();
		var total_overtime = $('#total-nominal-over').val();
		var hari = $('#total-hari').val();
		var hari_luar = $('#total-hari-luar').val();
		var total_hari_global = $('#total-hari-global').val();
		var jadwal = Array();

		var no = $('#nomor-jadwal').val();

		$('.row_jadwal').each(function () {
			var index = $(this).val();
			var tanggal = $('[name="tanggal[' + index + ']"]').val();
			var area = $('[name="area[' + index + ']"]').val();
			var biaya_overtime = str_to_number($('[name="biaya_overtime[' + index + ']"]').val());

			if (tanggal == '') {
				swal.fire({
					title: "Perhatian ...",
					text: "Tanggal Belum Terisi",
					type: "warning",
				})
			} else {
				var baru = {
					tanggal: tanggal,
					area: area,
					biaya_overtime: biaya_overtime
				};

				jadwal.push(baru);
			}
		});

		console.log('jadwal save')
		console.log(total_jadwal)
		console.log(JSON.stringify(jadwal))
		$('[name="total_jadwal[' + no + ']"]').val(total_jadwal);
		$('[name="total_overtime[' + no + ']"]').val(total_overtime);
		$('[name="hari[' + no + ']"]').val(hari);
		$('[name="hari_luar_kota[' + no + ']"]').val(hari_luar);
		$('[name="jadwal_mobil[' + no + ']"]').val(JSON.stringify(jadwal));
		$('#hari-label-'+no).html(total_hari_global);
		console.log('total hari = '+total_hari_global);

		$('#modal-jadwal').modal('hide');

		sub_total(no);
	});
}
