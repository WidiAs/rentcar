jQuery(document).ready(function () {
	tambah_pengeluaran();
	hapus_pengeluaran();
	jumlah_change();
	simpan();
});

function tambah_pengeluaran() {
	$('#button_add_pengeluaran').on('click', function () {
		var count = $(this).attr('data-count');
		count = parseInt(count) + 1;
		var text = '<tr id="catatan_row_' + count + '">';
		text += '<td><input type="time" class="form-control" name="jam[' + count + ']" value="" required></td>';
		text += '<td><input type="text" class="form-control" name="nama_pengeluaran[' + count + ']" value="" required></td>';
		text += '<td><input type="text" class="form-control input-angka" name="jumlah[' + count + ']" value="0" required></td>';
		text += '<td><button class="btn btn-danger delete-btn" data-no="'+count+'"><i class="flaticon2-trash"></i></button></td>';
		text += '</tr>';

		$('#item_child').append(text);
		$(this).attr('data-count', count);

		$('.input-angka').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});

		});
		jumlah_change();
		hapus_pengeluaran();
	});
}

function hapus_pengeluaran() {
	$('#item_child').on('click', '.delete-btn', function () {
		var no = $(this).data("no");
		$("#row_" + no).remove();
	});
}

function jumlah_change() {
	$('.input-angka').on('change keyup', function () {
		var val = $(this).val();
		if (!val){
			$(this).val(0);
		}
	});
}


function simpan() {

	$("#btn-simpan").click(function (e) {
		var pembayaran = true;
		$('.input-angka').each(function () {
			if ($(this).val() == 0){
				pembayaran = false;
			}
		});
		e.preventDefault();
		var btn = $(this);
		var form = $("#save-form");
		form.validate({
			rules: {
				password: {
					required: true,
					minlength: 5,
				},
				re_password: {
					required: true,
					minlength: 5,
					equalTo: '#password'
				}
			}
		});
		if (!form.valid()) {
			return;
		}
		if (pembayaran == false){
			swal.fire({
				type: 'error',
				text: 'Jumlah Tidak boleh 0',
				showConfirmButton: false,
				timer: 1500
			});
		}else {
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						Swal.fire({
							type: 'success',
							title: 'Data Disimpan',
						}).then(function () {
							form.clearForm();
							form.validate().resetForm();
							window.location.replace($('#redirect').val());
						});
					} else {
						swal.fire({
							type: 'error',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
	})
}
