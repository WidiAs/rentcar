jQuery(document).ready(function () {
	// $('.tanggal').datepicker({
	// 	rtl: KTUtil.isRTL(),
	// 	todayHighlight: true,
	// 	orientation: "bottom left",
	// 	autoclose: true,
	// 	format: 'dd-mm-yyyy',
	// });

	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		});
	});

	$('.input-numeral').on('change keyup', function () {
		var val = $(this).val();

		if (val == '' || val < 0){
			$(this).val(0);
		}
	})

	kedepan_datatable();
	history_datatable();
	history_pengeluaran_datatable();
	simpan();
	show_image();

});

function show_image() {

	$('#history-table').on('click', '.btn-foto', function () {
		console.log('test')
		var src = $(this).attr('data-url');
		$('#img01').attr('src', src);
		$('#myModal').modal('show');
	});

	$('#myModal').on('click', function () {
		$('#myModal').modal('hide');
	});
}

function kedepan_datatable() {
	if ($('#kedepan-table').length) {
		var kedepan_url = $("#kedepan_url").val();
		$("#kedepan-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: kedepan_url,
			columns: [{data: "no"}, {data: 'mobil_jenis_nama'}, {data: 'mobil_plat'}, {data: 'tanggal'}]

		});
	}
}


function history_datatable() {
	if ($('#history-table').length) {
		var history_url = $("#history_url").val();
		$("#history-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: history_url,
			columns: [{data: "no"}, {data: 'mobil_jenis'}, {data: 'mobil_plat'}, {data: 'tanggal'}, {data: 'waktu_mulai'}, {data: 'waktu_selesai'}, {data: 'jenis_pekerjaan'}, {data: 'status_pembayaran'}, {data: 'bukti'}]

		});
	}
}

function history_pengeluaran_datatable() {
	if ($('#history-pengeluaran-table').length) {
		var history_pengeluaran_url = $("#history_pengeluaran_url").val();
		$("#history-pengeluaran-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: history_pengeluaran_url,
			columns: [{data: "no"}, {data: 'nama_pengeluaran'}, {data: 'jumlah'}, {data: 'tanggal_lbl'}, {data: 'jam'}],

		});
	}
}

function simpan() {

	$("#submit-kegiatan").click(function (e) {
		e.preventDefault();
		var btn = $(this);
		var form = $(this).closest('form');
		form.validate({
			rules: {
				password: {
					required: true,
					minlength: 5,
				},
				re_password: {
					required: true,
					minlength: 5,
					equalTo: '#password'
				}
			}
		});
		if (!form.valid()) {
			return;
		}
		btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
		form.ajaxSubmit({
			url: form.attr("action"),
			beforeSend: function () {
				$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
			},
			success: function (response, status, xhr, $form) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				var data = jQuery.parseJSON(response);
				btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
				if (data.success) {
					console.log(data)
					$("#kt_modal_pembayaran").modal("hide");
					Swal.fire({
						type: 'success',
						title: 'Berhasil Menyimpan Data',
					}).then(function () {
						// form.clearForm();
						// form.validate().resetForm();
						// var url = data.url;
						// window.open(url, "_blank");
						location.reload();
					});
				} else {
					console.log(data);
					swal.fire({
						type: 'error',
						title: 'Peringatan',
						text: data.message,
						showConfirmButton: false,
						timer: 1500
					});
				}
			}, error: function (xhr, ajaxOptions, thrownError) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	})
}
