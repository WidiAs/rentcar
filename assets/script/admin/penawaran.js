jQuery(document).ready(function () {
	select_guest();
	select_mobil();
	mobil_add();
	tambah_catatan();
	simpan();
});
var noCount = 0;
var DriverNo = 0;
var date = $('#today').val();

function mobil_add() {
	$('#button_add_trans').on('click', function () {
		noCount = parseInt($(this).attr('data-count'));
		console.log(noCount);
	});
}

function mobil_edit() {
	$('.change-mobil').on('click', function () {
		noCount = parseInt($(this).attr('data-no'));
		console.log(noCount);
	});
}

function select_guest() {
	$("#guest_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$.each(object, function (key, value) {
			$('#guest-form [name="' + key + '"]').val(value);
		})
		$("#kt_modal_search_pelanggan").modal("hide");
	});
}

function select_mobil() {
	$("#mobil_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		if ($('#row_' + noCount)[0]) {

			// $('[name = "tanggal_ambil[' + noCount + ']"]').val(date);
			$('[name = "mobil_id[' + noCount + ']"]').val(object.mobil_id);
			$('[name = "jenis_mobil_nama[' + noCount + ']"]').val(object.jenis_mobil_nama);
			$('[name = "driver_id[' + noCount + ']"]').val('');
			$('[name = "driver_harga[' + noCount + ']"]').val('');
			$('[name = "bbm[' + noCount + ']"]').val('');
			// $('#driver_' + noCount).val('');
			$('#jenis_mobil_' + noCount).val(object.mobil_jenis_nama);
			$('#plat_' + noCount).val(object.plat_mobil);
			// $('[name = "hari[' + noCount + ']"]').val(0);
			$('[name = "harga[' + noCount + ']"]').val(0);
			$('[name = "keterangan[' + noCount + ']"]').val('');
			$('[name = "subtotal[' + noCount + ']"]').val(0);
			$('[name = "jenis_sewa[' + noCount + ']"]').val('Lepas Kunci').trigger('change');

			sub_total(noCount);
			cek_tipe(noCount);
		} else {
			var text = '<tr id="row_' + noCount + '">';
			text += '<td style="display:none">' +
				'<input type="hidden" name="mobil_id[' + noCount + ']" value="' + object.mobil_id + '">' +
				'<input type="hidden" name="driver_id[' + noCount + ']" value="">' +
				'<input type="hidden" class="subtotal" name="subtotal[' + noCount + ']" value="">' +
				'<input type="hidden" name="jenis_mobil_nama[' + noCount + ']" value="' + object.jenis_mobil_nama + '">' +
				'</td>';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" id="plat_' + noCount + '" readonly="" value="' + object.plat_mobil + '" name="plat_mobil[' + noCount + ']">' +
				'<div class="input-group-append"><button class="btn btn-primary change-mobil" data-toggle="modal"' +
				'data-target="#kt_modal_search_mobil" type="button" data-no="' + noCount + '"><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="jenis_mobil_' + noCount + '">' + object.jenis_mobil_nama + '</span></td>';
			text += '<td><select class="form-control tipe-sewa" name="jenis_sewa[' + noCount + ']" data-no="' + noCount + '">' +
				'<option value="Lepas Kunci">Lepas Kunci</option>' +
				'<option value="All In">All In</option>' +
				'</select></td>';
			// text += '<td><input type="text" class="form-control tanggal" name="tanggal_ambil[' + noCount + ']" value="' + date + '" readonly></td>';
			// text += '<td><input type="text" class="form-control input-numeral hari" value="0" name="hari[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><input type="text" class="form-control input-numeral harga" value="0" name="harga[' + noCount + ']" data-no="' + noCount + '"></td>';
			text += '<td><input type="text" class="form-control input-numeral driver-harga" value="0" name="driver_harga[' + noCount + ']" data-no="' + noCount + '" readonly></td>';
			text += '<td><input type="text" class="form-control input-numeral bbm" value="0" name="bbm[' + noCount + ']" data-no="' + noCount + '"></td>';
			// text += '<td><div class="input-group col-12">' +
			// 	'<input type="text" class="form-control" name="driver_nama[' + noCount + ']" id="driver_' + noCount + '" value="" readonly>' +
			// 	'<div class="input-group-append"><button class="btn btn-primary driver-search" type="button" id="btn_driver_' + noCount + '" data-no="' + noCount + '" disabled><i class="flaticon-search"></i></button></div>' +
			// 	'</div></td>';
			text += '<td><span id="subtotal_' + noCount + '">0</span></td>';
			text += '<td><textarea class="form-control" name="keterangan[' + noCount + ']"></textarea></td>';
			text += '<td><button class="btn btn-danger delete-btn" data-no="' + noCount + '"><i class="flaticon2-trash"></i></button></td>';
			text += '</tr>';

			$('#item_child').append(text);
			noCount = parseInt(noCount) + 1;
			$('#button_add_trans').attr('data-count', noCount);

			$('.tanggal').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				autoclose: true,
				format: 'dd-mm-yyyy',
			});

			$('.input-numeral').toArray().forEach(function (field) {
				new Cleave(field, {
					numeral: true,
					numeralThousandsGroupStyle: 'thousand'
				});
			});

			mobil_edit();
			harga();
			driver_harga();
			bbm();
			hari();
			tipe_sewa();
			modal_driver();
			select_driver();
			hapus_mobil();
		}


		$("#kt_modal_search_mobil").modal("hide");
	});
}

function hapus_mobil() {
	$('#item_child').on('click', '.delete-btn', function () {
		var no = $(this).data("no");
		$("#row_" + no).remove();
	});
}

function tambah_catatan() {
	$('#add-catatan').on('click', function () {
		var count = $(this).attr('data-count');
		var text = '<tr id="catatan_row_' + count + '">';
		text += '<td><textarea class="form-control" name="catatan[' + count + ']"></textarea></td>';
		text += '<td><button class="btn btn-danger delete-btn" data-no="' + count + '"><i class="flaticon2-trash"></i></button></td>';
		text += '</tr>';

		$('#catatan_child').append(text);
		count = parseInt(count) + 1;

		$(this).attr('data-count', count);

		hapus_catatan();
	});
}

function hapus_catatan() {
	$('#catatan_child').on('click', '.delete-btn', function () {
		var no = $(this).data("no");
		$("#catatan_row_" + no).remove();
	});
}

function harga() {
	$('.harga').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function driver_harga() {
	$('.driver-harga').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function hari() {
	$('.hari').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function bbm() {
	$('.bbm').on('change keyup', function () {
		var no = $(this).attr('data-no');
		sub_total(no);
	});
}

function sub_total(no) {
	var harga = str_to_number($('[name="harga[' + no + ']"]').val());
	var driver_harga = str_to_number($('[name="driver_harga[' + no + ']"]').val());
	// var hari = str_to_number($('[name="hari[' + no + ']"]').val());
	var bbm = str_to_number($('[name="bbm[' + no + ']"]').val());

	// var total = (harga + driver_harga + bbm) * hari;
	var total = (harga + driver_harga + bbm);

	$('[name="subtotal[' + no + ']"]').val(total);
	$('#subtotal_' + no).html(format_number(total));

	grand_total();
}

function grand_total() {
	var total = 0;
	$('.subtotal').each(function () {

		total += str_to_number($(this).val());

	});
	$('[name="grand_total"]').val(total);
	$('#total-item').html(format_number(total));
}

function tipe_sewa() {
	$('.tipe-sewa').on('change', function () {
		var no = $(this).attr('data-no');
		cek_tipe(no);
	});
}

function cek_tipe(no) {
	var tipe = $('[name="jenis_sewa[' + no + ']"]').val();

	if (tipe == 'Lepas Kunci') {
		$('[name="driver_harga['+no+']"]').val(0).attr('readonly', true);
		$('#driver_' + no).val(null).attr('readonly', true);
		$('#btn_driver_' + no).attr('disabled', true);
	} else {
		$('[name="driver_harga['+no+']"]').val(0).attr('readonly', false);
		$('#driver_' + no).val(null).attr('readonly', false);
		$('#btn_driver_' + no).attr('disabled', false);
	}

	console.log('tipe')
}

function modal_driver() {
	$('.driver-search').on('click', function () {
		DriverNo = $(this).attr('data-no');

		$('#kt_modal_search_driver').modal('show');
	});
}


function select_driver() {
	$("#driver_child").on('click', '.chose-btn', function () {
		var json = $(this).siblings('textarea').val();
		var object = JSON.parse(json);
		$('[name="driver_id[' + DriverNo + ']"]').val(object.user_id);
		$('[name="driver_nama[' + DriverNo + ']"]').val(object.staff_nama).attr('readonly', true);
		$("#kt_modal_search_driver").modal("hide");
	});
}

function simpan() {

	$("#btn-save").click(function () {
		var btn = $(this);
		var form = $("#save-form");
		var enough = true;
		btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
		form.ajaxSubmit({
			url: form.attr("action"),
			beforeSend: function () {
				$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
			},
			success: function (response, status, xhr, $form) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				var data = jQuery.parseJSON(response);
				btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
				if (data.success) {
					console.log(data)
					$("#kt_modal_pembayaran").modal("hide");
					Swal.fire({
						type: 'success',
						title: 'Transaksi Berhasil',
					}).then(function () {
						form.clearForm();
						form.validate().resetForm();
						var url = data.url;
						window.open(url, "_blank");
						location.reload();
					});
				} else {
					console.log(data);
					swal.fire({
						type: 'error',
						title: 'Peringatan',
						text: data.message,
						showConfirmButton: false,
						timer: 1500
					});
				}
			}, error: function (xhr, ajaxOptions, thrownError) {
				$('.wrapper-loading').fadeOut().addClass('hidden');
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	})
}
