jQuery(document).ready(function () {
	select_mobil();
	select_kegiatan();
	show_image();
	hapus_mobil();
	select_pembayaran();
	simpan();
	select_change();
	redirect_pengeluaran();
});

function select_mobil() {
	$('.table-jadwal').on('click', '.manage-btn', function () {
		var data = $(this).siblings('textarea').val();
		data = JSON.parse(data);

		$('[name="mobil_id"]').val(data.mobil_id);
		$('[name="penyewaan_id"]').val(data.penyewaan_id);
		$('[name="penyewaan_rekanan_id"]').val(data.penyewaan_rekanan_id);
		$('[name="mobil_jenis_nama"]').val(data.mobil_jenis_nama);
		$('[name="mobil_plat"]').val(data.mobil_plat);


		$('[name="mobil_jenis_nama_lbl"]').html(data.mobil_jenis_nama);
		$('[name="mobil_plat_lbl"]').html(data.mobil_plat);
		$('[name="jenis"]').html(data.jenis);

		if (data.jenis == 'Lepas Kunci') {
			$('#foto-ktp').show();
			$('[name="foto_ktp"]').attr('required', true);
		} else {
			$('#foto-ktp').hide();
			$('[name="foto_ktp"]').attr('required', false);
		}
		console.log('data');
		console.log(data);

		$('#modal-pencatatan').modal('show');
	});
}

function select_kegiatan() {
	$('.table-kegiatan').on('click', '.manage-btn', function () {
		var data = $(this).siblings('textarea').val();
		data = JSON.parse(data);
		$('#item-pengeluaran').html('');
		$('#total-pengeluaran').html('');
		$('[name="kegiatan_driver_id"]').val(data.kegiatan_driver_id);
		$('[name="driver_nama"]').html(data.driver_nama);
		$('[name="mobil_jenis"]').html(data.mobil_jenis);
		$('[name="mobil_plat"]').html(data.mobil_plat);
		$('[name="tanggal_lbl"]').html(data.tanggal_lbl);
		$('[name="waktu_mulai"]').val(data.waktu_mulai);
		$('[name="waktu_selesai"]').val(data.waktu_selesai);
		$('[name="waktu_kerja"]').html(data.waktu_kerja);
		$('#btn-manage-pengeluaran').attr('data-id', data.kegiatan_driver_id);

		var row = '';
		var jumlah = 0;
		$.each(data.item, function (index, value) {
			row += '<tr>' +
				'<td>' + value.jam + '</td>' +
				'<td>' + value.nama_pengeluaran + '</td>' +
				'<td>' + format_number(value.jumlah) + '</td>' +
				'</tr>';

			jumlah = jumlah + parseFloat(value.jumlah);

		});
		$('#item-pengeluaran').append(row);
		$('#total-pengeluaran').html(format_number(jumlah));
		console.log('data');
		console.log(data);

		$('#modal-kegiatan').modal('show');
	});
}

function select_pembayaran() {
	$('.table-kegiatan').on('click', '.payment-btn', function () {
		var data = $(this).siblings('textarea').val();
		data = JSON.parse(data);
		$('#item-pengeluaran').html('');
		$('#total-pengeluaran').html('');
		$('[name="kegiatan_driver_id"]').val(data.kegiatan_driver_id);
		$('[name="driver_nama"]').html(data.driver_nama);
		$('[name="mobil_jenis"]').html(data.mobil_jenis);
		$('[name="mobil_plat"]').html(data.mobil_plat);
		$('[name="tanggal_lbl"]').html(data.tanggal_lbl);
		$('[name="waktu_mulai"]').html(data.waktu_mulai);
		$('[name="waktu_selesai"]').html(data.waktu_selesai);
		$('[name="waktu_kerja"]').html(data.waktu_kerja);

		// var row = '';
		var jumlah = 0;
		$.each(data.item, function (index, value) {
			// 	row += '<tr>' +
			// 		'<td>'+value.jam+'</td>' +
			// 		'<td>'+value.nama_pengeluaran+'</td>' +
			// 		'<td>'+format_number(value.jumlah)+'</td>' +
			// 		'</tr>';
			//
			jumlah = jumlah + parseFloat(value.jumlah);
			//
		});
		// $('#item-pengeluaran').append(row);
		// $('#total-pengeluaran').html(format_number(jumlah));

		$('[name="total_pengeluaran"]').html(jumlah);

		$('.input-numeral').toArray().forEach(function (field) {
			new Cleave(field, {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			});
		});

		$('#modal-bayar').modal('show');
	});
}

function redirect_pengeluaran() {
	$('#btn-manage-pengeluaran').on('click', function () {
		var id = $(this).attr('data-id');
		var url = $('#url-edit-pengeluaran').val();
		window.location.replace(url+id);
	});
}

function show_image() {

	$('.modal-arus-kendaraan').on('click', '.btn-foto', function () {
		var src = $(this).attr('data-url');
		$('#img01').attr('src', src);
		$('#myModal').modal('show');
	});

	$('#myModal').on('click', function () {
		$('#myModal').modal('hide');
	});
}

function hapus_mobil() {

	$(".modal-arus-kendaraan").on('click', '.hapus-btn', function () {
		var route = $(this).attr('data-url');
		var id = $(this).attr('data-ksu');
		swal.fire({
			title: "Perhatian ...",
			text: "Yakin hapus data ini ?",
			type: "warning",
			showCancelButton: !0,
			confirmButtonColor: "#0abb87",
			confirmButtonText: "Ya, yakin",
			cancelButtonText: "Batal",
		}).then(function (e) {
			if (e.value) {

				$.ajax({
					url: route,
					type: "delete",
					data: {"id": id},
					beforeSend: function () {
						$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
					},
					success: function (response) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						var data = jQuery.parseJSON(response);
						if (data.success) {
							if ($('.datatable').length > 0) {
								$('.datatable').DataTable().ajax.reload();
							}
						} else {
							swal.fire({
								type: 'error',
								text: data.message,
								showConfirmButton: false,
								timer: 1500
							});
						}
					},
					error: function (request) {
						$('.wrapper-loading').fadeOut().addClass('hidden');
						swal.fire({
							title: "Ada yang Salah",
							html: request.responseJSON.message,
							type: "warning"
						});
					}
				});
			}
		});
	});
}

function format_number(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function str_to_number(i) {
	return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
}

function select_change(){
	$('[name="lokasi_id"]').on('change', function () {
		console.log('lokasi')
		var data = $(this).find(":selected").text();
		console.log(data)
		$('[name="lokasi_nama"]').val(data);
	})
	$('[name="tipe_pembayaran_id"]').on('change', function () {
		console.log('bayar')
		var data = $(this).find(":selected").text();
		console.log(data)
		$('[name="tipe_pembayaran"]').val(data);
	})
}

function simpan() {

	$("#btn-bayar").click(function (e) {
		var bayar = str_to_number($('[name="jumlah_bayaran"]').val());
		e.preventDefault();
		var btn = $(this);
		var form = $(this).closest('form');
		form.validate({
			rules: {
				password: {
					required: true,
					minlength: 5,
				},
				re_password: {
					required: true,
					minlength: 5,
					equalTo: '#password'
				}
			}
		});
		if (!form.valid()) {
			return;
		}
		if (bayar < 1){
			swal.fire({
				type: 'error',
				text: 'Jumlah Bayaran Tidak boleh 0',
				showConfirmButton: false,
				timer: 1500
			});
		}else {
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						Swal.fire({
							type: 'success',
							title: 'Transaksi Berhasil',
						}).then(function () {
							form.clearForm();
							form.validate().resetForm();
							location.reload();
						});
					} else {
						swal.fire({
							type: 'error',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
	})
}
