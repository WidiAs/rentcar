"use strict";

// Class Definition
var KTPOS = function () {
	var general = function () {
		var base_url = $("#base_url").val();
		var noCount = 1;
		var noChose = 0;
		var totalItem = 0;
		var potongan = 0;
		var potongan_persen = 0;
		var nominal_potongan_persen = 0;
		var tambahan = 0;
		var grandTotal = 0;
		var transaksi = {};
		var textTrans = {};
		var terbayar = 0;
		var guest_url = $("#guest_url").val();
		var produk_url = $("#produk_url").val();
		$('.input-numeral').keyup(function () {
			if ($(this).val() == "") {
				$(this).val("0");
			}
		});
		$('.tanggal').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			autoclose: true,
			format: 'yyyy-mm-dd',
		});
		var guest_table = $("#guest-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: false,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			pageLength: 10,
			ajax: guest_url,
			columns: [{data: "no"}, {data: 'guest_nama'}, {data: 'guest_alamat'}, {data: 'guest_telepon'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
		$('.modal').on('hidden.bs.modal', function () {
			$("#process-button").css('display', 'block');
		})
		$('.modal').on('show.bs.modal', function () {
			$("#process-button").css('display', 'none');
		})
		var produk_table = $("#produk-table").DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ordering: true,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			pageLength: 10,
			ajax: produk_url,
			columns: [{data: "produk_kode"}, {data: 'produk_nama'}, {data: 'spec'},
				// {data: 'jenis_produk_nama'},
				{data: 'stock_produk_seri'}, {data: 'stock_produk_qty'}, {data: 'aksi'}],
			columnDefs: [{
				targets: -1,
				responsivePriority: 1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">' + JSON.stringify(full) + '</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
					temp += '</div>'
					return temp;
				}
			}]

		});
		$("#generalSearch").on('keyup', function (e) {
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function () {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				} else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function (i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
		$(".textSearch").keyup(function () {
			$(this).trigger('change');
		})
		$(".searchInput").change(function () {
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function () {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				} else {
					params[i] = $(this).val();
				}

			});
			$.each(params, function (i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
		$("#barcode").keypress(function (event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == 13) {
				var barcode = $(this).val();
				$.ajax({
					type: "POST",
					url: produk_url + "?stock_produk_seri=" + barcode,
					cache: false,
					success: function (response) {
						var result = jQuery.parseJSON(response);
						var object = result.aaData[0]
						$("#produk_nama_" + noChose).html(object.produk_nama);
						if (transaksi["trans" + noChose].tipe_penjualan == "eceran") {
							$("#harga_" + noChose).html(object.harga_eceran);
						} else {
							$("#harga_" + noChose).html(object.harga_grosir);
						}
						$("#kode_" + noChose).val(object.produk_kode);
						transaksi["trans" + noChose].produk_nama = object.produk_nama;
						transaksi["trans" + noChose].produk_kode = object.produk_kode;
						transaksi["trans" + noChose].produk_id = object.produk_id;
						transaksi["trans" + noChose].harga_eceran = object.harga_eceran;
						transaksi["trans" + noChose].harga_grosir = object.harga_grosir;
						transaksi["trans" + noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
						transaksi["trans" + noChose].ar_harga = object.ar_harga;
						transaksi["trans" + noChose].hpp = object.hpp;
						transaksi["trans" + noChose].stock_produk_id = object.stock_produk_id;
						transaksi["trans" + noChose].lokasi_id = object.lokasi_id;
						$("#kt_modal_search_produk").modal("hide");
						getSubtotal(noChose);
						$("#jumlah_" + noChose).focus();
						$("#jumlah_" + noChose).val("");
						$("#barcode").val("");
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}

		})
		$("#guest_child").on('click', '.chose-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$.each(object, function (key, value) {
				$('#guest-form [name="' + key + '"]').val(value);
			})
			$("#kt_modal_search_pelanggan").modal("hide");
		})
		$("#item_child").on('click', '.item-search', function () {
			noChose = $(this).data('no');
			$("#kt_modal_search_produk").modal("show");
			$("#barcode").focus();
		})
		$("#produk_child").on('click', '.chose-btn', function () {
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$("#produk_nama_" + noChose).html(object.produk_nama);
			$("#produk_spec_" + noChose).html(object.spec);
			if (transaksi["trans" + noChose].tipe_penjualan == "eceran") {
				$("#harga_" + noChose).html(object.harga_eceran);
			} else {
				$("#harga_" + noChose).html(object.harga_grosir);
			}
			$("#kode_" + noChose).val(object.produk_kode);
			transaksi["trans" + noChose].produk_nama = object.produk_nama;
			transaksi["trans" + noChose].produk_kode = object.produk_kode;
			transaksi["trans" + noChose].produk_id = object.produk_id;
			transaksi["trans" + noChose].harga_eceran = object.harga_eceran;
			transaksi["trans" + noChose].harga_grosir = object.harga_grosir;
			transaksi["trans" + noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
			transaksi["trans" + noChose].ar_harga = object.ar_harga;
			transaksi["trans" + noChose].hpp = object.hpp;
			transaksi["trans" + noChose].stock_produk_id = object.stock_produk_id;
			transaksi["trans" + noChose].lokasi_id = object.lokasi_id;
			$("#kt_modal_search_produk").modal("hide");
			getSubtotal(noChose);
		})
		$("#button_add_trans").click(function () {
			var item = {
				produk_id: null,
				produk_nama: null,
				produk_kode: null,
				tipe_penjualan: "eceran",
				jumlah: 0,
				diskon: 0,
				diskon_nominal: 0,
				harga_eceran: 0,
				harga_grosir: 0,
				hpp: 0,
				subtotal: 0
			}
			transaksi["trans" + noCount] = item;
			var tr = itemRow();
			$("#item_child").append(tr);
			new Cleave($("#jumlah_" + noCount), {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			})
			new Cleave($("#diskon_" + noCount), {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand'
			})
			noChose = noCount;
			$("#kt_modal_search_produk").modal("show");
			$("#barcode").focus();
			noCount++;
		})
		var itemRow = function () {
			var text = '<tr id="row_' + noCount + '">';
			text += '<td><div class="input-group col-12">' +
				'<input type="text" class="form-control" id="kode_' + noCount + '" readonly="">' +
				'<div class="input-group-append"><button class="btn btn-primary item-search" type="button" data-no="' + noCount + '"><i class="flaticon-search"></i></button></div>' +
				'</div></td>';
			text += '<td><span id="produk_nama_' + noCount + '"></span></td>';
			text += '<td><span id="produk_spec_' + noCount + '"></span></td>';
			text += '<td><input type="text" class="form-control input-numeral" value="0" id="jumlah_' + noCount + '"></td>';
			text += '<td><span id="harga_' + noCount + '">0</span></td>';
			text += '<td><input type="text" class="form-control input-numeral" value="0" id="diskon_' + noCount + '"></td>';
			text += '<td><span id="subtotal_' + noCount + '">0</span></td>';
			text += '<td><button class="btn btn-danger delete-btn" data-no="' + noCount + '"><i class="flaticon2-trash"></i></button></td>';
			text += '</tr>';
			return text;
		}
		$('#item_child').on('click', '.delete-btn', function () {
			var no = $(this).data("no");
			$("#row_" + no).remove();
			delete transaksi["trans" + no];
		})
		$('#item_child').on('keyup', '.input-numeral', function () {
			if ($(this).attr('id').includes("diskon") && $(this).val() > 100) {
				$(this).val("100")
			}
			var element = $(this);
			var id = $(this).attr('id');
			if ($(this).attr('id').includes("jumlah")) {
				var no = id.replace("jumlah_", "");
				if (transaksi["trans" + no].ar_minimal_pembelian != undefined) {
					var ar_min = transaksi["trans" + no].ar_minimal_pembelian.split("|");
					var ar_harga = transaksi["trans" + no].ar_harga.split("|");
					$.each(ar_min, function (i, value) {
						if (element.val() >= value) {
							transaksi["trans" + no].harga_eceran = ar_harga[i];
							$("#harga_" + noChose).html(transaksi["trans" + no].harga_eceran);
							return false;
						}
					})
					transaksi["trans" + no].jumlah = $(this).val();
					getSubtotal(no);
				} else {
					$(this).val("0");
				}
			} else {
				var no = id.replace("diskon_", "");
				transaksi["trans" + no].diskon = $(this).val();

				getSubtotal(no);
			}


		})
		$('#item_child').on('change', 'input[type=radio]', function () {
			var id = $(this).attr('name');
			var no = id.replace("radio", "");
			transaksi["trans" + no].tipe_penjualan = $(this).val();
			if (transaksi["trans" + no].tipe_penjualan == "eceran") {
				$("#harga_" + no).html(transaksi["trans" + no].harga_eceran);
			} else {
				$("#harga_" + no).html(transaksi["trans" + no].harga_grosir);
			}
			getSubtotal(no);
		})
		var getSubtotal = function (index) {
			var total = 0;
			var nominal_diskon = 0;
			if (transaksi["trans" + index].tipe_penjualan == "eceran") {
				total = intVal(transaksi["trans" + index].jumlah) * intVal(transaksi["trans" + index].harga_eceran);
				nominal_diskon = ((total * intVal(transaksi["trans" + index].diskon)) / 100);
				transaksi["trans" + index].diskon_nominal = nominal_diskon;
				total = total - nominal_diskon;
				transaksi["trans" + index].subtotal = KTUtil.numberString(total.toFixed(0))
			} else {
				total = intVal(transaksi["trans" + index].jumlah) * intVal(transaksi["trans" + index].harga_grosir);
				transaksi["trans" + index].subtotal = KTUtil.numberString(total.toFixed(0))
			}
			$("#subtotal_" + index).html(transaksi["trans" + index].subtotal);
			getTotal(index);
		}
		var intVal = function (i) {
			return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
		};
		var getTotal = function (index) {
			totalItem = 0;
			$.each(transaksi, function (i, data) {
				totalItem += intVal(data.subtotal)
			})
			nominal_potongan_persen = (totalItem * potongan_persen) / 100;
			$("#total-item").html(KTUtil.numberString(totalItem.toFixed(0)));
			$("#total-bayar").html(KTUtil.numberString(totalItem.toFixed(0)));
			grandTotal = ((totalItem + tambahan) - potongan) - nominal_potongan_persen;
			$("#grand-total").html(KTUtil.numberString(grandTotal.toFixed(0)));
			$("#grand-bayar").html(KTUtil.numberString(grandTotal.toFixed(0)));
			textTrans = {
				potongan: potongan,
				potongan_persen: potongan_persen,
				nominal_potongan_persen: nominal_potongan_persen,
				tambahan: tambahan,
				total_item: totalItem,
				grand_total: grandTotal,
				terbayar: terbayar,
				item: transaksi
			}
			$("#textTransaksi").html(JSON.stringify(textTrans));

		}
		$("#potongan").keyup(function () {
			$("#potongan-bayar").html($(this).val());
			potongan = intVal($(this).val());
			getTotal()
		})
		$("#potongan_persen").keyup(function () {
			potongan_persen = intVal($(this).val())
			if (potongan_persen > 100) {
				$(this).val(100);
				potongan_persen = 100;
			}
			$("#potongan-bayar-persen").html(potongan_persen + '%');
			getTotal()
		})
		$("#terbayar").keyup(function () {
			terbayar = intVal($(this).val())
			getTotal()
		})
		$("#tambahan").keyup(function () {
			$("#tambahan-bayar").html($(this).val());
			tambahan = intVal($(this).val())
			getTotal()
		})
		$("#process-button").click(function () {
			if (grandTotal == 0) {
				swal.fire({
					type: 'error',
					title: 'Peringatan',
					text: "Total transaksi 0",
					showConfirmButton: false,
					timer: 1500
				});
				return
			}
			$("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
			var form = $("#save-form");
			terbayar = grandTotal;
			if ($("#tipe_pembayaran").find(':selected').data('kembalian') == 1) {
				$("#terbayar").removeClass("readonly");
				$("#terbayar").val(0);
			}
			if ($("#lokasi_id").val() != undefined && $("#lokasi_id").val() == "") {
				form.validate().element("#lokasi_id");
				return
			}
			$("#kt_modal_pembayaran").modal("show")
		})
		$("#btn-save").click(function () {
			var btn = $(this);
			var form = $("#save-form");
			var enough = true;
			if ($("#terbayar").prop("disabled") == false) {
				if (intVal($("#terbayar").val()) < grandTotal) {
					enough = false;
				}
			}
			if (!enough && ($("#tipe_pembayaran").find(':selected').data('jenis') != "kredit")) {
				swal.fire({
					type: 'error',
					title: 'Peringatan',
					text: "Uang terbayar kurang",
					showConfirmButton: false,
					timer: 1500
				});
				return false;
			}
			if ($("#tipe_pembayaran").find(':selected').data('jenis') == "kredit" && $("#tenggat_pelunasan").val() == "") {
				swal.fire({
					type: 'error',
					title: 'Peringatan',
					text: "Tenggat pembayaran tidak boleh kosong",
					showConfirmButton: false,
					timer: 1500
				});
				return false;
			}
			form.validate({});
			if (!form.valid()) {
				return false;
			}
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						console.log(data)
						$("#kt_modal_pembayaran").modal("hide");
						if ($("#tipe_pembayaran").find(":selected").data("kembalian") == 1) {
							Swal.fire({
								type: 'success',
								title: 'Rp. ' + data.kembalian,
								text: 'sisa kembalian'
							}).then(function () {
								form.clearForm();
								form.validate().resetForm();
								var url = data.url;
								window.open(url, "_blank");
								clearForm();
							});
						} else {
							Swal.fire({
								type: 'success',
								title: 'Transaksi Berhasil',
							}).then(function () {
								form.clearForm();
								form.validate().resetForm();
								var url = data.url;
								window.open(url, "_blank");
								clearForm();
							});
						}
					} else {
						console.log(data);
						swal.fire({
							type: 'error',
							title: 'Peringatan',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		})
		$("#btn-save-kas").click(function () {
			var btn = $(this);
			var form = $("#save-form");
			form.validate({});
			if (!form.valid()) {
				return false;
			}
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function (response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
						swal.fire({
							type: 'success',
							title: 'Berhasil melakukan opening POS',
						}).then(function () {
							window.location.reload()
						});
					} else {
						swal.fire({
							type: 'error',
							title: 'Peringatan',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}, error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		})
		$("#btn-closing").click(function () {
			$.ajax({
				type: "POST",
				url: base_url + 'pos/closing',
				cache: false,
				success: function (response) {
					var data = JSON.parse(response)
					if (data.success) {
						swal.fire({
							type: 'success',
							title: 'Berhasil melakukan closing',
							text: 'total uang tunai adalah Rp. ' + data.total_kas,
						}).then(function () {
							window.open(base_url + 'pos/print-closing/' + data.log_kasir_id, '__blank')
							window.location.reload()
						});

					} else {
						swal.fire({
							type: 'error',
							title: 'Peringatan',
							text: data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		})
		var clearForm = function () {
			$(".input-numeral").val(0);
			$("#total-item").html(0);
			$("#total-bayar").html(0);
			$("#grand-total").html(0);
			$("#grand-bayar").html(0);
			$("#tambahan").html(0);
			$("#tambahan-bayar").html(0);
			$("#potongan").html(0);
			$("#potongan-bayar").html(0);
			$("#potongan-bayar-persen").html(0 + '%');
			$("#item_child").html("");
			noCount = 1;
			noChose = 0;
			totalItem = 0;
			potongan = 0;
			potongan_persen = 0;
			nominal_potongan_persen = 0;
			tambahan = 0;
			grandTotal = 0;
			transaksi = {};
			textTrans = {};
			terbayar = 0;
			window.location.reload();
		}
		$("#tipe_pembayaran").change(function () {
			$("#input_tipe_pembayaran").val($(this).val());
			$("#input_jenis_pembayaran").val($(this).find(':selected').data('jenis'));
			if ($(this).find(':selected').data('kembalian') == 1 || $(this).find(':selected').data('jenis') == "kredit") {
				$("#terbayar").removeClass("read");
				$("#terbayar").addClass("input-numeral");
				$("#terbayar").val(0);
				terbayar = 0;
				getTotal()
			} else {
				$("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
				terbayar = grandTotal;
				getTotal()
				$("#terbayar").addClass("read");
				$("#terbayar").removeClass("input-numeral");
			}
			if ($(this).find(':selected').data('jenis') == "kredit") {
				$("#kredit_container").css('display', 'block');
				$("#piutang-tunai").prop("checked", true);
				$('[name="jenis_piutang"]').val('tunai');
				$("#terbayar").removeClass("read");
				$("#terbayar").addClass("input-numeral");
				$("#terbayar").val(0);
				terbayar = 0;
				getTotal()
				// $("#terbayar_container").css('display','none');

			} else {
				$("#kredit_container").css('display', 'none');
				$("#terbayar_container").css('display', 'flex');
			}
			if ($(this).find(':selected').data('additional') == 1) {
				console.log(123)
				$(".additional").removeClass("read");
				$("#additional_container").css('display', 'block');
			} else {
				console.log(321)
				$(".additional").addClass("read");
				$(".additional").val("");
				$("#additional_container").css('display', 'none');
			}
		})
		$('#tenggat_pelunasan').change(function () {
			$('#input_tenggat_pelunasan').val($(this).val());
		})
		$('#item-container').on('keydown', '.readonly', function (e) {
			e.preventDefault();
		});
		$(".readonly").keydown(function (e) {
			if ($("#tipe_pembayaran").find(":selected").data("kembalian") == 0 && $("#tipe_pembayaran").find(":selected").data("jenis") != "kredit") {
				e.preventDefault()
			}

		})
		$('.additional').change(function () {
			var name = $(this).attr("name");
			var value = $(this).val();
			$("input[name=" + name + "]").val(value)
		})
		$("#lokasi_id").change(function () {
			var id = $(this).val();
			sess_change_lokasi(id)
			$(".input-numeral").val(0);
			$("#total-item").html(0);
			$("#total-bayar").html(0);
			$("#grand-total").html(0);
			$("#grand-bayar").html(0);
			$("#tambahan").html(0);
			$("#tambahan-bayar").html(0);
			$("#potongan").html(0);
			$("#potongan-bayar").html(0);
			$("#potongan-bayar-persen").html(0 + '%');
			$("#item_child").html("");
			noCount = 1;
			noChose = 0;
			totalItem = 0;
			potongan = 0;
			potongan_persen = 0;
			nominal_potongan_persen = 0;
			tambahan = 0;
			grandTotal = 0;
			transaksi = {};
			textTrans = {};
			terbayar = 0;
		})

		function sess_change_lokasi(key) {
			$.ajax({
				type: "POST",
				url: base_url + 'pos/utility/change-location',
				data: {'lokasi_id': key},
				cache: false,
				success: function (response) {
					console.log(response)
					produk_table.ajax.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
	}
	return {
		// public functions
		init: function () {
			general();
		}
	};
}();

// Class Initialization
jQuery(document).ready(function () {
	KTPOS.init();
	keterangan_tambahan();
	jenis_piutang();
});

function keterangan_tambahan() {
	$('#keterangan_tambahan').on('change keyup', function () {
		var keterangan = $(this).val();

		$('#input_keterangan').val(keterangan);
	})
}

document.addEventListener('DOMContentLoaded', () => {
	$('.input-numeral').toArray().forEach(function (field) {
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})
	});
});

function jenis_piutang() {
	$('[name="jenis_piutang_box"]').on('change', function () {
		if ($(this).prop("checked")) {
			$('[name="jenis_piutang"]').val($(this).val());
		}
		console.log($(this).val());
	})
}
