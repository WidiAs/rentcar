-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Jul 2021 pada 06.24
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_balioz`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `arus_stock_bahan`
--

CREATE TABLE `arus_stock_bahan` (
  `arus_stock_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `po_bahan_detail_id` int(11) DEFAULT NULL,
  `po_bahan` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT 'insert',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `arus_stock_bahan`
--

INSERT INTO `arus_stock_bahan` (`arus_stock_bahan_id`, `tanggal`, `table_name`, `stock_bahan_id`, `po_bahan_detail_id`, `po_bahan`, `bahan_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `keterangan`, `method`, `created_at`, `updated_at`) VALUES
(139, '2019-10-04', 'stock_bahan', 36, NULL, NULL, 6, 100, 0, 100, 100, 'Insert stok bahan', 'insert', '2019-10-04 02:27:56', '2019-10-04 02:27:56'),
(140, '2019-10-15', 'stock_bahan', 36, NULL, NULL, 6, 0, NULL, 100, 100, 'Produksi produk', 'update', '2019-10-15 08:22:26', '2019-10-15 08:22:26'),
(141, '2019-10-15', 'stock_bahan', 36, NULL, NULL, 6, 0, NULL, 100, 100, 'Produksi produk', 'update', '2019-10-15 08:22:26', '2019-10-15 08:22:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `arus_stock_produk`
--

CREATE TABLE `arus_stock_produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` float DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `arus_stock_produk`
--

INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(133, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 10, NULL, 0, 0, 10, 10, 'insert', 'Insert stok produk', '2019-10-03 04:55:52', '2019-10-03 04:55:52'),
(134, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 10, NULL, 0, NULL, NULL, 10, 'delete', 'Delete stok produk', '2019-10-03 05:12:14', '2019-10-03 05:12:14'),
(135, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 10, NULL, 0, NULL, NULL, 10, 'delete', 'Delete stok produk', '2019-10-03 05:12:19', '2019-10-03 05:12:19'),
(136, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 25, NULL, 0, 0, 10, 10, 'insert', 'Insert stok produk', '2019-10-03 07:31:54', '2019-10-03 07:31:54'),
(137, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 25, NULL, 0, 2, 8, 8, 'delete', 'Penjualan produk', '2019-10-03 07:32:32', '2019-10-03 07:32:32'),
(138, '2019-10-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 25, NULL, 0, NULL, NULL, 8, 'delete', 'Delete stok produk', '2019-10-03 07:36:11', '2019-10-03 07:36:11'),
(139, '2019-10-04', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 188, NULL, 0, 0, 10, 18, 'insert', 'Insert stok produk', '2019-10-04 00:50:11', '2019-10-04 00:50:11'),
(140, '2019-10-04', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 188, NULL, 0, 4, 6, 14, 'delete', 'Penjualan produk', '2019-10-04 01:49:23', '2019-10-04 01:49:23'),
(141, '2019-10-04', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 188, NULL, 0, 4, 2, 10, 'update', 'Transfer stok produk', '2019-10-04 01:59:30', '2019-10-04 01:59:30'),
(142, '2019-10-04', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 188, NULL, 0, 1, 1, 9, 'delete', 'Penjualan produk', '2019-10-04 02:15:24', '2019-10-04 02:15:24'),
(143, '2019-10-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 188, NULL, 0, 2, -1, 7, 'delete', 'Penjualan produk', '2019-10-08 06:52:28', '2019-10-08 06:52:28'),
(144, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 3, NULL, 7, 'delete', 'Penjualan produk', '2019-10-10 02:37:55', '2019-10-10 02:37:55'),
(145, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, 7, 'delete', 'Penjualan produk', '2019-10-10 02:37:55', '2019-10-10 02:37:55'),
(146, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 3, NULL, 7, 'delete', 'Penjualan produk', '2019-10-10 02:47:21', '2019-10-10 02:47:21'),
(147, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 7, 'delete', 'Penjualan produk', '2019-10-10 02:47:21', '2019-10-10 02:47:21'),
(148, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 0, 9, 17, 'insert', 'Insert stok produk', '2019-10-10 08:07:16', '2019-10-10 08:07:16'),
(149, '2019-10-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 2, 7, 15, 'delete', 'Penjualan produk', '2019-10-10 08:07:37', '2019-10-10 08:07:37'),
(150, '2019-10-15', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 192, NULL, 0, 0, 1, 16, 'insert', 'Insert stok produk', '2019-10-15 06:58:07', '2019-10-15 06:58:07'),
(151, '2019-10-15', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 192, NULL, 0, 1, 0, 15, 'delete', 'Penjualan produk', '2019-10-15 07:04:55', '2019-10-15 07:04:55'),
(152, '2019-10-15', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 1, 6, 14, 'delete', 'Penjualan produk', '2019-10-15 07:06:14', '2019-10-15 07:06:14'),
(153, '2019-10-18', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 0, 10, 24, 'insert', 'Insert stok produk', '2019-10-18 02:57:29', '2019-10-18 02:57:29'),
(154, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 2, 4, 22, 'delete', 'Penjualan produk', '2019-10-20 08:49:52', '2019-10-20 08:49:52'),
(155, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 0, 4, 22, 'delete', 'Penjualan produk', '2019-10-20 08:49:52', '2019-10-20 08:49:52'),
(156, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 1, 3, 21, 'delete', 'Penjualan produk', '2019-10-20 13:00:40', '2019-10-20 13:00:40'),
(157, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 2, 1, 19, 'delete', 'Penjualan produk', '2019-10-20 13:08:46', '2019-10-20 13:08:46'),
(158, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 1, 0, 18, 'delete', 'Penjualan produk', '2019-10-20 13:10:04', '2019-10-20 13:10:04'),
(159, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 1, 9, 17, 'delete', 'Penjualan produk', '2019-10-20 13:11:00', '2019-10-20 13:11:00'),
(160, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 1, 8, 16, 'delete', 'Penjualan produk', '2019-10-20 13:13:04', '2019-10-20 13:13:04'),
(161, '2019-10-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 1, 7, 15, 'delete', 'Penjualan produk', '2019-10-20 13:14:19', '2019-10-20 13:14:19'),
(162, '2019-10-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 11, NULL, 2, 0, 9, 17, 'insert', 'Produksi produk', '2019-10-21 07:28:25', '2019-10-21 07:28:25'),
(163, '2019-10-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 3, 6, 14, 'delete', 'Penjualan produk', '2019-10-21 07:32:57', '2019-10-21 07:32:57'),
(164, '2019-10-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 11, NULL, 0, 1, 5, 13, 'delete', 'Penjualan produk', '2019-10-26 03:45:47', '2019-10-26 03:45:47'),
(165, '2019-10-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 2, 3, 11, 'delete', 'Penjualan produk', '2019-10-26 04:15:26', '2019-10-26 04:15:26'),
(166, '2019-10-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 1, 2, 10, 'delete', 'Penjualan produk', '2019-10-26 04:21:14', '2019-10-26 04:21:14'),
(167, '2019-11-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 1, 3, 11, 'delete', 'Penjualan produk', '2019-11-22 02:09:20', '2019-11-22 02:09:20'),
(168, '2019-11-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 11, NULL, 0, 2, 1, 9, 'delete', 'Penjualan produk', '2019-11-27 07:02:33', '2019-11-27 07:02:33'),
(169, '2021-06-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 188, NULL, 0, 1, -1, 8, 'delete', 'Penjualan produk', '2021-06-29 02:15:10', '2021-06-29 02:15:10'),
(170, '2021-07-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 11, NULL, 0, 5, -4, 3, 'delete', 'Penjualan produk', '2021-07-08 03:26:46', '2021-07-08 03:26:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan`
--

CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahan`
--

INSERT INTO `bahan` (`bahan_id`, `bahan_jenis_id`, `bahan_satuan_id`, `bahan_suplier_id`, `bahan_kode`, `bahan_nama`, `bahan_minimal_stock`, `bahan_qty`, `bahan_last_stock`, `bahan_harga`, `bahan_keterangan`, `created_at`, `updated_at`) VALUES
(7, 4, 6, 5, '1', '100% COTTON  WHITE', 0, 0, NULL, NULL, '', '2019-10-17 12:07:16', '2019-10-17 04:08:43'),
(8, 4, 6, 6, '2', '100% COTTON CAMELL', 0, 0, NULL, NULL, '', '2019-10-17 12:08:36', '2019-10-17 04:08:36'),
(9, 4, 6, 6, '3', '100% COTTON BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:09:24', '2019-10-17 04:09:24'),
(10, 4, 6, 6, '4', '100% COTTON BROWN', 0, 0, NULL, NULL, '', '2019-10-17 12:11:48', '2019-10-17 04:11:48'),
(11, 4, 6, 6, '5', '100% COTTON GREEN', 0, 0, NULL, NULL, '', '2019-10-17 12:20:51', '2019-10-17 04:20:51'),
(12, 4, 6, 6, '6', '100% COTTON GREY', 0, 0, NULL, NULL, '', '2019-10-17 12:21:20', '2019-10-17 04:21:20'),
(13, 4, 6, 6, '7', '100% COTTON MAROON', 0, 0, NULL, NULL, '', '2019-10-17 12:22:00', '2019-10-17 04:22:00'),
(14, 4, 6, 6, '8', '100% COTTON TURQUISE', 0, 0, NULL, NULL, '', '2019-10-17 12:25:39', '2019-10-17 04:25:39'),
(15, 4, 6, 6, '9', '100% COTTON WHITE-LIGHT-BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:26:39', '2019-10-17 04:26:39'),
(16, 4, 6, 6, '10', '100% COTTON WHITE-DARK-BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:27:47', '2019-10-17 04:27:47'),
(17, 4, 6, 6, '11', '100% COTTON DK-GREEN DK-BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:29:48', '2019-10-17 04:29:48'),
(18, 4, 6, 6, '12', '100% COTTON ORANGE-YELLOW', 0, 0, NULL, NULL, '', '2019-10-17 12:31:42', '2019-10-17 04:31:42'),
(19, 4, 6, 6, '13', '100% COTTON YELLOW-GREEN-BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:35:30', '2019-10-17 04:35:30'),
(20, 4, 6, 6, '14', '100% COTTON BROWN-IVORY', 0, 0, NULL, NULL, '', '2019-10-17 12:36:06', '2019-10-17 04:36:06'),
(21, 4, 6, 6, '15', '100% COTTON WHITE-MAGENTHA', 0, 0, NULL, NULL, '', '2019-10-17 12:38:22', '2019-10-17 04:38:22'),
(22, 4, 6, 6, '16', '100% COTTON WHITE-GREEN', 0, 0, NULL, NULL, '', '2019-10-17 12:40:51', '2019-10-17 04:40:51'),
(23, 4, 6, 6, '17', '100% COTTON DK BROWN DK BLUE', 0, 0, NULL, NULL, '', '2019-10-17 12:42:49', '2019-10-17 04:42:49'),
(24, 4, 6, 6, '18', '100% COTTON WHITE/NAVY', 0, 0, NULL, NULL, '', '2019-10-17 12:45:18', '2019-10-17 04:45:18'),
(25, 4, 6, 6, '19', 'TC 300 PLAIN', 0, 0, NULL, NULL, '', '2019-10-17 13:01:40', '2019-10-17 05:01:40'),
(26, 5, 6, 6, '20', 'SILICONE', 0, 0, NULL, NULL, '', '2019-10-17 13:02:36', '2019-10-17 05:02:36'),
(27, 5, 6, 6, '21', 'MICROFIBER', 0, 0, NULL, NULL, '', '2019-10-17 13:02:56', '2019-10-17 05:02:56'),
(28, 5, 6, 6, '22', 'DACRON', 0, 0, NULL, NULL, '', '2019-10-17 13:03:16', '2019-10-17 05:03:16'),
(29, 5, 6, 6, '23', 'DACRON 3OZ', 0, 0, NULL, NULL, '', '2019-10-17 13:03:38', '2019-10-17 05:03:38'),
(30, 5, 6, 6, '24', 'DACRON 6OZ', 0, 0, NULL, NULL, '', '2019-10-17 13:04:17', '2019-10-17 05:04:17'),
(31, 5, 6, 6, '25', 'MICROFIBER 6OZ ', 0, 0, NULL, NULL, '', '2019-10-17 13:29:16', '2019-10-17 05:29:48'),
(32, 5, 6, 6, '26', 'MICROFIBER 8OZ', 0, 0, NULL, NULL, '', '2019-10-17 13:29:37', '2019-10-17 05:29:37'),
(33, 4, 6, 6, '27', 'TC 200 PALIN', 0, 0, NULL, NULL, '', '2019-10-17 13:30:59', '2019-10-17 05:30:59'),
(34, 4, 6, 5, '28', 'TC 260 PLAIN', 0, 0, NULL, NULL, '', '2019-10-17 13:32:16', '2019-10-17 05:32:16'),
(35, 4, 6, 5, '29', 'TC 300 SQUARE', 0, 0, NULL, NULL, '', '2019-10-17 13:34:18', '2019-10-17 05:34:18'),
(36, 4, 6, 6, '30', 'CVC TC 200 PLAIN', 0, 0, NULL, NULL, '', '2019-10-17 13:35:24', '2019-10-17 05:35:24'),
(37, 4, 6, 5, '31', 'MATRASS PROTECTOR', 0, 0, NULL, NULL, '', '2019-10-17 13:36:27', '2019-10-17 05:36:27'),
(38, 4, 6, 5, '32', 'PILLLOW PROTECTOR', 0, 0, NULL, NULL, '', '2019-10-17 13:36:51', '2019-10-17 05:36:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `color`
--

CREATE TABLE `color` (
  `color_id` int(11) NOT NULL,
  `color_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `color`
--

INSERT INTO `color` (`color_id`, `color_nama`, `created_at`, `updated_at`) VALUES
(2, 'WHITE', '2019-10-03 12:10:23', '2019-10-03 04:10:23'),
(3, 'CAMEL', '2019-10-03 12:10:28', '2019-10-03 04:10:28'),
(4, 'BLUE', '2019-10-03 12:10:36', '2019-10-03 04:10:36'),
(5, 'BROWN', '2019-10-03 12:10:46', '2019-10-03 04:10:46'),
(6, 'GREY', '2019-10-03 12:10:50', '2019-10-03 04:10:50'),
(7, 'GREEN', '2019-10-03 12:15:23', '2019-10-03 04:15:23'),
(8, 'MAROON', '2019-10-03 12:15:37', '2019-10-03 04:15:37'),
(9, 'TURQUISE', '2019-10-03 12:15:50', '2019-10-03 04:15:50'),
(10, 'WHITE-LIGHT BLUE', '2019-10-03 12:19:21', '2019-10-03 04:19:21'),
(11, 'WHITE-DARK BLUE', '2019-10-03 12:19:35', '2019-10-03 04:19:35'),
(12, 'DK GREEN-DK BLUE', '2019-10-03 12:19:54', '2019-10-03 04:19:54'),
(13, 'ORANGE YELLOW', '2019-10-03 12:20:04', '2019-10-03 04:20:04'),
(14, 'YELLOW-GREEN-BLUE', '2019-10-03 12:20:28', '2019-10-03 04:20:28'),
(15, 'BROWN-IVORY', '2019-10-03 12:20:48', '2019-10-03 04:20:48'),
(16, 'WHITE-MAGENTA', '2019-10-03 12:20:57', '2019-10-03 04:20:57'),
(17, 'WHITE-GREEN', '2019-10-03 12:21:07', '2019-10-03 04:21:07'),
(18, 'DK BROWN-DK BLUE', '2019-10-03 12:21:19', '2019-10-03 04:21:19'),
(19, 'WHITE-NAVY', '2019-10-03 12:21:36', '2019-10-03 04:21:36'),
(20, 'WHITE/QUILTED', '2019-10-03 14:42:59', '2019-10-03 06:42:59'),
(21, '-', '2019-10-03 17:06:42', '2019-10-03 09:06:42');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `display_stock_bahan`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `display_stock_bahan` (
`bahan_id` int(11)
,`jumlah` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `display_stock_bahan_lokasi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `display_stock_bahan_lokasi` (
`bahan_id` int(11)
,`jumlah` decimal(41,0)
,`stock_bahan_lokasi_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `display_stock_produk`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `display_stock_produk` (
`produk_id` int(11)
,`jumlah` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `display_stock_produk_lokasi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `display_stock_produk_lokasi` (
`produk_id` int(11)
,`jumlah` decimal(41,0)
,`stock_produk_lokasi_id` int(11)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fabric`
--

CREATE TABLE `fabric` (
  `fabric_id` int(11) NOT NULL,
  `fabric_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fabric`
--

INSERT INTO `fabric` (`fabric_id`, `fabric_nama`, `created_at`, `updated_at`) VALUES
(2, '100 % Cotton', '2019-10-03 12:09:01', '2019-10-03 04:09:01'),
(3, 'SILICONE', '2019-10-03 13:24:26', '2019-10-03 05:24:26'),
(4, 'MICROFIBER', '2019-10-03 13:24:35', '2019-10-03 05:24:35'),
(5, 'DACRON', '2019-10-03 13:24:42', '2019-10-04 02:47:25'),
(6, 'DACRON 3OZ', '2019-10-03 13:24:52', '2019-10-04 02:47:19'),
(7, 'DACRON 6OZ', '2019-10-03 13:25:01', '2019-10-04 02:47:12'),
(8, 'MICROFIBER 6OZ', '2019-10-03 13:25:17', '2019-10-03 05:25:17'),
(9, 'MICROFIBER 8OZ', '2019-10-03 13:25:29', '2019-10-03 05:25:29'),
(10, '-', '2019-10-03 17:07:16', '2019-10-03 09:07:16'),
(11, 'TC 260 Stripe', '2019-10-08 07:45:54', '2019-10-07 23:49:51'),
(12, 'TC 300 Square', '2019-10-08 07:46:31', '2019-10-07 23:49:36'),
(13, 'TC 300 Plain', '2019-10-08 07:51:18', '2019-10-07 23:51:18'),
(14, 'CVC TC 200', '2019-10-08 07:51:40', '2019-10-07 23:51:40'),
(15, 'TC 200 Plain', '2019-10-08 07:52:00', '2019-10-07 23:52:00'),
(16, 'Matras Protector', '2019-10-08 07:52:59', '2019-10-07 23:52:59'),
(17, 'Pillow Protector', '2019-10-08 07:53:11', '2019-10-07 23:53:11'),
(18, 'TC 260 Plain', '2019-10-08 11:02:52', '2019-10-08 03:02:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `forget_request`
--

CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guest`
--

CREATE TABLE `guest` (
  `guest_id` int(11) NOT NULL,
  `guest_nama` varchar(50) DEFAULT NULL,
  `guest_alamat` varchar(50) DEFAULT NULL,
  `guest_telepon` varchar(50) DEFAULT NULL,
  `kewarganegaraan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga_produk`
--

CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `minimal_pembelian` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `harga_produk`
--

INSERT INTO `harga_produk` (`harga_produk_id`, `produk_id`, `minimal_pembelian`, `harga`, `created_at`, `updated_at`) VALUES
(9, 68, 26, 177000, '2019-10-03 17:08:17', '2019-10-03 09:08:17'),
(10, 70, 26, 186000, '2019-10-03 17:14:21', '2019-10-03 09:14:21'),
(11, 74, 26, 204000, '2019-10-03 17:18:03', '2019-10-03 09:18:03'),
(12, 78, 26, 222000, '2019-10-03 17:21:42', '2019-10-03 09:21:42'),
(13, 80, 26, 240000, '2019-10-03 17:26:26', '2019-10-03 09:26:26'),
(14, 83, 26, 249000, '2019-10-03 17:29:00', '2019-10-03 09:29:00'),
(15, 85, 26, 258000, '2019-10-03 17:31:50', '2019-10-03 09:31:50'),
(16, 87, 26, 268000, '2019-10-03 17:34:21', '2019-10-03 09:34:21'),
(17, 88, 26, 277000, '2019-10-03 17:38:04', '2019-10-03 09:38:04'),
(18, 92, 26, 164000, '2019-10-03 17:43:12', '2019-10-03 09:43:12'),
(19, 94, 26, 174000, '2019-10-03 17:44:28', '2019-10-03 09:44:28'),
(20, 95, 26, 191000, '2019-10-03 17:45:57', '2019-10-03 09:45:57'),
(21, 97, 26, 209000, '2019-10-03 17:47:12', '2019-10-03 09:47:12'),
(22, 100, 26, 227000, '2019-10-03 17:48:33', '2019-10-03 09:48:33'),
(23, 102, 26, 246000, '2019-10-03 17:49:32', '2019-10-03 09:49:32'),
(24, 105, 26, 264000, '2019-10-03 17:50:37', '2019-10-03 09:50:37'),
(25, 111, 26, 326000, '2019-10-03 17:55:27', '2019-10-03 09:55:27'),
(26, 113, 26, 349000, '2019-10-03 17:56:37', '2019-10-03 09:56:37'),
(27, 115, 26, 362000, '2019-10-03 17:58:11', '2019-10-03 09:58:11'),
(28, 118, 26, 398000, '2019-10-03 17:59:40', '2019-10-03 09:59:40'),
(29, 121, 26, 434000, '2019-10-03 18:01:01', '2019-10-03 10:01:01'),
(30, 122, 26, 470000, '2019-10-03 18:06:55', '2019-10-03 10:06:55'),
(31, 124, 26, 480000, '2019-10-03 18:10:38', '2019-10-03 10:10:38'),
(32, 126, 26, 493000, '2019-10-03 18:11:49', '2019-10-03 10:11:49'),
(33, 129, 26, 37000, '2019-10-03 18:14:49', '2019-10-03 10:14:49'),
(34, 131, 26, 44000, '2019-10-03 18:16:38', '2019-10-03 10:16:38'),
(35, 135, 26, 53000, '2019-10-03 18:20:44', '2019-10-03 10:20:44'),
(36, 137, 26, 56000, '2019-10-03 18:23:00', '2019-10-03 10:23:00'),
(37, 142, 26, 42000, '2019-10-03 18:27:25', '2019-10-03 10:27:25'),
(38, 145, 26, 48000, '2019-10-03 18:28:33', '2019-10-03 10:28:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_penyesuaian_bahan`
--

CREATE TABLE `history_penyesuaian_bahan` (
  `history_penyesuaian_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jenis_bahan_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_bahan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_penyesuaian_produk`
--

CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_transfer_bahan`
--

CREATE TABLE `history_transfer_bahan` (
  `history_transfer_bahan_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_nama` text COLLATE utf8mb4_unicode_ci,
  `bahan_seri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_transfer_produk`
--

CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `history_transfer_produk`
--

INSERT INTO `history_transfer_produk` (`history_transfer_produk_id`, `tanggal`, `tanggal_konfirmasi`, `produk_id`, `stock_produk_id`, `produk_kode`, `produk_nama`, `produk_seri`, `history_transfer_qty`, `qty_terima`, `histori_lokasi_awal_id`, `histori_lokasi_tujuan_id`, `dari`, `tujuan`, `last_stock`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(19, '2019-10-04', NULL, 188, 40, 'BOPC-200P-595F', 'PILLOW CASE - GS : 50 X 90 - BS : 50 X 95 -', '1019008021', 4, 0, 1, 5, 'Gudang', 'Balioz Sanur', 2, 'Menunggu Konfirmasi', NULL, '2019-10-04 01:59:30', '2019-10-04 01:59:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hutang`
--

CREATE TABLE `hutang` (
  `hutang_id` int(11) NOT NULL,
  `po_bahan_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_bahan`
--

CREATE TABLE `jenis_bahan` (
  `jenis_bahan_id` int(11) NOT NULL,
  `jenis_bahan_kode` varchar(10) DEFAULT NULL,
  `jenis_bahan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_bahan`
--

INSERT INTO `jenis_bahan` (`jenis_bahan_id`, `jenis_bahan_kode`, `jenis_bahan_nama`, `created_at`, `updated_at`) VALUES
(4, '001', 'Kain', '2019-10-04 10:26:14', '2019-10-17 04:02:51'),
(5, '002', 'Linen', '2019-10-17 12:02:45', '2019-10-17 04:02:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_produk`
--

CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_produk`
--

INSERT INTO `jenis_produk` (`jenis_produk_id`, `jenis_produk_kode`, `jenis_produk_nama`, `created_at`, `updated_at`) VALUES
(9, '001', 'PILLOW', '2019-10-03 12:44:08', '2019-10-03 04:44:08'),
(10, '002', 'BOLSTER', '2019-10-03 12:44:23', '2019-10-03 04:44:23'),
(11, '003', 'CUSHION INSERT', '2019-10-03 12:44:45', '2019-10-03 04:44:45'),
(12, '004', 'PILLOW PROTECTOR', '2019-10-03 13:26:43', '2019-10-03 05:26:43'),
(13, '005', 'MATTRESS PROTECTOR', '2019-10-03 13:29:11', '2019-10-03 05:29:11'),
(14, '006', 'INNER DUVET', '2019-10-03 13:29:33', '2019-10-03 05:29:33'),
(15, '007', 'DUVET COVER', '2019-10-03 13:30:02', '2019-10-03 05:30:02'),
(16, '008', 'PILLOW CASE', '2019-10-03 13:30:12', '2019-10-03 05:30:12'),
(17, '009', 'BOLSTER CASE', '2019-10-03 13:30:24', '2019-10-03 05:30:24'),
(18, '010', 'BEDSHEET', '2019-10-03 13:31:14', '2019-10-03 05:31:14'),
(19, '011', 'OHIBORY', '2019-10-03 13:32:17', '2019-10-03 05:32:17'),
(20, '012', 'FACE TOWEL', '2019-10-03 13:32:27', '2019-10-03 05:32:27'),
(21, '013', 'HAND TOWEL', '2019-10-03 13:32:38', '2019-10-03 05:32:38'),
(22, '014', 'BATH MAT', '2019-10-03 13:32:48', '2019-10-03 05:32:48'),
(23, '015', 'BATH TOWEL', '2019-10-03 13:33:01', '2019-10-03 05:33:01'),
(24, '016', 'MEDIUM TOWEL', '2019-10-03 13:33:13', '2019-10-03 05:33:13'),
(25, '017', 'LARGE TOWEL', '2019-10-03 13:33:31', '2019-10-03 05:33:31'),
(26, '018', 'POOL TOWEL', '2019-10-03 13:33:41', '2019-10-03 05:33:41'),
(27, '000', 'WASH LAP', '2019-10-03 15:02:09', '2019-10-03 07:02:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komposisi_produk`
--

CREATE TABLE `komposisi_produk` (
  `komposisi_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `komposisi_qty` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `konversi_bahan`
--

CREATE TABLE `konversi_bahan` (
  `konversi_bahan_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konversi_bahan`
--

INSERT INTO `konversi_bahan` (`konversi_bahan_id`, `produk_id`, `bahan_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(2, 11, 26, 5, '2019-10-17 14:19:18', NULL),
(3, 11, 33, 0.4, '2019-10-17 14:19:59', NULL),
(4, 68, 34, 2.5, '2019-10-17 14:20:27', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_aktivitas`
--

CREATE TABLE `log_aktivitas` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama_menu` varchar(0) DEFAULT NULL,
  `aktivitas` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_kasir`
--

CREATE TABLE `log_kasir` (
  `log_kasir_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `waktu_buka` datetime DEFAULT NULL,
  `waktu_tutup` datetime DEFAULT NULL,
  `kas_awal` bigint(20) DEFAULT NULL,
  `kas_akhir` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log_kasir`
--

INSERT INTO `log_kasir` (`log_kasir_id`, `user_id`, `waktu_buka`, `waktu_tutup`, `kas_awal`, `kas_akhir`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-10-07 16:13:27', '2019-10-07 16:14:23', 200000, 200000, '2019-10-07 16:13:27', '2019-10-07 16:14:23'),
(2, 1, '2019-10-07 16:14:52', '2019-10-07 16:14:55', 0, 0, '2019-10-07 16:14:52', '2019-10-07 16:14:55'),
(3, 1, '2019-10-08 14:51:25', NULL, 200000, NULL, '2019-10-08 14:51:25', NULL),
(4, 1, '2019-10-09 11:00:30', '2019-10-09 11:18:00', 200000, 200000, '2019-10-09 11:00:30', '2019-10-09 11:18:00'),
(5, 1, '2019-10-09 11:21:26', '2019-10-09 11:21:33', 20000, 20000, '2019-10-09 11:21:26', '2019-10-09 11:21:33'),
(6, 1, '2019-10-09 11:21:46', NULL, 200000, NULL, '2019-10-09 11:21:46', NULL),
(7, 1, '2019-10-10 10:26:43', NULL, 200000, NULL, '2019-10-10 10:26:43', NULL),
(8, 1, '2019-10-10 14:25:52', NULL, 200000, NULL, '2019-10-10 14:25:52', NULL),
(9, 1, '2019-10-15 14:19:56', NULL, 50000, NULL, '2019-10-15 14:19:56', NULL),
(10, 1, '2019-10-20 15:32:58', NULL, 20000, NULL, '2019-10-20 15:32:58', NULL),
(11, 1, '2019-10-20 20:30:37', NULL, 0, NULL, '2019-10-20 20:30:37', NULL),
(12, 1, '2019-10-21 15:28:58', NULL, 200000, NULL, '2019-10-21 15:28:58', NULL),
(13, 1, '2019-10-26 11:45:35', NULL, 20000, NULL, '2019-10-26 11:45:35', NULL),
(14, 1, '2019-11-22 10:09:08', NULL, 200000, NULL, '2019-11-22 10:09:08', NULL),
(15, 1, '2019-11-27 15:02:20', NULL, 0, NULL, '2019-11-27 15:02:20', NULL),
(16, 1, '2021-06-28 18:08:23', NULL, 0, NULL, '2021-06-28 18:08:23', NULL),
(17, 1, '2021-06-29 10:04:50', NULL, 0, NULL, '2021-06-29 10:04:50', NULL),
(18, 1, '2021-07-08 11:25:58', '2021-07-08 11:26:06', 1000, 1000, '2021-07-08 11:25:58', '2021-07-08 11:26:06'),
(19, 1, '2021-07-08 11:26:15', NULL, 1000, NULL, '2021-07-08 11:26:15', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi`
--

CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi`
--

INSERT INTO `lokasi` (`lokasi_id`, `lokasi_kode`, `lokasi_nama`, `lokasi_tipe`, `lokasi_alamat`, `lokasi_telepon`, `created_at`, `updated_at`) VALUES
(1, '02', 'Gudang', 'Gudang', 'Gang nusa kambanagan', '0361 223456', '2019-05-05 15:58:24', '2019-08-12 09:29:36'),
(5, '001', 'Balioz Sanur', 'Toko', 'Sanur', '+62---', '2019-10-04 09:53:08', '2019-10-04 01:53:56'),
(6, '002', 'Balioz Jimbaran', 'Toko', 'Jimbaran', '+62---', '2019-10-04 09:53:50', '2019-10-04 01:53:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_kode` varchar(50) DEFAULT NULL,
  `menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_kode`, `menu_nama`, `action`, `url`, `keterangan`, `icon`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 'pos', 'POS', NULL, 'pos', NULL, 'cart', 1, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(2, 'master_data', 'Master Data', NULL, '#', NULL, 'master', 3, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(3, 'guest', 'Guest', 'list|add|edit|delete', 'guest', NULL, 'address-book', 4, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(4, 'inventori', 'Inventori', '', '#', NULL, 'commode', 5, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(5, 'produksi', 'produksi', '', '#', NULL, 'hammers', 7, '2019-06-18 03:13:50', '2019-10-10 08:00:10'),
(6, 'laporan', 'Laporan', NULL, '#', NULL, 'clipboards', 9, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(7, 'dashboard', 'Dashboard', NULL, 'home', NULL, 'layers', 2, '2019-06-18 04:12:56', '2019-06-17 20:12:56'),
(8, 'order_bahan', 'Order Bahan', 'list|add|edit|delete|view', 'po-bahan', NULL, 'order', 6, '2019-06-29 11:14:43', '2019-06-29 03:14:43'),
(9, 'hutang_piutang', 'Hutang/Piutang', NULL, '#', NULL, 'debt', 8, '2019-07-08 10:58:16', '2019-07-08 02:58:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `patern`
--

CREATE TABLE `patern` (
  `patern_id` int(11) NOT NULL,
  `patern_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `patern`
--

INSERT INTO `patern` (`patern_id`, `patern_nama`, `created_at`, `updated_at`) VALUES
(3, '-', '2019-10-03 14:17:50', '2019-10-03 07:49:34'),
(5, 'PLAIN', '2019-10-03 14:58:18', '2019-10-03 06:58:18'),
(6, 'STRIPE', '2019-10-03 14:58:32', '2019-10-03 06:58:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_hutang`
--

CREATE TABLE `pembayaran_hutang` (
  `pembayaran_hutang_id` int(11) NOT NULL,
  `hutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_piutang`
--

CREATE TABLE `pembayaran_piutang` (
  `pembayaran_piutang_id` int(11) NOT NULL,
  `piutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `penjualan_id` int(11) NOT NULL,
  `ekspedisi_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `log_kasir_id` int(11) DEFAULT NULL,
  `nama_pelanggan` varchar(50) DEFAULT NULL,
  `alamat_pelanggan` varchar(100) DEFAULT NULL,
  `telepon_pelanggan` varchar(20) DEFAULT NULL,
  `urutan` bigint(20) DEFAULT NULL,
  `no_faktur` varchar(100) DEFAULT NULL,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `status_distribusi` enum('not_yet','done') DEFAULT 'not_yet',
  `tanggal` date DEFAULT NULL,
  `jenis_transaksi` enum('retail','distributor') DEFAULT 'distributor',
  `pengiriman` enum('ya','tidak') DEFAULT NULL,
  `total` float DEFAULT NULL,
  `biaya_tambahan` float DEFAULT NULL,
  `potongan_akhir` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `terbayar` float DEFAULT NULL,
  `sisa_pembayaran` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `additional_no` varchar(50) DEFAULT NULL,
  `additional_tanggal` date DEFAULT NULL,
  `additional_keterangan` text,
  `status_distribusi_done_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`penjualan_id`, `ekspedisi_id`, `lokasi_id`, `tipe_pembayaran_id`, `log_kasir_id`, `nama_pelanggan`, `alamat_pelanggan`, `telepon_pelanggan`, `urutan`, `no_faktur`, `status_pembayaran`, `status_distribusi`, `tanggal`, `jenis_transaksi`, `pengiriman`, `total`, `biaya_tambahan`, `potongan_akhir`, `grand_total`, `terbayar`, `sisa_pembayaran`, `kembalian`, `additional_no`, `additional_tanggal`, `additional_keterangan`, `status_distribusi_done_at`, `created_at`, `updated_at`) VALUES
(90, NULL, 1, 6, NULL, 'Guest', '', '', 1, 'PL-191004/00/1', 'Lunas', 'not_yet', '2019-10-04', 'distributor', 'tidak', 192000, 0, 0, 192000, 200000, 0, 8000, NULL, NULL, NULL, NULL, '2019-10-04 09:49:23', '2019-10-04 01:49:23'),
(91, NULL, 1, 6, NULL, 'Guest', '', '', 2, 'PL-191004/00/2', 'Lunas', 'not_yet', '2019-10-04', 'distributor', 'tidak', 48000, 0, 0, 48000, 100000, 0, 52000, NULL, NULL, NULL, NULL, '2019-10-04 10:15:24', '2019-10-04 02:15:24'),
(92, NULL, 1, 6, 3, 'Guest', '', '', 1, 'PL-191008/00/1', 'Lunas', 'not_yet', '2019-10-08', 'distributor', 'tidak', 96000, 0, 0, 96000, 100000, 0, 4000, NULL, NULL, NULL, NULL, '2019-10-08 14:52:28', '2019-10-08 06:52:28'),
(93, NULL, 1, 6, 7, 'Guest', '', '', 1, 'PL-191010/00/1', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 1660000, 0, 0, 1660000, 1700000, 0, 40000, NULL, NULL, NULL, NULL, '2019-10-10 10:37:55', '2019-10-10 02:37:55'),
(94, NULL, 1, 6, 7, 'Guest', '', '', 2, 'PL-191010/00/2', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 420000, 0, 0, 420000, 500000, 0, 80000, NULL, NULL, NULL, NULL, '2019-10-10 10:47:21', '2019-10-10 02:47:21'),
(95, NULL, 1, 6, 8, 'Guest', '', '', 3, 'PL-191010/00/3', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 80000, 0, 0, 80000, 100000, 0, 20000, NULL, NULL, NULL, NULL, '2019-10-10 14:28:01', '2019-10-10 06:28:01'),
(96, NULL, 1, 6, 8, 'Guest', '', '', 3, 'PL-191010/00/3', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 80000, 0, 0, 80000, 100000, 0, 20000, NULL, NULL, NULL, NULL, '2019-10-10 14:30:11', '2019-10-10 06:30:11'),
(97, NULL, 1, 6, 8, 'Guest', '', '', 3, 'PL-191010/00/3', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 80000, 0, 0, 80000, 100000, 0, 20000, NULL, NULL, NULL, NULL, '2019-10-10 14:30:54', '2019-10-10 06:30:54'),
(98, NULL, 1, 6, 8, 'Guest', '', '', 4, 'PL-191010/00/4', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 400000, 0, 0, 400000, 400000, 0, 0, NULL, NULL, NULL, NULL, '2019-10-10 15:00:53', '2019-10-10 07:00:53'),
(99, NULL, 1, 6, 8, 'Guest', '', '', 5, 'PL-191010/00/5', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 400000, 0, 0, 400000, 1000000, 0, 600000, NULL, NULL, NULL, NULL, '2019-10-10 15:25:09', '2019-10-10 07:25:09'),
(100, NULL, 1, 6, 8, 'Guest', '', '', 6, 'PL-191010/00/6', 'Lunas', 'not_yet', '2019-10-10', 'distributor', 'tidak', 96000, 0, 0, 96000, 100000, 0, 4000, NULL, NULL, NULL, NULL, '2019-10-10 16:07:37', '2019-10-10 08:07:37'),
(101, NULL, 1, 6, 9, 'Guest', '', '', 1, 'PL-191015/00/1', 'Lunas', 'not_yet', '2019-10-15', 'distributor', 'tidak', 300000, 0, 0, 300000, 300000, 0, 0, NULL, NULL, NULL, NULL, '2019-10-15 14:20:45', '2019-10-15 06:20:45'),
(102, NULL, 1, 6, 9, 'Guest', '', '', 2, 'PL-191015/00/2', 'Lunas', 'not_yet', '2019-10-15', 'distributor', 'tidak', 52000, 0, 0, 52000, 60000, 0, 8000, NULL, NULL, NULL, NULL, '2019-10-15 15:04:55', '2019-10-15 07:04:55'),
(103, NULL, 1, 6, 9, 'Guest', '', '', 3, 'PL-191015/00/3', 'Lunas', 'not_yet', '2019-10-15', 'distributor', 'tidak', 48000, 0, 0, 48000, 50000, 0, 2000, NULL, NULL, NULL, NULL, '2019-10-15 15:06:14', '2019-10-15 07:06:14'),
(104, NULL, 1, 7, 10, 'Guest', '', '', 1, 'PL-191020/00/1', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 96000, 0, 0, 96000, 96000, 0, 0, NULL, NULL, NULL, NULL, '2019-10-20 16:49:52', '2019-10-20 08:49:52'),
(105, NULL, 1, 7, 11, 'Guest', '', '', 2, 'PL-191020/00/2', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 48000, 0, 0, 48000, 48000, 0, 0, NULL, NULL, NULL, NULL, '2019-10-20 21:00:40', '2019-10-20 13:00:40'),
(106, NULL, 1, 7, 11, 'Guest', '', '', 3, 'PL-191020/00/3', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 96000, 0, 0, 96000, 96000, 0, 0, '123455', '0000-00-00', '', NULL, '2019-10-20 21:08:46', '2019-10-20 13:08:46'),
(107, NULL, 1, 7, 11, 'Guest', '', '', 4, 'PL-191020/00/4', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 48000, 0, 0, 48000, 48000, 0, 0, '123', '0000-00-00', '', NULL, '2019-10-20 21:10:04', '2019-10-20 13:10:04'),
(108, NULL, 1, 7, 11, 'Guest', '', '', 5, 'PL-191020/00/5', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 70000, 0, 0, 70000, 70000, 0, 0, '344', '0000-00-00', '', NULL, '2019-10-20 21:11:00', '2019-10-20 13:11:00'),
(109, NULL, 1, 7, 11, 'Guest', '', '', 6, 'PL-191020/00/6', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 70000, 0, 0, 70000, 70000, 0, 0, '3456', '0000-00-00', '', NULL, '2019-10-20 21:13:04', '2019-10-20 13:13:04'),
(110, NULL, 1, 7, 11, 'Guest', '', '', 7, 'PL-191020/00/7', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 70000, 0, 0, 70000, 70000, 0, 0, '45673', '2019-10-10', '', NULL, '2019-10-20 21:14:19', '2019-10-20 13:14:19'),
(111, NULL, 1, 6, 11, 'Guest', '', '', 8, 'PL-191020/00/8', 'Lunas', 'not_yet', '2019-10-20', 'distributor', 'tidak', 500000, 0, 0, 500000, 500000, 0, 0, '', '0000-00-00', '', NULL, '2019-10-20 21:31:43', '2019-10-20 13:31:43'),
(112, NULL, 1, 7, 12, 'Guest', '', '', 1, 'PL-191021/00/1', 'Lunas', 'not_yet', '2019-10-21', 'distributor', 'tidak', 840000, 0, 0, 840000, 840000, 0, 0, '666', '2019-10-22', '', NULL, '2019-10-21 15:31:39', '2019-10-21 07:31:39'),
(113, NULL, 1, 6, 12, 'Guest', '', '', 2, 'PL-191021/00/2', 'Lunas', 'not_yet', '2019-10-21', 'distributor', 'tidak', 210000, 0, 0, 210000, 300000, 0, 90000, '', '0000-00-00', '', NULL, '2019-10-21 15:32:57', '2019-10-21 07:32:57'),
(115, NULL, 1, 6, 13, 'Guest', '', '', 1, 'PL-191026/00/1', 'Lunas', 'not_yet', '2019-10-26', 'distributor', 'tidak', 140000, 0, 0, 140000, 150000, 0, 10000, '', '0000-00-00', '', NULL, '2019-10-26 12:15:26', '2019-10-26 04:15:26'),
(116, NULL, 1, 6, 14, 'Guest', '', '', 1, 'PL-191122/00/1', 'Lunas', 'not_yet', '2019-11-22', 'distributor', 'tidak', 70000, 0, 0, 70000, 100000, 0, 30000, '', '0000-00-00', '', NULL, '2019-11-22 10:09:20', '2019-11-22 02:09:20'),
(117, NULL, 1, 6, 15, 'Guest', '', '', 1, 'PL-191127/00/1', 'Lunas', 'not_yet', '2019-11-27', 'distributor', 'tidak', 140000, 0, 0, 140000, 150000, 0, 10000, '', '0000-00-00', '', NULL, '2019-11-27 15:02:33', '2019-11-27 07:02:33'),
(118, NULL, 1, 6, 17, 'Guest', '', '', 1, 'PL-210629/00/1', 'Lunas', 'not_yet', '2021-06-29', 'distributor', 'tidak', 48000, 0, 0, 48000, 50000, 0, 2000, '', '0000-00-00', '', NULL, '2021-06-29 10:15:10', '2021-06-29 02:15:10'),
(119, NULL, 1, 6, 19, 'Guest', 'jahnfjf', 'ksfnalk', 1, 'PL-210708/00/1', 'Lunas', 'not_yet', '2021-07-08', 'distributor', 'tidak', 350000, 0, 0, 350000, 400000, 0, 50000, '', '0000-00-00', '', NULL, '2021-07-08 11:26:46', '2021-07-08 03:26:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_produk`
--

CREATE TABLE `penjualan_produk` (
  `penjualan_produk_id` int(11) NOT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `produk_custom_id` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `hpp` float DEFAULT NULL,
  `potongan` float DEFAULT NULL,
  `ppn_persen` float DEFAULT NULL,
  `ppn_nominal` float DEFAULT NULL,
  `harga_net` float DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_produk`
--

INSERT INTO `penjualan_produk` (`penjualan_produk_id`, `penjualan_id`, `produk_id`, `produk_custom_id`, `qty`, `harga`, `hpp`, `potongan`, `ppn_persen`, `ppn_nominal`, `harga_net`, `stock_produk_id`, `sub_total`, `created_at`, `updated_at`) VALUES
(101, 90, 188, NULL, 4, 48000, 12000, NULL, 0.1, NULL, NULL, NULL, 192000, '2019-10-04 09:49:23', '2019-10-04 01:49:23'),
(102, 91, 188, NULL, 1, 48000, 12000, NULL, 0.1, NULL, NULL, NULL, 48000, '2019-10-04 10:15:24', '2019-10-04 02:15:24'),
(103, 92, 188, NULL, 2, 48000, 12000, NULL, 0.1, NULL, NULL, NULL, 96000, '2019-10-08 14:52:28', '2019-10-08 06:52:28'),
(111, 98, NULL, 4, 2, 200000, 0, NULL, 0.1, NULL, NULL, NULL, 400000, '2019-10-10 15:00:53', '2019-10-10 07:00:53'),
(112, 99, NULL, 5, 2, 200000, 0, NULL, 0.1, NULL, NULL, NULL, 400000, '2019-10-10 15:25:09', '2019-10-10 07:25:09'),
(113, 100, 188, NULL, 2, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 96000, '2019-10-10 16:07:37', '2019-10-10 08:07:37'),
(114, 101, NULL, 6, 6, 50000, 0, NULL, 0.1, NULL, NULL, NULL, 300000, '2019-10-15 14:20:45', '2019-10-15 06:20:45'),
(115, 102, 192, NULL, 1, 52000, 4, NULL, 0.1, NULL, NULL, NULL, 52000, '2019-10-15 15:04:55', '2019-10-15 07:04:55'),
(116, 103, 188, NULL, 1, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 48000, '2019-10-15 15:06:14', '2019-10-15 07:06:14'),
(117, 104, 188, NULL, 2, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 96000, '2019-10-20 16:49:52', '2019-10-20 08:49:52'),
(118, 104, 188, NULL, 0, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 0, '2019-10-20 16:49:52', '2019-10-20 08:49:52'),
(119, 105, 188, NULL, 1, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 48000, '2019-10-20 21:00:40', '2019-10-20 13:00:40'),
(120, 106, 188, NULL, 2, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 96000, '2019-10-20 21:08:46', '2019-10-20 13:08:46'),
(121, 107, 188, NULL, 1, 48000, 0, NULL, 0.1, NULL, NULL, NULL, 48000, '2019-10-20 21:10:04', '2019-10-20 13:10:04'),
(122, 108, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 70000, '2019-10-20 21:11:00', '2019-10-20 13:11:00'),
(123, 109, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 70000, '2019-10-20 21:13:04', '2019-10-20 13:13:04'),
(124, 110, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 70000, '2019-10-20 21:14:19', '2019-10-20 13:14:19'),
(125, 111, NULL, 7, 2, 250000, 0, NULL, 0.1, NULL, NULL, NULL, 500000, '2019-10-20 21:31:43', '2019-10-20 13:31:43'),
(126, 112, NULL, 8, 3, 280000, 0, NULL, 0.1, NULL, NULL, NULL, 840000, '2019-10-21 15:31:39', '2019-10-21 07:31:39'),
(127, 113, 11, NULL, 3, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 210000, '2019-10-21 15:32:57', '2019-10-21 07:32:57'),
(128, 114, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 70000, '2019-10-26 11:45:47', '2019-10-26 03:45:47'),
(129, 115, 11, NULL, 2, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 140000, '2019-10-26 12:15:26', '2019-10-26 04:15:26'),
(130, 116, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, 43, 70000, '2019-10-26 12:21:14', '2019-10-26 04:21:14'),
(131, 117, NULL, 9, 2, 200000, 0, NULL, 0.1, NULL, NULL, NULL, 400000, '2019-10-26 12:41:28', '2019-10-26 04:41:28'),
(132, 118, NULL, 10, 2, 200000, 0, NULL, 0.1, NULL, NULL, NULL, 400000, '2019-10-26 12:47:49', '2019-10-26 04:47:49'),
(133, 116, 11, NULL, 1, 70000, 0, NULL, 0.1, NULL, NULL, 43, 70000, '2019-11-22 10:09:20', '2019-11-22 02:09:20'),
(134, 117, 11, NULL, 2, 70000, 0, NULL, 0.1, NULL, NULL, 43, 140000, '2019-11-27 15:02:33', '2019-11-27 07:02:33'),
(135, 118, 188, NULL, 1, 48000, 0, NULL, 0.1, NULL, NULL, 41, 48000, '2021-06-29 10:15:10', '2021-06-29 02:15:10'),
(136, 119, 11, NULL, 5, 70000, 0, NULL, 0.1, NULL, NULL, NULL, 350000, '2021-07-08 11:26:46', '2021-07-08 03:26:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `piutang`
--

CREATE TABLE `piutang` (
  `piutang_id` int(11) NOT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_bahan`
--

CREATE TABLE `po_bahan` (
  `po_bahan_id` int(11) NOT NULL,
  `po_bahan_no` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `suplier_id` int(11) DEFAULT NULL,
  `tipe_pembayaran` int(11) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_no` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_keterangan` varchar(100) DEFAULT NULL,
  `pengiriman` varchar(50) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `tanggal_pemesanan` date DEFAULT NULL,
  `total` bigint(20) DEFAULT '0',
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `grand_total` bigint(20) DEFAULT '0',
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `status_penerimaan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `po_bahan`
--

INSERT INTO `po_bahan` (`po_bahan_id`, `po_bahan_no`, `urutan`, `suplier_id`, `tipe_pembayaran`, `tipe_pembayaran_nama`, `tipe_pembayaran_no`, `tipe_pembayaran_keterangan`, `pengiriman`, `tanggal_penerimaan`, `tanggal_pemesanan`, `total`, `tambahan`, `potongan`, `grand_total`, `status_pembayaran`, `jenis_pembayaran`, `lokasi_penerimaan_id`, `status_penerimaan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'POB/0220/1', 1, 7, 6, NULL, '123456789098765', '', NULL, NULL, '2019-08-02', 20000, 0, 0, 20000, 'Lunas', 'kas', NULL, 'Belum Diterima', '', '2020-02-04 16:53:54', '2020-02-04 08:53:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_bahan_detail`
--

CREATE TABLE `po_bahan_detail` (
  `po_bahan_detail_id` int(11) NOT NULL,
  `po_bahan_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `sub_total` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `po_bahan_detail`
--

INSERT INTO `po_bahan_detail` (`po_bahan_detail_id`, `po_bahan_id`, `bahan_id`, `jumlah`, `harga`, `tambahan`, `potongan`, `sub_total`, `created_at`, `updated_at`) VALUES
(1, 1, 34, 1, 20000, 0, 0, 20000, '2020-02-04 16:53:54', '2020-02-04 08:53:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `produk_satuan_id` int(11) DEFAULT NULL,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `fabric_id` int(11) DEFAULT NULL,
  `patern_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `bed_size_id` int(11) DEFAULT NULL,
  `good_size_id` int(11) DEFAULT NULL,
  `weight_pc` int(11) DEFAULT NULL,
  `weight_m` int(11) DEFAULT NULL,
  `style` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `harga_eceran` double DEFAULT '0',
  `harga_grosir` double DEFAULT '0',
  `harga_konsinyasi` double DEFAULT '0',
  `produk_minimal_stock` bigint(20) NOT NULL,
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`produk_id`, `produk_satuan_id`, `produk_jenis_id`, `produk_kode`, `barcode`, `fabric_id`, `patern_id`, `color_id`, `size_id`, `bed_size_id`, `good_size_id`, `weight_pc`, `weight_m`, `style`, `file`, `harga_eceran`, `harga_grosir`, `harga_konsinyasi`, `produk_minimal_stock`, `disc_persen`, `disc_nominal`, `created_at`, `updated_at`) VALUES
(11, 5, 9, 'BOPS-800-570', NULL, 3, 3, 2, 122, 0, 0, 0, 1, '', '', 70000, 0, 0, 0, 0, 0, '2019-10-03 14:48:25', '2019-10-08 05:59:08'),
(12, 5, 9, 'BOPS-1200-590', NULL, 3, 3, 2, 123, NULL, NULL, 0, 1, '', '', 89000, 0, 0, 0, 0, 0, '2019-10-03 14:52:19', '2019-10-08 05:58:45'),
(13, 5, 9, 'BOPM-1200-570', NULL, 4, 3, 2, 123, 0, 0, 1, 0, '', '', 175000, 0, 0, 0, 0, 0, '2019-10-03 14:58:30', '2019-10-08 05:59:40'),
(14, 5, 9, 'BOPM-1500-590', NULL, 4, 3, 2, 123, 0, 0, 1, 0, '', '', 195000, 0, 0, 0, 0, 0, '2019-10-03 15:03:12', '2019-10-08 05:59:29'),
(15, 5, 19, 'B00S-001-LWT', NULL, 2, 5, 2, 88, 0, 0, 25, 400, '', '16/1', 6000, 0, 0, 0, 0, 0, '2019-10-03 15:22:57', '2019-10-08 06:16:19'),
(16, 5, 10, 'BOBS-1000-90', NULL, 3, 3, 2, 124, 0, 0, 1, 0, '', '', 79000, 0, 0, 0, 0, 0, '2019-10-03 15:23:49', '2019-10-08 06:00:40'),
(17, 5, 10, 'BOBS-1200-100', NULL, 3, 3, 2, 125, 0, 0, 1, 0, '', '', 89000, 0, 0, 0, 0, 0, '2019-10-03 15:25:53', '2019-10-08 06:00:53'),
(18, 5, 20, 'BOFT-001-LWT', NULL, 2, 5, 2, 89, 0, 0, 50, 550, '', '30/2', 9500, 0, 0, 0, 0, 0, '2019-10-03 15:26:19', '2019-10-08 06:17:06'),
(19, 5, 11, 'BOCI-DC01-30', NULL, 5, 3, 2, 89, 0, 0, 0, 0, '', '', 30000, 0, 0, 0, 0, 0, '2019-10-03 15:27:24', '2019-10-08 06:01:30'),
(20, 5, 11, 'BOCI-DC01-40', NULL, 5, 3, 2, 127, 0, 0, 0, 0, '', '', 38000, 0, 0, 0, 0, 0, '2019-10-03 15:28:07', '2019-10-08 06:01:40'),
(21, 5, 20, 'BOFT-001-LCL', NULL, 2, 5, 3, 89, 0, 0, 50, 550, '', '30/2', 10000, 0, 0, 0, 0, 0, '2019-10-03 15:28:32', '2019-10-08 06:17:13'),
(22, 5, 11, 'BOCI-DC01-45', NULL, 5, 3, 2, 128, 0, 0, 0, 0, '', '', 45000, 0, 0, 0, 0, 0, '2019-10-03 15:28:42', '2019-10-08 06:01:48'),
(23, 5, 11, 'BOCI-DC01-50', NULL, 5, 3, 2, 129, 0, 0, 0, 0, '', '', 63000, 0, 0, 0, 0, 0, '2019-10-03 15:29:23', '2019-10-08 06:02:03'),
(24, 5, 20, 'BOFT-001-LBL', NULL, 2, 5, 4, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:29:52', '2019-10-08 06:17:19'),
(25, 5, 11, 'BOCI-DC01-60', NULL, 5, 3, 2, 130, 0, 0, 0, 0, '', '', 70000, 0, 0, 0, 0, 0, '2019-10-03 15:30:00', '2019-10-08 06:02:15'),
(26, 5, 12, 'BOPP-DC03-570', NULL, 6, 3, 20, 122, 0, 0, 0, 0, '', '', 49000, 0, 0, 0, 0, 0, '2019-10-03 15:32:27', '2019-10-08 06:05:08'),
(27, 5, 12, 'BOPP-DC03-590', NULL, 6, 3, 20, 123, 0, 0, 0, 0, '', '', 59000, 0, 0, 0, 0, 0, '2019-10-03 15:33:04', '2019-10-08 06:04:40'),
(28, 5, 13, 'BOBP-DC06-90', NULL, 7, 3, 20, 131, 0, 0, 0, 0, '', '', 122000, 0, 0, 0, 0, 0, '2019-10-03 15:36:22', '2019-10-08 06:07:34'),
(29, 5, 13, 'BOBP-DC06-100', NULL, 7, 3, 20, 132, 0, 0, 0, 0, '', '', 134000, 0, 0, 0, 0, 0, '2019-10-03 15:38:08', '2019-10-08 06:07:25'),
(30, 5, 20, 'BOFT-001-LBR', NULL, 2, 5, 5, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:38:11', '2019-10-08 06:17:28'),
(31, 5, 20, 'BOFT-001-LGN', NULL, 2, 5, 7, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:40:19', '2019-10-08 06:17:37'),
(32, 5, 13, 'BOBP-DC06-110', NULL, 7, 3, 20, 134, 0, 0, 0, 0, '', '', 145000, 0, 0, 0, 0, 0, '2019-10-03 15:40:24', '2019-10-08 06:07:15'),
(33, 5, 13, 'BOBP-DC06-120', NULL, 7, 3, 20, 133, 0, 0, 0, 0, '', '', 157000, 0, 0, 0, 0, 0, '2019-10-03 15:41:19', '2019-10-08 06:07:07'),
(34, 5, 13, 'BOBP-DC06-140', NULL, 7, 3, 20, 135, 0, 0, 0, 0, '', '', 180000, 0, 0, 0, 0, 0, '2019-10-03 15:42:05', '2019-10-08 06:06:27'),
(35, 5, 13, 'BOBP-DC06-160', NULL, 7, 3, 20, 136, 0, 0, 0, 0, '', '', 203000, 0, 0, 0, 0, 0, '2019-10-03 15:42:38', '2019-10-08 06:06:18'),
(36, 5, 20, 'BOFT-001-LGR', NULL, 2, 5, 6, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:42:49', '2019-10-08 06:17:45'),
(37, 5, 13, 'BOBP-DC06-180', NULL, 7, 3, 20, 138, 0, 0, 0, 0, '', '', 226000, 0, 0, 0, 0, 0, '2019-10-03 15:43:21', '2019-10-08 06:06:07'),
(38, 5, 20, 'BOFT-001-LMR', NULL, 2, 5, 8, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:43:47', '2019-10-08 06:17:53'),
(39, 5, 13, 'BOBP-DC06-200', NULL, 7, 3, 20, 139, 0, 0, 0, 0, '', '', 249000, 0, 0, 0, 0, 0, '2019-10-03 15:43:57', '2019-10-08 06:05:56'),
(40, 5, 14, 'BOID-MC06-150', NULL, 8, 3, 2, 107, 0, 0, 0, 0, '', '', 293000, 0, 0, 0, 0, 0, '2019-10-03 15:44:58', '2019-10-08 06:10:10'),
(41, 5, 20, 'BOFT-001-LTQ', NULL, 2, 5, 9, 89, 0, 0, 50, 550, '', '30/2', 11000, 0, 0, 0, 0, 0, '2019-10-03 15:45:45', '2019-10-08 06:17:59'),
(42, 5, 14, 'BOID-MC06-160', NULL, 8, 3, 2, 108, 0, 0, 0, 0, '', '', 308000, 0, 0, 0, 0, 0, '2019-10-03 15:47:09', '2019-10-08 06:10:01'),
(43, 5, 21, 'BOHT-001-LWT', NULL, 2, 5, 2, 90, 0, 0, 154, 550, '', '30/2', 27000, 0, 0, 0, 0, 0, '2019-10-03 15:47:11', '2019-10-08 06:18:27'),
(44, 5, 14, 'BOID-MC06-180', NULL, 8, 3, 2, 109, 0, 0, 0, 0, '', '', 323000, 0, 0, 0, 0, 0, '2019-10-03 15:47:42', '2019-10-08 06:09:49'),
(45, 5, 14, 'BOID-MC06-200', NULL, 8, 3, 2, 110, 0, 0, 0, 0, '', '', 346000, 0, 0, 0, 0, 0, '2019-10-03 15:48:11', '2019-10-08 06:09:37'),
(46, 5, 21, 'BOHT-001-LCL', NULL, 2, 5, 3, 90, 0, 0, 154, 550, '', '30/2', 29000, 0, 0, 0, 0, 0, '2019-10-03 15:48:36', '2019-10-08 06:18:33'),
(47, 5, 14, 'BOID-MC06-220', NULL, 8, 3, 2, 111, 0, 0, 0, 0, '', '', 383000, 0, 0, 0, 0, 0, '2019-10-03 15:49:07', '2019-10-08 06:09:27'),
(48, 5, 14, 'BOID-MC06-240', NULL, 8, 3, 2, 119, 0, 0, 0, 0, '', '', 413000, 0, 0, 0, 0, 0, '2019-10-03 15:49:43', '2019-10-08 06:09:17'),
(49, 5, 14, 'BOID-MC06-260', NULL, 8, 3, 2, 120, 0, 0, 0, 0, '', '', 430000, 0, 0, 0, 0, 0, '2019-10-03 15:50:38', '2019-10-08 06:09:09'),
(50, 5, 14, 'BOID-MC06-280', NULL, 8, 3, 2, 121, 0, 0, 0, 0, '', '', 476000, 0, 0, 0, 0, 0, '2019-10-03 15:51:23', '2019-10-08 06:08:59'),
(51, 5, 21, 'BOHT-001-LBL', NULL, 2, 5, 4, 90, 0, 0, 154, 550, '', '30/2', 32000, 0, 0, 0, 0, 0, '2019-10-03 15:51:44', '2019-10-08 06:18:41'),
(52, 5, 14, 'BOID-MC06-285', NULL, 8, 3, 2, 140, 0, 0, 0, 0, '', '', 500000, 0, 0, 0, 0, 0, '2019-10-03 15:52:58', '2019-10-08 06:08:46'),
(53, 5, 14, 'BOID-MC08-150', NULL, 9, 3, 2, 107, 0, 0, 0, 0, '', '', 308000, 0, 0, 0, 0, 0, '2019-10-03 15:55:06', '2019-10-08 06:13:30'),
(54, 5, 14, 'BOID-MC08-160', NULL, 9, 3, 2, 108, 0, 0, 0, 0, '', '', 323000, 0, 0, 0, 0, 0, '2019-10-03 15:55:54', '2019-10-08 06:13:20'),
(55, 5, 14, 'BOID-MC08-180', NULL, 9, 3, 2, 109, 0, 0, 0, 0, '', '', 338000, 0, 0, 0, 0, 0, '2019-10-03 15:56:50', '2019-10-08 06:13:12'),
(56, 5, 26, 'BOPT-002-LOR/YL', NULL, 2, 6, 13, 96, 0, 0, 804, 550, '', '20/2', 166000, 0, 0, 0, 0, 0, '2019-10-03 16:23:42', '2019-10-08 06:47:09'),
(57, 5, 21, 'BOHT-001-LBR', NULL, 2, 5, 5, 90, 0, 0, 154, 550, '', '30/2', 32000, 0, 0, 0, 0, 0, '2019-10-03 16:25:59', '2019-10-08 06:18:49'),
(58, 5, 26, 'BOPT-002-LYL/GN/BL', NULL, 2, 6, 14, 96, 0, 0, 804, 550, '', '20/2', 166000, 0, 0, 0, 0, 0, '2019-10-03 16:31:37', '2019-10-08 06:46:58'),
(59, 5, 26, 'BOPT-002-SBR/IY', NULL, 2, 6, 15, 95, 0, 0, 774, 600, '', '20/2', 149000, 0, 0, 0, 0, 0, '2019-10-03 16:34:19', '2019-10-08 06:46:41'),
(60, 5, 26, 'BOPT-002-LBR/IY', NULL, 2, 6, 15, 96, 0, 0, 804, 550, '', '20/2', 166000, 0, 0, 0, 0, 0, '2019-10-03 16:35:44', '2019-10-08 06:46:25'),
(61, 5, 26, 'BOPT-002-LWT/MG', NULL, 2, 6, 16, 96, 0, 0, 804, 550, '', '20/2', 155000, 0, 0, 0, 0, 0, '2019-10-03 16:36:58', '2019-10-08 06:46:11'),
(62, 5, 26, 'BOPT-002-LWT/GN', NULL, 2, 6, 17, 96, 0, 0, 804, 550, '', '20/2', 155000, 0, 0, 0, 0, 0, '2019-10-03 16:37:43', '2019-10-08 06:44:39'),
(63, 5, 26, 'BOPT-002-LBR/BL', NULL, 2, 6, 18, 96, 0, 0, 804, 550, '', '20/2', 166000, 0, 0, 0, 0, 0, '2019-10-03 16:38:51', '2019-10-08 06:44:27'),
(64, 5, 21, 'BOHT-001-LGR', NULL, 2, 5, 6, 90, 0, 0, 154, 550, '', '30/2', 32000, 0, 0, 0, 0, 0, '2019-10-03 16:40:42', '2019-10-08 06:18:57'),
(65, 5, 21, 'BOHT-001-LMR', NULL, 2, 5, 8, 90, 0, 0, 154, 550, '', '30/2', 32000, 0, 0, 0, 0, 0, '2019-10-03 16:41:25', '2019-10-08 06:19:06'),
(66, 5, 26, 'BOPT-002-LWT/NY', NULL, 2, 6, 19, 94, NULL, NULL, 891, 550, '', '20/2', 169000, 0, 0, 0, 0, 0, '2019-10-03 16:43:23', '2019-10-08 06:47:59'),
(67, 5, 26, 'BOPT-002-LBR/IY', NULL, 2, 6, 15, 96, 0, 0, 891, 550, '', '20/2', 175000, 0, 0, 0, 0, 0, '2019-10-03 16:55:37', '2019-10-08 06:44:06'),
(68, 5, 18, 'BOFL-260P-190', NULL, 18, 3, 21, 98, 112, 98, 0, 0, 'FLAT - SINGLE', '', 186000, 0, 0, 0, 0, 0, '2019-10-03 17:07:20', '2019-10-08 04:28:56'),
(69, 5, 26, 'BOPT-002-SOR/YL', NULL, 2, 6, 13, 96, 0, 0, 710, 550, '', '20/2', 147000, 0, 0, 0, 0, 0, '2019-10-03 17:12:43', '2019-10-08 06:43:53'),
(70, 5, 18, 'BOFL-260P-200', NULL, 18, 3, 21, 99, 113, 99, 0, 0, 'FLAT - SINGLE', '', 196000, 0, 0, 0, 0, 0, '2019-10-03 17:14:00', '2019-10-08 04:28:18'),
(71, 5, 26, 'BOPT-002-LGN/BL', NULL, 2, 6, 12, 96, 0, 0, 804, 550, '', '20/2', 166000, 0, 0, 0, 0, 0, '2019-10-03 17:14:09', '2019-10-08 06:43:43'),
(72, 5, 26, 'BOPT-001-SGN/BL', NULL, 2, 6, 12, 95, 0, 0, 710, 550, '', '20/2', 147000, 0, 0, 0, 0, 0, '2019-10-03 17:15:21', '2019-10-08 06:43:32'),
(73, 5, 26, 'BOPT-002-LWT/DB', NULL, 2, 6, 11, 96, 0, 0, 804, 550, '', '20/2', 155000, 0, 0, 0, 0, 0, '2019-10-03 17:17:35', '2019-10-08 06:40:31'),
(74, 5, 18, 'BOFL-260P-210', NULL, 18, 5, 21, 100, 114, 100, 0, 0, 'FLAT - TWIN', '', 215000, 0, 0, 0, 0, 0, '2019-10-03 17:17:36', '2019-10-08 04:27:47'),
(75, 5, 26, 'BOPT-001-SWT/DB', NULL, 2, 6, 11, 95, 0, 0, 710, 550, '', '20/2', 139000, 0, 0, 0, 0, 0, '2019-10-03 17:18:48', '2019-10-08 06:40:22'),
(76, 5, 26, 'BOPT-002-LWT/LB', NULL, 2, 6, 10, 96, 0, 0, 804, 550, '', '20/2', 144000, 0, 0, 0, 0, 0, '2019-10-03 17:21:10', '2019-10-08 06:40:10'),
(77, 5, 21, 'BOHT-001-LTQ', NULL, 2, 5, 9, 90, 0, 0, 154, 550, '', '30/2', 32000, 0, 0, 0, 0, 0, '2019-10-03 17:21:18', '2019-10-08 06:19:13'),
(78, 5, 18, 'BOFL-260P-230', NULL, 18, 3, 21, 101, 115, 101, 0, 0, 'FLAT - TWIN-QUEEN', '', 234000, 0, 0, 0, 0, 0, '2019-10-03 17:21:20', '2019-10-08 04:27:06'),
(79, 5, 26, 'BOPT-001-SWT/LB', NULL, 2, 6, 10, 95, 0, 0, 710, 550, '', '20/2', 128000, 0, 0, 0, 0, 0, '2019-10-03 17:22:43', '2019-10-08 06:40:01'),
(80, 5, 18, 'BOFL-260P-250', NULL, 18, 3, 21, 102, 116, 102, 0, 0, 'FLAT - QUEEN', '', 253000, 0, 0, 0, 0, 0, '2019-10-03 17:26:04', '2019-10-08 04:26:38'),
(81, 5, 25, 'BOLT-001-LWT', NULL, 2, 6, 2, 94, 0, 0, 958, 625, '', '20/2', 169000, 0, 0, 0, 0, 0, '2019-10-03 17:26:42', '2019-10-08 06:34:21'),
(82, 5, 24, 'BOMT-001-LTQ', NULL, 2, 6, 9, 93, 0, 0, 797, 625, '', '20/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:28:15', '2019-10-08 06:33:44'),
(83, 5, 18, 'BOFL-260P-260', NULL, 18, 3, 21, 103, 116, 103, 0, 0, 'FLAT - QUEEN', '', 263000, 0, 0, 0, 0, 0, '2019-10-03 17:28:35', '2019-10-08 04:25:52'),
(84, 5, 24, 'BOMT-001-LMR', NULL, 2, 6, 8, 93, 0, 0, 797, 625, '', '30/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:29:50', '2019-10-08 06:33:37'),
(85, 5, 18, 'BOFL-260P-270', NULL, 18, 3, 21, 104, 117, 104, 0, 0, 'FLAT -KING', '', 272000, 0, 0, 0, 0, 0, '2019-10-03 17:31:23', '2019-10-08 04:25:24'),
(86, 5, 24, 'BOMT-001-LGN', NULL, 2, 6, 7, 93, 0, 0, 797, 625, '', '30/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:31:30', '2019-10-08 06:33:30'),
(87, 5, 18, 'BOFL-260P-280', NULL, 18, 3, 21, 105, 117, 105, 0, 0, 'FLAT - KING', '', 282000, 0, 0, 0, 0, 0, '2019-10-03 17:34:01', '2019-10-08 04:24:38'),
(88, 5, 18, 'BOFL-260P-290', NULL, 18, 3, 21, 106, 118, 106, 0, 0, 'FLAT - EXTRA KING', '', 292000, 0, 0, 0, 0, 0, '2019-10-03 17:37:49', '2019-10-08 04:13:22'),
(89, 5, 24, 'BOMT-001-LGR', NULL, 2, 6, 6, 93, 0, 0, 797, 625, '', '30/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:38:06', '2019-10-08 06:33:23'),
(90, 5, 24, 'BOMT-001-LBR', NULL, 2, 6, 5, 93, 0, 0, 797, 625, '', '30/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:39:18', '2019-10-08 06:33:13'),
(91, 5, 24, 'BOMT-001-LBL', NULL, 2, 6, 4, 93, 0, 0, 797, 625, '', '30/2', 158000, 0, 0, 0, 0, 0, '2019-10-03 17:40:43', '2019-10-08 06:32:56'),
(92, 5, 18, 'BOFT-260P-90', NULL, 18, 3, 21, 112, 112, 0, 0, 0, 'FITTED - SINGLE', '', 173000, 0, 0, 0, 0, 0, '2019-10-03 17:42:57', '2019-10-08 05:20:09'),
(93, 5, 24, 'BOMT-001-LCL', NULL, 2, 6, 3, 93, 0, 0, 797, 625, '', '30/2', 148000, 0, 0, 0, 0, 0, '2019-10-03 17:43:39', '2019-10-08 06:32:47'),
(94, 5, 18, 'BOFT-260P-100', NULL, 18, 3, 21, 113, 113, 0, 0, 0, 'FITTED - SINGLE', '', 183000, 0, 0, 0, 0, 0, '2019-10-03 17:44:05', '2019-10-08 05:19:46'),
(95, 5, 18, 'BOFT-260P-120', NULL, 18, 3, 21, 114, 114, 0, 0, 0, 'FITTED - TWIN', '', 201000, 0, 0, 0, 0, 0, '2019-10-03 17:45:31', '2019-10-08 05:19:25'),
(96, 5, 22, 'BOBM-002-EWT', NULL, 2, 5, 2, 91, 0, 0, 375, 1, '', '20/1', 64000, 0, 0, 0, 0, 0, '2019-10-03 17:45:57', '2019-10-08 06:20:47'),
(97, 5, 18, 'BOFT-260P-140', NULL, 18, 3, 21, 115, 115, 0, 0, 0, 'FITTED - TWIN-QUEEN', '', 220000, 0, 0, 0, 0, 0, '2019-10-03 17:46:57', '2019-10-08 05:19:03'),
(98, 5, 22, 'BOBM-002-LCL', NULL, 2, 5, 3, 91, 0, 0, 375, 1, '', '20/2', 67000, 0, 0, 0, 0, 0, '2019-10-03 17:46:58', '2019-10-08 06:20:33'),
(99, 5, 24, 'BOMT-001-LWT', NULL, 2, 6, 2, 93, 0, 0, 797, 625, '', '30/2', 139000, 0, 0, 0, 0, 0, '2019-10-03 17:47:52', '2019-10-08 06:32:40'),
(100, 5, 18, 'BOFT-260P-160', NULL, 18, 3, 21, 116, 116, 0, 0, 0, 'FITTED - QUEEN', '', 239000, 0, 0, 0, 0, 0, '2019-10-03 17:48:15', '2019-10-08 05:18:31'),
(101, 5, 22, 'BOBM-002-LBL', NULL, 2, 5, 4, 91, 0, 0, 375, 1, '', '20/2', 14000, 0, 0, 0, 0, 0, '2019-10-03 17:48:41', '2019-10-08 06:20:26'),
(102, 5, 18, 'BOFT-260P-180', NULL, 18, 3, 21, 117, 117, 0, 0, 0, 'FITTED - KING', '', 259000, 0, 0, 0, 0, 0, '2019-10-03 17:49:15', '2019-10-08 05:18:09'),
(103, 5, 22, 'BOBM-002-LBR', NULL, 2, 5, 5, 91, 0, 0, 375, 1, '', '20/2', 74000, 0, 0, 0, 0, 0, '2019-10-03 17:49:45', '2019-10-08 06:20:19'),
(104, 5, 23, 'BOBT-002-LTR', NULL, 2, 6, 9, 92, 0, 0, 524, 550, '', '30/2', 102000, 0, 0, 0, 0, 0, '2019-10-03 17:50:09', '2019-10-08 06:28:41'),
(105, 5, 18, 'BOFT-260P-200', NULL, 18, 3, 21, 118, 118, 0, 0, 0, 'FITEED - EXTRA KING', '', 278000, 0, 0, 0, 0, 0, '2019-10-03 17:50:24', '2019-10-08 05:17:47'),
(106, 5, 22, 'BOBM-002-LGN', NULL, 2, 5, 7, 91, 0, 0, 375, 1, '', '20/2', 74000, 0, 0, 0, 0, 0, '2019-10-03 17:50:53', '2019-10-08 06:20:13'),
(107, 5, 22, 'BOBN-002-LGR', NULL, 2, 5, 6, 87, NULL, NULL, 375, 1, '', '20/2', 74000, 0, 0, 0, 0, 0, '2019-10-03 17:51:44', '2019-10-08 02:12:57'),
(108, 5, 23, 'BOBT-002-LMR', NULL, 2, 6, 8, 92, 0, 0, 524, 550, '', '30/2', 102000, 0, 0, 0, 0, 0, '2019-10-03 17:52:58', '2019-10-08 06:28:30'),
(109, 5, 22, 'BOBN-002-LMR', NULL, 2, 5, 8, 87, NULL, NULL, 375, 1, '', '20/2', 74000, 0, 0, 0, 0, 0, '2019-10-03 17:53:15', '2019-10-08 02:12:57'),
(110, 5, 22, 'BOBM-002-LTQ', NULL, 2, 5, 9, 91, 0, 0, 375, 1, '', '20/2', 74000, 0, 0, 0, 0, 0, '2019-10-03 17:54:33', '2019-10-08 06:20:05'),
(111, 5, 15, 'BODC-260P-150', NULL, 18, 3, 21, 107, 112, 107, 0, 0, 'DOUBLE - SINGLE', '', 343000, 0, 0, 0, 0, 0, '2019-10-03 17:54:54', '2019-10-08 04:16:10'),
(112, 5, 23, 'BOBT-002-LWT', NULL, 2, 5, 2, 92, NULL, NULL, 524, 550, '', '30/2', 87000, 0, 0, 0, 0, 0, '2019-10-03 17:56:11', '2019-10-08 06:26:34'),
(113, 5, 15, 'BODC-260P-160', NULL, 18, 3, 21, 108, 113, 108, 0, 0, 'DOUBLE - SINGLE', '', 367000, 0, 0, 0, 0, 0, '2019-10-03 17:56:20', '2019-10-08 04:15:38'),
(114, 5, 23, 'BOBT-002-LCL', NULL, 2, 5, 3, 92, 0, 0, 524, 550, '', '30/2', 98000, 0, 0, 0, 0, 0, '2019-10-03 17:57:46', '2019-10-08 06:26:24'),
(115, 5, 15, 'BODC-260P-180', NULL, 18, 3, 21, 109, 114, 109, 0, 0, 'DOUBLE - TWIN', '', 381000, 0, 0, 0, 0, 0, '2019-10-03 17:57:54', '2019-10-08 04:15:11'),
(116, 5, 23, 'BOBT-002-LGR', NULL, 2, 6, 6, 92, 0, 0, 524, 550, '', '30/2', 102000, 0, 0, 0, 0, 0, '2019-10-03 17:57:54', '2019-10-08 06:27:59'),
(117, 5, 23, 'BOBT-002-LBL', NULL, 2, 5, 4, 92, 0, 0, 524, 550, '', '30/2', 102000, 0, 0, 0, 0, 0, '2019-10-03 17:59:13', '2019-10-08 06:27:22'),
(118, 5, 15, 'BODC-260P-200', NULL, 18, 3, 21, 110, 115, 110, 0, 0, 'DOUBLE - TWIN-QUEEN', '', 419000, 0, 0, 0, 0, 0, '2019-10-03 17:59:17', '2019-10-08 04:14:38'),
(119, 5, 23, 'BOBT-002-LGN', NULL, 2, 6, 7, 92, 0, 0, 524, 550, '', '30/2', 102000, 0, 0, 0, 0, 0, '2019-10-03 17:59:35', '2019-10-08 06:27:52'),
(120, 5, 23, 'BOBT-002-LBR', NULL, 2, 5, 5, 92, 0, 0, 524, 550, '', '30/3', 102000, 0, 0, 0, 0, 0, '2019-10-03 18:00:33', '2019-10-08 06:27:11'),
(121, 5, 15, 'BODC-260P-220', NULL, 18, 3, 21, 111, 116, 111, 0, 0, 'DOUBLE - QUEEN', '', 457000, 0, 0, 0, 0, 0, '2019-10-03 18:00:41', '2019-10-08 04:14:08'),
(122, 5, 15, 'BODC-260P-240', NULL, 18, 3, 21, 119, 117, 119, 0, 0, 'DOUBLE - KING', '', 495000, 0, 0, 0, 0, 0, '2019-10-03 18:06:41', '2019-10-08 03:04:09'),
(123, 5, 18, 'BOFL-300P-190', NULL, 13, 3, 21, 98, 112, 98, 0, 0, 'FLAT SINGLE', '-', 219000, 0, 0, 0, 0, 0, '2019-10-03 18:08:19', '2019-10-08 05:49:09'),
(124, 5, 15, 'BODC-260P-260', NULL, 18, 3, 21, 120, 118, 120, 0, 0, 'DOUBLE - EXTRA KING', '', 505000, 0, 0, 0, 0, 0, '2019-10-03 18:10:20', '2019-10-08 03:08:23'),
(125, 5, 18, 'BOFL-300P-200', NULL, 13, 3, 21, 99, 113, 99, 0, 0, 'FLAT SINGLE', '-', 223000, 0, 0, 0, 0, 0, '2019-10-03 18:10:40', '2019-10-08 05:48:45'),
(126, 5, 15, 'BODC-260P-280', NULL, 18, 3, 21, 121, 118, 121, 0, 0, 'DOUBLE - EXTRA KING', '', 519000, 0, 0, 0, 0, 0, '2019-10-03 18:11:35', '2019-10-08 03:09:55'),
(127, 5, 18, 'BOFL-300P-210', NULL, 13, 3, 21, 100, 114, 100, 0, 0, 'FLAT-SINGLE', '-', 246000, 0, 0, 0, 0, 0, '2019-10-03 18:11:48', '2019-10-08 05:48:21'),
(128, 5, 18, 'BOFL-300P-230', NULL, 13, 3, 21, 101, 115, 101, 0, 0, 'FLAT - TWIN-QUEEN', '-', 269000, 0, 0, 0, 0, 0, '2019-10-03 18:13:56', '2019-10-08 05:48:01'),
(129, 5, 16, 'BOPC-260P-575', NULL, 18, 3, 21, 91, 91, 122, 0, 0, 'STANDARD - SMALL', '', 39000, 0, 0, 0, 0, 0, '2019-10-03 18:14:20', '2019-10-08 04:08:27'),
(130, 5, 18, 'BOFL-300P-250', NULL, 13, 3, 21, 102, 116, 102, 0, 0, 'FLAT - QUEEN', '-', 285000, 0, 0, 0, 0, 0, '2019-10-03 18:15:11', '2019-10-08 05:47:32'),
(131, 5, 16, 'BOPC-260P-595', NULL, 18, 3, 21, 126, 126, 123, 0, 0, 'STANDARD - LARGE', '', 46000, 0, 0, 0, 0, 0, '2019-10-03 18:16:11', '2019-10-08 04:09:22'),
(132, 5, 18, 'BOFL-300P-260', NULL, 13, 3, 21, 103, 116, 103, 0, 0, 'FLAT - QUEEN', '-', 296000, 0, 0, 0, 0, 0, '2019-10-03 18:16:57', '2019-10-08 05:46:52'),
(133, 5, 18, 'BOFL-200P-190', NULL, 15, 3, 21, 98, 112, 98, 0, 0, 'FLAT - SINGLE', '', 148000, 0, 0, 0, 0, 0, '2019-10-03 18:17:05', '2019-10-08 05:24:59'),
(134, 5, 18, 'BOFL-200P-200', NULL, 15, 3, 21, 99, 113, 99, 0, 0, '', 'FLAT - SINGLE', 153000, 0, 0, 0, 0, 0, '2019-10-03 18:19:47', '2019-10-08 05:24:30'),
(135, 5, 16, 'BOPC-260P-575F', NULL, 18, 3, 21, 91, 91, 122, 0, 0, 'FRAME - SMALL', '', 56000, 0, 0, 0, 0, 0, '2019-10-03 18:20:07', '2019-10-08 04:08:08'),
(136, 5, 18, 'BOFL-300P-270', NULL, 13, 3, 21, 104, 117, 104, 0, 0, 'FLAT - KING', '-', 306000, 0, 0, 0, 0, 0, '2019-10-03 18:21:01', '2019-10-08 05:46:19'),
(137, 5, 16, 'BOPC-260P-595F', NULL, 18, 3, 21, 126, 126, 123, 0, 0, 'FRAME - LARGE', '', 59000, 0, 0, 0, 0, 0, '2019-10-03 18:22:19', '2019-10-08 04:08:57'),
(138, 5, 18, 'BOFL-200P-210', NULL, 15, 3, 21, 100, 114, 100, 0, 0, '', 'FLAT - TWIN', 162000, 0, 0, 0, 0, 0, '2019-10-03 18:22:58', '2019-10-08 05:23:58'),
(139, 5, 18, 'BOFL-300P-280', NULL, 13, 3, 21, 105, 117, 105, 0, 0, 'FLAT - KING ', '-', 316000, 0, 0, 0, 0, 0, '2019-10-03 18:23:36', '2019-10-08 05:45:47'),
(140, 5, 18, 'BOFL-300P-290', NULL, 13, 3, 21, 106, 118, 106, 0, 0, 'FLAT - KING', '-', 326000, 0, 0, 0, 0, 0, '2019-10-03 18:25:03', '2019-10-08 05:45:09'),
(141, 5, 18, 'BOFT-300P-90', NULL, 13, 3, 21, 112, 112, 0, 0, 0, 'FITTED - SINGLE', '-', 189000, 0, 0, 0, 0, 0, '2019-10-03 18:26:08', '2019-10-08 05:44:27'),
(142, 5, 17, 'BOBC-260P-90', NULL, 18, 3, 21, 124, 124, 124, 0, 0, 'STANDARD - SMALL', '', 44000, 0, 0, 0, 0, 0, '2019-10-03 18:27:05', '2019-10-08 04:10:45'),
(143, 5, 18, 'BOFT-300P-100', NULL, 13, 3, 21, 113, 113, 0, 0, 0, 'FITTED - SINGLE', '-', 199000, 0, 0, 0, 0, 0, '2019-10-03 18:27:19', '2019-10-08 05:44:11'),
(144, 5, 18, 'BOFL-200P-230', NULL, 15, 5, 21, 101, 115, 101, 0, 0, 'FLAT - TWIN QUEEN', '', 171000, 0, 0, 0, 0, 0, '2019-10-03 18:28:12', '2019-10-08 05:23:37'),
(145, 5, 17, 'BOBC-260P-100', NULL, 18, 3, 21, 125, 125, 125, 0, 0, 'STANDARD - LARGE', '', 50000, 0, 0, 0, 0, 0, '2019-10-03 18:28:14', '2019-10-08 04:10:20'),
(146, 5, 18, 'BOFL-200P-250', NULL, 15, 3, 21, 102, 116, 102, 0, 0, 'FLAT - QUEEN', '', 189000, 0, 0, 0, 0, 0, '2019-10-03 18:30:34', '2019-10-08 05:23:04'),
(147, 5, 16, 'BOPC-300P-575', NULL, 13, 3, 21, 91, 91, 122, 0, 0, 'STANDARD - SMALL', '', 47000, 0, 0, 0, 0, 0, '2019-10-03 18:32:23', '2019-10-08 05:31:52'),
(148, 5, 18, 'BOFT-300P-120', NULL, 13, 3, 21, 114, 114, 0, 0, 0, 'FITTED - TWIN', '-', 219000, 0, 0, 0, 0, 0, '2019-10-03 18:32:27', '2019-10-08 05:43:49'),
(149, 5, 18, 'BOFL-200P-260', NULL, 15, 3, 21, 103, 116, 103, 0, 0, 'FLAT - QUEEN', '', 196000, 0, 0, 0, 0, 0, '2019-10-03 18:32:47', '2019-10-08 05:22:36'),
(150, 5, 16, 'BOPC-300P-595', NULL, 13, 3, 21, 126, 126, 123, 0, 0, 'STANDARD - LARGE', '', 54000, 0, 0, 0, 0, 0, '2019-10-03 18:33:05', '2019-10-08 05:31:28'),
(151, 5, 18, 'BOFT-300P-140', NULL, 13, 3, 21, 115, 115, 0, 0, 0, 'FITTED - TWIN-QUEEN', '', 242000, 0, 0, 0, 0, 0, '2019-10-03 18:34:05', '2019-10-08 05:43:20'),
(152, 5, 16, 'BOPC-300P-575F', NULL, 13, 3, 21, 91, 91, 122, 0, 0, 'FRAME - SMALL', '', 63000, 0, 0, 0, 0, 0, '2019-10-03 18:36:52', '2019-10-08 05:30:53'),
(153, 5, 18, 'BOFT-300P-160', NULL, 13, 3, 21, 116, 116, 0, 0, 0, 'FITTED - QUEEN', '-', 261000, 0, 0, 0, 0, 0, '2019-10-03 18:37:17', '2019-10-08 05:43:01'),
(154, 5, 16, 'BOPC-300P-595F', NULL, 13, 3, 21, 126, 126, 123, 0, 0, 'FRAME - LARGE', '', 70000, 0, 0, 0, 0, 0, '2019-10-03 18:37:44', '2019-10-08 05:30:22'),
(155, 5, 18, 'BOFT-300P-180', NULL, 13, 3, 21, 117, 117, 0, 0, 0, 'FITTED - KING', '-', 282000, 0, 0, 0, 0, 0, '2019-10-03 18:38:11', '2019-10-08 05:42:42'),
(156, 5, 18, 'BOFT-300P-200', NULL, 13, 3, 21, 118, 118, 0, 0, 0, 'FITTED - EXTRA KING', '-', 299000, 0, 0, 0, 0, 0, '2019-10-03 18:39:33', '2019-10-08 05:42:05'),
(157, 5, 17, 'BOBC-200P-90', NULL, 15, 3, 21, 124, 124, 124, 0, 0, 'STANDARD - SMALL', '', 49000, 0, 0, 0, 0, 0, '2019-10-03 18:39:48', '2019-10-08 05:03:38'),
(158, 5, 17, 'BOBC-200P-100', NULL, 15, 3, 21, 125, 125, 125, 0, 0, 'STANDARD - LARGE', '', 52000, 0, 0, 0, 0, 0, '2019-10-03 18:40:31', '2019-10-08 05:03:15'),
(159, 5, 15, 'BODC-300P-150', NULL, 13, 3, 21, 107, 112, 107, 0, 0, 'DOUBLE - SINGLE', '-', 359000, 0, 0, 0, 0, 0, '2019-10-03 18:40:49', '2019-10-08 05:40:21'),
(160, 5, 15, 'BODC-300P-160', NULL, 13, 3, 21, 108, 113, 108, 0, 0, 'DOUBLE-SINGLE', '-', 371000, 0, 0, 0, 0, 0, '2019-10-03 18:42:10', '2019-10-08 05:39:51'),
(161, 5, 18, 'BOFL-200P-270', NULL, 15, 3, 21, 104, 117, 104, 0, 0, 'FLAT - KING', '', 199000, 0, 0, 0, 0, 0, '2019-10-03 18:42:53', '2019-10-08 05:22:09'),
(162, 5, 18, 'BOFL-200P-280', NULL, 15, 3, 21, 105, 117, 105, 0, 0, 'FLAT - KING', '', 206000, 0, 0, 0, 0, 0, '2019-10-03 18:43:49', '2019-10-08 05:21:45'),
(163, 5, 18, 'BOFL-200P-290', NULL, 15, 3, 21, 106, 118, 106, 0, 0, 'FLAT - EXTRA KING', '', 219000, 0, 0, 0, 0, 0, '2019-10-03 18:44:42', '2019-10-08 05:21:06'),
(164, 5, 15, 'BODC-200P-150', NULL, 15, 3, 21, 107, 112, 107, 0, 0, 'DOUBLE - SINGLE', '', 237000, 0, 0, 0, 0, 0, '2019-10-03 18:45:14', '2019-10-08 05:13:22'),
(165, 5, 18, 'BOFT-200P-90', NULL, 15, 3, 21, 112, 112, NULL, 0, 0, 'FITTED - SINGLE', '', 134000, 0, 0, 0, 0, 0, '2019-10-03 18:45:51', '2019-10-08 05:16:38'),
(166, 5, 15, 'BODC-200P-160', NULL, 15, 3, 21, 108, 113, 108, 0, 0, 'DOUBLE - SINGLE', '', 251000, 0, 0, 0, 0, 0, '2019-10-03 18:46:15', '2019-10-08 05:12:45'),
(167, 5, 18, 'BOFT-200P-100', NULL, 15, 3, 21, 113, 113, NULL, 0, 0, 'FITTED - SINGLE', '', 139000, 0, 0, 0, 0, 0, '2019-10-03 18:47:19', '2019-10-08 05:16:29'),
(168, 5, 15, 'BODC-300P-180', NULL, 13, 3, 21, 109, 114, 109, 0, 0, 'DOUBLE - TWIN', '-', 414000, 0, 0, 0, 0, 0, '2019-10-03 18:47:31', '2019-10-08 05:39:22'),
(169, 5, 16, 'BOPC-200P-575', NULL, 15, 3, 21, 91, 91, 122, 0, 0, 'STANDARD - SMALL', '', 33000, 0, 0, 0, 0, 0, '2019-10-03 18:47:51', '2019-10-08 05:06:02'),
(170, 5, 18, 'BOFT-200P-120', NULL, 15, 3, 21, 114, 114, NULL, 0, 0, 'FITTED - TWIN', '', 153000, 0, 0, 0, 0, 0, '2019-10-03 18:48:08', '2019-10-08 05:16:19'),
(171, 5, 18, 'BOFT-200P-140', NULL, 18, 3, 21, 115, 115, 0, 0, 0, 'FITTED - TWIN QUEEN', '', 161000, 0, 0, 0, 0, 0, '2019-10-03 18:48:55', '2019-10-08 04:22:26'),
(172, 5, 15, 'BODC-300P-200', NULL, 13, 3, 21, 110, 115, 110, 0, 0, 'DOUBLE - TWIN - QUEN', '-', 459000, 0, 0, 0, 0, 0, '2019-10-03 18:48:55', '2019-10-08 05:38:52'),
(173, 5, 18, 'BOFT-200P-160', NULL, 15, 3, 21, 116, 116, NULL, 0, 0, 'FITTED - QUEEN', '', 176000, 0, 0, 0, 0, 0, '2019-10-03 18:49:46', '2019-10-08 05:16:09'),
(174, 5, 15, 'BODC-300P-220', NULL, 13, 3, 21, 111, 0, 116, 0, 0, 'DOUBLE - QUEEN', '-', 491000, 0, 0, 0, 0, 0, '2019-10-03 18:50:05', '2019-10-08 05:38:25'),
(175, 5, 18, 'BOFT-200P-180', NULL, 15, 3, 21, 117, 117, NULL, 0, 0, 'FITTED - KING', '', 189000, 0, 0, 0, 0, 0, '2019-10-03 18:50:42', '2019-10-08 05:16:01'),
(176, 5, 15, 'BODC-200P-200', NULL, 15, 3, 21, 110, 115, 110, 0, 0, 'DOUBLE - TWIN - QUEEN', '', 309000, 0, 0, 0, 0, 0, '2019-10-03 18:50:58', '2019-10-08 05:09:57'),
(177, 5, 15, 'BODC-300P-240', NULL, 13, 3, 21, 119, 117, 119, 0, 0, 'DOUBLE - KING', '-', 529000, 0, 0, 0, 0, 0, '2019-10-03 18:51:14', '2019-10-08 05:37:26'),
(178, 5, 18, 'BOFT-200P-200', NULL, 15, 3, 21, 118, 118, NULL, 0, 0, 'FITTED - EXTRA KING', '', 201000, 0, 0, 0, 0, 0, '2019-10-03 18:51:30', '2019-10-08 05:15:51'),
(179, 5, 15, 'BODC-200P-220', NULL, 15, 3, 21, 111, 116, 111, 0, 0, 'DOUBLE - QUEEN', '', 327000, 0, 0, 0, 0, 0, '2019-10-03 18:52:23', '2019-10-08 05:09:16'),
(180, 5, 15, 'BODC-300P-260', NULL, 13, 3, 21, 103, 118, 103, 0, 0, 'DOUBLE - EXTRA KING', '-', 564000, 0, 0, 0, 0, 0, '2019-10-03 18:52:28', '2019-10-08 05:36:43'),
(181, 5, 15, 'BODC-300P-280', NULL, 15, 3, 21, 121, 118, 121, 0, 0, 'DOUBLE - EXTRA KING', '-', 589000, 0, 0, 0, 0, 0, '2019-10-03 18:53:31', '2019-10-08 05:36:10'),
(182, 5, 15, 'BODC-200P-240', NULL, 15, 3, 21, 119, 117, 119, 0, 0, 'DOUBLE - KING', '', 353000, 0, 0, 0, 0, 0, '2019-10-03 18:53:31', '2019-10-08 05:08:41'),
(183, 5, 15, 'BODC-200P-260', NULL, 15, 3, 21, 120, 118, 120, 0, 0, 'DOUBLE - EXTRA KING', '', 372000, 0, 0, 0, 0, 0, '2019-10-03 18:54:22', '2019-10-08 05:08:04'),
(184, 5, 16, 'BOPC-200P-595', NULL, 15, 3, 21, 126, 126, 123, 0, 0, 'STANDARD - LARGE', '', 38000, 0, 0, 0, 0, 0, '2019-10-03 18:54:30', '2019-10-08 05:05:11'),
(185, 5, 15, 'BODC-300P-300', NULL, 13, 3, 21, 141, 118, 141, 0, 0, 'DOUBLE - EXTRA KING', '-', 609000, 0, 0, 0, 0, 0, '2019-10-03 18:55:00', '2019-10-08 05:35:35'),
(186, 5, 16, 'BOPC-200P-575F', NULL, 15, 3, 21, 91, 91, 122, 0, 0, 'FRAME - SMALL', '', 43000, 0, 0, 0, 0, 0, '2019-10-03 18:55:44', '2019-10-08 05:05:36'),
(187, 5, 15, 'BODC-200P-280', NULL, 15, 3, 21, 121, 118, 121, 0, 0, 'DOUBLE - EXTRA KING', '', 395000, 0, 0, 0, 0, 0, '2019-10-03 18:55:54', '2019-10-08 05:06:57'),
(188, 5, 16, 'BOPC-200P-595F', NULL, 15, 3, 21, 126, 126, 123, 0, 0, 'FRAME - LARGE', '', 48000, 0, 0, 0, 0, 0, '2019-10-03 18:56:51', '2019-10-08 05:04:47'),
(190, 5, 15, 'BODC-200P-180', NULL, 15, 3, 21, 109, 114, 109, 0, 0, 'DOUBLE - TWIN', '', 277000, 0, 0, 0, 0, 0, '2019-10-08 13:12:14', '2019-10-08 05:12:14'),
(191, 5, 17, 'BOBC-300P-90', NULL, 13, 3, 21, 124, 124, 124, 0, 0, 'STANDARD -SMALL', '', 49000, 0, 0, 0, 0, 0, '2019-10-08 13:28:14', '2019-10-08 05:28:14'),
(192, 5, 17, 'BOBC-300P-100', '123456', 13, 3, 21, 125, 125, 125, 0, 0, 'STANDARD - LARGE', '', 52000, 0, 0, 0, 0, 0, '2019-10-08 13:29:21', '2021-07-09 07:51:22'),
(193, 5, 11, 'qw-12345', '78945', 4, 5, 4, 89, 90, 90, 10, 10, '', '', 12000, 0, 0, 10, 0, 0, '2021-07-09 15:52:19', '2021-07-09 07:52:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produksi`
--

CREATE TABLE `produksi` (
  `produksi_id` int(11) NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `produksi_kode` varchar(255) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `estimasi_selesai` date DEFAULT NULL,
  `status_produksi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tanggal_selesai` date DEFAULT NULL,
  `status_penerimaan` varchar(255) DEFAULT NULL,
  `lokasi_bahan_id` int(11) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produksi`
--

INSERT INTO `produksi` (`produksi_id`, `urutan`, `produksi_kode`, `tanggal_mulai`, `estimasi_selesai`, `status_produksi`, `keterangan`, `tanggal_selesai`, `status_penerimaan`, `lokasi_bahan_id`, `lokasi_penerimaan_id`, `tanggal_penerimaan`, `created_at`, `updated_at`) VALUES
(1, 1, 'BLP-1', '2019-10-14', '2019-10-19', 'Dikerjakan', 'Sample', NULL, 'Belum Diterima', 1, NULL, NULL, '2019-10-14 11:29:52', '2019-10-14 03:29:52'),
(8, 2, 'BLP-2', '2019-10-01', '2019-10-18', 'Selesai', '', '2019-10-21', 'Diterima', 1, 1, '2019-10-21', '2019-10-18 14:33:18', '2019-10-21 07:28:25'),
(9, 3, 'BLP-3', '2019-10-01', '2019-10-10', 'Selesai', '', '2019-10-21', 'Belum Diterima', 1, NULL, NULL, '2019-10-21 15:26:14', '2019-10-21 07:26:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produksi_item`
--

CREATE TABLE `produksi_item` (
  `produksi_item_id` int(11) NOT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produksi_item`
--

INSERT INTO `produksi_item` (`produksi_item_id`, `produksi_id`, `produk_id`, `jumlah`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 190, 20, '', '2019-10-14 11:29:52', '2019-10-14 03:29:52'),
(9, 8, 11, 2, '', '2019-10-18 16:29:39', '2019-10-18 08:29:39'),
(10, 9, 11, 4, '', '2019-10-21 15:26:14', '2019-10-21 07:26:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produksi_item_bahan`
--

CREATE TABLE `produksi_item_bahan` (
  `produksi_item_bahan_id` int(11) NOT NULL,
  `produksi_item_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` double(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produksi_item_bahan`
--

INSERT INTO `produksi_item_bahan` (`produksi_item_bahan_id`, `produksi_item_id`, `bahan_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 3.00, '2019-10-14 11:29:52', '2019-10-14 03:29:52'),
(26, 9, 26, 10.00, '2019-10-18 16:29:39', '2019-10-18 08:29:39'),
(27, 9, 26, 0.80, '2019-10-18 16:29:39', '2019-10-18 08:29:39'),
(28, 10, 26, 5.00, '2019-10-21 15:26:14', '2019-10-21 07:26:14'),
(29, 10, 33, 0.40, '2019-10-21 15:26:14', '2019-10-21 07:26:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_custom`
--

CREATE TABLE `produk_custom` (
  `produk_custom_id` int(11) NOT NULL,
  `deskripsi` text,
  `note` text,
  `satuan_id` int(11) DEFAULT NULL,
  `harga_satuan` bigint(20) DEFAULT NULL,
  `jumlah_pesan` int(11) DEFAULT NULL,
  `status_produksi` enum('waiting','progress','finish','cancel') DEFAULT 'waiting',
  `tanggal_mulai` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `estimasi_selesai` datetime DEFAULT NULL,
  `status_penerimaan` enum('Belum Diterima','Diterima') DEFAULT 'Belum Diterima',
  `lokasi_bahan_id` int(11) DEFAULT NULL,
  `tanggal_penerimaan` datetime DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_custom`
--

INSERT INTO `produk_custom` (`produk_custom_id`, `deskripsi`, `note`, `satuan_id`, `harga_satuan`, `jumlah_pesan`, `status_produksi`, `tanggal_mulai`, `tanggal_selesai`, `estimasi_selesai`, `status_penerimaan`, `lokasi_bahan_id`, `tanggal_penerimaan`, `lokasi_penerimaan_id`, `hpp`, `created_at`, `updated_at`) VALUES
(6, 'PILLOW PROTECTOR 40 x 40  BLUE SILICONE PLAIN', '																																																									', 5, 50000, 6, 'finish', '2019-10-01 00:00:00', '2019-10-15 00:00:00', '2019-10-15 00:00:00', 'Diterima', 1, '2019-10-16 00:00:00', 1, 40000, '2019-10-15 00:00:00', NULL),
(7, 'PILLOW CASE 230 x 280 WHITE TC 260 STRIPE PLAIN', NULL, 5, 250000, 2, 'waiting', NULL, NULL, NULL, 'Belum Diterima', NULL, NULL, NULL, NULL, '2019-10-20 00:00:00', NULL),
(8, 'PILLOW PROTECTOR 86 x 150 WHITE 100 % COTTON PLAIN', NULL, 5, 280000, 3, 'waiting', NULL, NULL, NULL, 'Belum Diterima', NULL, NULL, NULL, NULL, '2019-10-21 00:00:00', NULL),
(9, 'PILLOW CASE 210 x 280 DK GREEN-DK BLUE TC 200 PLAIN PLAIN', NULL, 5, 200000, 2, 'waiting', NULL, NULL, NULL, 'Belum Diterima', NULL, NULL, NULL, NULL, '2019-10-26 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_custom_bahan`
--

CREATE TABLE `produk_custom_bahan` (
  `produk_custom_bahan_id` int(11) NOT NULL,
  `produk_custom_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_custom_bahan`
--

INSERT INTO `produk_custom_bahan` (`produk_custom_bahan_id`, `produk_custom_id`, `bahan_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(11, 6, 6, 2, '2019-10-15 14:22:58', NULL),
(12, 6, 6, 1, '2019-10-15 14:22:58', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`satuan_id`, `satuan_kode`, `satuan_nama`, `created_at`, `updated_at`) VALUES
(5, 'pcs', 'Pieces', '2019-10-03 11:58:59', '2019-10-03 03:58:59'),
(6, 'm', 'Meter', '2019-10-17 12:05:12', '2019-10-17 04:05:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `size`
--

CREATE TABLE `size` (
  `size_id` int(11) NOT NULL,
  `size_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `size`
--

INSERT INTO `size` (`size_id`, `size_nama`, `created_at`, `updated_at`) VALUES
(87, '-', '2019-10-08 10:12:40', '2019-10-08 02:12:40'),
(88, '25 x 25', '2019-10-08 10:15:39', '2019-10-08 02:15:39'),
(89, '30 x 30', '2019-10-08 10:15:47', '2019-10-08 02:15:47'),
(90, '40 x 70', '2019-10-08 10:16:07', '2019-10-08 02:16:07'),
(91, '50 x 75', '2019-10-08 10:16:35', '2019-10-08 02:16:35'),
(92, '70 x 140', '2019-10-08 10:16:51', '2019-10-08 02:16:51'),
(93, '85 x 150', '2019-10-08 10:17:19', '2019-10-08 02:17:19'),
(94, '90 x 180', '2019-10-08 10:17:34', '2019-10-08 02:17:34'),
(95, '86 x 150', '2019-10-08 10:18:05', '2019-10-08 02:18:05'),
(96, '86 x 170', '2019-10-08 10:18:50', '2019-10-08 02:18:50'),
(97, '80 x 150', '2019-10-08 10:19:51', '2019-10-08 02:19:51'),
(98, '180 x 280', '2019-10-08 10:24:19', '2019-10-08 02:24:19'),
(99, '190 x 280', '2019-10-08 10:24:30', '2019-10-08 02:24:30'),
(100, '210 x 280', '2019-10-08 10:24:37', '2019-10-08 02:24:37'),
(101, '230 x 280', '2019-10-08 10:24:53', '2019-10-08 02:24:53'),
(102, '250 x 280', '2019-10-08 10:25:09', '2019-10-08 02:25:09'),
(103, '260 x 280', '2019-10-08 10:25:26', '2019-10-08 02:25:26'),
(104, '270 x 280 ', '2019-10-08 10:25:52', '2019-10-08 02:25:52'),
(105, '280 x 280 ', '2019-10-08 10:25:57', '2019-10-08 02:25:57'),
(106, '290 x 280 ', '2019-10-08 10:26:05', '2019-10-08 02:26:05'),
(107, '150 x 230', '2019-10-08 10:28:45', '2019-10-08 02:28:45'),
(108, '160 x 230', '2019-10-08 10:28:55', '2019-10-08 02:28:55'),
(109, '180 x 230 ', '2019-10-08 10:29:04', '2019-10-08 02:29:04'),
(110, '200 x 230 ', '2019-10-08 10:29:14', '2019-10-08 02:29:14'),
(111, '220 x 230', '2019-10-08 10:29:21', '2019-10-08 02:29:21'),
(112, '90 x 200 / 30', '2019-10-08 10:30:09', '2019-10-08 02:30:09'),
(113, '100 x 200 / 30', '2019-10-08 10:30:16', '2019-10-08 02:30:16'),
(114, '120 x 200 / 30', '2019-10-08 10:30:28', '2019-10-08 02:30:28'),
(115, '140 x 200 / 30', '2019-10-08 10:30:40', '2019-10-08 02:30:40'),
(116, '160 x 200 / 30', '2019-10-08 10:30:50', '2019-10-08 02:30:50'),
(117, '180 x 200 / 30', '2019-10-08 10:31:20', '2019-10-08 02:31:20'),
(118, '200 x 200 / 30', '2019-10-08 10:32:00', '2019-10-08 02:32:00'),
(119, '240 x 230 ', '2019-10-08 10:36:14', '2019-10-08 02:36:14'),
(120, '260 x 230', '2019-10-08 10:36:22', '2019-10-08 02:36:22'),
(121, '280 x 230 ', '2019-10-08 10:36:35', '2019-10-08 02:36:35'),
(122, '50 x 70 ', '2019-10-08 10:37:23', '2019-10-08 02:37:23'),
(123, '50 x 90', '2019-10-08 10:37:36', '2019-10-08 02:37:36'),
(124, 'dia-23 x 90', '2019-10-08 10:38:01', '2019-10-08 02:38:01'),
(125, 'dia-23 x 100', '2019-10-08 10:38:15', '2019-10-08 02:38:15'),
(126, '50 x 95', '2019-10-08 10:38:36', '2019-10-08 02:38:36'),
(127, '40 x 40 ', '2019-10-08 10:40:54', '2019-10-08 02:40:54'),
(128, '45 x 45', '2019-10-08 10:43:02', '2019-10-08 02:43:02'),
(129, '50 x 50', '2019-10-08 10:43:17', '2019-10-08 02:43:17'),
(130, '60 x 60', '2019-10-08 10:43:42', '2019-10-08 02:43:42'),
(131, '90 x 200', '2019-10-08 10:44:59', '2019-10-08 02:44:59'),
(132, '100 x 200', '2019-10-08 10:45:08', '2019-10-08 02:45:08'),
(133, '120 x 200', '2019-10-08 10:45:13', '2019-10-08 02:45:13'),
(134, '110 x 200', '2019-10-08 10:45:33', '2019-10-08 02:45:33'),
(135, '140 x 200', '2019-10-08 10:45:49', '2019-10-08 02:45:49'),
(136, '160 x 200', '2019-10-08 10:45:58', '2019-10-08 02:45:58'),
(138, '180 x 200', '2019-10-08 10:46:30', '2019-10-08 02:46:30'),
(139, '200 x 200', '2019-10-08 10:47:27', '2019-10-08 02:47:27'),
(140, '285 x 230 ', '2019-10-08 10:51:27', '2019-10-08 02:51:27'),
(141, '280 x 240 ', '2019-10-08 13:33:39', '2019-10-08 05:33:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `staff`
--

INSERT INTO `staff` (`staff_id`, `nik`, `staff_nama`, `tempat_lahir`, `tanggal_lahir`, `staff_alamat`, `staff_status`, `staff_kelamin`, `mulai_bekerja`, `staff_email`, `staff_phone_number`, `staff_keterangan`, `created_at`, `updated_at`) VALUES
(1, '1909807889237', 'Astradanta', 'Denpasar, 13 Februari 1997', '1970-01-01', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', '2019-07-26 02:53:15');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `staff_lama_kerja`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `staff_lama_kerja` (
`staff_id` int(11)
,`lama_bekerja` varchar(30)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_bahan`
--

CREATE TABLE `stock_bahan` (
  `stock_bahan_id` int(11) NOT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `stock_bahan_lokasi_id` int(11) DEFAULT NULL,
  `stock_bahan_seri` varchar(50) DEFAULT NULL,
  `stock_bahan_qty` bigint(20) DEFAULT NULL,
  `stock_bahan_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_produk`
--

CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` bigint(20) NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` bigint(20) DEFAULT NULL,
  `stock_produk_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stock_produk`
--

INSERT INTO `stock_produk` (`stock_produk_id`, `produk_id`, `stock_produk_lokasi_id`, `month`, `year`, `urutan`, `stock_produk_seri`, `stock_produk_qty`, `hpp`, `last_stok`, `stock_produk_keterangan`, `delete_flag`, `created_at`, `updated_at`) VALUES
(39, 25, 1, '10', '19', 1, '1019003021', 8, 5000, NULL, 'OK', 1, '2019-10-03 15:31:54', '2019-10-03 07:36:11'),
(40, 188, 1, '10', '19', 1, '1019008021', -1, 12000, NULL, '', 0, '2019-10-04 08:50:11', '2019-10-08 06:52:28'),
(41, 188, 1, '10', '19', 2, '1019008022', 0, 0, NULL, '', 0, '2019-10-10 16:07:16', '2021-06-29 02:15:10'),
(42, 192, 1, '10', '19', 1, '1019009021', 0, 4, NULL, '', 0, '2019-10-15 14:58:07', '2019-10-15 07:04:55'),
(43, 11, 1, '10', '19', 1, '1019001021', 0, 0, NULL, '', 0, '2019-10-18 10:57:29', '2019-11-27 07:02:33'),
(44, 11, 1, '10', '19', 2, '1019001022', -4, 0, NULL, NULL, 0, '2019-10-21 15:28:25', '2021-07-08 03:26:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_menu`
--

CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `sub_menu_kode` varchar(50) DEFAULT NULL,
  `sub_menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `data` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_menu`
--

INSERT INTO `sub_menu` (`sub_menu_id`, `menu_id`, `sub_menu_kode`, `sub_menu_nama`, `action`, `data`, `url`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'penjualan', 'POS Reguler', 'delete|view', NULL, 'pos', NULL, '2019-06-18 03:17:08', '2019-10-26 03:57:03'),
(2, 1, 'custom-pos', 'POS Custom', 'delete|view', NULL, 'custom-pos', NULL, '2019-06-18 03:17:08', '2019-10-26 03:56:59'),
(3, 2, 'suplier', 'Suplier', 'list|add|edit|delete|view', NULL, 'suplier', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(4, 2, 'satuan', 'Satuan', 'list|add|edit|delete|view', NULL, 'satuan', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(5, 2, 'jenis-produk', 'Jenis Produk', 'list|add|edit|delete|view', NULL, 'jenis-produk', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(6, 2, 'jenis-bahan', 'Jenis Bahan', 'list|add|edit|delete|view', NULL, 'jenis-bahan', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(7, 2, 'lokasi', 'Lokasi', 'list|add|edit|delete|view', NULL, 'lokasi', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(8, 2, 'user', 'User', 'list|add|edit|delete|view', NULL, 'user', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(9, 2, 'user-role', 'User Role', '', NULL, 'user-role', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(10, 2, 'tipe-pembayaran', 'Tipe Pembayaran', 'list|add|edit|delete|view', NULL, 'tipe-pembayaran', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(11, 2, 'staff', 'Staf', 'list|add|edit|delete|view', NULL, 'staff', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(12, 4, 'bahan', 'Bahan', 'list|add|edit|delete|view|stock', NULL, 'bahan', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(13, 4, 'produk', 'Produk', 'list|add|edit|delete|view|stock|price|barcode', 'hpp', 'produk', NULL, '2019-06-18 03:51:52', '2021-07-09 08:01:51'),
(14, 6, 'produk-by-location', 'Produk per Lokasi', 'list', NULL, 'produk-by-location', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(16, 6, 'bahan-by-location', 'Bahan per Lokasi', 'list', NULL, 'bahan-by-location', NULL, '2019-06-28 15:26:43', '2019-06-28 07:26:43'),
(17, 4, 'transfer-bahan', 'Transfer Stok Bahan', 'list|transfer_bahan|konfirmasi_transfer_bahan|history_transfer_bahan', NULL, 'transfer-bahan', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(18, 4, 'transfer-produk', 'Transfer Stok Produk', 'list|transfer_produk|konfirmasi_transfer_produk|history_transfer_produk', NULL, 'transfer-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(19, 4, 'penyesuaian-bahan', 'Penyesuaian Stok Bahan', 'list|pesnyesuaian_bahan|history_penyesuaian_bahan', NULL, 'penyesuaian-bahan', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(20, 4, 'penyesuaian-produk', 'Penyesuaian Stok Produk', 'list|pesnyesuaian_produk|history_penyesuaian_produk', NULL, 'penyesuaian-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(21, 4, 'low-stock-bahan', 'Low Stock Bahan', 'list', NULL, 'low-stock-bahan', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(22, 4, 'low-stock-produk', 'Low Stock Produk', 'list', NULL, 'low-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(23, 4, 'kartu-stock-bahan', 'Kartu Stok Bahan', 'list', NULL, 'kartu-stock-bahan', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(24, 4, 'kartu-stock-produk', 'Kartu Stok Produk', 'list', NULL, 'kartu-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(25, 6, 'laporan-stock-bahan', 'Stok Bahan', 'list', NULL, 'laporan/laporan-stock-bahan', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(26, 6, 'laporan-stock-produk', 'Stok Produk', 'list', NULL, 'laporan/laporan-stock-produk', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(27, 6, 'laporan-po-bahan', 'Order Bahan', 'list', NULL, 'laporan/laporan-po-bahan', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(28, 6, 'laporan-penjualan', 'Penjualan', 'list', NULL, 'laporan/laporan-penjualan', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(29, 6, 'laporan-produksi', 'Produksi', 'list', NULL, 'laporan/laporan-produksi', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(30, 6, 'laporan-hutang', 'Hutang', 'list', NULL, 'laporan/laporan-hutang', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(31, 6, 'laporan-piutang', 'Piutang', 'list', NULL, 'laporan/laporan-piutang', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(34, 9, 'hutang', 'Hutang', '', NULL, 'hutang', NULL, '2019-07-08 11:19:09', '2019-07-08 03:19:09'),
(35, 9, 'piutang', 'Piutang', '', NULL, 'piutang', NULL, '2019-07-08 11:19:18', '2019-07-08 03:19:18'),
(36, 6, 'laporan-staff', 'Staff', 'list', NULL, 'laporan/laporan-staff', NULL, '2019-07-18 16:18:06', '2019-07-18 08:18:06'),
(37, 6, 'laporan-customer', 'Customer', 'list', NULL, 'laporan/laporan-customer', NULL, '2019-07-18 16:18:42', '2019-07-18 08:18:42'),
(38, 2, 'size', 'Size', 'list|add|edit|delete|view', NULL, 'size', NULL, '2019-08-05 11:07:44', '2019-08-05 03:08:10'),
(39, 2, 'color', 'Color', 'list|add|edit|delete|view', NULL, 'color', NULL, '2019-08-05 11:08:28', '2019-08-05 03:08:44'),
(40, 2, 'fabric', 'Fabric', 'list|add|edit|delete|view', NULL, 'fabric', NULL, '2019-08-05 11:09:02', '2019-08-05 03:09:02'),
(41, 2, 'patern', 'Patern', 'list|add|edit|delete|view', NULL, 'patern', NULL, '2019-08-05 11:09:22', '2019-08-05 03:09:22'),
(42, 5, 'produksi', 'Produksi reguler', 'list|add|edit|delete|view', NULL, 'produksi', NULL, '2019-10-10 16:00:55', '2019-10-10 08:01:45'),
(43, 5, 'custom-produksi', 'Produksi Custom', 'list|mulai|edit|view', NULL, 'custom-produksi', NULL, '2019-10-10 16:02:36', '2019-10-14 03:38:06'),
(44, 2, 'konversi-bahan', 'Konversi Bahan', 'list|add|edit|delete|view', NULL, 'konversi-bahan', NULL, '2019-10-16 13:47:41', '2019-10-16 08:17:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suplier`
--

CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `suplier`
--

INSERT INTO `suplier` (`suplier_id`, `suplier_kode`, `suplier_nama`, `suplier_alamat`, `suplier_telepon`, `suplier_email`, `created_at`, `updated_at`) VALUES
(5, NULL, 'pt . . ', NULL, NULL, NULL, '2019-10-04 10:26:54', '2019-10-04 02:26:54'),
(6, NULL, '', NULL, NULL, NULL, '2019-10-17 12:08:36', '2019-10-17 04:08:36'),
(7, NULL, 'asasas', NULL, NULL, NULL, '2020-02-04 16:53:54', '2020-02-04 08:53:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_pembayaran`
--

CREATE TABLE `tipe_pembayaran` (
  `tipe_pembayaran_id` int(11) NOT NULL,
  `tipe_pembayaran_kode` varchar(20) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `no_akun` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `additional` tinyint(1) DEFAULT NULL,
  `kembalian` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe_pembayaran`
--

INSERT INTO `tipe_pembayaran` (`tipe_pembayaran_id`, `tipe_pembayaran_kode`, `tipe_pembayaran_nama`, `no_akun`, `jenis_pembayaran`, `additional`, `kembalian`, `created_at`, `updated_at`) VALUES
(6, '01', 'Tunai', '', 'kas', NULL, 1, '2019-10-03 12:02:07', '2019-10-03 04:02:07'),
(7, '02', 'Visa', '', 'kas', 1, 0, '2019-10-20 15:30:11', '2019-10-20 08:14:35'),
(8, '03', 'Master Card', '', 'kas', 1, 0, '2019-10-20 15:30:22', '2019-10-20 08:14:43'),
(9, '04', 'American Expres (Amex)', '', 'kas', 1, 0, '2019-10-20 15:30:47', '2019-10-20 08:14:52'),
(10, '05', 'Debet', '', 'kas', 1, 0, '2019-10-20 15:31:45', '2019-10-20 08:14:55'),
(11, '06', 'Cek', '', 'kas', 1, 0, '2019-10-20 15:32:13', '2019-10-20 08:15:06'),
(12, '07', 'Bilyet Giro (BG)', '', 'kas', 1, 0, '2019-10-20 15:32:43', '2019-10-20 08:18:21'),
(13, '08', 'Piutang', '', 'kredit', NULL, 0, '2019-10-20 15:51:54', '2019-10-20 07:51:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `user_staff_id`, `password`, `user_role_id`, `avatar`, `lokasi_id`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$sIfZ5LEefpTjmSN9B0Wl6.gZ44N.km9zp34lIaTt8CIdIBs4g4uAS', 1, 'assets/media/users/1556714265JXOGF.jpg', NULL, '2019-05-01 18:06:31', '2019-05-01 12:37:45'),
(8, 7, '$2y$10$n4bsbwvtp5AwOt08Yq8J6e0IxFiZhdUPqXtDyqq.hpeQNIKRCiOAS', 7, 'assets/media/users/1570157779zD36G.jpg', 5, '2019-10-04 09:56:20', '2019-10-04 01:56:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_akses` text,
  `keterangan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_role_name`, `user_role_akses`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true},\"custom-pos\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":true},\"tipe-pembayaran\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"size\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"color\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"fabric\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"patern\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"konversi-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true},\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true,\"barcode\":true},\"transfer-bahan\":{\"akses_menu\":true,\"list\":true,\"transfer_bahan\":true,\"konfirmasi_transfer_bahan\":true,\"history_transfer_bahan\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-bahan\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_bahan\":true,\"history_penyesuaian_bahan\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"produksi\":{\"akses_menu\":true,\"produksi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"custom-produksi\":{\"akses_menu\":true,\"list\":true,\"mulai\":false,\"edit\":true,\"view\":true}},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true},\"piutang\":{\"akses_menu\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-po-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true}}}', NULL, '2019-04-26 00:00:00', '2021-07-09 08:02:53'),
(8, 'Manager', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":true,\"view\":true},\"custom-pos\":{\"akses_menu\":true,\"delete\":true,\"view\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"jenis-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"size\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"color\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"fabric\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"patern\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"konversi-bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true},\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true},\"transfer-bahan\":{\"akses_menu\":true,\"list\":true,\"transfer_bahan\":true,\"konfirmasi_transfer_bahan\":true,\"history_transfer_bahan\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-bahan\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_bahan\":true,\"history_penyesuaian_bahan\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"produksi\":{\"akses_menu\":true,\"produksi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"custom-produksi\":{\"akses_menu\":true,\"list\":true,\"mulai\":true,\"edit\":true,\"view\":true}},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true},\"piutang\":{\"akses_menu\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-po-bahan\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true}}}', '', '2019-10-20 14:46:59', '2019-10-26 03:57:44'),
(9, 'Admin Toko', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true},\"custom-pos\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":false,\"suplier\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"satuan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"lokasi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"size\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"color\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"fabric\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"patern\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"konversi-bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"bahan\":{\"akses_menu\":true,\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true,\"stock\":true},\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":false,\"edit\":false,\"delete\":false,\"view\":true,\"stock\":true,\"price\":true},\"transfer-bahan\":{\"akses_menu\":false,\"list\":false,\"transfer_bahan\":false,\"konfirmasi_transfer_bahan\":false,\"history_transfer_bahan\":false},\"transfer-produk\":{\"akses_menu\":false,\"list\":false,\"transfer_produk\":false,\"konfirmasi_transfer_produk\":false,\"history_transfer_produk\":false},\"penyesuaian-bahan\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_bahan\":true,\"history_penyesuaian_bahan\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-bahan\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"produksi\":{\"akses_menu\":false,\"produksi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"custom-produksi\":{\"akses_menu\":false,\"list\":false,\"mulai\":false,\"edit\":false,\"view\":false}},\"hutang_piutang\":{\"akses_menu\":false,\"hutang\":{\"akses_menu\":false},\"piutang\":{\"akses_menu\":false}},\"laporan\":{\"akses_menu\":false,\"produk-by-location\":{\"akses_menu\":false,\"list\":false},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":false,\"list\":false},\"laporan-po-bahan\":{\"akses_menu\":false,\"list\":false},\"laporan-penjualan\":{\"akses_menu\":false,\"list\":false},\"laporan-produksi\":{\"akses_menu\":false,\"list\":false},\"laporan-hutang\":{\"akses_menu\":false,\"list\":false},\"laporan-piutang\":{\"akses_menu\":false,\"list\":false},\"laporan-staff\":{\"akses_menu\":false,\"list\":false},\"laporan-customer\":{\"akses_menu\":false,\"list\":false}}}', '', '2019-10-20 14:47:31', '2019-10-26 03:58:18'),
(10, 'Kasir', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true},\"custom-pos\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":false,\"suplier\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"satuan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"jenis-bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"lokasi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"size\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"color\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"fabric\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"patern\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"konversi-bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false}},\"guest\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false},\"inventori\":{\"akses_menu\":false,\"bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"stock\":false},\"produk\":{\"akses_menu\":false,\"data\":{\"hpp\":false},\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"stock\":false,\"price\":false},\"transfer-bahan\":{\"akses_menu\":false,\"list\":false,\"transfer_bahan\":false,\"konfirmasi_transfer_bahan\":false,\"history_transfer_bahan\":false},\"transfer-produk\":{\"akses_menu\":false,\"list\":false,\"transfer_produk\":false,\"konfirmasi_transfer_produk\":false,\"history_transfer_produk\":false},\"penyesuaian-bahan\":{\"akses_menu\":false,\"list\":false,\"pesnyesuaian_bahan\":false,\"history_penyesuaian_bahan\":false},\"penyesuaian-produk\":{\"akses_menu\":false,\"list\":false,\"pesnyesuaian_produk\":false,\"history_penyesuaian_produk\":false},\"low-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"low-stock-produk\":{\"akses_menu\":false,\"list\":false},\"kartu-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"kartu-stock-produk\":{\"akses_menu\":false,\"list\":false}},\"order_bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"produksi\":{\"akses_menu\":false,\"produksi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"custom-produksi\":{\"akses_menu\":false,\"list\":false,\"mulai\":false,\"edit\":false,\"view\":false}},\"hutang_piutang\":{\"akses_menu\":false,\"hutang\":{\"akses_menu\":false},\"piutang\":{\"akses_menu\":false}},\"laporan\":{\"akses_menu\":false,\"produk-by-location\":{\"akses_menu\":false,\"list\":false},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-bahan\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":false,\"list\":false},\"laporan-po-bahan\":{\"akses_menu\":false,\"list\":false},\"laporan-penjualan\":{\"akses_menu\":false,\"list\":false},\"laporan-produksi\":{\"akses_menu\":false,\"list\":false},\"laporan-hutang\":{\"akses_menu\":false,\"list\":false},\"laporan-piutang\":{\"akses_menu\":false,\"list\":false},\"laporan-staff\":{\"akses_menu\":false,\"list\":false},\"laporan-customer\":{\"akses_menu\":false,\"list\":false}}}', '', '2019-10-20 14:51:10', '2019-10-26 03:58:04');

-- --------------------------------------------------------

--
-- Struktur untuk view `display_stock_bahan`
--
DROP TABLE IF EXISTS `display_stock_bahan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_bahan`  AS  select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah` from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by `bahan`.`bahan_id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `display_stock_bahan_lokasi`
--
DROP TABLE IF EXISTS `display_stock_bahan_lokasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_bahan_lokasi`  AS  select `bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`stock_bahan`.`stock_bahan_qty`)),0,sum(`stock_bahan`.`stock_bahan_qty`)) AS `jumlah`,`stock_bahan`.`stock_bahan_lokasi_id` AS `stock_bahan_lokasi_id` from (`bahan` left join `stock_bahan` on((`bahan`.`bahan_id` = `stock_bahan`.`bahan_id`))) where (`stock_bahan`.`delete_flag` = 0) group by `stock_bahan`.`stock_bahan_lokasi_id`,`stock_bahan`.`bahan_id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `display_stock_produk`
--
DROP TABLE IF EXISTS `display_stock_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_produk`  AS  select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by `produk`.`produk_id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `display_stock_produk_lokasi`
--
DROP TABLE IF EXISTS `display_stock_produk_lokasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_produk_lokasi`  AS  select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah`,`stock_produk`.`stock_produk_lokasi_id` AS `stock_produk_lokasi_id` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by `stock_produk`.`stock_produk_lokasi_id`,`stock_produk`.`produk_id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `staff_lama_kerja`
--
DROP TABLE IF EXISTS `staff_lama_kerja`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_lama_kerja`  AS  select `staff`.`staff_id` AS `staff_id`,if(((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12) >= 1),concat(round((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12),1),' Tahun'),if((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) = 0),'Kurang dari sebulan',concat(timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()),' Bulan'))) AS `lama_bekerja` from `staff` ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `arus_stock_bahan`
--
ALTER TABLE `arus_stock_bahan`
  ADD PRIMARY KEY (`arus_stock_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`) USING BTREE;

--
-- Indeks untuk tabel `fabric`
--
ALTER TABLE `fabric`
  ADD PRIMARY KEY (`fabric_id`) USING BTREE;

--
-- Indeks untuk tabel `forget_request`
--
ALTER TABLE `forget_request`
  ADD PRIMARY KEY (`forget_request_id`) USING BTREE;

--
-- Indeks untuk tabel `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`guest_id`) USING BTREE;

--
-- Indeks untuk tabel `harga_produk`
--
ALTER TABLE `harga_produk`
  ADD PRIMARY KEY (`harga_produk_id`) USING BTREE;

--
-- Indeks untuk tabel `history_penyesuaian_bahan`
--
ALTER TABLE `history_penyesuaian_bahan`
  ADD PRIMARY KEY (`history_penyesuaian_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  ADD PRIMARY KEY (`history_penyesuaian_produk_id`) USING BTREE;

--
-- Indeks untuk tabel `history_transfer_bahan`
--
ALTER TABLE `history_transfer_bahan`
  ADD PRIMARY KEY (`history_transfer_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  ADD PRIMARY KEY (`history_transfer_produk_id`) USING BTREE;

--
-- Indeks untuk tabel `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`hutang_id`) USING BTREE;

--
-- Indeks untuk tabel `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  ADD PRIMARY KEY (`jenis_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `jenis_produk`
--
ALTER TABLE `jenis_produk`
  ADD PRIMARY KEY (`jenis_produk_id`) USING BTREE;

--
-- Indeks untuk tabel `komposisi_produk`
--
ALTER TABLE `komposisi_produk`
  ADD PRIMARY KEY (`komposisi_id`) USING BTREE;

--
-- Indeks untuk tabel `konversi_bahan`
--
ALTER TABLE `konversi_bahan`
  ADD PRIMARY KEY (`konversi_bahan_id`);

--
-- Indeks untuk tabel `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  ADD PRIMARY KEY (`log_id`) USING BTREE;

--
-- Indeks untuk tabel `log_kasir`
--
ALTER TABLE `log_kasir`
  ADD PRIMARY KEY (`log_kasir_id`) USING BTREE;

--
-- Indeks untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`lokasi_id`) USING BTREE,
  ADD UNIQUE KEY `lokasi_kode` (`lokasi_kode`) USING BTREE;

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`) USING BTREE;

--
-- Indeks untuk tabel `patern`
--
ALTER TABLE `patern`
  ADD PRIMARY KEY (`patern_id`) USING BTREE;

--
-- Indeks untuk tabel `pembayaran_hutang`
--
ALTER TABLE `pembayaran_hutang`
  ADD PRIMARY KEY (`pembayaran_hutang_id`) USING BTREE;

--
-- Indeks untuk tabel `pembayaran_piutang`
--
ALTER TABLE `pembayaran_piutang`
  ADD PRIMARY KEY (`pembayaran_piutang_id`) USING BTREE;

--
-- Indeks untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`penjualan_id`) USING BTREE;

--
-- Indeks untuk tabel `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  ADD PRIMARY KEY (`penjualan_produk_id`) USING BTREE;

--
-- Indeks untuk tabel `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`piutang_id`) USING BTREE;

--
-- Indeks untuk tabel `po_bahan`
--
ALTER TABLE `po_bahan`
  ADD PRIMARY KEY (`po_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  ADD PRIMARY KEY (`po_bahan_detail_id`) USING BTREE,
  ADD KEY `po_bahan_constraint` (`po_bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`) USING BTREE;

--
-- Indeks untuk tabel `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`produksi_id`) USING BTREE;

--
-- Indeks untuk tabel `produksi_item`
--
ALTER TABLE `produksi_item`
  ADD PRIMARY KEY (`produksi_item_id`) USING BTREE,
  ADD KEY `produksi_item_relation` (`produksi_id`) USING BTREE;

--
-- Indeks untuk tabel `produksi_item_bahan`
--
ALTER TABLE `produksi_item_bahan`
  ADD PRIMARY KEY (`produksi_item_bahan_id`) USING BTREE,
  ADD KEY `produksi_item_bahan_relation` (`produksi_item_id`) USING BTREE;

--
-- Indeks untuk tabel `produk_custom`
--
ALTER TABLE `produk_custom`
  ADD PRIMARY KEY (`produk_custom_id`);

--
-- Indeks untuk tabel `produk_custom_bahan`
--
ALTER TABLE `produk_custom_bahan`
  ADD PRIMARY KEY (`produk_custom_bahan_id`);

--
-- Indeks untuk tabel `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`satuan_id`) USING BTREE;

--
-- Indeks untuk tabel `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`size_id`) USING BTREE;

--
-- Indeks untuk tabel `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`) USING BTREE;

--
-- Indeks untuk tabel `stock_bahan`
--
ALTER TABLE `stock_bahan`
  ADD PRIMARY KEY (`stock_bahan_id`) USING BTREE,
  ADD KEY `stock_bahan_constraint` (`bahan_id`) USING BTREE;

--
-- Indeks untuk tabel `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD PRIMARY KEY (`stock_produk_id`) USING BTREE,
  ADD KEY `stock_produk_constraint` (`produk_id`) USING BTREE;

--
-- Indeks untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`sub_menu_id`) USING BTREE;

--
-- Indeks untuk tabel `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`suplier_id`) USING BTREE;

--
-- Indeks untuk tabel `tipe_pembayaran`
--
ALTER TABLE `tipe_pembayaran`
  ADD PRIMARY KEY (`tipe_pembayaran_id`) USING BTREE;

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `arus_stock_bahan`
--
ALTER TABLE `arus_stock_bahan`
  MODIFY `arus_stock_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT untuk tabel `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT untuk tabel `bahan`
--
ALTER TABLE `bahan`
  MODIFY `bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `fabric`
--
ALTER TABLE `fabric`
  MODIFY `fabric_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `forget_request`
--
ALTER TABLE `forget_request`
  MODIFY `forget_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `guest`
--
ALTER TABLE `guest`
  MODIFY `guest_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `harga_produk`
--
ALTER TABLE `harga_produk`
  MODIFY `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `history_penyesuaian_bahan`
--
ALTER TABLE `history_penyesuaian_bahan`
  MODIFY `history_penyesuaian_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  MODIFY `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `history_transfer_bahan`
--
ALTER TABLE `history_transfer_bahan`
  MODIFY `history_transfer_bahan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  MODIFY `history_transfer_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `hutang`
--
ALTER TABLE `hutang`
  MODIFY `hutang_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  MODIFY `jenis_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `jenis_produk`
--
ALTER TABLE `jenis_produk`
  MODIFY `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `komposisi_produk`
--
ALTER TABLE `komposisi_produk`
  MODIFY `komposisi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `konversi_bahan`
--
ALTER TABLE `konversi_bahan`
  MODIFY `konversi_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `log_kasir`
--
ALTER TABLE `log_kasir`
  MODIFY `log_kasir_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `lokasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `patern`
--
ALTER TABLE `patern`
  MODIFY `patern_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_hutang`
--
ALTER TABLE `pembayaran_hutang`
  MODIFY `pembayaran_hutang_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_piutang`
--
ALTER TABLE `pembayaran_piutang`
  MODIFY `pembayaran_piutang_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `penjualan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT untuk tabel `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  MODIFY `penjualan_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `piutang`
--
ALTER TABLE `piutang`
  MODIFY `piutang_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `po_bahan`
--
ALTER TABLE `po_bahan`
  MODIFY `po_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  MODIFY `po_bahan_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT untuk tabel `produksi`
--
ALTER TABLE `produksi`
  MODIFY `produksi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `produksi_item`
--
ALTER TABLE `produksi_item`
  MODIFY `produksi_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `produksi_item_bahan`
--
ALTER TABLE `produksi_item_bahan`
  MODIFY `produksi_item_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `produk_custom`
--
ALTER TABLE `produk_custom`
  MODIFY `produk_custom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `produk_custom_bahan`
--
ALTER TABLE `produk_custom_bahan`
  MODIFY `produk_custom_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `satuan`
--
ALTER TABLE `satuan`
  MODIFY `satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `size`
--
ALTER TABLE `size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT untuk tabel `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `stock_bahan`
--
ALTER TABLE `stock_bahan`
  MODIFY `stock_bahan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `stock_produk`
--
ALTER TABLE `stock_produk`
  MODIFY `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `suplier`
--
ALTER TABLE `suplier`
  MODIFY `suplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tipe_pembayaran`
--
ALTER TABLE `tipe_pembayaran`
  MODIFY `tipe_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  ADD CONSTRAINT `po_bahan_detail_ibfk_1` FOREIGN KEY (`po_bahan_id`) REFERENCES `po_bahan` (`po_bahan_id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `produksi_item`
--
ALTER TABLE `produksi_item`
  ADD CONSTRAINT `produksi_item_ibfk_1` FOREIGN KEY (`produksi_id`) REFERENCES `produksi` (`produksi_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `produksi_item_bahan`
--
ALTER TABLE `produksi_item_bahan`
  ADD CONSTRAINT `produksi_item_bahan_ibfk_1` FOREIGN KEY (`produksi_item_id`) REFERENCES `produksi_item` (`produksi_item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stock_bahan`
--
ALTER TABLE `stock_bahan`
  ADD CONSTRAINT `stock_bahan_ibfk_1` FOREIGN KEY (`bahan_id`) REFERENCES `bahan` (`bahan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD CONSTRAINT `stock_produk_ibfk_1` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
