<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

$route['home/country'] = "Home/country";
$route['home/most'] = "Home/most";
$route['home/least'] = "Home/least";
$route['home/sales'] = "Home/sales";
$route['home/test-view'] = "Home/test_view";

$route['404_override'] = 'page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['login/auth'] = "Login/auth";
$route['login/multi_check'] = "Login/multi";
$route['logout'] = "Login/logout";
$route['forget'] = "Login/forget";
$route['forget/:any/:any'] = 'Login/forget_index';
$route['reset-password'] = "Login/reset_password";
$route['upload-image'] = "Home/upload_image";

$route['profile'] = "Profile";
$route['profile/personal-info'] = "Profile/personal_info";
$route['profile/change-password'] = "Profile/change_password";
$route['profile/change-avatar'] = "Profile/change_avatar";
$route['reset-stok'] = "Profile/reset_stok";
$route['reset-sistem'] = "Profile/reset_sistem";

$route['user'] = "UserController";
$route['user/list'] = "UserController/list";
$route['user/add'] = "UserController/add";
$route['user/edit'] = "UserController/edit";
$route['user/delete'] = "UserController/delete";

$route['user-role'] = "UserRoleController";
$route['user-role/list'] = "UserRoleController/list";
$route['user-role/add'] = "UserRoleController/add";
$route['user-role/edit'] = "UserRoleController/edit";
$route['user-role/delete'] = "UserRoleController/delete";
$route['user-role/access/:any'] = "RoleAksesController";
$route['user-role/access/edit/:num'] = "RoleAksesController/edit";

$route['staff'] = "StaffController";
$route['staff/list'] = "StaffController/list";
$route['staff/add'] = "StaffController/add";
$route['staff/edit'] = "StaffController/edit";
$route['staff/delete'] = "StaffController/delete";

$route['lokasi'] = "LokasiController";
$route['lokasi/list'] = "LokasiController/list";
$route['lokasi/add'] = "LokasiController/add";
$route['lokasi/edit'] = "LokasiController/edit";
$route['lokasi/delete'] = "LokasiController/delete";

$route['tipe-pembayaran'] = "TipePembayaranController";
$route['tipe-pembayaran/list'] = "TipePembayaranController/list";
$route['tipe-pembayaran/add'] = "TipePembayaranController/add";
$route['tipe-pembayaran/edit'] = "TipePembayaranController/edit";
$route['tipe-pembayaran/delete'] = "TipePembayaranController/delete";

$route['jenis-mobil'] = "JenisMobilController";
$route['jenis-mobil/list'] = "JenisMobilController/list";
$route['jenis-mobil/add'] = "JenisMobilController/add";
$route['jenis-mobil/edit'] = "JenisMobilController/edit";
$route['jenis-mobil/delete'] = "JenisMobilController/delete";
$route['jenis-mobil/options'] = "JenisMobilController/options";

$route['investor'] = "InvestorController";
$route['investor/list'] = "InvestorController/list";
$route['investor/add'] = "InvestorController/add";
$route['investor/edit'] = "InvestorController/edit";
$route['investor/delete'] = "InvestorController/delete";

$route['rekanan'] = "RekananController";
$route['rekanan/list'] = "RekananController/list";
$route['rekanan/add'] = "RekananController/add";
$route['rekanan/edit'] = "RekananController/edit";
$route['rekanan/delete'] = "RekananController/delete";

$route['guest'] = "GuestController";
$route['guest/list'] = "GuestController/list";
$route['guest/add'] = "GuestController/add";
$route['guest/edit'] = "GuestController/edit";
$route['guest/delete'] = "GuestController/delete";

$route['arus-kas'] = "ArusKasController";
$route['arus-kas/list'] = "ArusKasController/list";
$route['arus-kas/pdf'] = "ArusKasController/pdf";
$route['arus-kas/excel'] = "ArusKasController/excel";
$route['arus-kas/insert'] = "ArusKasController/insert";
$route['arus-kas/update'] = "ArusKasController/update";
$route['arus-kas/delete/:any'] = "ArusKasController/delete";

$route['arus-kendaraan'] = "ArusKendaraanController";
$route['arus-kendaraan/list'] = "ArusKendaraanController/list";
$route['arus-kendaraan/add'] = "ArusKendaraanController/add";
$route['arus-kendaraan/edit'] = "ArusKendaraanController/edit";
$route['arus-kendaraan/delete'] = "ArusKendaraanController/delete";
$route['jadwal-mobil-besok'] = "ArusKendaraanController/pencatatan_besok_admin_index";
$route['pencatatan-kendaraan'] = "ArusKendaraanController/pencatatan_index";
$route['pencatatan-kendaraan-besok'] = "ArusKendaraanController/pencatatan_besok_index";
$route['jadwal-mobil/update'] = "ArusKendaraanController/jadwal_update";

$route['produk'] = "ProdukController";
$route['produk/list'] = "ProdukController/list";
$route['produk/add'] = "ProdukController/add";
$route['produk/edit'] = "ProdukController/edit";
$route['produk/harga/:num'] = "ProdukController/harga";
$route['produk/delete'] = "ProdukController/delete";

$route['mobil'] = "MobilController";
$route['mobil/list'] = "MobilController/list";
$route['mobil/add'] = "MobilController/add";
$route['mobil/edit'] = "MobilController/edit";
$route['mobil/delete'] = "MobilController/delete";

$route['penawaran'] = "PenawaranController";
$route['daftar-penawaran'] = "PenawaranController/index_list";
$route['penawaran/list'] = "PenawaranController/list";
$route['penawaran/detail/:num'] = "PenawaranController/detail";
$route['penawaran/save'] = "PenawaranController/save";
$route['penawaran/print/:num'] = "PenawaranController/print";

$route['penyewaan'] = "PenyewaanController";
$route['daftar-penyewaan'] = "PenyewaanController/index_list";
$route['penyewaan/list'] = "PenyewaanController/list";
$route['penyewaan/detail/:num'] = "PenyewaanController/detail";
$route['penyewaan/save'] = "PenyewaanController/save";
$route['penyewaan/delete'] = "PenyewaanController/delete";
$route['penyewaan/print/:num'] = "PenyewaanController/print";
$route['penyewaan/invoice/:num'] = "PenyewaanController/invoice";
$route['invoice-penyewaan/save/:num'] = "PenyewaanController/save_invoice";
$route['invoice-penyewaan/print/:num'] = "PenyewaanController/print_invoice";
$route['invoice-penyewaan-pemerintah/print/:num'] = "PenyewaanController/print_invoice_pemerintah";
$route['edit-penyewaan/:num'] = "PenyewaanController/edit";
$route['edit-penyewaan/update'] = "PenyewaanController/update";

$route['list/guest'] = "ListController/guest_list";
$route['list/jadwal-kedepan'] = "ListController/jadwal_kendaraan_kedepan_list";
$route['list/history-driver'] = "ListController/history_driver_list";
$route['list/utility/:any'] = "ListController/utility";
$route['list/mobil'] = "ListController/mobil_list";
$route['list/driver'] = "ListController/driver_list";
$route['list/penawaran'] = "ListController/penawaran_list";
$route['list/jadwal-today'] = "ListController/jadwal_kendaraan_today_list";
$route['list/jadwal-besok'] = "ListController/jadwal_kendaraan_besok_list";
$route['list/history-pengeluaran-driver'] = "ListController/history_pengeluaran_driver";

$route['hutang'] = "HutangController";
$route['hutang/list'] = "HutangController/list";
$route['hutang/pay/:any'] = "HutangController/pay";
$route['hutang/pay/list/:num'] = "HutangController/pay_list";
$route['hutang/pay/add/:num'] = "HutangController/add";
$route['hutang/pay/edit/:num'] = "HutangController/edit";
$route['hutang/pay/delete/:num'] = "HutangController/delete";
$route['hutang/detail/:num'] = "HutangController/detail";

$route['piutang'] = "PiutangController";
$route['piutang/list'] = "PiutangController/list";
$route['piutang/pay/:any'] = "PiutangController/pay";
$route['piutang/pay/list/:num'] = "PiutangController/pay_list";
$route['piutang/pay/add/:num'] = "PiutangController/add";
$route['piutang/pay/edit/:num'] = "PiutangController/edit";
$route['piutang/pay/delete/:num'] = "PiutangController/delete";
$route['piutang/detail/:num'] = "PiutangController/detail";
$route['piutang/cek-status'] = "PiutangController/cek_status";

$route['laporan/:any'] = "LaporanController";
$route['laporan/:any/:any'] = "LaporanController";
$route['laporan/:any/:any/:any'] = "LaporanController";

$route['kegiatan-driver'] = "KegiatanDriverController";
$route['kegiatan-driver/list'] = "KegiatanDriverController/list";
$route['kegiatan-driver/post'] = "KegiatanDriverController/post";
$route['kegiatan-driver/update'] = "KegiatanDriverController/update";
$route['kegiatan-driver/bayar'] = "KegiatanDriverController/bayar";
$route['pengeluaran-driver/add'] = "PengeluaranDriverController/add";
$route['pengeluaran-driver/edit/:any'] = "PengeluaranDriverController/edit";
$route['pengeluaran-driver/update'] = "PengeluaranDriverController/update";

