<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KegiatanDriverController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kegiatan_driver', '', true);
		$this->load->model('arus_kas', '', true);
		$this->load->model('jadwal_mobil', '', true);
		$this->load->model('penyewaan_mobil', '', true);
		$this->load->model('user', '', true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/kegiatan_driver.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Kegiatan Driver < Waisnawa";
		$data['parrent'] = "kegiatan-sriver";
		$data['page'] = $this->uri->segment(1);

		$user = $this->user->user_by_id($_SESSION['login']['user_id']);
		$penyewaan_mobil = $this->penyewaan_mobil->row_by_condition(array('driver_id' => $user->user_id), 'penyewaan_id', 'desc');
		$data['kegiatan'] = array();
		if ($penyewaan_mobil) {
			$jadwal_today = $this->jadwal_mobil->row_by_condition(array('mobil_id' => $penyewaan_mobil->mobil_id, 'penyewaan_id' => $penyewaan_mobil->penyewaan_id, 'tanggal' => date('Y-m-d')));
			$kegiatan_today = $this->kegiatan_driver->row_by_condition(array('driver_id' => $user->user_id, 'tanggal' => date('Y-m-d')));

			if (!empty($kegiatan_today)) {
				$data['kegiatan'] = $kegiatan_today;
			} else {
				if (!empty($jadwal_today)) {
					$kegiatan = array(
						'driver_id' => $user->user_id,
						'driver_nama' => $user->staff_nama,
						'mobil_id' => $jadwal_today->mobil_id,
						'tanggal' => $jadwal_today->tanggal,
						'mobil_jenis' => $jadwal_today->mobil_jenis_nama,
						'mobil_plat' => $jadwal_today->mobil_plat,
						'penyewaan_id' => $jadwal_today->penyewaan_id,
						'status' => 'Order',
						'waktu_mulai' => null,
						'waktu_selesai' => null
					);

					$this->kegiatan_driver->insert($kegiatan);
					$kegiatan['kegiatan_driver_id'] = $this->kegiatan_driver->last_id();

					$kegiatan = json_decode(json_encode($kegiatan), FALSE);

					$data['kegiatan'] = $kegiatan;
				}
			}
		}

		$data['user'] = $user;
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/kegiatan_driver');
		$this->load->view('admin/static/footer');
	}

	function post()
	{
		$kegiatan_driver_id = $this->input->post('kegiatan_driver_id');
		$waktu_mulai = $this->input->post('waktu_mulai');
		$waktu_selesai = $this->input->post('waktu_mulai');

		$data = array();
		$data['mobil_id'] = $this->input->post('mobil_id');
		$data['mobil_jenis'] = $this->input->post('mobil_jenis');
		$data['mobil_plat'] = $this->input->post('mobil_plat');
		$data['driver_id'] = $this->input->post('driver_id');
		$data['driver_nama'] = $this->input->post('driver_nama');
		$data['tanggal'] = $this->input->post('tanggal');
		$data['penyewaan_id'] = $this->input->post('penyewaan_id');
		$data['jenis_pekerjaan'] = $this->input->post('jenis_pekerjaan');

		if ($waktu_mulai) {
			$data['waktu_selesai'] = date('H:i:s');
			$data['status'] = 'Selesai';

			$starttime = $waktu_mulai;
			$stoptime = $data['waktu_selesai'];
			$diff = (strtotime($stoptime) - strtotime($starttime));
			$total = $diff / 60;
			$waktu_kerja = sprintf("%02d Jam %02d Menit", floor($total / 60), $total % 60);
			$data['waktu_kerja'] = $waktu_kerja;

		} else {
			$data['waktu_mulai'] = date('H:i:s');
			$data['status'] = 'Dalam Proses';
		}

		if ($kegiatan_driver_id) {
			$insert = $this->kegiatan_driver->update_by_id('kegiatan_driver_id', $kegiatan_driver_id, $data);
		} else {
			$insert = $this->kegiatan_driver->insert($data);
		}

		if ($insert) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}

	function update()
	{
		$result = array(
			'success' => false
		);
		$kegiatan_driver_id = $this->input->post('kegiatan_driver_id');
		$waktu_mulai = $this->format_time($this->input->post('waktu_mulai'));
		$waktu_selesai = $this->format_time($this->input->post('waktu_selesai'));

		$data['waktu_mulai'] = $waktu_mulai;
		$data['waktu_selesai'] = $waktu_selesai;

		$starttime = $waktu_mulai;
		$stoptime = $waktu_selesai;
		$diff = (strtotime($stoptime) - strtotime($starttime));
		$total = $diff / 60;
		$waktu_kerja = sprintf("%02d Jam %02d Menit", floor($total / 60), $total % 60);
		$data['waktu_kerja'] = $waktu_kerja;

		$insert = $this->kegiatan_driver->update_by_id('kegiatan_driver_id', $kegiatan_driver_id, $data);

		if ($insert) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}

	function bayar()
	{
		$result = array(
			'success' => false
		);
		$result['message'] = "Gagal menyimpan data";
		$kegiatan_driver_id = $this->input->post('kegiatan_driver_id');
		$bukti_pembayaran = $this->input->post('input_bukti_pembayaran');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');
		$lokasi_id = $this->input->post('lokasi_id');
		$lokasi_nama = $this->input->post('lokasi_nama');
		$tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
		$jumlah_bayaran = $this->string_to_number_new($this->input->post('jumlah_bayaran'));

		$data_pembayaran = array(
			'bukti_pembayaran' => $bukti_pembayaran,
			'jumlah_bayaran' => $jumlah_bayaran,
			'tipe_pembayaran_id' => $tipe_pembayaran_id,
			'tipe_pembayaran' => $tipe_pembayaran,
			'status_pembayaran' => 'Sudah Dibayar'
		);

		$update = $this->kegiatan_driver->update_by_id('kegiatan_driver_id', $kegiatan_driver_id, $data_pembayaran);

		if ($update) {
			$kegiatan = $this->kegiatan_driver->row_by_id($kegiatan_driver_id);
			$data_arus = array(
				'kegiatan_driver_id' => $kegiatan_driver_id,
				'tipe_pembayaran_id' => $tipe_pembayaran_id,
				'user_id' => $_SESSION['login']['user_id'],
				'lokasi_id' => $lokasi_id,
				'lokasi_nama' => $lokasi_nama,
				'jenis' => 'pengeluaran',
				'insert' => 'otomatis',
				'jumlah' => $jumlah_bayaran,
				'keterangan' => 'Pembayaran Supir ' . $kegiatan->driver_nama . ' pada tanggal ' . $this->date_label($kegiatan->tanggal),
			);

			$check = $this->arus_kas->row_by_condition(array('kegiatan_driver_id' => $kegiatan_driver_id));

			if (!empty($check)){
				$this->arus_kas->update_by_id('kegiatan_driver_id', $kegiatan_driver_id, $data_arus);
			}else{
				$data_arus['tanggal'] = date('Y-m-d');
				$this->arus_kas->insert($data_arus);
			}
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		}

		echo json_encode($result);
	}

}

/* End of file JenisProdukController.php */
/* Location: ./application/controllers/JenisProdukController.php */
