<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratJalanController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('surat_jalan', '', true);
		$this->load->model('surat_jalan_produk', '', true);
		$this->load->model('penjualan_produk', '', true);
		$this->load->model('guest', '', true);
		$this->load->model('stock_produk', '', true);
		$this->load->model('pos', '', true);
		$this->load->model('lokasi', '', true);
		$this->load->model('harga_produk', '', true);
		$this->load->model('tipe_pembayaran', '', true);
		$this->load->model('log_kasir', '', true);
		$this->load->model('stock_produk', '', true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/rekap_pos.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Surat Jalan < System Balioz Linen";
		$data['parrent'] = "surat_jalan";
		$data['page'] = "surat_jalan";
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal"));
		array_push($column, array("data" => "no_faktur"));
		array_push($column, array("data" => "lokasi_nama"));
		array_push($column, array("data" => "nama_pelanggan"));
		array_push($column, array("data" => "total"));
		array_push($column, array("data" => "biaya_tambahan"));
		array_push($column, array("data" => "potongan_akhir"));
		array_push($column, array("data" => "grand_total"));
		array_push($column, array("data" => "tipe_pembayaran_nama"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0, 5, 6, 7, 8)));
		$data['sumColumn'] = json_encode(array(5, 6, 7, 8));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();

		$action["surat_jalan"] = true;
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/surat_jalan');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		$lokasi_id = null;
//		echo json_encode(isset($_SESSION['login']['lokasi_id']));
		if (isset($_SESSION['login']['lokasi_id'])){
			$lokasi_id = $_SESSION['login']['lokasi_id'];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->surat_jalan->penjualan_count_all($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->surat_jalan->penjualan_count_all($query, $lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->surat_jalan->penjualan_list($start, $length, $query, $lokasi_id);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->total = number_format($key->total);
			$key->biaya_tambahan = number_format($key->biaya_tambahan);
			$key->potongan_akhir = number_format($key->potongan_akhir + $key->nominal_potongan_persen);
			$key->grand_total = number_format($key->grand_total);
			$key->row_id = $key->penjualan_id;
			$key->surat_jalan_url = base_url() . "surat-jalan/create/" . $key->penjualan_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	public function index_sj()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/history_sj.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "History Surat Jalan < System Balioz Linen";
		$data['parrent'] = "surat_jalan";
		$data['page'] = 'surat_jalan';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "no_surat_jalan"));
		array_push($column, array("data" => "no_faktur"));
		array_push($column, array("data" => "tanggal_kirim"));
		array_push($column, array("data" => "pelanggan"));
		array_push($column, array("data" => "kontak"));
		array_push($column, array("data" => "alamat"));
		$data['column'] = json_encode($column);
		$akses_menu = json_decode($this->menu_akses, true);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$action = array();

		$action["view"] = true;
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/surat_jalan_list');
		$this->load->view('admin/static/footer');
	}

	function list_sj()
	{
		$lokasi_id = null;
//		echo json_encode(isset($_SESSION['login']['lokasi_id']));
		if (isset($_SESSION['login']['lokasi_id'])){
			$lokasi_id = $_SESSION['login']['lokasi_id'];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->surat_jalan->sj_count_all($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->surat_jalan->sj_count_all($query,$lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->surat_jalan->sj_list($start, $length, $query,$lokasi_id);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->row_id = $key->surat_jalan_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function detail()
	{
		$id = $this->uri->segment(3);
		$data = $this->surat_jalan->detailItem($id);
		$result['data'] = $data;
		foreach ($result['data'] as $key) {
			$key->qty_kirim = number_format($key->qty_kirim);
			if ($key->produk_nama == null) {
				$key->produk_nama = $key->deskripsi;
			}
			if ($key->produk_kode == null) {
				$key->produk_kode = "custom";
			}
		}
		echo json_encode($result);
	}

	public function create()
	{

		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");

		array_push($this->js, "script/admin/surat_jalan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Tambah Data < Surat Jalan < System Balioz Linen";
		$data['parrent'] = "Surat Jalan";
		$data['page'] ='surat_jalan';
		$id = $this->uri->segment(3);
		$data['no_surat_jalan'] = $this->surat_jalan->getSJCode();
		$data['urutan'] = $this->surat_jalan->getUrutan();
		$data['penjualan'] = $this->pos->penjualan_by_id($id);
		$data['penjualan_produk'] = $this->pos->detailTransaksiSJ($id);

//		echo json_encode($data);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/surat_jalan_create');
		$this->load->view('admin/static/footer');
	}

	function save()
	{
		$result['success'] = false;
		$result['message'] = 'Gagal menyimpan data';

		$qty_kirim_arr = $this->input->post('qty_kirim');
		$penjualan_produk_id_arr = $this->input->post('penjualan_produk_id');
		$produk_id_arr = $this->input->post('produk_id');
		$produk_custom_id_arr = $this->input->post('produk_custom_id');
		$max_kirim_arr = $this->input->post('max_kirim');
		$stock_produk_id_arr = $this->input->post('stock_produk_id');

		$total_qty = 0;

		foreach ($qty_kirim_arr as $qty) {
			$qty = $this->string_to_number($qty);
			$total_qty += $qty;
		}

		if ($total_qty == 0) {
			$result['message'] = 'Total produk yang anda kirim 0';
		} else {
			$data = array();
			$this->db->trans_begin();
			$data['user_id'] = $_SESSION['login']['user_id'];
			$data['penjualan_id'] = $this->input->post('penjualan_id');
			$data['urutan'] = $this->input->post('urutan');
			$data['no_surat_jalan'] = $this->input->post('no_surat_jalan');
			$data['pelanggan'] = $this->input->post('pelanggan');
			$data['alamat'] = $this->input->post('alamat');
			$data['kontak'] = $this->input->post('kontak');
			$data['tanggal_kirim'] = $this->date_database_format($this->input->post('tanggal_kirim'));
			$data['created_at'] = Date('Y-m-d H:i:s');
			$insert = $this->surat_jalan->insert($data);
			$surat_jalan_id = $this->surat_jalan->last_id();
			foreach ($qty_kirim_arr as $key => $qty_kirim) {
				$qty_kirim = $this->string_to_number($qty_kirim);
				$stock_produk_id = $stock_produk_id_arr[$key];
				if ($qty_kirim > 0){
					$sisa_baru = $max_kirim_arr[$key] - $qty_kirim;
					$data_kirim = array();
					$data_kirim['surat_jalan_id'] = $surat_jalan_id;
					$data_kirim['qty_kirim'] = $qty_kirim;
					$data_kirim['penjualan_produk_id'] = $penjualan_produk_id_arr[$key];
					$data_kirim['produk_id'] = $produk_id_arr[$key];
					$data_kirim['produk_custom_id'] = $produk_custom_id_arr[$key];
					$data_kirim['created_at'] = Date('Y-m-d H:i:s');

					$this->surat_jalan_produk->insert($data_kirim);

					$data_penjualan = array(
						'sisa_qty' => $sisa_baru,
						'updated_at' => Date('Y-m-d H:i:s')
					);
					$this->penjualan_produk->update_by_id('penjualan_produk_id',$penjualan_produk_id_arr[$key],$data_penjualan);

					if ($produk_id_arr[$key]){
						$data_arus = array();
						$data_arus["tanggal"] = date("Y-m-d");
						$data_arus["table_name"] = "stock_produk";
						$data_arus["stock_produk_id"] = $stock_produk_id;
						$data_arus["produk_id"] = $produk_id_arr[$key];
						$data_arus["stock_out"] = $qty_kirim;
						$data_arus["stock_in"] = 0;
						$data_arus["last_stock"] = $this->stock_produk->last_stock($produk_id_arr[$key])->result;
						$data_arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
						$data_arus["keterangan"] = "Penjualan produk";
						$data_arus["method"] = "delete";
						$this->stock_produk->arus_stock_produk($data_arus);
					}
				}
			}

			$this->db->trans_commit();
			if($insert){
				$result['success'] = true;
				$result['message'] = 'Berhasil menyimpan data';
				$result['id'] = $surat_jalan_id;
			}
		}

		echo json_encode($result);
	}

	function print(){
		$id = $this->uri->segment(3);
		$sj = $this->surat_jalan->sj_by_id($id);
		$item = $this->surat_jalan->detailItem($id);
		$sj->tanggal_kirim = $this->date_label( $sj->tanggal_kirim);
		foreach ($item as $key){
			if($key->produk_nama == null){
				$key->produk_nama = $key->deskripsi;
			}
			if($key->produk_kode == null){
				$key->produk_kode = "custom";
			}
			if($key->satuan_nama == null){
				$key->satuan_nama = $key->satuan_custom;
			}
			$key->qty_kirim_label = $this->format_number($key->qty_kirim);
		}
		$data["sj"] = $sj;
		$data["item"] = $item;
//		echo json_encode($data);
		$this->load->view('admin/print_sj', $data);
	}
}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
