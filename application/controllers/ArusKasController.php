<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class ArusKasController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('arus_kas', '', true);
		$this->load->model('tipe_pembayaran', '', true);
		$this->load->model('lokasi', '', true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Arus Kas < Inventori < Waisnawa Transport";
		$data['parrent'] = "arus-kas";
		$data['page'] = 'arus_kas';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal"));
		array_push($column, array("data" => "jenis_lbl"));
		array_push($column, array("data" => "jumlah"));
		array_push($column, array("data" => "tipe_pembayaran_nama"));
		array_push($column, array("data" => "keterangan"));
		array_push($column, array("data" => "staff_nama"));
		if (!isset($_SESSION["login"]['lokasi_id'])) {
			array_push($column, array("data" => "lokasi_lbl"));
		}
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['arus_kas'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$action['view'] = false;
//        $action['delete'] = true;
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0, 3)));
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/arus_kas_list');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		if(isset($_GET["columns"][8]["search"]["value"]) && $_GET["columns"][8]["search"]["value"] != ""){
			$_GET['tipe_pembayaran'] = $_GET["columns"][8]["search"]["value"];
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->arus_kas->arus_count_all();
		$result['iTotalDisplayRecords'] = $this->arus_kas->arus_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->arus_kas->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->tanggal_lbl = null;
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);
			}
			$key->no = $i;
			$key->jenis_lbl = ucfirst($key->jenis);
			$key->jumlah = number_format($key->jumlah);
			$key->delete_url = base_url() . 'arus-kas/delete/' . $key->arus_kas_id;
			$key->row_id = $key->arus_kas_id;
			if ($key->insert == 'otomatis') {
				$key->deny_edit = true;
				$key->deny_delete = true;
			}
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function insert()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$lokasi_id = $this->input->post('lokasi_id');
		if (isset($_SESSION["login"]["lokasi_id"])) {
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
			$lokasi_id = $_SESSION["login"]["lokasi_id"];
		}
		$tipe_pembayaran = $this->tipe_pembayaran->row_by_id($this->input->post('tipe_pembayaran_id'));
		$tanggal = $this->input->post('tanggal');
		$time = strtotime($tanggal);
		$data['tanggal'] = date('Y-m-d', $time);
		$data["tipe_pembayaran_nama"] = $tipe_pembayaran->tipe_pembayaran_nama . " " . $tipe_pembayaran->no_akun;
		$data["user_id"] = isset($_SESSION['login']['user_id']) ? $_SESSION['login']['user_id'] : null;
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		$data["lokasi_id"] = $lokasi_id;
		$data["jenis"] = $this->input->post('jenis');
		$data["insert"] = 'manual';
		$data["jumlah"] = $this->string_to_number_new($this->input->post('jumlah'));
		$data["keterangan"] = $this->input->post('keterangan');
		$insert = $this->arus_kas->insert($data);
		if ($insert) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}

		echo json_encode($result);
	}

	function update()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$arus_kas_id = $this->input->post('arus_kas_id');
		$lokasi_id = $this->input->post('lokasi_id');
		if (isset($_SESSION["login"]["lokasi_id"])) {
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
			$lokasi_id = $_SESSION["login"]["lokasi_id"];
		}
		$tipe_pembayaran = $this->tipe_pembayaran->row_by_id($this->input->post('tipe_pembayaran_id'));
		$tanggal = $this->input->post('tanggal');
		$time = strtotime($tanggal);
		$data['tanggal'] = date('Y-m-d', $time);
		$data["tipe_pembayaran_nama"] = $tipe_pembayaran->tipe_pembayaran_nama . " " . $tipe_pembayaran->no_akun;
		$data["user_id"] = isset($_SESSION['login']['user_id']) ? $_SESSION['login']['user_id'] : null;
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		$data["lokasi_id"] = $lokasi_id;
		$data["jenis"] = $this->input->post('jenis');
		$data["jumlah"] = $this->string_to_number_new($this->input->post('jumlah'));
		$data["keterangan"] = $this->input->post('keterangan');

		$update = $this->arus_kas->update_by_id('arus_kas_id',$arus_kas_id,$data);
		if ($update) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}

		echo json_encode($result);
	}

	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->arus_kas->delete_by_id("arus_kas_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

	function pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$lokasi_id = null;
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		} else {
			$data['lokasi'] = " Semua Cabang ";
		}
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$lokasi_id = $_SESSION["login"]['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->arus_kas->arus_count_filter($query);
		$list = $this->arus_kas->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);
			}
			$key->no = $i;
			$key->jenis_lbl = ucfirst($key->jenis);
			$key->jumlah = number_format($key->jumlah);
			$i++;
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/arus_kas_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Arus Kas' . $date . ".pdf", "D");
	}

	function excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$lokasi_id = null;
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		} else {
			$data['lokasi'] = " Semua Cabang ";
		}
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$lokasi_id = $_SESSION["login"]['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->arus_kas->arus_count_filter($query);
		$list = $this->arus_kas->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);
			}
			$key->no = $i;
			$key->jenis_lbl = ucfirst($key->jenis);
			$i++;
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('CV. Waisnawa Transport Group')
			->setLastModifiedBy($_SESSION["login"]['user_name'])
			->setTitle('Laporan Arus Kas')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		if (!isset($_SESSION["login"]['lokasi_id'])){
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A8', 'No')
				->setCellValue('B8', 'Tanggal')
				->setCellValue('C8', 'Jenis')
				->setCellValue('D8', 'Jumlah')
				->setCellValue('E8', 'Tipe Pembayaran')
				->setCellValue('F8', 'Keterangan')
				->setCellValue('G8', 'Staff')
				->setCellValue('H8', 'Cabang');
			$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
			$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$spreadsheet->getActiveSheet()->getStyle("A8:H8")->applyFromArray($style);
		}else{
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A8', 'No')
				->setCellValue('B8', 'Tanggal')
				->setCellValue('C8', 'Jenis')
				->setCellValue('D8', 'Jumlah')
				->setCellValue('E8', 'Tipe Pembayaran')
				->setCellValue('F8', 'Keterangan')
				->setCellValue('G8', 'Staff');
			$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
			$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$spreadsheet->getActiveSheet()->getStyle("A8:G8")->applyFromArray($style);
		}

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i = 9;
		if (!isset($_SESSION["login"]['lokasi_id'])) {
			foreach ($list as $key) {

				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A' . $i, $key->no)
					->setCellValue('B' . $i, $key->tanggal)
					->setCellValue('C' . $i, $key->jenis)
					->setCellValue('D' . $i, $key->jumlah)
					->setCellValue('E' . $i, $key->tipe_pembayaran_nama)
					->setCellValue('F' . $i, $key->keterangan)
					->setCellValue('G' . $i, $key->staff_nama)
					->setCellValue('H' . $i, $key->lokasi_lbl);
				$i++;
			}
			$spreadsheet->getActiveSheet()->getStyle("A8:H" . $i)->applyFromArray($border);
			$spreadsheet->getActiveSheet()->getStyle('A8:H8')->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('BEBEBE');
			$spreadsheet->getActiveSheet()->getStyle('H9:H' . $i)->getAlignment()->setWrapText(true);
		}else{
			foreach ($list as $key) {

				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A' . $i, $key->no)
					->setCellValue('B' . $i, $key->tanggal)
					->setCellValue('C' . $i, $key->jenis)
					->setCellValue('D' . $i, $key->jumlah)
					->setCellValue('E' . $i, $key->tipe_pembayaran_nama)
					->setCellValue('F' . $i, $key->keterangan)
					->setCellValue('G' . $i, $key->staff_nama);
				$i++;
			}
			$spreadsheet->getActiveSheet()->getStyle("A8:G" . $i)->applyFromArray($border);
			$spreadsheet->getActiveSheet()->getStyle('A8:G8')->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('BEBEBE');
			$spreadsheet->getActiveSheet()->getStyle('G9:G' . $i)->getAlignment()->setWrapText(true);

		}
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', $_SESSION["redpos_company"]['company_name']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', $_SESSION["redpos_company"]['company_address']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', $_SESSION["redpos_company"]['company_phone']);
		if (!isset($_SESSION["login"]['lokasi_id'])) {
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Arus Kas');
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tanggal : ' . $data['tanggal_start'] . 's/d' . $data['tanggal_end']);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'Cabang : ' . $data['lokasi']);
			$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle("H1:H6")->applyFromArray($right);
		}else{
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G1', 'Laporan Arus Kas');
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G2', 'Tanggal : ' . $data['tanggal_start'] . 's/d' . $data['tanggal_end']);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G3', 'Cabang : ' . $data['lokasi']);
			$spreadsheet->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle("G1:G6")->applyFromArray($right);
		}
		$spreadsheet->getActiveSheet()->setTitle('Laporan Bahan per Cabang');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Stok Bahan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}

/* End of file BahanByLocation.php */
/* Location: ./application/controllers/BahanByLocation.php */
