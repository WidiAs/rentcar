<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ArusKendaraanController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('arus_kendaraan', '', true);
		$this->load->model('jenis_mobil', '', true);
		$this->load->model('mobil', '', true);
		$this->load->model('jadwal_mobil', '', true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/pencatatan_kendaraan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Arus Kendaraan < Inventori < Waisnawa Transport";
		$data['parrent'] = "inventori";
		$data['page'] = 'arus-kendaraan';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal"));
		array_push($column, array("data" => "mobil_jenis"));
		array_push($column, array("data" => "mobil_plat"));
		array_push($column, array("data" => "status"));
		array_push($column, array("data" => "driver"));
		array_push($column, array("data" => "kontak_driver"));
		array_push($column, array("data" => "keterangan"));
		array_push($column, array("data" => "waktu"));
		array_push($column, array("data" => "staff_nama"));
		array_push($column, array("data" => "foto_btn"));
		if ($_SESSION['login']['user_role_id'] == 1) {
			array_push($column, array("data" => "hapus_btn"));
		}
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['inventori']['arus-kendaraan'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['jenis_mobil'] = $this->arus_kendaraan->mobil_jenis_arus();
		$data['action'] = json_encode($action);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/arus_kendaraan_list');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		if (isset($_GET["columns"][8]["search"]["value"]) && $_GET["columns"][8]["search"]["value"] != "") {
			$_GET['mobil_jenis'] = $_GET["columns"][8]["search"]["value"];
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->arus_kendaraan->arus_count_all();
		$result['iTotalDisplayRecords'] = $this->arus_kendaraan->arus_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->arus_kendaraan->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->tanggal_lbl = null;
			$key->waktu = null;
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);

				$key->waktu = date('H:i:s', $time) . ' Wita';
			}
			$key->no = $i;
			$key->hapus_url = base_url() . 'arus-kendaraan/delete';
			$key->row_id = $key->arus_kendaraan_id;
			$key->foto_url = base_url() . $key->foto;

			$key->foto_btn = '<button class="btn btn-success btn-icon btn-icon-sm btn-foto" title="Foto" data-url="' . $key->foto_url . '">
								<i class="flaticon-visible"></i>
								</button>';
			$key->hapus_btn = '
								<button class="btn btn-danger btn-icon btn-icon-sm hapus-btn" title="Hapus" data-url="' . $key->hapus_url . '" data-ksu="' . $key->row_id . '">
								<i class="flaticon2-trash"></i>
								</button>';
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function delete()
	{
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if ($id != "") {
			$arus = $this->arus_kendaraan->row_by_id($id);
			$path = $arus->foto;
			if (file_exists($path)) {
				unlink($path);
			}
			$delete = $this->arus_kendaraan->delete_by_id("arus_kendaraan_id", $id);
			if ($delete) {
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

	function pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$lokasi_id = null;
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		} else {
			$data['lokasi'] = " Semua Lokasi ";
		}
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$lokasi_id = $_SESSION["login"]['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->arus_kas->arus_count_filter($query);
		$list = $this->arus_kas->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);
			}
			$key->no = $i;
			$key->jenis_lbl = ucfirst($key->jenis);
			$key->jumlah = number_format($key->jumlah);
			$i++;
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/arus_kas_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Arus Kas' . $date . ".pdf", "D");
	}

	function excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$lokasi_id = null;
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		} else {
			$data['lokasi'] = " Semua Lokasi ";
		}
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$lokasi_id = $_SESSION["login"]['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->arus_kas->arus_count_filter($query);
		$list = $this->arus_kas->arus_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal != null) {
				$key->tanggal = $key->tanggal;
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y', $time);
			}
			$key->no = $i;
			$key->jenis_lbl = ucfirst($key->jenis);
			$i++;
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('CV. Waisnawa Transport Group')
			->setLastModifiedBy($_SESSION["login"]['user_name'])
			->setTitle('Laporan Arus Kas')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		if (!isset($_SESSION["login"]['lokasi_id'])) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A8', 'No')
				->setCellValue('B8', 'Tanggal')
				->setCellValue('C8', 'Jenis')
				->setCellValue('D8', 'Jumlah')
				->setCellValue('E8', 'Tipe Pembayaran')
				->setCellValue('F8', 'Keterangan')
				->setCellValue('G8', 'Staff')
				->setCellValue('H8', 'Lokasi');
			$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
			$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$spreadsheet->getActiveSheet()->getStyle("A8:H8")->applyFromArray($style);
		} else {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A8', 'No')
				->setCellValue('B8', 'Tanggal')
				->setCellValue('C8', 'Jenis')
				->setCellValue('D8', 'Jumlah')
				->setCellValue('E8', 'Tipe Pembayaran')
				->setCellValue('F8', 'Keterangan')
				->setCellValue('G8', 'Staff');
			$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
			$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
			$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$spreadsheet->getActiveSheet()->getStyle("A8:G8")->applyFromArray($style);
		}

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i = 9;
		if (!isset($_SESSION["login"]['lokasi_id'])) {
			foreach ($list as $key) {

				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A' . $i, $key->no)
					->setCellValue('B' . $i, $key->tanggal)
					->setCellValue('C' . $i, $key->jenis)
					->setCellValue('D' . $i, $key->jumlah)
					->setCellValue('E' . $i, $key->tipe_pembayaran_nama)
					->setCellValue('F' . $i, $key->keterangan)
					->setCellValue('G' . $i, $key->staff_nama)
					->setCellValue('H' . $i, $key->lokasi_lbl);
				$i++;
			}
			$spreadsheet->getActiveSheet()->getStyle("A8:H" . $i)->applyFromArray($border);
			$spreadsheet->getActiveSheet()->getStyle('A8:H8')->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('BEBEBE');
			$spreadsheet->getActiveSheet()->getStyle('H9:H' . $i)->getAlignment()->setWrapText(true);
		} else {
			foreach ($list as $key) {

				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A' . $i, $key->no)
					->setCellValue('B' . $i, $key->tanggal)
					->setCellValue('C' . $i, $key->jenis)
					->setCellValue('D' . $i, $key->jumlah)
					->setCellValue('E' . $i, $key->tipe_pembayaran_nama)
					->setCellValue('F' . $i, $key->keterangan)
					->setCellValue('G' . $i, $key->staff_nama);
				$i++;
			}
			$spreadsheet->getActiveSheet()->getStyle("A8:G" . $i)->applyFromArray($border);
			$spreadsheet->getActiveSheet()->getStyle('A8:G8')->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('BEBEBE');
			$spreadsheet->getActiveSheet()->getStyle('G9:G' . $i)->getAlignment()->setWrapText(true);

		}
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', $_SESSION["redpos_company"]['company_name']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', $_SESSION["redpos_company"]['company_address']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', $_SESSION["redpos_company"]['company_phone']);
		if (!isset($_SESSION["login"]['lokasi_id'])) {
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Arus Kas');
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tanggal : ' . $data['tanggal_start'] . 's/d' . $data['tanggal_end']);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'Lokasi : ' . $data['lokasi']);
			$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle("H1:H6")->applyFromArray($right);
		} else {
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G1', 'Laporan Arus Kas');
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G2', 'Tanggal : ' . $data['tanggal_start'] . 's/d' . $data['tanggal_end']);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('G3', 'Lokasi : ' . $data['lokasi']);
			$spreadsheet->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle("G1:G6")->applyFromArray($right);
		}
		$spreadsheet->getActiveSheet()->setTitle('Laporan Bahan per Lokasi');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Stok Bahan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function pencatatan_index()
	{
//		echo date('Y-m-d H:i:s');
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/pencatatan_kendaraan.js");
		array_push($this->js, "script/admin/upload_image.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Arus Kendaraan < Inventori < Waisnawa Transport";
		$data['parrent'] = "inventori";
		$data['page'] = 'pencatatan-kendaraan';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal_lbl"));
		array_push($column, array("data" => "mobil_jenis_nama"));
		array_push($column, array("data" => "mobil_plat"));
//		array_push($column, array("data" => "status"));
		array_push($column, array("data" => "jenis"));
		array_push($column, array("data" => "pelanggan_nama"));
		array_push($column, array("data" => "driver_nama"));
		array_push($column, array("data" => "jam_ambil"));
		array_push($column, array("data" => "action_btn"));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['inventori']['pencatatan-kendaraan'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$action['view'] = false;
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/pencatatan_kendaraan');
		$this->load->view('admin/static/footer');
	}

	function pencatatan_besok_index()
	{
//		echo date('Y-m-d H:i:s');
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/pencatatan_kendaraan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Arus Kendaraan < Inventori < Waisnawa Transport";
		$data['parrent'] = "inventori";
		$data['page'] = 'pencatatan-kendaraan';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal_lbl"));
		array_push($column, array("data" => "mobil_jenis_nama"));
		array_push($column, array("data" => "mobil_plat"));
		array_push($column, array("data" => "jenis"));
		array_push($column, array("data" => "pelanggan_nama"));
		array_push($column, array("data" => "driver_nama"));
		array_push($column, array("data" => "jam_ambil"));
		array_push($column, array("data" => "keterangan_sewa"));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['inventori']['pencatatan-kendaraan'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$action['view'] = false;
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/pencatatan_kendaraan_besok');
		$this->load->view('admin/static/footer');
	}


	function pencatatan_besok_admin_index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Arus Kendaraan < Inventori < Waisnawa Transport";
		$data['parrent'] = "inventori";
		$data['page'] = 'pencatatan-kendaraan';
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "tanggal_lbl"));
		array_push($column, array("data" => "mobil_jenis_nama"));
		array_push($column, array("data" => "mobil_plat"));
		array_push($column, array("data" => "jenis"));
		array_push($column, array("data" => "pelanggan_nama"));
		array_push($column, array("data" => "driver_nama"));
		array_push($column, array("data" => "jam_ambil"));
		array_push($column, array("data" => "keterangan_sewa"));

		$action['view'] = false;
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['inventori']['jadwal-mobil-besok'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/jadwal_kendaraan_besok_admin');
		$this->load->view('admin/static/footer');
	}

	function add()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$mobil_id = $this->input->post('mobil_id');
		$mobil_plat = $this->input->post('mobil_plat');
		$mobil_jenis = $this->input->post('mobil_jenis_nama');
		$penyewaan_id = $this->input->post('penyewaan_id');
		$penyewaan_rekanan_id = $this->input->post('penyewaan_rekanan_id');
		$user_id = $_SESSION['login']['user_id'];
		$status = $this->input->post('status');
		$driver = $this->input->post('driver');
		$keterangan = $this->input->post('keterangan');
		$kontak_driver = $this->input->post('kontak_driver');
		$foto = $this->input->post('input_foto_mobil');
		$foto_bensin = $this->input->post('input_foto_bensin');
		$foto_ktp = $this->input->post('input_foto_ktp');

		$status_lbl = $status;
		if ($status == 'Stand By') {
			$status_lbl = 'Masuk';
		}

		$data = array();
		$data["mobil_id"] = $mobil_id;
		$data["mobil_plat"] = $mobil_plat;
		$data["mobil_jenis"] = $mobil_jenis;
		$data["penyewaan_id"] = $penyewaan_id;
		$data["penyewaan_rekanan_id"] = $penyewaan_rekanan_id;
		$data["user_id"] = $user_id;
		$data["status"] = $status_lbl;
		$data["driver"] = $driver;
		$data["keterangan"] = $keterangan;
		$data["kontak_driver"] = $kontak_driver;
		$data["tanggal"] = date('Y-m-d H:i:s');
		$data["foto"] = $foto;
		$data["foto_bensin"] = $foto_bensin;
		$data["foto_ktp"] = $foto_ktp ? $foto_ktp : null;

		$update = $this->mobil->update_by_id('mobil_id', $mobil_id, array('status' => $status));

		if ($update) {
			$insert = $this->arus_kendaraan->insert($data);
			if ($insert) {
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}

		echo json_encode($result);
	}

	function jadwal_update()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$jadwal_id = $this->input->post('jadwal_id');
		$jam_ambil = $this->input->post('jam_ambil');

		$update = $this->jadwal_mobil->update_by_id('jadwal_id', $jadwal_id, array('jam_ambil' => $jam_ambil));
		if ($update) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		}
		echo json_encode($result);
	}
}

/* End of file BahanByLocation.php */
/* Location: ./application/controllers/BahanByLocation.php */
