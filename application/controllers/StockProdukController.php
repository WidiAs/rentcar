<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StockProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('satuan','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/stock_produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Stok Produk< Inventori < Waisnawa";
		$data['parrent'] = "inventori";
		$data['page'] = 'produk';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
		$id = $this->encryption->decrypt($url);
		$data["lokasi"] = $this->lokasi->all_list();
		$produk = $this->produk->row_by_id($id);
		$data['id'] = $id;

		if ($produk != null) {
			$data['nama_satuan'] = '';
			if ($produk->produk_satuan_id){
				$data['nama_satuan'] = $this->satuan->row_by_id($produk->produk_satuan_id)->satuan_nama;
			}
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			array_push($column, array("data"=>"stock_produk_seri"));
			array_push($column, array("data"=>"stock_produk_qty"));
			$akses_menu = json_decode($this->menu_akses,true);
			$target = array(0,3);
			if($akses_menu["inventori"]["produk"]["data"]["hpp"]==true){
				array_push($column, array("data"=>"hpp"));
				array_push($target, 4);
			}
			$data['sumColumn'] = json_encode(array(3));
					$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
			$data["action"] = json_encode(array("edit"=>true,"delete"=>true,"barcode"=>false));
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/stock_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
		
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_produk->stock_produk_count($this->uri->segment(3));
		$result['iTotalDisplayRecords'] = $this->stock_produk->stock_produk_count_filter($query,$this->uri->segment(3));
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_produk->stock_produk_list($start,$length,$query,$this->uri->segment(3));
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'stock-produk/delete/'.$key->produk_id;
			$key->row_id = $key->stock_produk_id;
			$key->stock_produk_qty = number_format($key->stock_produk_qty);
			$key->hpp = number_format($key->hpp);
			$i++;
		}
		$result['aaData'] = $data;		
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Produk dengan seri yang sama tidak dapat ditambahkan dilokasi yang sama";
		$data["stock_produk_qty"] = $this->string_to_number($this->input->post('stock_produk_qty'));
		$data["produk_id"] = $this->input->post('produk_id');
		$data["stock_produk_lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
		if(isset($_SESSION['login']['lokasi_id'])){
			$data["stock_produk_lokasi_id"] = $_SESSION['login']['lokasi_id'];
		}		
		$data["year"] = date("y");
		$data["month"] = date("m");
		$lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
		$jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
		$data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
		$data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
		$data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
		$data["stock_produk_keterangan"] = $this->input->post('stock_produk_keterangan');
		$data["hpp"] = $this->string_to_number($this->input->post('hpp'));
			$insert = $this->stock_produk->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_produk";
				$data["stock_produk_id"] = $this->stock_produk->last_id();
				$data["produk_id"] = $this->input->post('produk_id');
				$data["stock_out"] = 0;
				$data["stock_in"] = $this->string_to_number($this->input->post('stock'));
				$data["last_stock"] = $this->stock_produk->last_stock($this->input->post('produk_id'))->result;
				$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
				$data["keterangan"] = "Insert stok produk";
				$data["method"] = "insert";
				$this->stock_produk->arus_stock_produk($data);
			} else {
				$result['message'] = "Gagal menyimpan data";
			}	
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Tidak dapat menyimpan produk dengan seri produk yang sama dengan data yang ada ";
		$data["stock_produk_qty"] = $this->string_to_number($this->input->post('stock_produk_qty'));
		$data["produk_id"] = $this->input->post('produk_id');
		$data["stock_produk_lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
		if(isset($_SESSION['login']['lokasi_id'])){
			$data["stock_produk_lokasi_id"] = $_SESSION['login']['lokasi_id'];
		}			
		$data["stock_produk_keterangan"] = $this->input->post('stock_produk_keterangan');
		$data["hpp"] = $this->string_to_number($this->input->post('hpp'));
		$stock_produk_id = $this->input->post('stock_produk_id');

		$old_data = $this->stock_produk->row_by_id($stock_produk_id);
		if($old_data->stock_produk_lokasi_id != $data["stock_produk_lokasi_id"]){
			$data["year"] = date("y");
			$data["month"] = date("m");
			$lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
			$jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
			$data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
			$data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
			$data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
		}
			$insert = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
			if($insert){
				$data = array();
				$diff = $old_data->stock_produk_qty - $this->string_to_number($this->input->post('stock_produk_qty'));
				if ($diff != 0) {
					$data["tanggal"] = date("Y-m-d");
					$data["table_name"] = "stock_produk";
					$data["stock_produk_id"] = $stock_produk_id;
					$data["produk_id"] = $this->input->post('produk_id');
					if ($diff > 0) {
						$data["stock_out"] = abs($diff);
						$data["stock_in"] = 0;
					} else {
						$data["stock_in"] = abs($old_data->stock - $this->string_to_number($this->input->post('stock_produk_qty')));
						$data["stock_out"] = 0;
					}
					$data["last_stock"] = $this->stock_produk->last_stock($this->input->post('produk_id'))->result;
					$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
					$data["keterangan"] = "Edit stok produk";
					$data["method"] = "update";
					$this->stock_produk->arus_stock_produk($data);
				}
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function delete(){
		$produk_id = $this->uri->segment(3);
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$old_data = $this->stock_produk->row_by_id($id);
		if($id != ""){
			$data["delete_flag"] = 1;
			$delete = $this->stock_produk->update_by_id("stock_produk_id",$id,$data);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_produk";
				$data["stock_produk_id"] = $id;
				$data["produk_id"] = $produk_id;
				$data["stock_out"] = $old_data->stock_produk_qty;
				$data["stock_in"] = 0;
				$data["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
				$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
				$data["keterangan"] = "Delete stok produk";
				$data["method"] = "delete";
				$this->stock_produk->arus_stock_produk($data);
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function barcode(){
		$code = $this->uri->segment(3);
		$this->load->library('zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');
		//generate barcode
		Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
	}
	function print_barcode(){
		$this->load->view('admin/print_barcode');
	}

	function total_stock(){
		$produk = $this->produk->all_list();
		$lokasi = $this->lokasi->all_list();

		$array = array();
		foreach($produk as $key => $p){
			foreach ($lokasi as $k => $l){
				$this->db->where('produk_id', $p->produk_id);
				$this->db->where('stock_produk_lokasi_id', $l->lokasi_id);
				$stock = $this->db->get('stock_produk')->result();

				if ($stock && !empty($stock)){
					$total_stock = 0;
					$stock_id = 0;
					foreach ($stock as $s){
						$total_stock += $s->stock_produk_qty;
						$stock_id = $s->stock_produk_id;

						$this->stock_produk->update_by_id('stock_produk_id', $s->stock_produk_id, array('stock_produk_qty' => 0));
					}
					$this->stock_produk->update_by_id('stock_produk_id', $stock_id, array('stock_produk_qty' => $total_stock));

					$array[$stock_id] = $total_stock;
				}
			}
		}

		echo json_encode($array);
	}
}

/* End of file StockProdukController.php */
/* Location: ./application/controllers/StockProdukController.php */
