<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisMobilController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('jenis_mobil','',true);
		$this->load->model('mobil','',true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Jenis Mobil < Master Data < Waisnawa";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"jenis_mobil_merk"));
		array_push($column, array("data"=>"jenis_mobil_nama"));
		array_push($column, array("data"=>"created_at"));
		array_push($column, array("data"=>"updated_at"));

		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['jenis-mobil'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/master_data/jenis_mobil');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = (isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null);
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->jenis_mobil->jenis_mobil_count_all();
		$result['iTotalDisplayRecords'] = $this->jenis_mobil->jenis_mobil_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->jenis_mobil->jenis_mobil_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'jenis-mobil/delete/';
			$key->row_id = $key->jenis_mobil_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function options(){
		$data = array();
		$jenis_mobil = $this->jenis_mobil->all_list();
		foreach ($jenis_mobil as $key) {
			array_push($data, strtoupper($key->jenis_mobil_nama));
		}
		echo json_encode($data);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Jenis ini telah terdaftar";
		$this->form_validation->set_rules('jenis_mobil_nama', '', 'required|is_unique[jenis_mobil.jenis_mobil_nama]');
		if ($this->form_validation->run() == TRUE) {
			$jenis_mobil_merk = $this->input->post('jenis_mobil_merk');
			$jenis_mobil_nama = $this->input->post('jenis_mobil_nama');
			$data = array("jenis_mobil_merk"=>$jenis_mobil_merk,"jenis_mobil_nama"=>$jenis_mobil_nama);
			$insert = $this->jenis_mobil->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Jenis sudah terdaftar";
		$data = array();
		$data['jenis_mobil_merk'] = $this->input->post('jenis_mobil_merk');
		$jenis_mobil_id = $this->input->post('jenis_mobil_id');

		if ($this->jenis_mobil->is_ready_nama($jenis_mobil_id,$this->input->post('jenis_mobil_nama'))){
			$data["jenis_mobil_nama"] = $this->input->post('jenis_mobil_nama');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->jenis_mobil->update_by_id('jenis_mobil_id',$jenis_mobil_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$count = $this->mobil->count_by_jenis($id);
		if($id != "" && $count == 0){
			$delete = $this->jenis_mobil->delete_by_id("jenis_mobil_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}elseif ($count > 0){
			$result['message'] = "Masih ada data dengan jenis mobil ini di data mobil";
		}
		echo json_encode($result);
	}
}

/* End of file JenisProdukController.php */
/* Location: ./application/controllers/JenisProdukController.php */
