<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user', '', true);
		$this->load->model('staff', '', true);
		$this->load->model('stock_bahan', '', true);
		$this->load->model('stock_produk', '', true);
		$this->load->helper('string');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}

	public function index()
	{
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/jquery-form/dist/jquery.form.min.js");
		array_push($this->js, "vendors/general/sweetalert2/dist/sweetalert2.min.js");
		array_push($this->js, "vendors/custom/components/vendors/sweetalert2/init.js");
		array_push($this->js, "script/admin/profile.js");
		array_push($this->js, "script/admin/upload_image.js");
		array_push($this->css, "vendors/general/sweetalert2/dist/sweetalert2.css");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$user = $this->user->user_by_id($_SESSION['login']['user_id']);
		$data['user'] = $user;
		$data["meta_title"] = "User Profile < Waisnawa";
		$data['parrent'] = "";
		$data['page'] = $this->uri->segment(1);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/profile');
		$this->load->view('admin/static/footer');
	}

	function personal_info()
	{
		$this->form_validation->set_rules('user_name', '', 'required');
		$this->form_validation->set_rules('email', '', 'required');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if ($this->form_validation->run() == TRUE) {
			$user_name = $this->input->post('user_name');
			$email = $this->input->post('email');
			$staff_phone_number = $this->input->post('phone_number');
			$staff_alamat = $this->input->post('staff_alamat');
			if ($this->staff->is_ready_email($_SESSION['login']['staff_id'], $email)) {
				$data = array(
					"staff_nama" => $user_name,
					"staff_email" => $email,
					"staff_phone_number" => $staff_phone_number,
					"staff_alamat" => $staff_alamat
				);
				$update = $this->staff->update_by_id('staff_id', $_SESSION['login']['staff_id'], $data);
				if ($update) {
					$result['success'] = true;
					$result['message'] = "Success updated personal info";
					$_SESSION["login"]["user_name"] = $user_name;
					$_SESSION["login"]["email"] = $email;
				} else {
					$result['message'] = "Failed to update";
				}
			} else {
				$result['message'] = "The email is already registered";
			}
		}
		echo json_encode($result);
	}

	function change_password()
	{
		$this->form_validation->set_rules('old_pass', '', 'required');
		$this->form_validation->set_rules('new_pass', '', 'required');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if ($this->form_validation->run() == TRUE) {
			$old_pass = $this->input->post('old_pass');
			$new_pass = $this->input->post('new_pass');
			$email = $_SESSION['login']['email'];
			$user = $this->user->user_by_id($_SESSION['login']['user_id']);
			if (password_verify($old_pass, $user->password)) {
				$password = password_hash($new_pass, PASSWORD_BCRYPT);
				$change = $this->user->change_password($email, $password);
				if ($change) {
					$result["success"] = true;
					$result["message"] = "Your password successfully changed";
				} else {
					$result["message"] = "Failed to change password";
				}
			} else {
				$result["message"] = "Wrong password";
			}
		}
		echo json_encode($result);
	}

	function change_avatar_old()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != null) {
			$url = $_SESSION['login']['avatar'];
			$url = $this->uploadImage("avatar", $url);
			$user_id = $_SESSION['login']['user_id'];
			if ($url != "failed") {
				$change = $this->user->change_avatar($user_id, $url);
				if ($change) {
					$temp = $_SESSION['login']['avatar'];
					if (file_exists(FCPATH . $temp)) {
						unlink(FCPATH . $temp);
					}
					$result['success'] = true;
					$result['message'] = "Success updating your avatar";
					$result['url'] = $url;
					$_SESSION['login']['avatar'] = $url;
				} else {
					$result['message'] = "Updating failed";
				}
			} else {
				$result['message'] = "Uploading file failed";
			}
		}
		echo json_encode($result);
	}

	function change_avatar()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$url = $this->input->post('input_avatar');
		$user_id = $_SESSION['login']['user_id'];

		if ($url) {
			$change = $this->user->change_avatar($user_id, $url);
			if ($change) {
				$result['success'] = true;
				$result['message'] = "Success updating your avatar";
				$result['url'] = $url;
				$_SESSION['login']['avatar'] = $url;
			} else {
				$result['message'] = "Updating failed";
			}
		}

		echo json_encode($result);
	}

	function uploadImage($file, $url)
	{
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp() . random_string('alnum', 5);
		$config['upload_path'] = 'assets/media/users/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 2000;

		$this->upload->initialize($config);

		if (!$this->upload->do_upload($file)) {
			$error = array('error' => $this->upload->display_errors());
			$url = "failed";
			return $url;
		} else {
			$data = array('upload_data' => $this->upload->data());
			$url = $config['upload_path'] . $data['upload_data']['orig_name'];
			$this->resizeImage($url, $data['upload_data']['orig_name'], 200);
		}
		return $url;
	}

	function resizeImage($url, $name, $size)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = FCPATH . $url;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = $size;
		$config['height'] = $size;
		$temp = explode(".", $name);
		$folder = $temp[0];
		$config['new_image'] = FCPATH . $url;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
	}

	function reset_stok()
	{
		$result = array();

		$this->stock_produk->reset_stok();
		$this->stock_bahan->reset_stok();

		$result['success'] = true;
		$result['message'] = "Sukses Mereset Stok";

		echo json_encode($result);
	}

	function reset_sistem()
	{
		$result = array();

		$this->db->empty_table('arus_stock_bahan');
		$this->db->empty_table('arus_stock_produk');
		$this->db->empty_table('history_penyesuaian_bahan');
		$this->db->empty_table('history_penyesuaian_produk');
		$this->db->empty_table('history_transfer_bahan');
		$this->db->empty_table('history_transfer_produk');
		$this->db->empty_table('hutang');
		$this->db->empty_table('log_kasir');
		$this->db->empty_table('pembayaran_hutang');
		$this->db->empty_table('pembayaran_piutang');
		$this->db->empty_table('penjualan');
		$this->db->empty_table('penjualan_produk');
		$this->db->empty_table('piutang');
		$this->db->empty_table('po_bahan');
		$this->db->empty_table('po_bahan_detail');
		$this->db->empty_table('po_produk');
		$this->db->empty_table('po_produk_detail');
		$this->db->empty_table('produksi');
		$this->db->empty_table('produksi_item');
		$this->db->empty_table('produksi_item_bahan');
		$this->db->empty_table('surat_jalan');
		$this->db->empty_table('surat_jalan_produk');

		$result['success'] = true;
		$result['message'] = "Sukses Mereset Stok";

		echo json_encode($result);
	}
}
