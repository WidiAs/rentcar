<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoleAksesController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_role','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/akses_role.js");
		$menu = $this->user_role->all_menu();
		foreach ($menu as $key) {
			if($key->action != null){
				$temp = explode('|', $key->action);
				$key->action = $temp;
			}
			if($key->sub_action != null){
				$temp = explode('|', $key->sub_action);
				$key->sub_action = $temp;
			}
			if($key->data != null){
				$temp = explode('|', $key->data);
				$key->data = $temp;
			}				
		}
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Akses User Role < Master Data < Waisnawa";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		$data['menu'] = $menu;
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$role = $this->user_role->row_by_id($id);
		$data['id'] = $id;
		if ($role != null) {
			$data['akses_role'] = $this->user_role->get_akses_role($id);
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/akses_role');
			$this->load->view('admin/static/footer');			
		} else {
			redirect('404_override','refresh');
		}

	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal mengubah data";		
		$menu = $this->user_role->all_menu();
		$akses_role = array();
foreach ($menu as $key) {
									$akses_role["".$key->menu_kode]["akses_menu"] = false;
									if($key->action != null){
											$action = explode("|", $key->action);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key2] = false;
											}				
									}
									if($key->sub_menu_kode != null){
										$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["akses_menu"] = false;
										
										if($key->data != null){
											$action = explode("|", $key->data);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["data"]["".$key2] = false;
											}
										}
										if($key->sub_action != null){
											$action = explode("|", $key->sub_action);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["".$key2] = false;
											}
										}
									}
								}
		$role = $this->input->post('role');
		foreach ($role as $key => $value) {
			foreach ($value as $key2 => $value2) {
				if(is_array($value2)){
					foreach ($value2 as $key3 => $value3) {
						if(is_array($value3)){
							foreach ($value3 as $key4 => $value4) {
								$akses_role[$key][$key2]["data"][$key4] = true;		
							}
						}else{
							$akses_role[$key][$key2][$key3] = true;		
						}
					}
				}else{
					$akses_role[$key][$key2] = true;
				}

			}		
		}
		$id = $this->uri->segment(4);
		$data['user_role_akses'] = json_encode($akses_role);
		$update = $this->user_role->update_by_id('user_role_id',$id,$data);
		if($update){
			$result['success'] = true;
			$result['message'] = "Berhasil mengubah data";				
		}
		echo json_encode($result);
	}

}
