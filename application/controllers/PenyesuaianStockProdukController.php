<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenyesuaianStockProdukController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('history_penyesuaian_produk','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Penyesuaian Stock Produk < Inventori < System Balioz Linen";
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-produk';
		$target = array(0,4);
		$sumColumn = array(4);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"stock"));
		if(isset($_SESSION['login']['lokasi_id'])){
			array_push($column, array("data"=>"jumlah_lokasi"));
			array_push($target, 5);
			array_push($sumColumn, 5);
		}		
		$data['sumColumn'] = json_encode($sumColumn);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$data["list_url"] = base_url()."penyesuaian-produk/list";
		$data["action"] = json_encode(array("stock"=>true,"view"=>false,"edit"=>false,"delete"=>false));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/penyesuaian_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->delete_url = base_url().'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url().'penyesuaian-produk/stock/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$i++;
			if(isset($_SESSION['login']['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}			
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function stock_index(){
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penyesuaian Stok < Inventori < System Balioz Linen";
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-produk';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$produk = $this->produk->produk_by_id($id);
		$data['id'] = $id;
		if ($produk != null) {
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			array_push($column, array("data"=>"stock_produk_seri"));
			array_push($column, array("data"=>"stock_produk_qty"));
			$data['sumColumn'] = json_encode(array(3));
			$data['column'] = json_encode($column);
			$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,3)));
			$data["action"] = json_encode(array("adjust"=>true));
			$data["list_url"] = base_url()."stock-produk/list/".$id;
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/penyesuaian_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
	}
	function adjust(){
		// var_dump($this->input->post());
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data["stock_produk_qty"] = $this->string_to_number($this->input->post('stock_produk_qty'));
		$stock_produk_id = $this->input->post('stock_produk_id');
		$old_data = $this->stock_produk->row_by_id($stock_produk_id);
		$update = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
		if($update){
				$data = array();
				$diff = $old_data->stock_produk_qty - $this->string_to_number($this->input->post('stock_produk_qty'));
				if ($diff != 0) {
					$data["tanggal"] = date("Y-m-d");
					$data["table_name"] = "stock_produk";
					$data["stock_produk_id"] = $stock_produk_id;
					$data["produk_id"] = $this->input->post('produk_id');
					if ($diff > 0) {
						$data["stock_out"] = abs($diff);
						$data["stock_in"] = 0;
					} else {
						$data["stock_in"] = abs($old_data->stock_produk_qty - $this->string_to_number($this->input->post('stock_produk_qty')));
						$data["stock_out"] = 0;
					}
					$data["last_stock"] = $this->stock_produk->last_stock($this->input->post('produk_id'))->result;
					$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
					$data["keterangan"] = "Penyesuaian stok produk";
					$data["method"] = "update";
					$this->stock_produk->arus_stock_produk($data);
				}
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["produk_id"] = $this->input->post('produk_id');
				$data["jenis_produk_id"] = $this->input->post('jenis_produk_id');
				$data["lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
				$data["stock_produk_id"] = $stock_produk_id;
				$data["produk_kode"] = $this->input->post('produk_kode');
				$data["produk_nama"] = $this->input->post('produk_nama');
				$data["jenis_produk_nama"] = $this->input->post('jenis_produk_nama');
				$data["lokasi_nama"] = $this->input->post('lokasi_nama');
				$data["qty_awal"] = $old_data->stock_produk_qty;
				$data["qty_akhir"] = $this->string_to_number($this->input->post('stock_produk_qty'));
				$data["keterangan"] = $this->input->post('keterangan');
				$this->history_penyesuaian_produk->insert($data);
		}
		echo json_encode($result);
	}
}

/* End of file PenyesuaianStockProdukController.php */
/* Location: ./application/controllers/PenyesuaianStockProdukController.php */