<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
class ProdukByLocation extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('satuan','',true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produk per lokasi < Inventori < System Balioz Linen";
		$data['parrent'] = "laporan";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"spec"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"satuan_nama"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"stock"));
		$data['sumColumn'] = json_encode(array(7));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,7)));
		$data["lokasi"] = $this->lokasi->all_list();
		$data["jenis_produk"] = $this->jenis_produk->all_list();
		$data["produk"] = $this->produk->all_list();
		$data["satuan"] = $this->satuan->all_list();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/stock_by_location');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$lokasi_id = null;
		if(isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != ""){
			$lokasi_id = $_GET["columns"][5]["search"]["value"];
		}
//		else if(isset($_SESSION['login']['lokasi_id'])){
//			$lokasi_id = $_SESSION['login']['lokasi_id'];
//		}
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$_GET['produk_id'] = $_GET["columns"][2]["search"]["value"];
		}
		if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$_GET['jenis_produk_id'] = $_GET["columns"][3]["search"]["value"];
		}
		if(isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != ""){
			$_GET['satuan_id'] = $_GET["columns"][4]["search"]["value"];
		}		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_produk->stock_by_location_count($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->stock_produk->stock_by_location_filter($query,$lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_produk->stock_by_location_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'stock-produk/delete/'.$key->produk_id;
			$key->row_id = $key->produk_id;
			$key->stock = number_format($key->stock);
			$i++;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function pdf(){
		$lokasi_id = null;
		if(isset($_GET['lokasi_id']) && $this->input->get('lokasi_id')!=""){
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}else{
			$data['lokasi'] = " Semua Lokasi ";
		}
		if(isset($_SESSION['login']['lokasi_id'])){
			$lokasi_id = $_SESSION['login']['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if(isset($_GET['jenis_produk_id']) && $this->input->get('jenis_produk_id')!=""){
			$jenis_produk_id = $this->input->get('jenis_produk_id');
			$jenis_produk = $this->jenis_produk->row_by_id($jenis_produk_id);
			$data['jenis_produk'] = $jenis_produk->jenis_produk_nama;
		}else{
			$data['jenis_produk'] = " Semua Jenis ";
		}
		if(isset($_GET['satuan_id'])&& $this->input->get('satuan_id')!=""){
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		}else{
			$data['satuan'] = " Semua Satuan ";
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$produk_id = $this->input->get('produk_id');
			$produk = $this->produk->row_by_id($produk_id);
			$data['produk'] = $produk->produk_nama;
		}else{
			$data['produk'] = " Semua Produk ";
		}		
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->stock_produk->stock_by_location_filter($query,$lokasi_id);
		$list =  $this->stock_produk->stock_by_location_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($list as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'stock-produk/delete/'.$key->produk_id;
			$key->row_id = $key->produk_id;
			$key->stock = number_format($key->stock);
			$i++;
		}
		$data['list'] = $list;			
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_produk_by_location_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Stok Produk'.$date.".pdf","D");		
	}
	function excel(){
		$lokasi_id = null;
		if(isset($_GET['lokasi_id']) && $this->input->get('lokasi_id')!=""){
			$lokasi_id = $this->input->get('lokasi_id');
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}else{
			$data['lokasi'] = " Semua Lokasi ";
		}
		if(isset($_SESSION['login']['lokasi_id'])){
			$lokasi_id = $_SESSION['login']['lokasi_id'];
			$lokasi = $this->lokasi->row_by_id($lokasi_id);
			$data['lokasi'] = $lokasi->lokasi_nama;
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if(isset($_GET['jenis_produk_id']) && $this->input->get('jenis_produk_id')!=""){
			$jenis_produk_id = $this->input->get('jenis_produk_id');
			$jenis_produk = $this->jenis_produk->row_by_id($jenis_produk_id);
			$data['jenis_produk'] = $jenis_produk->jenis_produk_nama;
		}else{
			$data['jenis_produk'] = " Semua Jenis ";
		}
		if(isset($_GET['satuan_id'])&& $this->input->get('satuan_id')!=""){
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		}else{
			$data['satuan'] = " Semua Satuan ";
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$produk_id = $this->input->get('produk_id');
			$produk = $this->produk->row_by_id($produk_id);
			$data['produk'] = $produk->produk_nama;
		}else{
			$data['produk'] = " Semua Produk ";
		}		
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->stock_produk->stock_by_location_filter($query,$lokasi_id);
		$list =  $this->stock_produk->stock_by_location_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($list as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'stock-produk/delete/'.$key->produk_id;
			$key->row_id = $key->produk_id;
			$key->stock = number_format($key->stock);
			$i++;
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Balioz linen')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Kartu Stock Produk')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A8', 'No')
		->setCellValue('B8', 'Kode Produk')
		->setCellValue('C8', 'Nama Produk')
		->setCellValue('D8', 'Spec')
		->setCellValue('E8', 'Jenis Produk')
		->setCellValue('F8', 'Satuan')
		->setCellValue('G8', 'Lokasi')
		->setCellValue('H8', 'Stok')
		;
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$spreadsheet->getActiveSheet()->getStyle("A8:H8")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=9; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->produk_kode)
		->setCellValue('C'.$i, $key->produk_nama)
		->setCellValue('D'.$i, $key->spec)
		->setCellValue('E'.$i, $key->jenis_produk_nama)
		->setCellValue('F'.$i, $key->satuan_nama)
		->setCellValue('G'.$i, $key->lokasi_nama)
		->setCellValue('H'.$i, $key->stock);
		$i++;
		$sum += str_replace(",", "", $key->stock);
		}
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A'.$i,'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H'.$i,number_format($sum));
		$spreadsheet->getActiveSheet()->mergeCells('A'.$i.':G'.$i);
		$spreadsheet->getActiveSheet()->getStyle("A8:H".$i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A8:H8')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('H9:H'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1','PT.Balioz Sadajiwa');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2','Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3','+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G1','Laporan Produk per Lokasi');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G2','Produk : '.$data['produk']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G3','Jenis Produk : '.$data['jenis_produk']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G4','Satuan : '.$data['satuan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G5','Lokasi : '.$data['lokasi']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G6','Pencarian : '.$data['cari']);
		$spreadsheet->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("G1:G6")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Produk per Lokasi');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Stok Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */
