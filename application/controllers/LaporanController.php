<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class LaporanController extends MY_Controller
{
	var $g_data = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mobil', '', true);
		$this->load->model('kegiatan_driver', '', true);
		$this->load->model('penyewaan', '', true);
		$this->load->model('penyewaan_mobil', '', true);
		$this->load->model('jadwal_mobil', '', true);
		$this->load->model('pos', '', true);
		$this->load->model('hutang', '', true);
		$this->load->model('piutang', '', true);
		$this->load->model('jenis_bahan', '', true);
		$this->load->model('jenis_produk', '', true);
		$this->load->model('satuan', '', true);
		$this->load->model('suplier', '', true);
		$this->load->model('lokasi', '', true);
		$this->load->model('tipe_pembayaran', '', true);
		$this->load->model('pengeluaran_driver', '', true);
		$this->load->model('produksi', '', true);
		$this->load->model('staff', '', true);
		$this->load->model('guest', '', true);
	}

	public function index()
	{
		$segment1 = $this->uri->segment(2);
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "script/app.js");

		$this->g_data["css"] = $this->css;
		$this->g_data["js"] = $this->js;
		$this->g_data["meta_title"] = "Laporan < Waisnawa";
		$this->g_data['parrent'] = "laporan";
		$this->g_data['page'] = ucwords(str_replace("-", " ", $this->uri->segment(1)));
		switch ($segment1) {
			case 'laporan-penyewaan':
				$this->laporan_penyewaan();
				break;
			case 'laporan-penyewaan-mobil':
				$this->laporan_penyewaan_mobil();
				break;
			case 'laporan-mobil':
				$this->laporan_mobil();
				break;
			case 'laporan-jadwal-driver':
				$this->laporan_jadwal_driver();
				break;
			case 'laporan-history-driver':
				$this->laporan_history_driver();
				break;
//			case 'laporan-penjualan':
//				$this->laporan_penjualan();
//				break;
			case 'laporan-penjualan-detail':
				$this->laporan_penjualan_detail();
				break;
			case 'laporan-produksi':
				$this->laporan_produksi();
				break;
			case 'laporan-produksi-detail':
				$this->laporan_produksi_detail();
				break;
			case 'laporan-hutang':
				$this->laporan_hutang();
				break;
			case 'laporan-pembayaran-hutang':
				$this->laporan_pembayaran_hutang();
				break;
			case 'laporan-piutang':
				$this->laporan_piutang();
				break;
			case 'laporan-pembayaran-piutang':
				$this->laporan_pembayaran_piutang();
				break;
			case 'laporan-staff':
				$this->laporan_staff();
				break;
			case 'laporan-customer':
				$this->laporan_customer();
				break;
			default:
				redirect('404_override', 'refresh');
				break;
		}
	}

	function laporan_penyewaan()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_penyewaan_list();
				break;
			case 'pdf':
				$this->laporan_penyewaan_pdf();
				break;
			case 'excel':
				$this->laporan_penyewaan_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "no_invoice"));
				array_push($column, array("data" => "pelanggan_nama"));
				array_push($column, array("data" => "pelanggan_kontak"));
				array_push($column, array("data" => "tanggal_order_lbl"));
				array_push($column, array("data" => "grand_total_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['pelanggan'] = $this->penyewaan->get_pelanggan();
				$data['page'] = 'laporan-penyewaan';
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan/penyewaan');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_penyewaan_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		if(isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != ""){
			$_GET['pelanggan_nama'] = $_GET["columns"][4]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penyewaan->penyewaan_count_all_selesai();
		$result['iTotalDisplayRecords'] = $this->penyewaan->penyewaan_count_filter_selesai($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->penyewaan->penyewaan_list_selesai($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$time = strtotime($key->tanggal_order);
			$key->tanggal_order_lbl = date('d-m-Y', $time);
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->grand_total_lbl = number_format($key->grand_total);
			$key->row_id = $key->penyewaan_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_penyewaan_pdf()
	{
		if (isset($_GET['key']) && $this->input->get('key') != "") {
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if (isset($_GET['jenis_bahan_id']) && $this->input->get('jenis_bahan_id') != "") {
			$jenis_bahan_id = $this->input->get('jenis_bahan_id');
			$jenis_bahan = $this->jenis_bahan->row_by_id($jenis_bahan_id);
			$data['jenis_bahan'] = $jenis_bahan->jenis_bahan_nama;
		} else {
			$data['jenis_bahan'] = " Semua Jenis ";
		}
		if (isset($_GET['satuan_id']) && $this->input->get('satuan_id') != "") {
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		} else {
			$data['satuan'] = " Semua Satuan ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bahan->laporan_bahan_count_filter($query);
		$list = $this->bahan->laporan_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'bahan/delete/';
			$key->row_id = $key->bahan_id;
			$key->stok_url = base_url() . 'stock-bahan/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->bahan_id));
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->stock = number_format($key->stock);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_stock_bahan_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Stok Bahan' . $date . ".pdf", "D");
	}

	function laporan_penyewaan_excel()
	{
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bahan->laporan_bahan_count_filter($query);
		$list = $this->bahan->laporan_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'bahan/delete/';
			$key->row_id = $key->bahan_id;
			$key->stok_url = base_url() . 'stock-bahan/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->bahan_id));
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->stock = number_format($key->stock);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Stok Bahan')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);
		if (isset($_GET['key']) && $this->input->get('key') != "") {
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if (isset($_GET['jenis_bahan_id']) && $this->input->get('jenis_bahan_id') != "") {
			$jenis_bahan_id = $this->input->get('jenis_bahan_id');
			$jenis_bahan = $this->jenis_bahan->row_by_id($jenis_bahan_id);
			$data['jenis_bahan'] = $jenis_bahan->jenis_bahan_nama;
		} else {
			$data['jenis_bahan'] = " Semua Jenis ";
		}
		if (isset($_GET['satuan_id']) && $this->input->get('satuan_id') != "") {
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		} else {
			$data['satuan'] = " Semua Satuan ";
		}

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A7', 'No')
			->setCellValue('B7', 'Kode Bahan')
			->setCellValue('C7', 'Nama Bahan')
			->setCellValue('D7', 'Jenis Bahan')
			->setCellValue('E7', 'Satuan')
			->setCellValue('F7', 'Global Stok');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
		$spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i = 8;
		foreach ($list as $key) {

			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->bahan_kode)
				->setCellValue('C' . $i, $key->bahan_nama)
				->setCellValue('D' . $i, $key->jenis_bahan_nama)
				->setCellValue('E' . $i, $key->satuan_nama)
				->setCellValue('F' . $i, $key->stock);
			$i++;
			$sum += str_replace(",", "", $key->stock);
		}
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $i, number_format($sum));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':E' . $i);
		$spreadsheet->getActiveSheet()->getStyle("A7:F" . $i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:F7')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('F8:F' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F1', 'Laporan Stok Bahan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F2', 'Jenis Bahan : ' . $data['jenis_bahan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F3', 'Satuan : ' . $data['satuan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F4', 'Pencarian : ' . $data['cari']);
		$spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("F1:F4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Stok Bahan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Stok Bahan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_penyewaan_mobil()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_stock_produk_list();
				break;
			case 'pdf':
				$this->laporan_stock_produk_pdf();
				break;
			case 'excel':
				$this->laporan_stock_produk_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 6);
				$sumColumn = array(6);
				$column = array();
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "produk_kode"));
				array_push($column, array("data" => "produk_nama"));
				array_push($column, array("data" => "spec"));
				array_push($column, array("data" => "jenis_produk_nama"));
				array_push($column, array("data" => "satuan_nama"));
				array_push($column, array("data" => "stock"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['jenis_produk'] = $this->jenis_produk->all_list();
				$data['sumColumn'] = json_encode($sumColumn);
				$data['satuan'] = $this->satuan->all_list();
				$data['page'] = "laporan-stock-produk";
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_produk');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_penyewaan_mobil_list()
	{
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['jenis_produk_id'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			$_GET['satuan_id'] = $_GET["columns"][4]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->laporan_produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->laporan_produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->produk->laporan_produk_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url() . 'stock-produk/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->produk_id));
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
			$key->stock = number_format($key->stock);
			if (isset($_SESSION['login']['lokasi_id'])) {
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);
			}
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_penyewaan_mobil_pdf()
	{
		if (isset($_GET['key']) && $this->input->get('key') != "") {
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if (isset($_GET['jenis_produk_id']) && $this->input->get('jenis_produk_id') != "") {
			$jenis_produk_id = $this->input->get('jenis_produk_id');
			$jenis_produk = $this->jenis_produk->row_by_id($jenis_produk_id);
			$data['jenis_produk'] = $jenis_produk->jenis_produk_nama;
		} else {
			$data['jenis_produk'] = " Semua Jenis ";
		}
		if (isset($_GET['satuan_id']) && $this->input->get('satuan_id') != "") {
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		} else {
			$data['satuan'] = " Semua Satuan ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produk->laporan_produk_count_filter($query);
		$list = $this->produk->laporan_produk_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url() . 'stock-produk/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->produk_id));
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
			$key->stock = number_format($key->stock);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_stock_produk_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Stok Produk' . $date . ".pdf", "D");
	}

	function laporan_penyewaan_mobil_excel()
	{
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produk->laporan_produk_count_filter($query);
		$list = $this->produk->laporan_produk_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url() . 'stock-produk/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->produk_id));
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
			$key->stock = number_format($key->stock);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Stok Produk')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);
		if (isset($_GET['key']) && $this->input->get('key') != "") {
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		if (isset($_GET['jenis_produk_id']) && $this->input->get('jenis_produk_id') != "") {
			$jenis_produk_id = $this->input->get('jenis_produk_id');
			$jenis_produk = $this->jenis_produk->row_by_id($jenis_produk_id);
			$data['jenis_produk'] = $jenis_produk->jenis_produk_nama;
		} else {
			$data['jenis_produk'] = " Semua Jenis ";
		}
		if (isset($_GET['satuan_id']) && $this->input->get('satuan_id') != "") {
			$satuan_id = $this->input->get('satuan_id');
			$satuan = $this->satuan->row_by_id($satuan_id);
			$data['satuan'] = $satuan->satuan_nama;
		} else {
			$data['satuan'] = " Semua Satuan ";
		}

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A7', 'No')
			->setCellValue('B7', 'Kode Produk')
			->setCellValue('C7', 'Nama Produk')
			->setCellValue('D7', 'Spec')
			->setCellValue('E7', 'Jenis Produk')
			->setCellValue('F7', 'Satuan')
			->setCellValue('G7', 'Global Stok');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$spreadsheet->getActiveSheet()->getStyle("A7:G7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i = 8;
		foreach ($list as $key) {

			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->produk_kode)
				->setCellValue('C' . $i, $key->produk_nama)
				->setCellValue('D' . $i, $key->spec)
				->setCellValue('E' . $i, $key->jenis_produk_nama)
				->setCellValue('F' . $i, $key->satuan_nama)
				->setCellValue('G' . $i, $key->stock);
			$i++;
			$sum += str_replace(",", "", $key->stock);
		}
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $i, number_format($sum));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':F' . $i);
		$spreadsheet->getActiveSheet()->getStyle("A7:G" . $i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:G7')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('G8:G' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F1', 'Laporan Stok Produk');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F2', 'Jenis Produk : ' . $data['jenis_produk']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F3', 'Satuan : ' . $data['satuan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F4', 'Pencarian : ' . $data['cari']);
		$spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("F1:F4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Stok Produk');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Stok Produk' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_mobil()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_po_bahan_list();
				break;
			case 'pdf':
				$this->laporan_po_bahan_pdf();
				break;
			case 'excel':
				$this->laporan_po_bahan_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "po_bahan_no"));
				array_push($column, array("data" => "tanggal_pemesanan"));
				array_push($column, array("data" => "suplier_nama"));
				array_push($column, array("data" => "grand_total"));
				array_push($column, array("data" => "jenis_pembayaran"));
				array_push($column, array("data" => "tipe_pembayaran_nama"));
				array_push($column, array("data" => "status_pembayaran"));
				array_push($column, array("data" => "tanggal_penerimaan"));
				array_push($column, array("data" => "status_penerimaan"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['jenis_produk'] = $this->jenis_produk->all_list();
				$data['sumColumn'] = json_encode($sumColumn);
				$data['satuan'] = $this->satuan->all_list();
				$data['suplier'] = $this->suplier->all_list();
				$data['page'] = 'laporan-po-bahan';
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_po_bahan_global');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_mobil_list()
	{
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['suplier_id'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['jenis_pembayaran'] = $_GET["columns"][5]["search"]["value"];
		}
		if (isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != "") {
			$_GET['status_pembayaran'] = $_GET["columns"][7]["search"]["value"];
		}
		if (isset($_GET["columns"][9]["search"]["value"]) && $_GET["columns"][9]["search"]["value"] != "") {
			$_GET['status_penerimaan'] = $_GET["columns"][9]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_pemesanan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_pemesanan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][8]["search"]["value"]) && $_GET["columns"][8]["search"]["value"] != "") {
			$temp = $_GET["columns"][8]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_penerimaan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_penerimaan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_bahan->po_bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->po_bahan->po_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->po_bahan->po_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_pemesanan != null) {
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
			$key->grand_total = number_format($key->grand_total);
			$key->penerimaan_status_btn = true;
			$key->edit_url = base_url() . 'po-bahan/edit/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->no = $i;
			if ($key->jenis_pembayaran == "kas") {
				$key->tipe_pembayaran_nama = $key->kas_nama . " " . $key->no_akun;
			}
			$i++;
			$key->delete_url = base_url() . 'po-bahan/delete/';
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_mobil_pdf()
	{
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['suplier_id'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['jenis_pembayaran'] = $_GET["columns"][5]["search"]["value"];
		}
		if (isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != "") {
			$_GET['status_pembayaran'] = $_GET["columns"][7]["search"]["value"];
		}
		if (isset($_GET["columns"][9]["search"]["value"]) && $_GET["columns"][9]["search"]["value"] != "") {
			$_GET['status_penerimaan'] = $_GET["columns"][9]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_pemesanan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_pemesanan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][8]["search"]["value"]) && $_GET["columns"][8]["search"]["value"] != "") {
			$temp = $_GET["columns"][8]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_penerimaan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_penerimaan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_bahan->po_bahan_count_filter($query);
		$list = $this->po_bahan->po_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_pemesanan != null) {
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
			$key->grand_total = number_format($key->grand_total);
			$key->penerimaan_status_btn = true;
			$key->edit_url = base_url() . 'po-bahan/edit/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->no = $i;
			if ($key->jenis_pembayaran == "kas") {
				$key->tipe_pembayaran_nama = $key->kas_nama . " " . $key->no_akun;
			}
			$i++;
			$key->delete_url = base_url() . 'po-bahan/delete/';
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_po_bahan_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Order Bahan ' . $date . ".pdf", "D");
	}

	function laporan_mobil_excel()
	{
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['suplier_id'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['jenis_pembayaran'] = $_GET["columns"][5]["search"]["value"];
		}
		if (isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != "") {
			$_GET['status_pembayaran'] = $_GET["columns"][7]["search"]["value"];
		}
		if (isset($_GET["columns"][9]["search"]["value"]) && $_GET["columns"][9]["search"]["value"] != "") {
			$_GET['status_penerimaan'] = $_GET["columns"][9]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_pemesanan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_pemesanan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][8]["search"]["value"]) && $_GET["columns"][8]["search"]["value"] != "") {
			$temp = $_GET["columns"][8]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_penerimaan'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['end_penerimaan'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_bahan->po_bahan_count_filter($query);
		$list = $this->po_bahan->po_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_pemesanan != null) {
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
			$key->grand_total = number_format($key->grand_total);
			$key->penerimaan_status_btn = true;
			$key->edit_url = base_url() . 'po-bahan/edit/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->no = $i;
			if ($key->jenis_pembayaran == "kas") {
				$key->tipe_pembayaran_nama = $key->kas_nama . " " . $key->no_akun;
			}
			$i++;
			$key->delete_url = base_url() . 'po-bahan/delete/';
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Order Bahan')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A7', 'No')
			->setCellValue('B7', 'Order Bahan No')
			->setCellValue('C7', 'Tanggal Pemesanan')
			->setCellValue('D7', 'Suplier')
			->setCellValue('E7', 'Grand Total')
			->setCellValue('F7', 'Jenis Pembayaran')
			->setCellValue('G7', 'Metode Pembayaran')
			->setCellValue('H7', 'Status Pembayaran')
			->setCellValue('I7', 'Tanggal Penerimaan')
			->setCellValue('J7', 'Status Penerimaan');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A7:J7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i = 8;
		foreach ($list as $key) {

			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->po_bahan_no)
				->setCellValue('C' . $i, $key->tanggal_pemesanan)
				->setCellValue('D' . $i, $key->suplier_nama)
				->setCellValue('E' . $i, $key->grand_total)
				->setCellValue('F' . $i, $key->jenis_pembayaran)
				->setCellValue('G' . $i, $key->tipe_pembayaran_nama)
				->setCellValue('H' . $i, $key->status_pembayaran)
				->setCellValue('I' . $i, $key->tanggal_penerimaan)
				->setCellValue('J' . $i, $key->status_penerimaan);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:J" . ($i - 1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:J7')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('J8:J' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1', 'Laporan Stok Produk');
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Order Bahan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Bahan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_jadwal_driver()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_jadwal_driver_list();
				break;
			case 'pdf':
				$this->laporan_jadwal_driver_pdf();
				break;
			case 'excel':
				$this->laporan_jadwal_driver_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0);
				$sumColumn = array(8, 9);
				$column = array();
				$data['page'] = "laporan-jadwal-driver";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "driver_nama"));
				array_push($column, array("data" => "tanggal_lbl"));
				array_push($column, array("data" => "mobil_jenis_nama"));
				array_push($column, array("data" => "mobil_plat"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
//				$data['sumColumn'] = json_encode($sumColumn);
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$akses_menu = json_decode($this->menu_akses, true);
				$action = array();
				foreach ($akses_menu['laporan']['laporan-jadwal-driver'] as $key => $value) {
					if ($key != "akses_menu") {
						$action[$key] = $value;
					}
				}
				$data['action'] = json_encode($action);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan/jadwal_driver');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_jadwal_driver_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$_GET['driver_nama'] = $_GET["columns"][2]["search"]["value"];
		}
//		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
//			$_GET['lokasi_id'] = $_GET["columns"][3]["search"]["value"];
//		} else if (isset($_SESSION['login']['lokasi_id'])) {
//			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
//		}
//		if (isset($_GET["columns"][9]["search"]["value"]) && $_GET["columns"][9]["search"]["value"] != "") {
//			$_GET['tipe_pembayaran_id'] = $_GET["columns"][9]["search"]["value"];
//		}
//		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
//			json_encode($_GET["columns"][4]["search"]["value"]);
//			$_GET['staff_id'] = $_GET["columns"][4]["search"]["value"];
//		}
		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : '';
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->jadwal_mobil->jadwal_kedepan_all_count_all();
		$result['iTotalDisplayRecords'] = $this->jadwal_mobil->jadwal_kedepan_all_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->jadwal_mobil->jadwal_kedepan_all_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->tanggal != null) {
				$time = strtotime($key->tanggal);
				$key->tanggal_lbl = date('d-m-Y', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_jadwal_driver_pdf()
	{
		if (isset($_GET["po_bahan_no"]) && $this->input->get('po_bahan_no') != "") {
			$data['po_bahan_no'] = $_GET["po_bahan_no"];
		}
		if (isset($_GET["pemesanan_start"]) && $this->input->get('pemesanan_start') != "") {
			$_GET['pemesanan_start'] = (($_GET['pemesanan_start'] != "") ? $_GET['pemesanan_start'] : date("Y-m-d", 0));
			$_GET['pemesanan_end'] = (($_GET['pemesanan_end'] != "") ? $_GET['pemesanan_end'] : date("Y-m-d"));
			$data['pemesanan_start'] = $_GET['pemesanan_start'];
			$data['pemesanan_end'] = $_GET['pemesanan_end'];
		}
		if (isset($_GET["suplier_id"]) && $this->input->get('suplier_id') != "") {
			$suplier = $this->suplier->row_by_id($_GET['suplier_id']);
			$data['suplier'] = $suplier->suplier_nama;
		}
		if (isset($_GET["bahan_id"]) && $this->input->get('bahan_id') != "") {
			$bahan = $this->bahan->row_by_id($_GET['bahan_id']);
			$data['bahan'] = $bahan->bahan_nama;
		}
		if (isset($_GET["tipe_pembayaran_id"]) && $this->input->get('tipe_pembayaran_id') != "") {
			if ($this->input->get('tipe_pembayaran_id') != "kredit") {
				$tipe_pembayaran = $this->tipe_pembayaran->row_by_id($_GET['tipe_pembayaran_id']);
				$data['tipe_pembayaran'] = $tipe_pembayaran->tipe_pembayaran_nama . " " . $tipe_pembayaran->no_akun;
			} else {
				$data['tipe_pembayaran'] = "Kredit";
			}

		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_bahan->laporan_po_bahan_count_filter($query);
		$list = $this->po_bahan->laporan_po_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_pemesanan != null) {
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y', $time);
			}
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
			$key->jumlah = number_format($key->jumlah);
			$key->no = $i;
			if ($key->jenis_pembayaran == "kas") {
				$key->tipe_pembayaran_nama = $key->kas_nama . " " . $key->no_akun;
			} else {
				$key->tipe_pembayaran_nama = "Kredit";
			}
			$i++;
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_po_bahan_detail_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}
		$mpdf->Output('Laporan Order Bahan Detail ' . $date . ".pdf", "D");
	}

	function laporan_jadwal_driver_excel()
	{
		if (isset($_GET["po_bahan_no"]) && $this->input->get('po_bahan_no') != "") {
			$data['po_bahan_no'] = $_GET["po_bahan_no"];
		} else {
			$data['po_bahan_no'] = "";
		}
		if (isset($_GET["pemesanan_start"]) && $this->input->get('pemesanan_start') != "") {
			$_GET['pemesanan_start'] = (($_GET['pemesanan_start'] != "") ? $_GET['pemesanan_start'] : date("Y-m-d", 0));
			$_GET['pemesanan_end'] = (($_GET['pemesanan_end'] != "") ? $_GET['pemesanan_end'] : date("Y-m-d"));
			$data['pemesanan_start'] = $_GET['pemesanan_start'];
			$data['pemesanan_end'] = $_GET['pemesanan_end'];
		} else {
			$data['pemesanan_start'] = date("Y-m-d", 0);
			$data['pemesanan_end'] = date("Y-m-d");
		}
		if (isset($_GET["suplier_id"]) && $this->input->get('suplier_id') != "") {
			$suplier = $this->suplier->row_by_id($_GET['suplier_id']);
			$data['suplier'] = $suplier->suplier_nama;
		} else {
			$data['suplier'] = "Semua Suplier";
		}
		if (isset($_GET["bahan_id"]) && $this->input->get('bahan_id') != "") {
			$bahan = $this->bahan->row_by_id($_GET['bahan_id']);
			$data['bahan'] = $bahan->bahan_nama;
		} else {
			$data['bahan'] = "Semua Bahan";
		}
		if (isset($_GET["tipe_pembayaran_id"]) && $this->input->get('tipe_pembayaran_id') != "") {
			if ($this->input->get('tipe_pembayaran_id') != "kredit") {
				$tipe_pembayaran = $this->bahan->row_by_id($_GET['tipe_pembayaran_id']);
				$data['tipe_pembayaran'] = $tipe_pembayaran->tipe_pembayaran_nama . " " . $tipe_pembayaran->no_akun;
			} else {
				$data['tipe_pembayaran'] = "Kredit";
			}

		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_bahan->laporan_po_bahan_count_filter($query);
		$list = $this->po_bahan->laporan_po_bahan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_pemesanan != null) {
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y', $time);
			}
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
			$key->jumlah = number_format($key->jumlah);
			$key->no = $i;
			if ($key->jenis_pembayaran == "kas") {
				$key->tipe_pembayaran_nama = $key->kas_nama . " " . $key->no_akun;
			} else {
				$key->tipe_pembayaran_nama = "Kredit";
			}
			$i++;
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Order Bahan')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A7', 'No')
			->setCellValue('B7', 'Order Bahan No')
			->setCellValue('C7', 'Tanggal Pemesanan')
			->setCellValue('D7', 'Suplier')
			->setCellValue('E7', 'Kode Bahan')
			->setCellValue('F7', 'Nama Bahan')
			->setCellValue('G7', 'Metode Pembayaran')
			->setCellValue('H7', 'Harga Bahan')
			->setCellValue('I7', 'Jumlah')
			->setCellValue('J7', 'Subtotal');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A7:J7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sumJum = 0;
		$sumTot = 0;
		$i = 8;
		foreach ($list as $key) {
			$sumJum += str_replace(",", "", $key->jumlah);
			$sumTot += str_replace(",", "", $key->sub_total);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->po_bahan_no)
				->setCellValue('C' . $i, $key->tanggal_pemesanan)
				->setCellValue('D' . $i, $key->suplier_nama)
				->setCellValue('E' . $i, $key->bahan_kode)
				->setCellValue('F' . $i, $key->bahan_nama)
				->setCellValue('G' . $i, $key->tipe_pembayaran_nama)
				->setCellValue('H' . $i, $key->harga)
				->setCellValue('I' . $i, $key->jumlah)
				->setCellValue('J' . $i, $key->sub_total);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:J" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:J7')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('J8:J' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . $i, number_format($sumJum));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J' . $i, number_format($sumTot));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1', 'Laporan Stok Produk');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J2', 'Tanggal pemesanan :' . $data['pemesanan_start'] . " s/d " . $data['pemesanan_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J3', 'No Order Bahan : ' . $data['po_bahan_no']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J4', 'Suplier : ' . $data['suplier']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J5', 'Bahan : ' . $data['bahan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J6', 'Metode Pembayaran : ' . $data['tipe_pembayaran']);
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J6")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Order Bahan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Bahan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_history_driver()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_history_driver_list();
				break;
			case 'pdf':
				$this->laporan_history_driver_pdf();
				break;
			case 'excel':
				$this->laporan_history_driver_excel();
				break;
			default:
				$data = $this->g_data;

				array_push($this->js, "script/admin/upload_image.js");
				array_push($this->js, "script/admin/pencatatan_kendaraan.js");
				$target = array(0);
//				$sumColumn = array(8);
				$column = array();
				$data['page'] = "laporan-history-driver";
				$data["js"] = $this->js;
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "driver_nama"));
				array_push($column, array("data" => "tanggal_lbl"));
				array_push($column, array("data" => "mobil_jenis"));
				array_push($column, array("data" => "mobil_plat"));
				array_push($column, array("data" => "waktu_mulai"));
				array_push($column, array("data" => "waktu_selesai"));
				array_push($column, array("data" => "waktu_kerja"));
				array_push($column, array("data" => "status_pembayaran"));
				if ($_SESSION['login']['user_role_id'] == 1) {
					array_push($column, array("data" => "action_btn"));
				}
				$data['lokasi'] = $this->lokasi->all_list();
				$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$akses_menu = json_decode($this->menu_akses, true);
				$action = array();
				foreach ($akses_menu['laporan']['laporan-history-driver'] as $key => $value) {
					if ($key != "akses_menu") {
						$action[$key] = $value;
					}
				}
				$data['action'] = json_encode($action);
//				$data['sumColumn'] = json_encode($sumColumn);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan/history_driver');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_history_driver_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$_GET['driver_nama'] = $_GET["columns"][2]["search"]["value"];
		}
//		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
//			$_GET['lokasi_id'] = $_GET["columns"][3]["search"]["value"];
//		} else if (isset($_SESSION['login']['lokasi_id'])) {
//			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
//		}
//		if (isset($_GET["columns"][9]["search"]["value"]) && $_GET["columns"][9]["search"]["value"] != "") {
//			$_GET['tipe_pembayaran_id'] = $_GET["columns"][9]["search"]["value"];
//		}
//		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
//			json_encode($_GET["columns"][4]["search"]["value"]);
//			$_GET['staff_id'] = $_GET["columns"][4]["search"]["value"];
//		}
		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : '';
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->kegiatan_driver->history_all_count_all();
		$result['iTotalDisplayRecords'] = $this->kegiatan_driver->history_all_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->kegiatan_driver->history_all_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->tanggal != null) {
				$time = strtotime($key->tanggal);
				$key->tanggal_lbl = date('d-m-Y', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->item = $this->pengeluaran_driver->get_pengeluaran($key->driver_id, $key->tanggal);
			if (!empty($key->item)) {
				foreach ($key->item as $item) {
					$item->jam = $this->format_time($item->tanggal);
				}
			}
			$key->action_btn = '<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" style="width: 100%">
								<textarea style="display:none">' . json_encode($key) . '</textarea>
								<button class="btn btn-success btn-icon btn-icon-sm manage-btn" title="Manage">
								<i class="flaticon2-edit"></i>&nbsp; Manage
								</button>';
			if ($key->status_pembayaran == 'Belum Dibayar' || $_SESSION['login']['user_role_id'] == 1) {
				$key->action_btn .= '<button class="btn btn-warning btn-icon btn-icon-sm payment-btn" title="Bayar">
								<i class="fas fa-dollar-sign"></i>&nbsp; Bayar
								</button>';
			}

			$key->action_btn .= '</div>';

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
//		echo $this->input->get('staff_id');
	}

	function laporan_history_driver_pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$data['driver_nama'] = ($this->input->get('driver_nama') != "") ? $this->input->get('driver_nama') : '';
//		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
//		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
//		$data['staff'] = ($this->input->get('staff_id') != "") ? $this->staff->row_by_id($this->input->get('staff_id'))->staff_nama : 'Semua Staff';
//		if (isset($_SESSION['login']['lokasi_id'])) {
//			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
//		}
//		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->kegiatan_driver->history_all_count_filter($query);
		$list = $this->kegiatan_driver->history_all_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->total = number_format($key->total);
			$key->biaya_tambahan = number_format($key->biaya_tambahan);
			$key->potongan_akhir = number_format($key->potongan_akhir);
			$key->grand_total = number_format($key->grand_total);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_penjualan_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Penjualan ' . $date . ".pdf", "D");
	}

	function laporan_history_driver_excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
		$data['staff'] = ($this->input->get('staff_id') != "") ? $this->staff->row_by_id($this->input->get('staff_id'))->staff_nama : 'Semua Staff';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->kegiatan_driver->history_all_count_filter($query);
		$list = $this->kegiatan_driver->history_all_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->total = number_format($key->total);
			$key->biaya_tambahan = number_format($key->biaya_tambahan);
			$key->potongan_akhir = number_format($key->potongan_akhir);
			$key->grand_total = number_format($key->grand_total);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Penjualan')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A8', 'No')
			->setCellValue('B8', 'Tanggal')
			->setCellValue('C8', 'No Faktur')
			->setCellValue('D8', 'Lokasi')
			->setCellValue('E8', 'Pelanggan')
			->setCellValue('F8', 'Total')
			->setCellValue('G8', 'Biaya Tambahan')
			->setCellValue('H8', 'Potongan Akhir')
			->setCellValue('I8', 'Grand Total')
			->setCellValue('J8', 'Tipe Pembayaran');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A8:J8")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$grand_total = 0;
		$i = 9;
		foreach ($list as $key) {
			$grand_total += str_replace(",", "", $key->grand_total);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->tanggal)
				->setCellValue('C' . $i, $key->no_faktur)
				->setCellValue('D' . $i, $key->lokasi_nama)
				->setCellValue('E' . $i, $key->nama_pelanggan)
				->setCellValue('F' . $i, $key->total)
				->setCellValue('G' . $i, $key->biaya_tambahan)
				->setCellValue('H' . $i, $key->potongan_akhir)
				->setCellValue('I' . $i, $key->grand_total)
				->setCellValue('J' . $i, $key->tipe_pembayaran_nama);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A8:J" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A8:J8')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('J9:J' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . $i, number_format($grand_total));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', "'+6285858424039'");
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1', 'Laporan Penjualan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J2', 'Tanggal :' . $data['tanggal_start'] . " s/d " . $data['tanggal_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J3', 'No Faktur : ' . $data['no_faktur']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J4', 'Lokasi : ' . $data['lokasi']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J5', 'Tipe Pembayaran : ' . $data['tipe_pembayaran']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J6', 'Staff : ' . $data['staff']);
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J6")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Pembayaran');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Penjualan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_penjualan_detail()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_penjualan_detail_list();
				break;
			case 'pdf':
				$this->laporan_penjualan_detail_pdf();
				break;
			case 'excel':
				$this->laporan_penjualan_detail_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 7, 8, 9);
				$sumColumn = array(8, 9);
				$column = array();
				$data['page'] = "laporan-penjualan-detail";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "tanggal"));
				array_push($column, array("data" => "no_faktur"));
				array_push($column, array("data" => "lokasi_nama"));
				array_push($column, array("data" => "nama_pelanggan"));
				array_push($column, array("data" => "produk_kode"));
				array_push($column, array("data" => "produk_nama"));
				array_push($column, array("data" => "harga"));
				array_push($column, array("data" => "qty"));
				array_push($column, array("data" => "sub_total"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['lokasi'] = $this->lokasi->all_list();
				$data['produk'] = $this->produk->all_list();
				$data['staff'] = $this->pos->staff_penjualan();
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_penjualan_detail');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_penjualan_detail_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$_GET['no_faktur'] = $_GET["columns"][2]["search"]["value"];
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['lokasi_id'] = $_GET["columns"][3]["search"]["value"];
		} else if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['produk_id'] = $_GET["columns"][5]["search"]["value"];
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			json_encode($_GET["columns"][4]["search"]["value"]);
			$_GET['staff_id'] = $_GET["columns"][4]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pos->laporan_detail_transaksi_count_all();
		$result['iTotalDisplayRecords'] = $this->pos->laporan_detail_transaksi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->pos->laporan_detail_transaksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->qty = number_format($key->qty);
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_penjualan_detail_pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
		$data['staff'] = ($this->input->get('staff_id') != "") ? $this->staff->row_by_id($this->input->get('staff_id'))->staff_nama : 'Semua Staff';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pos->laporan_detail_transaksi_count_filter($query);
		$list = $this->pos->laporan_detail_transaksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->qty = number_format($key->qty);
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_penjualan_detail_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Penjualan ' . $date . ".pdf", "D");
	}

	function laporan_penjualan_detail_excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
		$data['staff'] = ($this->input->get('staff_id') != "") ? $this->staff->row_by_id($this->input->get('staff_id'))->staff_nama : 'Semua Staff';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pos->laporan_detail_transaksi_count_filter($query);
		$list = $this->pos->laporan_detail_transaksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->qty = number_format($key->qty);
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Detail Penjualan')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A8', 'No')
			->setCellValue('B8', 'Tanggal')
			->setCellValue('C8', 'No Faktur')
			->setCellValue('D8', 'Lokasi')
			->setCellValue('E8', 'Pelanggan')
			->setCellValue('F8', 'Kode Produk')
			->setCellValue('G8', 'Nama Produk')
			->setCellValue('H8', 'Harga')
			->setCellValue('I8', 'Jumlah')
			->setCellValue('J8', 'Subtotal');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A8:J8")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sub_total = 0;
		$qty = 0;
		$i = 9;
		foreach ($list as $key) {
			$sub_total += str_replace(",", "", $key->sub_total);
			$qty += str_replace(",", "", $key->qty);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->tanggal)
				->setCellValue('C' . $i, $key->no_faktur)
				->setCellValue('D' . $i, $key->lokasi_nama)
				->setCellValue('E' . $i, $key->nama_pelanggan)
				->setCellValue('F' . $i, $key->produk_kode)
				->setCellValue('G' . $i, $key->produk_nama)
				->setCellValue('H' . $i, $key->harga)
				->setCellValue('I' . $i, $key->qty)
				->setCellValue('J' . $i, $key->sub_total);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A8:J" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A8:J8')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('J9:J' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . $i, number_format($qty));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J' . $i, number_format($sub_total));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '"+6285858424039"');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1', 'Laporan Penjualan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J2', 'Tanggal :' . $data['tanggal_start'] . " s/d " . $data['tanggal_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J3', 'No Faktur : ' . $data['no_faktur']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J4', 'Lokasi : ' . $data['lokasi']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J5', 'Produk : ' . $data['produk']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J6', 'Staff : ' . $data['staff']);
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J6")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Detail Penjualan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Detail Penjualan' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_produksi()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_produksi_list();
				break;
			case 'pdf':
				$this->laporan_produksi_pdf();
				break;
			case 'excel':
				$this->laporan_produksi_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 7);
				$sumColumn = array(7);
				$column = array();
				$data['page'] = "laporan-produksi";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "produksi_kode"));
				array_push($column, array("data" => "tanggal_mulai"));
				array_push($column, array("data" => "status_produksi"));
				array_push($column, array("data" => "tanggal_penerimaan"));
				array_push($column, array("data" => "status_penerimaan"));
				array_push($column, array("data" => "produk_nama"));
				array_push($column, array("data" => "jumlah"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['produk'] = $this->produk->all_list();
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_produksi');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_produksi_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['produksi_kode'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][2]["search"]["value"]);
			$_GET['mulai_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['mulai_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['status_produksi'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][4]["search"]["value"]);
			$_GET['penerimaan_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['penerimaan_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['status_penerimaan'] = $_GET["columns"][5]["search"]["value"];
		}
		if (isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != "") {
			$_GET['produk_id'] = $_GET["columns"][6]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produksi->laporan_produksi_all();
		$result['iTotalDisplayRecords'] = $this->produksi->laporan_produksi_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->produksi->laporan_produksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_produksi_pdf()
	{
		$data['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
		$data['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		if ($this->input->get('mulai_start') != "" && $this->input->get('mulai_end') != "") {
			$_GET['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
			$_GET['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		}
		$data['produksi_kode'] = ($this->input->get('produksi_kode') != "") ? $this->input->get('produksi_kode') : '';
		$data['status_produksi'] = ($this->input->get('status_produksi') != "") ? $this->input->get('status_produksi') : 'Semua Status';
		$data['status_penerimaan'] = ($this->input->get('status_penerimaan') != "") ? $this->input->get('status_penerimaan') : 'Semua Status';
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$data['penerimaan_start'] = ($this->input->get('penerimaan_start') != "") ? $this->input->get('penerimaan_start') : date("Y-m-d", 0);
		$data['penerimaan_end'] = ($this->input->get('penerimaan_end') != "") ? $this->input->get('penerimaan_end') : date("Y-m-d");
		if ($this->input->get('penerimaan_start') != "" && $this->input->get('penerimaan_end')) {
			$_GET['penerimaan_start'] = ($this->input->get('penerimaan_start') != "") ? $this->input->get('penerimaan_start') : date("Y-m-d", 0);
			$_GET['penerimaan_end'] = ($this->input->get('penerimaan_end') != "") ? $this->input->get('penerimaan_end') : date("Y-m-d");
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produksi->laporan_produksi_filter($query);
		$list = $this->produksi->laporan_produksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_produksi_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Produksi ' . $date . ".pdf", "D");
	}

	function laporan_produksi_excel()
	{
		$data['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
		$data['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		if ($this->input->get('mulai_start') != "" && $this->input->get('mulai_end') != "") {
			$_GET['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
			$_GET['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		}
		$data['produksi_kode'] = ($this->input->get('produksi_kode') != "") ? $this->input->get('produksi_kode') : '';
		$data['status_produksi'] = ($this->input->get('status_produksi') != "") ? $this->input->get('status_produksi') : 'Semua Status';
		$data['status_penerimaan'] = ($this->input->get('status_penerimaan') != "") ? $this->input->get('status_penerimaan') : 'Semua Status';
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$data['penerimaan_start'] = ($this->input->get('penerimaan_start') != "") ? $this->input->get('penerimaan_start') : date("Y-m-d", 0);
		$data['penerimaan_end'] = ($this->input->get('penerimaan_end') != "") ? $this->input->get('penerimaan_end') : date("Y-m-d");
		if ($this->input->get('penerimaan_start') != "" && $this->input->get('penerimaan_end')) {
			$_GET['penerimaan_start'] = ($this->input->get('penerimaan_start') != "") ? $this->input->get('penerimaan_start') : date("Y-m-d", 0);
			$_GET['penerimaan_end'] = ($this->input->get('penerimaan_end') != "") ? $this->input->get('penerimaan_end') : date("Y-m-d");
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produksi->laporan_produksi_filter($query);
		$list = $this->produksi->laporan_produksi_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Produksi')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Kode Produksi')
			->setCellValue('C9', 'Tanggal Produksi')
			->setCellValue('D9', 'Status Produksi')
			->setCellValue('E9', 'Tanggal Penerimaan')
			->setCellValue('F9', 'Status Penerimaan')
			->setCellValue('G9', 'Produk')
			->setCellValue('H9', 'Jumlah');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A9:H9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$qty = 0;
		$i = 10;
		foreach ($list as $key) {
			$qty += str_replace(",", "", $key->jumlah);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->produksi_kode)
				->setCellValue('C' . $i, $key->tanggal_mulai)
				->setCellValue('D' . $i, $key->status_produksi)
				->setCellValue('E' . $i, $key->tanggal_penerimaan)
				->setCellValue('F' . $i, $key->status_penerimaan)
				->setCellValue('G' . $i, $key->produk_nama)
				->setCellValue('H' . $i, $key->jumlah);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:H" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:H9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('H10:H' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . $i, number_format($qty));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Produksi');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tanggal Produksi:' . $data['mulai_start'] . " s/d " . $data['mulai_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'Kode Produksi : ' . $data['produksi_kode']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H4', 'Status Produksi : ' . $data['status_produksi']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H5', 'Tanggal Penerimaan : ' . $data['penerimaan_start'] . " s/d " . $data['penerimaan_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H6', 'Status Penerimaan : ' . $data['status_penerimaan']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H7', 'Produk : ' . $data['produk']);
		$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("H1:H7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Produksi');
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Produksi ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_produksi_detail()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_produksi_detail_list();
				break;
			case 'pdf':
				$this->laporan_produksi_detail_pdf();
				break;
			case 'excel':
				$this->laporan_produksi_detail_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 6);
				$sumColumn = array(6);
				$column = array();
				$data['page'] = "laporan-produksi";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "produksi_kode"));
				array_push($column, array("data" => "tanggal_mulai"));
				array_push($column, array("data" => "status_produksi"));
				array_push($column, array("data" => "produk_nama"));
				array_push($column, array("data" => "bahan_nama"));
				array_push($column, array("data" => "jumlah"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['produk'] = $this->produk->all_list();
				$data['bahan'] = $this->bahan->all_list();
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_produksi_detail');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_produksi_detail_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['produksi_kode'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][2]["search"]["value"]);
			$_GET['mulai_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['mulai_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			$_GET['produk_id'] = $_GET["columns"][4]["search"]["value"];
		}
		if (isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != "") {
			$_GET['bahan_id'] = $_GET["columns"][5]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produksi->laporan_produksi_detail_all();
		$result['iTotalDisplayRecords'] = $this->produksi->laporan_produksi_detail_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->produksi->laporan_produksi_detail_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_produksi_detail_pdf()
	{
		$data['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
		$data['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		if ($this->input->get('mulai_start') != "" && $this->input->get('mulai_end') != "") {
			$_GET['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
			$_GET['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		}
		$data['produksi_kode'] = ($this->input->get('produksi_kode') != "") ? $this->input->get('produksi_kode') : '';
		$data['status_produksi'] = ($this->input->get('status_produksi') != "") ? $this->input->get('status_produksi') : 'Semua Status';
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$data['bahan'] = ($this->input->get('bahan_id') != "") ? $this->bahan->row_by_id($this->input->get('bahan_id'))->bahan_nama : 'Semua Bahan';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produksi->laporan_produksi_detail_filter($query);
		$list = $this->produksi->laporan_produksi_detail_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_produksi_detail_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Produksi Detail ' . $date . ".pdf", "D");
	}

	function laporan_produksi_detail_excel()
	{
		$data['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
		$data['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		if ($this->input->get('mulai_start') != "" && $this->input->get('mulai_end') != "") {
			$_GET['mulai_start'] = ($this->input->get('mulai_start') != "") ? $this->input->get('mulai_start') : date("Y-m-d", 0);
			$_GET['mulai_end'] = ($this->input->get('mulai_end') != "") ? $this->input->get('mulai_end') : date("Y-m-d");
		}
		$data['produksi_kode'] = ($this->input->get('produksi_kode') != "") ? $this->input->get('produksi_kode') : '';
		$data['status_produksi'] = ($this->input->get('status_produksi') != "") ? $this->input->get('status_produksi') : 'Semua Status';
		$data['produk'] = ($this->input->get('produk_id') != "") ? $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama : 'Semua Produk';
		$data['bahan'] = ($this->input->get('bahan_id') != "") ? $this->bahan->row_by_id($this->input->get('bahan_id'))->bahan_nama : 'Semua Bahan';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->produksi->laporan_produksi_detail_filter($query);
		$list = $this->produksi->laporan_produksi_detail_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->tanggal_mulai != null) {
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y', $time);
			}
			if ($key->tanggal_penerimaan != null) {
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->jumlah = number_format($key->jumlah);
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Produksi Detail')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Kode Produksi')
			->setCellValue('C9', 'Tanggal Produksi')
			->setCellValue('D9', 'Status Produksi')
			->setCellValue('E9', 'Produk')
			->setCellValue('F9', 'Bahan')
			->setCellValue('G9', 'Jumlah');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:G9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$qty = 0;
		$i = 10;
		foreach ($list as $key) {
			$qty += str_replace(",", "", $key->jumlah);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->produksi_kode)
				->setCellValue('C' . $i, $key->tanggal_mulai)
				->setCellValue('D' . $i, $key->status_produksi)
				->setCellValue('E' . $i, $key->produk_nama)
				->setCellValue('F' . $i, $key->bahan_nama)
				->setCellValue('G' . $i, $key->jumlah);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:G" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:G9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('G10:G' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $i, number_format($qty));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':G' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G1', 'Laporan Produksi Detail');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G2', 'Tanggal Produksi:' . $data['mulai_start'] . " s/d " . $data['mulai_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G3', 'Kode Produksi : ' . $data['produksi_kode']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G4', 'Produk : ' . $data['produk']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G5', 'Bahan : ' . $data['bahan']);
		$spreadsheet->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("G1:G7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Produksi Detail');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Produksi Detail ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_hutang()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_hutang_list();
				break;
			case 'pdf':
				$this->laporan_hutang_pdf();
				break;
			case 'excel':
				$this->laporan_hutang_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 5, 6, 7);
				$sumColumn = array(5, 6, 7);
				$column = array();
				$data['page'] = "laporan-hutang";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "suplier_nama"));
				array_push($column, array("data" => "po_bahan_no"));
				array_push($column, array("data" => "status_pembayaran"));
				array_push($column, array("data" => "tenggat_pelunasan"));
				array_push($column, array("data" => "grand_total"));
				array_push($column, array("data" => "terbayar"));
				array_push($column, array("data" => "sisa"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['suplier'] = $this->suplier->all_list();
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_hutang');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_hutang_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['suplier_id'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][4]["search"]["value"]);
			$_GET['tenggat_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tenggat_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['status_pembayaran'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$_GET['po_bahan_no'] = $_GET["columns"][2]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang->hutang_count_all();
		$result['iTotalDisplayRecords'] = $this->hutang->hutang_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->hutang->hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'hutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->hutang_id));
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_hutang_pdf()
	{
		$data['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
		$data['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
			$_GET['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		}
		$data['po_bahan_no'] = ($this->input->get('po_bahan_no') != "") ? $this->input->get('po_bahan_no') : '';
		$data['status_pembayaran'] = ($this->input->get('status_pembayaran') != "") ? $this->input->get('status_pembayaran') : 'Semua Status';
		$data['suplier'] = ($this->input->get('suplier_id') != "") ? $this->suplier->row_by_id($this->input->get('suplier_id'))->suplier_nama : 'Semua Suplier';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang->hutang_count_filter($query);
		$list = $this->hutang->hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'hutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->hutang_id));
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_hutang_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Hutang ' . $date . ".pdf", "D");
	}

	function laporan_hutang_excel()
	{
		$data['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
		$data['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
			$_GET['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		}
		$data['po_bahan_no'] = ($this->input->get('po_bahan_no') != "") ? $this->input->get('po_bahan_no') : '';
		$data['status_pembayaran'] = ($this->input->get('status_pembayaran') != "") ? $this->input->get('status_pembayaran') : 'Semua Status';
		$data['suplier'] = ($this->input->get('suplier_id') != "") ? $this->suplier->row_by_id($this->input->get('suplier_id'))->suplier_nama : 'Semua Suplier';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang->hutang_count_filter($query);
		$list = $this->hutang->hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'hutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->hutang_id));
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Hutang')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Suplier')
			->setCellValue('C9', 'No Order')
			->setCellValue('D9', 'Status Pembayaran')
			->setCellValue('E9', 'Tenggat Pelunasan')
			->setCellValue('F9', 'Grand Total')
			->setCellValue('G9', 'Terbayar')
			->setCellValue('H9', 'Sisa');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:H9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$terbayar = 0;
		$sisa = 0;
		$i = 10;
		foreach ($list as $key) {
			$gt += str_replace(",", "", $key->grand_total);
			$terbayar += str_replace(",", "", $key->terbayar);
			$sisa += str_replace(",", "", $key->sisa);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->suplier_nama)
				->setCellValue('C' . $i, $key->po_bahan_no)
				->setCellValue('D' . $i, $key->status_pembayaran)
				->setCellValue('E' . $i, $key->tenggat_pelunasan)
				->setCellValue('F' . $i, $key->grand_total)
				->setCellValue('G' . $i, $key->terbayar)
				->setCellValue('H' . $i, $key->sisa);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:H" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:H9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('H10:H' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $i, number_format($gt));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $i, number_format($terbayar));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . $i, number_format($sisa));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':E' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Hutang');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tenggat Pelunasan:' . $data['tenggat_start'] . " s/d " . $data['tenggat_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'No Order : ' . $data['po_bahan_no']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H4', 'Status Pembayaran : ' . $data['status_pembayaran']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H5', 'Suplier : ' . $data['suplier']);
		$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("H1:H7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Hutang');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Hutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_pembayaran_hutang()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_pembayaran_hutang_list();
				break;
			case 'pdf':
				$this->laporan_pembayaran_hutang_pdf();
				break;
			case 'excel':
				$this->laporan_pembayaran_hutang_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 4);
				$sumColumn = array(4);
				$column = array();
				$data['page'] = "laporan-pembayaran-hutang";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "po_bahan_no"));
				array_push($column, array("data" => "tanggal"));
				array_push($column, array("data" => "tipe_pembayaran_nama"));
				array_push($column, array("data" => "jumlah"));
				array_push($column, array("data" => "keterangan"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_pembayaran_hutang');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_pembayaran_hutang_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['po_bahan_no'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][2]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['tipe_pembayaran_id'] = $_GET["columns"][3]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang->laporan_pembayaran_hutang_all();
		$result['iTotalDisplayRecords'] = $this->hutang->laporan_pembayaran_hutang_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->hutang->laporan_pembayaran_hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_pembayaran_hutang_pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['po_bahan_no'] = ($this->input->get('po_bahan_no') != "") ? $this->input->get('po_bahan_no') : '';
		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang->laporan_pembayaran_hutang_filter($query);
		$list = $this->hutang->laporan_pembayaran_hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_pembayaran_hutang_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Pembayaran Hutang ' . $date . ".pdf", "D");
	}

	function laporan_pembayaran_hutang_excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['po_bahan_no'] = ($this->input->get('po_bahan_no') != "") ? $this->input->get('po_bahan_no') : '';
		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang->laporan_pembayaran_hutang_filter($query);
		$list = $this->hutang->laporan_pembayaran_hutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->hutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Pembayaran Hutang')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'po_bahan_no')
			->setCellValue('C9', 'Tanggal Pembayaran')
			->setCellValue('D9', 'Tipe Pembayaran')
			->setCellValue('E9', 'Jumlah')
			->setCellValue('F9', 'Keterangan');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:F9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$i = 10;
		foreach ($list as $key) {
			$gt += str_replace(",", "", $key->jumlah);;
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->po_bahan_no)
				->setCellValue('C' . $i, $key->tanggal)
				->setCellValue('D' . $i, $key->tipe_pembayaran_nama)
				->setCellValue('E' . $i, $key->jumlah)
				->setCellValue('F' . $i, $key->keterangan);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:F" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:F9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('F10:F' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . $i, number_format($gt));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':D' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F1', 'Laporan Pembayaran Hutang');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F2', 'Tanggal Pembayaran:' . $data['tanggal_start'] . " s/d " . $data['tanggal_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F3', 'No Order : ' . $data['po_bahan_no']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F4', 'Tipe Pembayaran : ' . $data['tipe_pembayaran']);
		$spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("F1:F7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Pembayaran Hutang');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Pembayaran Hutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_piutang()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_piutang_list();
				break;
			case 'pdf':
				$this->laporan_piutang_pdf();
				break;
			case 'excel':
				$this->laporan_piutang_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 5, 6, 7);
				$sumColumn = array(5, 6, 7);
				$column = array();
				$data['page'] = "laporan-piutang";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "pelanggan_nama"));
				array_push($column, array("data" => "no_invoice"));
				array_push($column, array("data" => "status_pembayaran"));
				array_push($column, array("data" => "tenggat_pelunasan"));
				array_push($column, array("data" => "grand_total"));
				array_push($column, array("data" => "terbayar"));
				array_push($column, array("data" => "sisa"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_piutang');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_piutang_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['pelanggan_nama'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][4]["search"]["value"]);
			$_GET['tenggat_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tenggat_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['status_pembayaran'] = $_GET["columns"][3]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$_GET['no_faktur'] = $_GET["columns"][2]["search"]["value"];
		}
		$lokasi_id = null;
		if (isset($_SESSION['login']['lokasi_id'])) {
			$lokasi_id = $_SESSION['login']['lokasi_id'];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->piutang->piutang_count_all($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->piutang->piutang_count_filter($query, $lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->piutang->piutang_list($start, $length, $query, $lokasi_id);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'piutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->piutang_id));
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_piutang_pdf()
	{
		$data['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
		$data['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
			$_GET['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		}
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['status_pembayaran'] = ($this->input->get('status_pembayaran') != "") ? $this->input->get('status_pembayaran') : 'Semua Status';
		$data['nama_pelanggan'] = ($this->input->get('nama_pelanggan') != "") ? $this->input->get('status_pembayaran') : '';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->piutang->piutang_count_filter($query);
		$list = $this->piutang->piutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'piutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->piutang_id));
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_piutang_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Piutang ' . $date . ".pdf", "D");
	}

	function laporan_piutang_excel()
	{
		$data['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
		$data['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tenggat_start'] = ($this->input->get('tenggat_start') != "") ? $this->input->get('tenggat_start') : date("Y-m-d", 0);
			$_GET['tenggat_end'] = ($this->input->get('tenggat_end') != "") ? $this->input->get('tenggat_end') : date("Y-m-d");
		}
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['status_pembayaran'] = ($this->input->get('status_pembayaran') != "") ? $this->input->get('status_pembayaran') : 'Semua Status';
		$data['nama_pelanggan'] = ($this->input->get('nama_pelanggan') != "") ? $this->input->get('nama_pelanggan') : '';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->piutang->piutang_count_filter($query);
		$list = $this->piutang->piutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->grand_total = number_format($key->grand_total);
			$key->terbayar = number_format($key->terbayar);
			$key->sisa = number_format($key->sisa);
			$key->pay_url = base_url() . 'piutang/pay/' . str_replace(array("+", "/"), array("-", "_"), $this->encryption->encrypt($key->piutang_id));
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Piutang')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Pelanggan')
			->setCellValue('C9', 'No Faktur')
			->setCellValue('D9', 'Status Pembayaran')
			->setCellValue('E9', 'Tenggat Pelunasan')
			->setCellValue('F9', 'Grand Total')
			->setCellValue('G9', 'Terbayar')
			->setCellValue('H9', 'Sisa');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:H9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$terbayar = 0;
		$sisa = 0;
		$i = 10;
		foreach ($list as $key) {
			$gt += str_replace(",", "", $key->grand_total);
			$terbayar += str_replace(",", "", $key->terbayar);
			$sisa += str_replace(",", "", $key->sisa);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->nama_pelanggan)
				->setCellValue('C' . $i, $key->no_faktur)
				->setCellValue('D' . $i, $key->status_pembayaran)
				->setCellValue('E' . $i, $key->tenggat_pelunasan)
				->setCellValue('F' . $i, $key->grand_total)
				->setCellValue('G' . $i, $key->terbayar)
				->setCellValue('H' . $i, $key->sisa);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:H" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:H9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('H10:H' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $i, number_format($gt));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $i, number_format($terbayar));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . $i, number_format($sisa));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':E' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Piutang');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tenggat Pelunasan:' . $data['tenggat_start'] . " s/d " . $data['tenggat_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'No Faktur : ' . $data['no_faktur']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H4', 'Status Pembayaran : ' . $data['status_pembayaran']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H5', 'Pelanggan : ' . $data['nama_pelanggan']);
		$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("H1:H7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Piutang');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Piutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_pembayaran_piutang()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_pembayaran_piutang_list();
				break;
			case 'pdf':
				$this->laporan_pembayaran_piutang_pdf();
				break;
			case 'excel':
				$this->laporan_pembayaran_piutang_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 4);
				$sumColumn = array(4);
				$column = array();
				$data['page'] = "laporan-pembayaran-piutang";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "no_faktur"));
				array_push($column, array("data" => "tanggal"));
				array_push($column, array("data" => "tipe_pembayaran_nama"));
				array_push($column, array("data" => "jumlah"));
				array_push($column, array("data" => "keterangan"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_pembayaran_piutang');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_pembayaran_piutang_list()
	{
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$_GET['no_faktur'] = $_GET["columns"][1]["search"]["value"];
		}
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][2]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$_GET['tipe_pembayaran_id'] = $_GET["columns"][3]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->piutang->laporan_pembayaran_piutang_all();
		$result['iTotalDisplayRecords'] = $this->piutang->laporan_pembayaran_piutang_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->piutang->laporan_pembayaran_piutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_pembayaran_piutang_pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->piutang->laporan_pembayaran_piutang_filter($query);
		$list = $this->piutang->laporan_pembayaran_piutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_pembayaran_piutang_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Pembayaran Piutang ' . $date . ".pdf", "D");
	}

	function laporan_pembayaran_piutang_excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tenggat_start') != "" && $this->input->get('tenggat_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['no_faktur'] = ($this->input->get('no_faktur') != "") ? $this->input->get('no_faktur') : '';
		$data['tipe_pembayaran'] = ($this->input->get('tipe_pembayaran_id') != "") ? $this->tipe_pembayaran->row_by_id($this->input->get('tipe_pembayaran_id'))->tipe_pembayaran_nama : 'Semua Tipe Pembayaran';
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->piutang->laporan_pembayaran_piutang_filter($query);
		$list = $this->piutang->laporan_pembayaran_piutang_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->piutang_id;
			$key->jumlah = number_format($key->jumlah);
		}
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Pembayaran Piutang')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'No Faktur')
			->setCellValue('C9', 'Tanggal Pembayaran')
			->setCellValue('D9', 'Tipe Pembayaran')
			->setCellValue('E9', 'Jumlah')
			->setCellValue('F9', 'Keterangan');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:F9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$i = 10;
		foreach ($list as $key) {
			$gt += str_replace(",", "", $key->jumlah);;
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->no_faktur)
				->setCellValue('C' . $i, $key->tanggal)
				->setCellValue('D' . $i, $key->tipe_pembayaran_nama)
				->setCellValue('E' . $i, $key->jumlah)
				->setCellValue('F' . $i, $key->keterangan);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:F" . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:F9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('F10:F' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . $i, number_format($gt));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':D' . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F1', 'Laporan Pembayaran Piutang');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F2', 'Tanggal Pembayaran:' . $data['tanggal_start'] . " s/d " . $data['tanggal_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F3', 'No Faktur : ' . $data['no_faktur']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('F4', 'Tipe Pembayaran : ' . $data['tipe_pembayaran']);
		$spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("F1:F7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Pembayaran Piutang');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Pembayaran Piutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_staff()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_staff_list();
				break;
			case 'pdf':
				$this->laporan_staff_pdf();
				break;
			case 'excel':
				$this->laporan_staff_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 4);
				$sumColumn = array(4);
				$column = array();
				$data['page'] = "laporan-staff";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "nik"));
				array_push($column, array("data" => "staff_nama"));
				array_push($column, array("data" => "tempat_lahir"));
				array_push($column, array("data" => "tanggal_lahir"));
				array_push($column, array("data" => "staff_alamat"));
				array_push($column, array("data" => "staff_status"));
				array_push($column, array("data" => "staff_kelamin"));
				array_push($column, array("data" => "mulai_bekerja"));
				array_push($column, array("data" => "staff_email"));
				array_push($column, array("data" => "staff_phone_number"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['column'] = json_encode($column);
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_staff');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_staff_list()
	{
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->staff->staff_count_all();
		$result['iTotalDisplayRecords'] = $this->staff->staff_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->staff->staff_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_lahir != null) {
				$time = strtotime($key->tanggal_lahir);
				$key->tanggal_lahir = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'staff/delete/';
			$key->row_id = $key->staff_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_staff_pdf()
	{
		$data['cari'] = (($this->input->get('key') != "") ? $this->input->get('key') : "");
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->staff->staff_count_filter($query);
		$list = $this->staff->staff_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_lahir != null) {
				$time = strtotime($key->tanggal_lahir);
				$key->tanggal_lahir = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'staff/delete/';
			$key->row_id = $key->staff_id;
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_staff_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Staff ' . $date . ".pdf", "D");
	}

	function laporan_staff_excel()
	{
		$data['cari'] = (($this->input->get('key') != "") ? $this->input->get('key') : "");
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->staff->staff_count_filter($query);
		$list = $this->staff->staff_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->tanggal_lahir != null) {
				$time = strtotime($key->tanggal_lahir);
				$key->tanggal_lahir = date('d-m-Y', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'staff/delete/';
			$key->row_id = $key->staff_id;
		}
		$spreadsheet = new Spreadsheet();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Staff')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'NIK')
			->setCellValue('C9', 'Nama')
			->setCellValue('D9', 'Tempat Lahir')
			->setCellValue('E9', 'Tanggal Lahir')
			->setCellValue('F9', 'Alamat')
			->setCellValue('G9', 'Status')
			->setCellValue('H9', 'Jenis Kelamin')
			->setCellValue('I9', 'Mulai Bekerja')
			->setCellValue('J9', 'Email')
			->setCellValue('K9', 'No Telp');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:K9")->applyFromArray($style);


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$i = 10;
		foreach ($list as $key) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->nik)
				->setCellValue('C' . $i, $key->staff_nama)
				->setCellValue('D' . $i, $key->tempat_lahir)
				->setCellValue('E' . $i, $key->tanggal_lahir)
				->setCellValue('F' . $i, $key->staff_alamat)
				->setCellValue('G' . $i, $key->staff_status)
				->setCellValue('H' . $i, $key->staff_kelamin)
				->setCellValue('I' . $i, $key->mulai_bekerja)
				->setCellValue('J' . $i, $key->staff_email)
				->setCellValue('K' . $i, $key->staff_phone_number);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:K" . ($i - 1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:K9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('A10:K' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('K1', 'Laporan Staff');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('K2', 'Pencarian : ' . $data['cari']);
		$spreadsheet->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("K1:K7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Staff');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Staff ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function laporan_customer()
	{
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->laporan_customer_list();
				break;
			case 'pdf':
				$this->laporan_customer_pdf();
				break;
			case 'excel':
				$this->laporan_customer_excel();
				break;
			default:
				$data = $this->g_data;
				$target = array(0, 4);
				$sumColumn = array(4);
				$column = array();
				$data['page'] = "laporan-customer";
				array_push($column, array("data" => "no"));
				array_push($column, array("data" => "guest_nama"));
				array_push($column, array("data" => "guest_alamat"));
				array_push($column, array("data" => "guest_telepon"));
				array_push($column, array("data" => "kewarganegaraan"));
				array_push($column, array("data" => "perusahaan"));
				array_push($column, array("data" => "lokasi_nama"));
				array_push($column, array("data" => "tanggal"));
				$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['column'] = json_encode($column);
				$data['lokasi'] = $this->lokasi->all_list();
				$this->load->view('admin/static/header', $data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/laporan_customer');
				$this->load->view('admin/static/footer');
				break;
		}
	}

	function laporan_customer_list()
	{
		if (isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][7]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		if (isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != "") {
			$_GET['lokasi_id'] = $_GET["columns"][6]["search"]["value"];
		} else if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->guest->guest_count_all();
		$result['iTotalDisplayRecords'] = $this->guest->guest_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->guest->guest_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'guest/delete/';
			$key->row_id = $key->guest_id;
			$key->action = null;
			$time = strtotime($key->tanggal);
			$key->tanggal = date("d-m-Y H:i:s", $time);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function laporan_customer_pdf()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tanggal_start') != "" && $this->input->get('tanggal_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
		if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$data['cari'] = (($this->input->get('key') != "") ? $this->input->get('key') : "");
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->guest->guest_count_filter($query);
		$list = $this->guest->guest_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'guest/delete/';
			$key->row_id = $key->guest_id;
			$key->action = null;
			$time = strtotime($key->tanggal);
			$key->tanggal = date("d-m-Y H:i:s", $time);
		}
		$data['list'] = $list;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$html = $this->load->view('admin/pdf/laporan_customer_pdf', $data, true);
		$mpdf->WriteHTML($html);
		$date = date("Y-m-d");
		if ($this->input->get('start_date') != "") {
			$date = $this->input->get('start_date') . " s.d " . $this->input->get('end_date');
		}

		$mpdf->Output('Laporan Customer ' . $date . ".pdf", "D");
	}

	function laporan_customer_excel()
	{
		$data['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
		$data['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		if ($this->input->get('tanggal_start') != "" && $this->input->get('tanggal_end') != "") {
			$_GET['tanggal_start'] = ($this->input->get('tanggal_start') != "") ? $this->input->get('tanggal_start') : date("Y-m-d", 0);
			$_GET['tanggal_end'] = ($this->input->get('tanggal_end') != "") ? $this->input->get('tanggal_end') : date("Y-m-d");
		}
		$data['lokasi'] = ($this->input->get('lokasi_id') != "") ? $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama : 'Semua Lokasi';
		$data['lokasi'] = (isset($_SESSION['login']['lokasi_id'])) ? $this->lokasi->row_by_id($_SESSION['login']['lokasi_id'])->lokasi_nama : $data['lokasi'];
		if (isset($_SESSION['login']['lokasi_id'])) {
			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
		}
		$data['cari'] = (($this->input->get('key') != "") ? $this->input->get('key') : "");
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->guest->guest_count_filter($query);
		$list = $this->guest->guest_list($start, $length, $query);
		$i = $start + 1;
		foreach ($list as $key) {
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'guest/delete/';
			$key->row_id = $key->guest_id;
			$key->action = null;
			$time = strtotime($key->tanggal);
			$key->tanggal = date("d-m-Y H:i:s", $time);
		}
		$spreadsheet = new Spreadsheet();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Laporan Customer')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Nama')
			->setCellValue('C9', 'Alamat')
			->setCellValue('D9', 'Telepon')
			->setCellValue('E9', 'kewarganegaraan')
			->setCellValue('F9', 'Perusahaan')
			->setCellValue('G9', 'Lokasi')
			->setCellValue('H9', 'Tanggal');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:H9")->applyFromArray($style);


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$i = 10;
		foreach ($list as $key) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $key->no)
				->setCellValue('B' . $i, $key->guest_nama)
				->setCellValue('C' . $i, $key->guest_alamat)
				->setCellValue('D' . $i, $key->guest_telepon)
				->setCellValue('E' . $i, $key->kewarganegaraan)
				->setCellValue('F' . $i, $key->perusahaan)
				->setCellValue('G' . $i, $key->lokasi_nama)
				->setCellValue('H' . $i, $key->tanggal);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:H" . ($i - 1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A9:H9')->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle('A10:H' . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1', 'CV. Waisnawa Trans Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2', 'Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3', '+6285858424039');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1', 'Laporan Customer');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2', 'Tanggal Kunjungan : ' . $data['tanggal_start'] . " s/d " . $data['tanggal_end']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H3', 'Lokasi : ' . $data['lokasi']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H4', 'Pencarian : ' . $data['cari']);
		$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("H1:H7")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Customer');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Customer ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

}

/* End of file LaporanController.php */
/* Location: ./application/controllers/LaporanController.php */
