<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarcodeController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest','',true);
		$this->load->model('lokasi','',true);

		$this->load->library('ciqrcode');

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/barcode.js");

		$angka = array();
		for ($i = 1; $i < 6; $i++){
			$angka[$i] = rand();
		}
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Barcode < Master Data < Waisnawa";
		$data['parrent'] = "guest";
		$data['page'] = $this->uri->segment(1);
		$data['action'] = null;
		$data['angka'] = $angka;
//		echo json_encode($angka);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/barcode');
		$this->load->view('admin/static/footer');
	}
	function barcode(){
		$code = $this->uri->segment(2);
		$this->load->library('zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');
		//generate barcode
		Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
	}

	function print_barcode(){
		$number = $this->uri->segment(2);
		$data['number'] = $number;
		$data['produk'] = $this->produk->produk_by_barcode($number);
		$this->load->view('admin/print_barcode',$data);
	}

}
