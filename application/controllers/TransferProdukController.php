<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransferProdukController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('history_transfer_produk','',true);
		$this->load->model('lokasi','',true);
	}

	public function index()
	{
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Transfer Stock Produk < Inventori < System Balioz Linen";
		$data['parrent'] = "inventori";
		$data['page'] = 'transfer-produk';
		$target = array(0,5);
		$sumColumn = array(5);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"spec"));
		array_push($column, array("data"=>"stock"));
		if(isset($_SESSION['login']['lokasi_id'])){
			array_push($column, array("data"=>"jumlah_lokasi"));
			array_push($target, 6);
			array_push($sumColumn, 6);
		}

		$data['sort'] = array(
			'produk_kode' => 'Kode Produk',
			'jenis_produk_nama' => 'Nama Produk',

		);
		$data['sumColumn'] = json_encode($sumColumn);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$data["action"] = json_encode(array("stock"=>true,"view"=>false,"edit"=>false,"delete"=>false));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transfer_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$sort = 'none';
		$order = 'asc';
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$sort = $_GET["columns"][2]["search"]["value"];
		}
		if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$order = $_GET["columns"][3]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list_sort($start,$length,$query,$sort,$order);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url().'transfer-produk/stock/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->stock = number_format($key->stock);
			if(isset($_SESSION['login']['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function stock_index(){
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
	
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Transfer Stok < Inventori < System Balioz Linen";
		$data['parrent'] = "inventori";
		$data['page'] = 'transfer-produk';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$data["lokasi"] = $this->lokasi->all_list();
		$produk = $this->produk->get_data_by_id($id);
		$data['id'] = $id;
		if ($produk != null) {
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			array_push($column, array("data"=>"stock_produk_seri"));
			array_push($column, array("data"=>"stock_produk_qty"));
			$data['sumColumn'] = json_encode(array(3));
					$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,3)));
			$data["action"] = json_encode(array("transfer"=>true));
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/transfer_stock_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
	}
	function transfer(){
		$result['success'] = false;
		$result['message'] = "Jumlah produk yang di transfer melebihi stock";
		if($this->input->post('qty') <= $this->string_to_number($this->input->post('stock_produk_qty'))){
			$transfer = $this->stock_produk->transfer_stock();
			if ($transfer){
				$result['sjl'] = base_url()."sjl/produk/".$transfer;
				$result['success'] = true;
				$result['message'] = "Berhasil menyimpan data";
			}else {
				$result['message'] = "Gagal menyimpan data";
			}
		} 	
		echo json_encode($result);
	}

	function print(){
		$id = $this->uri->segment(3);
		$transfer = $this->history_transfer_produk->row_by_id($id);
		$transfer->tanggal_label = $this->date_label($transfer->tanggal);
		$data["transfer"] = $transfer;
		$this->load->view('admin/print_sj_produk', $data);
	}
}

/* End of file TransferProdukController.php */
/* Location: ./application/controllers/TransferProdukController.php */
