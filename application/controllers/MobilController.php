<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MobilController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mobil','',true);
		$this->load->model('jenis_mobil','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Mobil < Inventori < Waisnawa Transport Group";
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$target = array(0);
		$data['jenis_mobil'] = $this->jenis_mobil->all_list();
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"jenis_mobil_merk"));
		array_push($column, array("data"=>"jenis_mobil_nama"));
		array_push($column, array("data"=>"plat_mobil"));
		array_push($column, array("data"=>"nomor_stnk"));
		array_push($column, array("data"=>"warna"));
		$data['column'] = json_encode($column);

		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['inventori']['mobil'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['sort'] = array(
			'jenis_mobil_nama' => 'Jenis Mobil',
			'jenis_mobil_merk' => 'Merek',

		);
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/inventory/mobil');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$sort = 'none';
		$order = 'asc';
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$sort = $_GET["columns"][2]["search"]["value"];
		}
		if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$order = $_GET["columns"][3]["search"]["value"];
		}
		$query = (isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null);
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->mobil->mobil_count_all();
		$result['iTotalDisplayRecords'] = $this->mobil->mobil_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->mobil->mobil_list_sort($start,$length,$query,$sort,$order);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'mobil/delete/';
			$key->row_id = $key->mobil_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Pastikan plat mobil dan nomor stnk ini belum terdaftar";
		$this->form_validation->set_rules('plat_mobil', '', 'required|is_unique[mobil.plat_mobil]');
		if ($this->form_validation->run() == TRUE) {
			$data["plat_mobil"] = $this->input->post('plat_mobil');
			$data["nomor_stnk"] = $this->input->post('nomor_stnk');
			$data["warna"] = $this->input->post('warna');
			$data["jenis_mobil_id"] = $this->input->post('jenis_mobil_id');
			$data["tipe_mobil"] = $this->input->post('tipe_mobil');
			$data["status"] = 'Stand By';
			$insert = $this->mobil->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$mobil_id = $this->input->post('mobil_id');
		$exists = $this->mobil->get_exist_with_id($this->input->post('plat_mobil'));
		if(($exists == TRUE)) {
			$result['success'] = false;
			$result['message'] = "Pastikan plat mobil ini belum terdaftar";
		}else{
			$data = array();
			$data["plat_mobil"] = $this->input->post('plat_mobil');
			$data["nomor_stnk"] = $this->input->post('nomor_stnk');
			$data["warna"] = $this->input->post('warna');
			$data["jenis_mobil_id"] = $this->input->post('jenis_mobil_id');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$mobil_id = $this->input->post('mobil_id');
			$update = $this->mobil->update_by_id('mobil_id',$mobil_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}

		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->mobil->delete_by_id("mobil_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file ProdukController.php */
/* Location: ./application/controllers/ProdukController.php */
