<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LowStockProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('satuan','',true);
		$this->load->model('suplier','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produk < Inventori < System Balioz Linen";
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"barcode"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"produk_minimal_stock"));
		array_push($column, array("data"=>"harga_eceran"));
		array_push($column, array("data"=>"harga_grosir"));
		array_push($column, array("data"=>"stock"));
		array_push($column, array("data"=>"jumlah_lokasi"));
		array_push($column, array("data"=>"lokasi_nama"));		
		array_push($column, array("data"=>"disc_persen"));
		$data['sumColumn'] = json_encode(array(8));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,7)));
		$data["jenis_produk"] = $this->jenis_produk->all_list();
		$data["satuan"] = $this->satuan->all_list();
		$data["suplier"] = $this->suplier->all_list();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->low_produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->low_produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->low_produk_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url().'stock-produk/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->stock = number_format($key->stock);
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
			$key->harga_eceran = number_format($key->harga_eceran);
			$key->harga_grosir = number_format($key->harga_grosir);
			$key->harga_konsinyasi = number_format($key->harga_konsinyasi);
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}

}

/* End of file LowStockProdukController.php */
/* Location: ./application/controllers/LowStockProdukController.php */
