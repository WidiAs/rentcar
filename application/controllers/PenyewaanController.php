<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class PenyewaanController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('lokasi', '', true);
		$this->load->model('penawaran', '', true);
		$this->load->model('penyewaan', '', true);
		$this->load->model('penyewaan_mobil', '', true);
		$this->load->model('penawaran_mobil', '', true);
		$this->load->model('biaya_tambahan', '', true);
		$this->load->model('tipe_pembayaran', '', true);
		$this->load->model('jadwal_mobil', '', true);
		$this->load->model('piutang', '', true);
		$this->load->model('user', '', true);
		$this->load->library('mail');
	}

	function notif_driver($email, $staff_nama, $html){
		$this->mail->mailer_auth('Order Masuk', $email, $staff_nama, $html);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->css, "vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css");
		array_push($this->js, "vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/penyewaan.js");
		array_push($this->js, "script/app.js");


		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penyewaan < Transaksi < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = 'penyewaan';
		$data['lokasi'] = $this->lokasi->all_list();
		unset($_SESSION['pos']);
		$_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
		$data['no_penyewaan'] = $this->penyewaan->getFakturCode();
		$data['urutan'] = $this->penyewaan->getUrutan();
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['transaksi']['penyewaan'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/penyewaan');
		$this->load->view('admin/static/footer');
	}

	public function edit()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->css, "vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css");
		array_push($this->js, "vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/penyewaan.js");

		$penyewaan_id = $this->uri->segment(2);
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penyewaan < Transaksi < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = 'penyewaan';
		$data['lokasi'] = $this->lokasi->all_list();
		unset($_SESSION['pos']);
		$_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
		$penyewaan = $this->penyewaan->penyewaan_by_id($penyewaan_id);
		$penyewaan->tanggal_lbl = $this->date_label($penyewaan->tanggal_order);
		$penyewaan->grand_total_lbl = $this->format_number($penyewaan->grand_total);
		$item = $this->penyewaan_mobil->result_by_field('penyewaan_id', $penyewaan_id);

		foreach ($item as $i) {
			$biaya_array = $this->biaya_tambahan->result_by_field('penyewaan_mobil_id', $i->penyewaan_mobil_id);
			$i->biaya_array = json_encode($biaya_array);
			$i->tanggal_ambil_lbl = $this->datetime_label($i->tanggal_ambil);
			$i->tanggal_selesai_lbl = $this->datetime_label($i->tanggal_selesai);
			$i->harga_lbl = $this->format_number($i->harga);
			$i->biaya_tambahan_lbl = $this->format_number($i->biaya_tambahan);
			$i->sub_total_lbl = $this->format_number($i->sub_total);
		}
		$data['penyewaan'] = $penyewaan;
		$data['item'] = $item;
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/edit_penyewaan');
		$this->load->view('admin/static/footer');
	}

	function save()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data_penyewaan = array();
		$data_penyewaan['pelanggan_id'] = $this->input->post('guest_id') ? $this->input->post('guest_id') : null;
		$data_penyewaan['pelanggan_nama'] = $this->input->post('guest_nama');
		$data_penyewaan['pelanggan_alamat'] = $this->input->post('guest_alamat');
		$data_penyewaan['pelanggan_kontak'] = $this->input->post('guest_telepon');
		$data_penyewaan['tanggal_order'] = $this->date_database_format($this->input->post('tanggal_order'));
		$data_penyewaan['no_invoice'] = $this->input->post('no_invoice');
		$data_penyewaan['penawaran_id'] = $this->input->post('penawaran_id');
		$data_penyewaan['count'] = $this->input->post('count');
		$data_penyewaan['grand_total'] = $this->input->post('grand_total');
		$data_penyewaan['status'] = 'order';
		$data_penyewaan['keterangan'] = $this->input->post('keterangan_tambahan');
		$data_penyewaan['jenis_sewa'] = $this->input->post('jenis_penyewaan');
		$data_penyewaan['user_id'] = $_SESSION['login']['user_id'];

		$this->db->trans_begin();

		$this->penawaran->update_by_id('penawaran_id', $this->input->post('penawaran_id'), array('status' => 'selesai'));
		$this->penyewaan->insert($data_penyewaan);
		$penyewaan_id = $this->penyewaan->last_id();

		$mobil_id_arr = $this->input->post('mobil_id');
		$jenis_mobil_nama_arr = $this->input->post('jenis_mobil_nama');
		$plat_mobil_arr = $this->input->post('plat_mobil');
		$harga_arr = $this->input->post('harga');
		$hari_arr = $this->input->post('hari');
		$driver_id_arr = $this->input->post('driver_id');
		$driver_nama_arr = $this->input->post('driver_nama');
		$jenis_arr = $this->input->post('jenis_sewa');
		$tanggal_ambil_arr = $this->input->post('tanggal_ambil');
		$tanggal_selesai_arr = $this->input->post('tanggal_selesai');
		$biaya_tambahan_arr = $this->input->post('biaya_tambahan');
		$biaya_array_arr = $this->input->post('biaya_array');
		$keterangan_arr = $this->input->post('keterangan');
		$sub_total_arr = $this->input->post('subtotal');

		foreach ($mobil_id_arr as $key => $mobil_id) {
			$tanggal_ambil = $this->datetime_database_format($tanggal_ambil_arr[$key]);
			$tanggal_selesai = $this->datetime_database_format($tanggal_selesai_arr[$key]);
			$tanggal_a = $this->date_database_format($tanggal_ambil);
			$tanggal_a_lbl = $this->date_label($tanggal_ambil);
			$tanggal_s = $this->date_database_format($tanggal_selesai);
			$tanggal_s_lbl = $this->date_label($tanggal_selesai);
			$jam_ambil = $this->format_time($tanggal_ambil);
			$jam_selesai = $this->format_time($tanggal_selesai);
			$tanggal_awal = $tanggal_a;
			$hari = $this->string_to_number_new($hari_arr[$key]);
			$driver_id = $driver_id_arr[$key];
			$data_item = array();
			$data_item['mobil_id'] = $mobil_id;
			$data_item['jenis_mobil_nama'] = $jenis_mobil_nama_arr[$key];
			$data_item['plat_mobil'] = $plat_mobil_arr[$key];
			$data_item['harga'] = $this->string_to_number_new($harga_arr[$key]);
			$data_item['hari'] = $hari;
			$data_item['driver_id'] = $driver_id_arr[$key] ? $driver_id_arr[$key] : null;
			$data_item['driver_nama'] = $driver_nama_arr[$key];
			$data_item['jenis'] = $jenis_arr[$key];
			$data_item['tanggal_ambil'] = $tanggal_ambil;
			$data_item['tanggal_selesai'] = $tanggal_selesai;
			$data_item['sub_total'] = $sub_total_arr[$key];
			$data_item['keterangan'] = $keterangan_arr[$key];
			$data_item['biaya_tambahan'] = $this->string_to_number_new($biaya_tambahan_arr[$key]);
			$data_item['penyewaan_id'] = $penyewaan_id;

			$this->penyewaan_mobil->insert($data_item);

			$penyewaan_mobil_id = $this->penyewaan_mobil->last_id();

			$biaya = json_decode($biaya_array_arr[$key]);

			foreach ($biaya as $item) {
				$data_biaya = array(
					'penyewaan_id' => $penyewaan_id,
					'penyewaan_mobil_id' => $penyewaan_mobil_id,
					'nama_kegiatan' => $item->nama_kegiatan,
					'jumlah' => $item->jumlah
				);

				$this->biaya_tambahan->insert($data_biaya);
			}

			for ($i = 0; $i < $hari; $i++) {
				$data_jadwal = array(
					'penyewaan_id' => $penyewaan_id,
					'penyewaan_mobil_id' => $penyewaan_mobil_id,
					'mobil_id' => $mobil_id,
					'mobil_jenis_nama' => $jenis_mobil_nama_arr[$key],
					'mobil_plat' => $plat_mobil_arr[$key],
					'tanggal' => $tanggal_awal
				);

				if ($tanggal_awal == $tanggal_a) {
					$data_jadwal['jam_ambil'] = $jam_ambil;
					$data_jadwal['status'] = 'ambil';
				} elseif ($tanggal_awal == $tanggal_s) {
					$data_jadwal['jam_kembali'] = $jam_selesai;
					$data_jadwal['status'] = 'selesai';
				}

				$this->jadwal_mobil->insert($data_jadwal);

				$stop_date = new DateTime($tanggal_awal);
				$stop_date->modify('+1 day');
				$tanggal_awal = $stop_date->format('Y-m-d');
			}
			if ($driver_id) {
				$driver = $this->user->user_by_id($driver_id);
				$pesan = '<div class="">
	<div></div>
	<div tabindex="-1"></div>
	<div class="ii gt">
		<div>
			<div class="adM">
			</div>
			<div lang="IN">
				<div class="adM">
				</div>
				<div class="m_-4856453269414697152WordSection1">
					<div class="adM">
					</div>
					<p class="MsoNormal" style="margin-bottom:12.0pt"><span lang="EN-US"><br>
					<br>Anda menerima order All In dari Waisnawa Trans Group.
					<br><br>Detail Order :</span></p>
					<table border="0" cellpadding="0" width="99%" style="width:99.48%">
						<tbody>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Driver</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['driver_nama'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Jenis Mobil</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['jenis_mobil_nama'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Plat Mobil</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['plat_mobil'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Tanggal Mulai</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$tanggal_a_lbl.'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Tanggal Selesai</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$tanggal_s_lbl.'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Waktu Penjemputan</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$jam_ambil.'</p>
							</td>
						</tr>
						</tbody>
					</table>
					<p class="MsoNormal"><span lang="EN-US"><br>
<br>
Email ini dihasilkan secara otomatis oleh sistem dan mohon untuk tidak membalas email ini. Informasi lebih lanjut hubungi Waisnawa Trans Group.<br>
<br>
Salam hangat,<br>
Waisnawa Trans Group </span></p>
				</div>
				<div class="yj6qo"></div>
				<div class="adL">
				</div>
			</div>
			<div class="adL">

			</div>
		</div>
	</div>
	<div id=":1ty" class="ii gt" style="display:none">
		<div id=":1z3" class="a3s aiL "></div>
	</div>
	<div class="hi"></div>
</div>';

				$this->notif_driver($driver->staff_email, $driver->staff_nama, $pesan);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			echo json_encode($result);
		}

		$this->db->trans_commit();


		$result['success'] = true;
		$result['message'] = "Transaksi tersimpan";
//		$result["url"] = base_url()."penyewaan/print/".$penawaran_id;
		echo json_encode($result);
	}

	function update()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$penyewaan_id = $this->input->post('penyewaan_id');
		$penawaran_id = $this->input->post('penawaran_id_old');
		$data_penyewaan = array();
		$data_penyewaan['pelanggan_id'] = $this->input->post('guest_id') ? $this->input->post('guest_id') : null;
		$data_penyewaan['pelanggan_nama'] = $this->input->post('guest_nama');
		$data_penyewaan['pelanggan_alamat'] = $this->input->post('guest_alamat');
		$data_penyewaan['pelanggan_kontak'] = $this->input->post('guest_telepon');
		$data_penyewaan['tanggal_order'] = $this->date_database_format($this->input->post('tanggal_order'));
		$data_penyewaan['penawaran_id'] = $this->input->post('penawaran_id');
		$data_penyewaan['grand_total'] = $this->input->post('grand_total');
		$data_penyewaan['keterangan'] = $this->input->post('keterangan_tambahan');
		$data_penyewaan['jenis_sewa'] = $this->input->post('jenis_penyewaan');
		$data_penyewaan['updated_by'] = $_SESSION['login']['user_id'];

		$this->db->trans_begin();
		if ($penawaran_id) {
			$this->penawaran->update_by_id('penawaran_id', $penawaran_id, array('status' => 'order'));
		}
		$this->penawaran->update_by_id('penawaran_id', $this->input->post('penawaran_id'), array('status' => 'selesai'));

		$update = $this->penyewaan->update_by_id('penyewaan_id', $penyewaan_id, $data_penyewaan);
		if ($update) {
			$this->penyewaan_mobil->delete_by_id('penyewaan_id', $penyewaan_id);
			$this->biaya_tambahan->delete_by_id('penyewaan_id', $penyewaan_id);
			$this->jadwal_mobil->delete_by_id('penyewaan_id', $penyewaan_id);

			$mobil_id_arr = $this->input->post('mobil_id');
			$jenis_mobil_nama_arr = $this->input->post('jenis_mobil_nama');
			$plat_mobil_arr = $this->input->post('plat_mobil');
			$harga_arr = $this->input->post('harga');
			$hari_arr = $this->input->post('hari');
			$driver_id_arr = $this->input->post('driver_id');
			$driver_nama_arr = $this->input->post('driver_nama');
			$jenis_arr = $this->input->post('jenis_sewa');
			$tanggal_ambil_arr = $this->input->post('tanggal_ambil');
			$tanggal_selesai_arr = $this->input->post('tanggal_selesai');
			$biaya_tambahan_arr = $this->input->post('biaya_tambahan');
			$biaya_array_arr = $this->input->post('biaya_array');
			$keterangan_arr = $this->input->post('keterangan');
			$sub_total_arr = $this->input->post('subtotal');

			foreach ($mobil_id_arr as $key => $mobil_id) {
				$tanggal_ambil = $this->datetime_database_format($tanggal_ambil_arr[$key]);
				$tanggal_selesai = $this->datetime_database_format($tanggal_selesai_arr[$key]);
				$tanggal_a = $this->date_database_format($tanggal_ambil);
				$tanggal_a_lbl = $this->date_label($tanggal_ambil);
				$tanggal_s = $this->date_database_format($tanggal_selesai);
				$tanggal_s_lbl = $this->date_label($tanggal_selesai);
				$jam_ambil = $this->format_time($tanggal_ambil);
				$jam_selesai = $this->format_time($tanggal_selesai);
				$tanggal_awal = $tanggal_a;
				$hari = $this->string_to_number_new($hari_arr[$key]);
				$driver_id = $driver_id_arr[$key];
				$data_item = array();
				$data_item['mobil_id'] = $mobil_id;
				$data_item['jenis_mobil_nama'] = $jenis_mobil_nama_arr[$key];
				$data_item['plat_mobil'] = $plat_mobil_arr[$key];
				$data_item['harga'] = $this->string_to_number_new($harga_arr[$key]);
				$data_item['hari'] = $hari;
				$data_item['driver_id'] = $driver_id_arr[$key] ? $driver_id_arr[$key] : null;
				$data_item['driver_nama'] = $driver_nama_arr[$key];
				$data_item['jenis'] = $jenis_arr[$key];
				$data_item['tanggal_ambil'] = $tanggal_ambil;
				$data_item['tanggal_selesai'] = $tanggal_selesai;
				$data_item['sub_total'] = $sub_total_arr[$key];
				$data_item['keterangan'] = $keterangan_arr[$key];
				$data_item['biaya_tambahan'] = $this->string_to_number_new($biaya_tambahan_arr[$key]);
				$data_item['penyewaan_id'] = $penyewaan_id;

				$this->penyewaan_mobil->insert($data_item);

				$penyewaan_mobil_id = $this->penyewaan_mobil->last_id();

				$biaya = json_decode($biaya_array_arr[$key]);

				foreach ($biaya as $item) {
					$data_biaya = array(
						'penyewaan_id' => $penyewaan_id,
						'penyewaan_mobil_id' => $penyewaan_mobil_id,
						'nama_kegiatan' => $item->nama_kegiatan,
						'jumlah' => $item->jumlah
					);

					$this->biaya_tambahan->insert($data_biaya);
				}

				for ($i = 0; $i < $hari; $i++) {
					$data_jadwal = array(
						'penyewaan_id' => $penyewaan_id,
						'penyewaan_mobil_id' => $penyewaan_mobil_id,
						'mobil_id' => $mobil_id,
						'mobil_jenis_nama' => $jenis_mobil_nama_arr[$key],
						'mobil_plat' => $plat_mobil_arr[$key],
						'tanggal' => $tanggal_awal
					);

					if ($tanggal_awal == $tanggal_a) {
						$data_jadwal['jam_ambil'] = $jam_ambil;
						$data_jadwal['status'] = 'ambil';
					} elseif ($tanggal_awal == $tanggal_s) {
						$data_jadwal['jam_kembali'] = $jam_selesai;
						$data_jadwal['status'] = 'selesai';
					}

					$this->jadwal_mobil->insert($data_jadwal);

					$stop_date = new DateTime($tanggal_awal);
					$stop_date->modify('+1 day');
					$tanggal_awal = $stop_date->format('Y-m-d');

				}

				if ($driver_id) {
					$driver = $this->user->user_by_id($driver_id);
					$pesan = '<div class="">
	<div></div>
	<div tabindex="-1"></div>
	<div class="ii gt">
		<div>
			<div class="adM">
			</div>
			<div lang="IN">
				<div class="adM">
				</div>
				<div class="m_-4856453269414697152WordSection1">
					<div class="adM">
					</div>
					<p class="MsoNormal" style="margin-bottom:12.0pt"><span lang="EN-US"><br>
					<br>Anda menerima order All In dari Waisnawa Trans Group.
					<br><br>Detail Order :</span></p>
					<table border="0" cellpadding="0" width="99%" style="width:99.48%">
						<tbody>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Driver</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['driver_nama'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Jenis Mobil</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['jenis_mobil_nama'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Plat Mobil</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$data_item['plat_mobil'].'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Tanggal Mulai</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$tanggal_a_lbl.'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Tanggal Selesai</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$tanggal_s_lbl.'</p>
							</td>
						</tr>
						<tr>
							<td width="30%" style="width:30.0%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">Waktu Penjemputan</p>
							</td>
							<td width="69%" style="width:69.02%;padding:.75pt .75pt .75pt .75pt">
								<p class="MsoNormal">: '.$jam_ambil.'</p>
							</td>
						</tr>
						</tbody>
					</table>
					<p class="MsoNormal"><span lang="EN-US"><br>
<br>
Email ini dihasilkan secara otomatis oleh sistem dan mohon untuk tidak membalas email ini. Informasi lebih lanjut hubungi Waisnawa Trans Group.<br>
<br>
Salam hangat,<br>
Waisnawa Trans Group </span></p>
				</div>
				<div class="yj6qo"></div>
				<div class="adL">
				</div>
			</div>
			<div class="adL">

			</div>
		</div>
	</div>
	<div id=":1ty" class="ii gt" style="display:none">
		<div id=":1z3" class="a3s aiL "></div>
	</div>
	<div class="hi"></div>
</div>';

					$this->notif_driver($driver->staff_email, $driver->staff_nama, $pesan);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			echo json_encode($result);
		}

		$this->db->trans_commit();


		$result['success'] = true;
		$result['message'] = "Transaksi tersimpan";
//		$result["url"] = base_url()."penyewaan/print/".$penawaran_id;
		echo json_encode($result);
	}

	function print()
	{
		$id = $this->uri->segment(3);
		$trans = $this->penawaran->row_by_id($id);
		$detail = $this->penawaran_mobil->result_by_condition(array('penawaran_id' => $id));
		foreach ($detail as $key) {
			if ($key->produk_nama == null) {
				$key->produk_nama = $key->deskripsi;
			}
			if ($key->produk_kode == null) {
				$key->produk_kode = "custom";
			}
			if ($key->satuan_nama == null) {
				$key->satuan_nama = $key->satuan_custom;
			}
		}
		$trans->detail = $detail;
		$data["trans"] = $trans;
		$this->load->view('admin/print_struct', $data);
	}

	public function index_list()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
//		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/penawaran_list.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Transaksi < Daftar Penyewaan < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = "penyewaan";
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "no_invoice"));
		array_push($column, array("data" => "pelanggan_nama"));
		array_push($column, array("data" => "pelanggan_kontak"));
		array_push($column, array("data" => "tanggal_order_lbl"));
		array_push($column, array("data" => "grand_total_lbl"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0, 5)));
		$data['sumColumn'] = json_encode(array());
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['transaksi']['penyewaan'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}

		$action["print"] = true;
		$action["invoice"] = true;
		$data['lokasi'] = $this->lokasi->all_list();
		$data['pelanggan'] = $this->penyewaan->get_pelanggan();
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/penyewaan_list');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
//		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
//			$_GET['lokasi_id'] = $_GET["columns"][3]["search"]["value"];
//		} else if (isset($_SESSION['login']['lokasi_id'])) {
//			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
//		}

		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		if(isset($_GET["columns"][4]["search"]["value"]) && $_GET["columns"][4]["search"]["value"] != ""){
			$_GET['pelanggan_nama'] = $_GET["columns"][4]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penyewaan->penyewaan_count_all();
		$result['iTotalDisplayRecords'] = $this->penyewaan->penyewaan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->penyewaan->penyewaan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$time = strtotime($key->tanggal_order);
			$key->tanggal_order_lbl = date('d-m-Y', $time);
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->grand_total_lbl = number_format($key->grand_total);
			$key->delete_url = base_url() . 'penyewaan/delete/';
			$key->print_url = base_url() . "invoice-penyewaan-pemerintah/print/" . $key->penyewaan_id;
			$key->invoice_url = base_url() . "penyewaan/invoice/" . $key->penyewaan_id;
			$key->edit_url = base_url() . "edit-penyewaan/" . $key->penyewaan_id;
			$key->row_id = $key->penyewaan_id;
			$key->class_detail = 'detail-penyewaan-btn';

			if ($key->status != 'order') {
//				$key->invoice_hide = true;
//				$key->hide_delete = true;
//				$key->hide_edit = true;
			}
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function detail()
	{
		$id = $this->uri->segment(3);
		$data = $this->penyewaan_mobil->result_by_condition(array('penyewaan_id' => $id));
		$result['data'] = $data;
		foreach ($result['data'] as $key) {
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
			$time = strtotime($key->tanggal_ambil);
			$key->tanggal_ambil_lbl = date('d-m-Y', $time);
		}
		echo json_encode($result);
	}

	function delete()
	{
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if ($id != "") {
			$this->biaya_tambahan->delete_by_id("penyewaan_id", $id);
			$this->jadwal_mobil->delete_by_id("penyewaan_id", $id);
			$this->penyewaan_mobil->delete_by_id("penyewaan_id", $id);
			$delete = $this->penyewaan->delete_by_id("penyewaan_id", $id);
			if ($delete) {
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

	function invoice()
	{
		$id = $this->uri->segment(3);
		$data_penyewaan = $this->penyewaan->row_by_id($id);
		$data_mobil = $this->penyewaan_mobil->list_by_penawaran($id);
		foreach ($data_mobil as $item) {
			$biaya_tambahan = $this->biaya_tambahan->result_by_field('penyewaan_mobil_id', $item->penyewaan_mobil_id);

			$jadwal_mobil = $this->jadwal_mobil->result_by_field('penyewaan_mobil_id', $item->penyewaan_mobil_id);

			$item->biaya_tambahan_list = array();
			$item->jadwal_mobil = array();

			if (!empty($biaya_tambahan)) {
				$item->biaya_tambahan_list = $biaya_tambahan;
			}
			if (!empty($jadwal_mobil)) {
				foreach ($jadwal_mobil as $j) {
					$j->tanggal = $this->date_label($j->tanggal);
				}
				$item->jadwal_mobil = $jadwal_mobil;
			}
		}

		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/invoice.js");


		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Invoice < Transaksi < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = 'penyewaan';
		$data['penyewaan'] = $data_penyewaan;
		$data['penyewaan_mobil'] = $data_mobil;
		$data['lokasi'] = $this->lokasi->all_list();
		unset($_SESSION['utility']);
		$_SESSION['utility']['location_id'] = $data['lokasi'][0]->lokasi_id;

		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/penyewaan_invoice');
		$this->load->view('admin/static/footer');
	}

	function save_invoice()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";

		$penyewaan_id = $this->uri->segment(3);
		$grand_total = $this->input->post('grand_total');
		$tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
		$tanggal_invoice = date('Y-m-d');

		$mobil_id_arr = $this->input->post('mobil_id');
		$plat_mobil_arr = $this->input->post('plat_mobil');
		$jenis_mobil_nama_arr = $this->input->post('jenis_mobil_nama');
		$jenis_arr = $this->input->post('jenis_sewa');
		$tanggal_ambil_arr = $this->input->post('tanggal_ambil');
		$harga_arr = $this->input->post('harga');
		$harga_luar_kota_arr = $this->input->post('harga_luar_kota');
		$driver_id_arr = $this->input->post('driver_id');
		$driver_nama_arr = $this->input->post('driver_nama');
		$keterangan_arr = $this->input->post('keterangan');
		$hari_arr = $this->input->post('hari');
		$hari_luar_kota_arr = $this->input->post('hari_luar_kota');
		$total_jadwal_arr = $this->input->post('total_jadwal');
		$total_overtime_arr = $this->input->post('total_overtime');
		$biaya_tambahan_arr = $this->input->post('biaya_tambahan');
		$sub_total_arr = $this->input->post('subtotal');
		$biaya_array_arr = $this->input->post('biaya_array');
		$jadwal_mobil_arr = $this->input->post('jadwal_mobil');
		$jenis_pembayaran = 'kredit';

		$user_id = $_SESSION["login"]["user_id"];

		$this->db->trans_begin();

		$data_penyewaan = array(
			'grand_total' => $grand_total,
			'tipe_pembayaran_id' => $tipe_pembayaran_id,
			'tanggal_invoice' => $tanggal_invoice,
			'status' => 'selesai'
		);

		$update = $this->penyewaan->update_by_id('penyewaan_id', $penyewaan_id, $data_penyewaan);
		if ($update) {
			$this->penyewaan_mobil->delete_by_id('penyewaan_id', $penyewaan_id);
			$this->biaya_tambahan->delete_by_id('penyewaan_id', $penyewaan_id);
			$this->jadwal_mobil->delete_by_id('penyewaan_id', $penyewaan_id);
			foreach ($mobil_id_arr as $index => $mobil_id) {
				$plat_mobil = $plat_mobil_arr[$index];
				$jenis_mobil_nama = $jenis_mobil_nama_arr[$index];
				$jenis = $jenis_arr[$index];
				$tanggal_ambil = $tanggal_ambil_arr[$index];
				$harga = $this->string_to_number_new($harga_arr[$index]);
				$harga_luar_kota = $this->string_to_number_new($harga_luar_kota_arr[$index]);
				$driver_id = $driver_id_arr[$index];
				$driver_nama = $driver_nama_arr[$index];
				$keterangan = $keterangan_arr[$index];
				$hari = $hari_arr[$index];
				$hari_luar_kota = $hari_luar_kota_arr[$index];
				$total_jadwal = $total_jadwal_arr[$index];
				$total_overtime = $total_overtime_arr[$index];
				$biaya_tambahan = $biaya_tambahan_arr[$index];
				$sub_total = $sub_total_arr[$index];
				$biaya_array = json_decode($biaya_array_arr[$index]);
				$jadwal_mobil = json_decode($jadwal_mobil_arr[$index]);

				$data = array(
					'penyewaan_id' => $penyewaan_id,
					'mobil_id' => $mobil_id,
					'jenis_mobil_nama' => $jenis_mobil_nama,
					'plat_mobil' => $plat_mobil,
					'jenis' => $jenis,
					'tanggal_ambil' => $tanggal_ambil,
					'harga' => $harga,
					'harga_luar_kota' => $harga_luar_kota,
					'hari' => $hari,
					'hari_luar_kota' => $hari_luar_kota,
					'total_overtime' => $total_overtime,
					'driver_id' => $driver_id,
					'driver_nama' => $driver_nama,
					'biaya_tambahan' => $biaya_tambahan,
					'sub_total' => $sub_total,
					'keterangan' => $keterangan,
					'user_id' => $user_id,
				);

				$insert = $this->penyewaan_mobil->insert($data);
				if ($insert) {
					$penyewaan_mobil_id = $this->penyewaan_mobil->last_id();
					foreach ($biaya_array as $item) {
						$data_biaya = array(
							'penyewaan_id' => $penyewaan_id,
							'penyewaan_mobil_id' => $penyewaan_mobil_id,
							'nama_kegiatan' => $item->nama_kegiatan,
							'jumlah' => $item->jumlah
						);

						$this->biaya_tambahan->insert($data_biaya);
					}

					foreach ($jadwal_mobil as $jadwal) {
						$data_jadwal = array(
							'penyewaan_id' => $penyewaan_id,
							'penyewaan_mobil_id' => $penyewaan_mobil_id,
							'mobil_id' => $mobil_id,
							'mobil_jenis_nama' => $jenis_mobil_nama,
							'mobil_plat' => $plat_mobil,
							'tanggal' => $this->date_database_format($jadwal->tanggal),
							'area' => $jadwal->area,
							'biaya_overtime' => $jadwal->biaya_overtime,
						);

						$this->jadwal_mobil->insert($data_jadwal);
					}
				}
			}
			$result['success'] = true;
			$result['message'] = "Berhasil Menyimpan Data";
		}
		if ($jenis_pembayaran == "kredit") {
			$tanggal_awal = date('Y-m-d');
			$stop_date = new DateTime($tanggal_awal);
			$stop_date->modify('+7 day');
			$tenggat_pelunasan = $stop_date->format('Y-m-d');

			$data = array();
			$data['penyewaan_id'] = $penyewaan_id;
			$data['tenggat_pelunasan'] = $tenggat_pelunasan;
			$this->piutang->insert($data);
		}
		if ($this->db->trans_status() === FALSE) {
			echo json_encode($result);
		}

		$this->db->trans_commit();

		echo json_encode($result);
	}

	function print_invoice()
	{
		$penyewaan_id = $this->uri->segment(3);
		$penyewaan = $this->penyewaan->row_by_id($penyewaan_id);
		$penyewaan_mobil = $this->penyewaan_mobil->result_by_field('penyewaan_id', $penyewaan_id);
		$tanggal_sewa = $this->jadwal_mobil->range_tanggal_sewa($penyewaan_id);

		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa Transport')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Invoice')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Jenis Mobil');
		$cell = 'B';
		$row = 9;
		foreach ($tanggal_sewa as $t) {
			$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . $row, $t->tanggal);
			$t->cell = $cell;
		}
		$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . $row, 'Sub Total');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		foreach ($tanggal_sewa as $t) {
			$spreadsheet->getActiveSheet()->getColumnDimension($t->cell)->setWidth(22);
		}
		$spreadsheet->getActiveSheet()->getColumnDimension($cell)->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell . "9")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$terbayar = 0;
		$sisa = 0;
		$i = 10;
		$no = 1;
		foreach ($penyewaan_mobil as $key) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $no++)
				->setCellValue('B' . $i, $key->jenis_mobil_nama);
			$jadwal_mobil = $this->jadwal_mobil->result_by_field('penyewaan_mobil_id', $key->penyewaan_mobil_id);
			foreach ($jadwal_mobil as $j) {
				$bayar = $this->string_to_number_new($key->harga) + $this->string_to_number_new($key->biaya_tambahan);
				if ($j->area == 'Luar Kota') {
					$bayar = $this->string_to_number_new($key->harga_luar_kota) + $this->string_to_number_new($key->biaya_tambahan);
				}
				foreach ($tanggal_sewa as $t) {
					if ($t->tanggal == $j->tanggal) {
						$spreadsheet->setActiveSheetIndex(0)
							->setCellValue($t->cell . $i, $bayar);
					}
				}
			}

			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue($cell . $i, $key->sub_total);
			$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell . ($i))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell . "9")->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle($cell . '10:' . $cell . $i)->getAlignment()->setWrapText(true);
		// Rename worksheet
//		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
//		$drawing->setName('logo');
//		$drawing->setDescription('logo');
//		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
//		$drawing->setCoordinates('A1');
//		$drawing->setOffsetX(1);
//		$drawing->setWidth(80);
//		$drawing->setHeight(80);
//		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$let = ord($cell);
		$cell_before = chr($let - 1);
		$pph = $this->string_to_number_new($penyewaan->grand_total) * 10 / 100;
		$total_akhir = $this->string_to_number_new($penyewaan->grand_total) + $pph;

		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before . ++$i, 'Total');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $penyewaan->grand_total);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $penyewaan->grand_total);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before . ++$i, 'Pph 10%');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $pph);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before . ++$i, 'Jumlah');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $total_akhir);
//		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':'.$cell_before . $i);

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'CV. Waisnawa Transport Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A2', 'Jl. Penyaringan Gg. Tiying No. 3 Sanur Kauh - Denpasar Selatan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', '0811 393 1234/081 353 996 698/0811 395 7887/0361 4748201');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . '1', 'Invoice Penyewaan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . '2', 'Tanggal:' . date('d-m-Y'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . '3', 'No Invoice : ' . $penyewaan->no_invoice);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . '4', 'Pelanggan : ' . $penyewaan->pelanggan_nama);
		$spreadsheet->getActiveSheet()->getStyle("A1:" . $cell . "1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle($cell . "1:" . $cell . "10")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Invoice Penyewaan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Piutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	function print_invoice_pemerintah()
	{
		$penyewaan_id = $this->uri->segment(3);
		$penyewaan = $this->penyewaan->row_by_id($penyewaan_id);
		$penyewaan_mobil = $this->penyewaan_mobil->result_by_field('penyewaan_id', $penyewaan_id);
		$tanggal_sewa = $this->jadwal_mobil->range_tanggal_sewa($penyewaan_id);

		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Waisnawa Transport')
			->setLastModifiedBy($_SESSION['login']['user_name'])
			->setTitle('Invoice')
			->setSubject('');
		$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$right = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			)
		);
		$border = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			),
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],

		);
		$center = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			),
		);
		$center_all = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			),
		);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A9', 'No')
			->setCellValue('B9', 'Peruntukan Kendaraan')
			->setCellValue('C9', 'Jenis Kendaraan')
			->setCellValue('D9', 'TNKB')
			->setCellValue('E9', 'Tanggal');
		$cell = 'D';
		$row = 10;
		foreach ($tanggal_sewa as $t) {
			$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . $row, $this->date_label($t->tanggal));
			$t->cell = $cell;
		}
		$cell_before = $cell;
		$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . 9, 'Nama Pengemudi');
		$cell_pengemudi = $cell;
		$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . 9, 'Harga');
		$cell_harga = $cell;
		$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . 9, 'Hari');
		$cell_hari = $cell;
		$spreadsheet->setActiveSheetIndex(0)->setCellValue(++$cell . 9, 'Jumlah');
		$cell_total = $cell;
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		foreach ($tanggal_sewa as $t) {
			$spreadsheet->getActiveSheet()->getColumnDimension($t->cell)->setWidth(22);
		}
		$spreadsheet->getActiveSheet()->getColumnDimension($cell_pengemudi)->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension($cell_harga)->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension($cell_hari)->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension($cell_total)->setWidth(22);
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell_total . "10")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$gt = 0;
		$terbayar = 0;
		$sisa = 0;
		$i = 11;
		$no = 1;
		foreach ($penyewaan_mobil as $key) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $no++)
				->setCellValue('B' . $i, $key->keterangan)
				->setCellValue('C' . $i, $key->jenis_mobil_nama)
				->setCellValue('D' . $i, $key->plat_mobil);
			$jadwal_mobil = $this->jadwal_mobil->result_by_field('penyewaan_mobil_id', $key->penyewaan_mobil_id);
			foreach ($jadwal_mobil as $j) {
				$bayar = $this->string_to_number_new($key->harga) + $this->string_to_number_new($key->biaya_tambahan);
				if ($j->area == 'Luar Kota') {
					$bayar = $this->string_to_number_new($key->harga_luar_kota) + $this->string_to_number_new($key->biaya_tambahan);
				}
				foreach ($tanggal_sewa as $t) {
					if ($t->tanggal == $j->tanggal) {
						$spreadsheet->setActiveSheetIndex(0)
							->setCellValue($t->cell . $i, $bayar);
					}
				}
			}
			$harga = $this->string_to_number_new($key->harga) + $this->string_to_number_new($key->biaya_tambahan);
			$hari = $this->string_to_number_new($key->hari) + $this->string_to_number_new($key->hari_luar_kota);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue($cell_pengemudi . $i, $key->driver_nama);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue($cell_harga . $i, $harga);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue($cell_hari . $i, $hari);
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue($cell_total . $i, $key->sub_total);
			$i++;
		}
		$last = $i - 1;
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell . ($last))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle($cell_hari . $i . ":" . $cell_total . ($i + 4))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle("A9:" . $cell . "10")->getFill()
			->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			->getStartColor()->setARGB('BEBEBE');
		$spreadsheet->getActiveSheet()->getStyle($cell . '10:' . $cell . $last)->getAlignment()->setWrapText(true);
		// Rename worksheet
//		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
//		$drawing->setName('logo');
//		$drawing->setDescription('logo');
//		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
//		$drawing->setCoordinates('A1');
//		$drawing->setOffsetX(1);
//		$drawing->setWidth(80);
//		$drawing->setHeight(80);
//		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$let = ord($cell_total);
		$cell_before_total = chr($let - 1);
		$pph = $this->string_to_number_new($penyewaan->grand_total) * 10 / 100;
		$total_akhir = $this->string_to_number_new($penyewaan->grand_total) + $pph;

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . ++$i, 'Note :  Pembayaran via Transfer Melalui Account Bank a/n Waisnawa Transport Group - 14669671672');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . $i, 'Jumlah : ');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $penyewaan->grand_total);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . ($i + 5), 'Denpasar, ' . date('d-m-Y'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . ($i + 6), 'CV. Waisnawa Transport Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . ($i + 10), 'I Putu Ika Angga Habh Putra');
		$spreadsheet->getActiveSheet()->getStyle("A" . $i)->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle($cell_before_total . $i)->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle($cell . $i)->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("A" . $i)->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle("A" . $i)->applyFromArray($center);
		$spreadsheet->getActiveSheet()->getStyle($cell_before_total . ($i + 5))->applyFromArray($center_all);
		$spreadsheet->getActiveSheet()->getStyle($cell_before_total . ($i + 6))->applyFromArray($center_all)->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle($cell_before_total . ($i + 10))->applyFromArray($center_all)->getFont()->setBold(true);

//		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . ++$i, 'Pph 10%');
//		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $pph);
//		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_before_total . ++$i, 'Jumlah');
//		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell . $i, $total_akhir);
		$spreadsheet->getActiveSheet()->mergeCells('A9:A10');
		$spreadsheet->getActiveSheet()->mergeCells('B9:B10');
		$spreadsheet->getActiveSheet()->mergeCells('C9:C10');
		$spreadsheet->getActiveSheet()->mergeCells('D9:D10');
		$spreadsheet->getActiveSheet()->mergeCells('E9:' . $cell_before . 9);
		$spreadsheet->getActiveSheet()->mergeCells($cell_pengemudi . '9:' . $cell_pengemudi . 10);
		$spreadsheet->getActiveSheet()->mergeCells($cell_harga . '9:' . $cell_harga . 10);
		$spreadsheet->getActiveSheet()->mergeCells($cell_hari . '9:' . $cell_hari . 10);
		$spreadsheet->getActiveSheet()->mergeCells($cell_total . '9:' . $cell_total . 10);
		$spreadsheet->getActiveSheet()->mergeCells($cell_hari . ($last + 1) . ":" . $cell_total . ($last + 1));
		$spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':' . 'D' . ($i + 3));
		$spreadsheet->getActiveSheet()->mergeCells($cell_before_total . $i . ':' . $cell_before_total . ($i + 3));
		$spreadsheet->getActiveSheet()->mergeCells($cell . $i . ':' . $cell . ($i + 3));
		$spreadsheet->getActiveSheet()->mergeCells($cell_before_total . ($i + 5) . ':' . $cell . ($i + 5));
		$spreadsheet->getActiveSheet()->mergeCells($cell_before_total . ($i + 6) . ':' . $cell . ($i + 6));
		$spreadsheet->getActiveSheet()->mergeCells($cell_before_total . ($i + 10) . ':' . $cell . ($i + 10));

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'CV. Waisnawa Transport Group');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A2', 'Jl. Penyaringan Gg. Tiying No. 3 Sanur Kauh - Denpasar Selatan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', '0811 393 1234/081 353 996 698/0811 395 7887/0361 4748201');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_total . '1', 'Invoice Penyewaan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_total . '2', 'Tanggal:' . date('d-m-Y'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_total . '3', 'No Invoice : ' . $penyewaan->no_invoice);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue($cell_total . '4', 'Pelanggan : ' . $penyewaan->pelanggan_nama);
		$spreadsheet->getActiveSheet()->getStyle("A1:" . $cell_total . "1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle($cell_total . "1:" . $cell_total . "4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Invoice Penyewaan');
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Piutang ' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

	}
}
