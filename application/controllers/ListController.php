<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest', '', true);
		$this->load->model('mobil', '', true);
		$this->load->model('lokasi', '', true);
		$this->load->model('user', '', true);
		$this->load->model('penawaran', '', true);
		$this->load->model('penawaran_mobil', '', true);
		$this->load->model('jadwal_mobil', '', true);
		$this->load->model('kegiatan_driver', '', true);
		$this->load->model('pengeluaran_driver', '', true);
	}

	function guest_list()
	{
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->guest->guest_count_all();
		$result['iTotalDisplayRecords'] = $this->guest->guest_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->guest->guest_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->aksi = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function driver_list()
	{
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->user->driver_count_all();
		$result['iTotalDisplayRecords'] = $this->user->driver_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->user->driver_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->aksi = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function utility()
	{
		$key = $this->uri->segment(3);
		if ($key == "lokasi") {
			$_SESSION['utility']['lokasi'] = $this->input->post('lokasi_id');
		}
	}

	function mobil_list()
	{
		$mobil_id = array();
		if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
			$temp = explode("|", $_GET["columns"][1]["search"]["value"]);
			$_GET['tanggal_start'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d", 0));
			$_GET['tanggal_end'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));

			$jadwal = $this->jadwal_mobil->jadwal_range();
			foreach ($jadwal as $key => $item) {
				array_push($mobil_id, $item->mobil_id);
//			$mobil_id[$key] = $item->mobil_id;
			}
		}

		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->mobil->mobil_all_count($mobil_id);
		$result['iTotalDisplayRecords'] = $this->mobil->mobil_all_filter($query, $mobil_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->mobil->mobil_all_list($start, $length, $query, $mobil_id);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->mobil_id;
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function penawaran_list()
	{

		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penawaran->penawaran_count_all_order();
		$result['iTotalDisplayRecords'] = $this->penawaran->penawaran_count_filter_order($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->penawaran->penawaran_list_order($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->penawaran_id;
			$key->grand_total_lbl = number_format($key->grand_total);
			$time = strtotime($key->tanggal_order);
			$key->tanggal_order_lbl = date('d-m-Y', $time);
			$key->item = $this->penawaran_mobil->result_by_condition(array('penawaran_id' => $key->penawaran_id));
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function jadwal_kendaraan_today_list()
	{

		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->jadwal_mobil->jadwal_today_count_all();
		$result['iTotalDisplayRecords'] = $this->jadwal_mobil->jadwal_today_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->jadwal_mobil->jadwal_today_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->jadwal_id;
			$time = strtotime($key->tanggal);
			$key->tanggal_lbl = date('d-m-Y', $time);
			$i++;
			$key->action_btn = '<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" style="width: 100%">
								<textarea style="display:none">' . json_encode($key) . '</textarea>
								<button class="btn btn-success btn-icon btn-icon-sm manage-btn" title="Manage">
								<i class="flaticon2-edit"></i>&nbsp; Manage
								</button>
							</div>';
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function jadwal_kendaraan_besok_list()
	{

		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->jadwal_mobil->jadwal_besok_count_all();
		$result['iTotalDisplayRecords'] = $this->jadwal_mobil->jadwal_besok_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->jadwal_mobil->jadwal_besok_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->jadwal_id;
			$time = strtotime($key->tanggal);
			$key->tanggal_lbl = date('d-m-Y', $time);
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function jadwal_kendaraan_kedepan_list()
	{

		$user_id = $_SESSION['login']['user_id'];
		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->jadwal_mobil->jadwal_kedepan_count_all($user_id);
		$result['iTotalDisplayRecords'] = $this->jadwal_mobil->jadwal_kedepan_count_filter($query, $user_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->jadwal_mobil->jadwal_kedepan_list($start, $length, $query, $user_id);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->jadwal_id;
			$time = strtotime($key->tanggal);
//			$key->tanggal_lbl = date('d-m-Y', $time);
			$key->tanggal = date('d-m-Y', $time) . ' ' . $key->jam_ambil;
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function history_driver_list()
	{

		$user_id = $_SESSION['login']['user_id'];
		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->kegiatan_driver->history_driver_count_all($user_id);
		$result['iTotalDisplayRecords'] = $this->kegiatan_driver->history_driver_count_filter($query, $user_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->kegiatan_driver->history_driver_list($start, $length, $query, $user_id);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->kegiatan_driver_id;
			$time = strtotime($key->tanggal);
			$key->tanggal_lbl = date('d-m-Y', $time);
			$key->bukti = '';
			if ($key->bukti_pembayaran) {
				$key->bukti = '<button class="btn btn-success btn-icon btn-icon-sm btn-foto" title="Foto" data-url="' . base_url() . $key->bukti_pembayaran . '">
								<i class="flaticon-visible"></i>
								</button>';
			}
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function history_pengeluaran_driver()
	{

		$user_id = $_SESSION['login']['user_id'];
		$query = isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null;
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pengeluaran_driver->pengeluaran_driver_count_all($user_id);
		$result['iTotalDisplayRecords'] = $this->pengeluaran_driver->pengeluaran_driver_count_filter($query, $user_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->pengeluaran_driver->pengeluaran_driver_list($start, $length, $query, $user_id);
		$i = $start + 1;
		foreach ($data as $key) {
			$key->no = $i;
			$key->row_id = $key->pengeluaran_driver_id;
			$time = strtotime($key->tanggal);
			$key->tanggal_lbl = date('d-m-Y', $time);
			$key->jam = date('H:i:s', $time);
			$i++;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
