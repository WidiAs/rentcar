<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekananController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('rekanan', '', true);
		$this->load->model('mobil', '', true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Rekanan < Master Data < Waisnawa";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "rekanan_nama"));
		array_push($column, array("data" => "rekanan_kontak"));
		array_push($column, array("data" => "rekanan_keterangan"));

		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['master_data']['rekanan'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/master_data/rekanan');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		$query = (isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null);
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->rekanan->rekanan_count_all();
		$result['iTotalDisplayRecords'] = $this->rekanan->rekanan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->rekanan->rekanan_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'rekanan/delete/';
			$key->row_id = $key->rekanan_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function options()
	{
		$data = array();
		$rekanan = $this->rekanan->all_list();
		foreach ($rekanan as $key) {
			array_push($data, strtoupper($key->rekanan_nama));
		}
		echo json_encode($data);
	}

	function add()
	{
		$result['success'] = false;
		$result['message'] = "Rekanan ini telah terdaftar";
		$this->form_validation->set_rules('rekanan_nama', '', 'required|is_unique[rekanan.rekanan_nama]');
		if ($this->form_validation->run() == TRUE) {
			$rekanan_nama = $this->input->post('rekanan_nama');
			$rekanan_kontak = $this->input->post('rekanan_kontak');
			$rekanan_keterangan = $this->input->post('rekanan_keterangan');
			$data = array(
				"rekanan_keterangan" => $rekanan_keterangan,
				"rekanan_kontak" => $rekanan_kontak,
				"rekanan_nama" => $rekanan_nama
			);
			$insert = $this->rekanan->insert($data);
			if ($insert) {
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}

	function edit()
	{
		$result['success'] = false;
		$result['message'] = "Rekanan sudah terdaftar";
		$data = array();
		$data['rekanan_merk'] = $this->input->post('rekanan_merk');
		$rekanan_id = $this->input->post('rekanan_id');

		if ($this->rekanan->is_ready_nama($rekanan_id, $this->input->post('rekanan_nama'))) {
			$data["rekanan_nama"] = $this->input->post('rekanan_nama');
			$data["rekanan_kontak"] = $this->input->post('rekanan_kontak');
			$data["rekanan_keterangan"] = $this->input->post('rekanan_keterangan');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->rekanan->update_by_id('rekanan_id', $rekanan_id, $data);
			if ($update) {
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}

	function delete()
	{
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$count = $this->mobil->count_by_rekanan($id);
		if ($id != "" && $count == 0) {
			$delete = $this->rekanan->delete_by_id("rekanan_id", $id);
			if ($delete) {
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		} elseif ($count > 0) {
			$result['message'] = "Masih ada data mobil milik rekanan, silahkan hapus data mobil terlebih dahulu";
		}
		echo json_encode($result);
	}
}

