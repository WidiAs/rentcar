<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengeluaranDriverController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kegiatan_driver', '', true);
		$this->load->model('pengeluaran_driver', '', true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Mobil < Inventori < Waisnawa Transport Group";
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$target = array(0);
		$data['pengeluaran_driver'] = $this->pengeluaran_driver->all_list();
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "pengeluaran_driver_merk"));
		array_push($column, array("data" => "pengeluaran_driver_nama"));
		array_push($column, array("data" => "plat_pengeluaran_driver"));
		array_push($column, array("data" => "nomor_stnk"));
		array_push($column, array("data" => "warna"));
		$data['column'] = json_encode($column);

		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => $target));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['inventori']['pengeluaran_driver'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['sort'] = array(
			'pengeluaran_driver_nama' => 'Jenis Mobil',
			'pengeluaran_driver_merk' => 'Merek',

		);
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/inventory/pengeluaran_driver');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		$sort = 'none';
		$order = 'asc';
		if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
			$sort = $_GET["columns"][2]["search"]["value"];
		}
		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
			$order = $_GET["columns"][3]["search"]["value"];
		}
		$query = (isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null);
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pengeluaran_driver->pengeluaran_driver_count_all();
		$result['iTotalDisplayRecords'] = $this->pengeluaran_driver->pengeluaran_driver_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->pengeluaran_driver->pengeluaran_driver_list_sort($start, $length, $query, $sort, $order);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'pengeluaran_driver/delete/';
			$key->row_id = $key->pengeluaran_driver_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function add()
	{
		$result['success'] = false;
		$data["driver_id"] = $_SESSION['login']['user_id'];
		$data["tanggal"] = date('Y-m-d H:i:s');
		$data["nama_pengeluaran"] = $this->input->post('nama_pengeluaran');
		$data["jumlah"] = $this->string_to_number_new($this->input->post('jumlah'));
		$insert = $this->pengeluaran_driver->insert($data);
		if ($insert) {
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}

	function edit(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/pengeluaran_driver.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Edit Pengeluaran Driver < Inventori < Waisnawa Transport Group";
		$data['parrent'] = "laporan";
		$data['page'] = $this->uri->segment(1);
		$kegiatan_id = $this->uri->segment(3);
		$data['kegiatan_driver'] = $this->kegiatan_driver->row_by_id($kegiatan_id);
		$data['pengeluaran_driver'] = $this->pengeluaran_driver->get_pengeluaran($data['kegiatan_driver']->driver_id, $this->date_database_format($data['kegiatan_driver']->tanggal));

		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/pengeluaran_driver');
		$this->load->view('admin/static/footer');
	}

	function update()
	{
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$driver_id = $this->input->post('driver_id');
		$tanggal = $this->input->post('tanggal');

		$nama_pengeluaran_arr = $this->input->post('nama_pengeluaran');
		$jumlah_arr = $this->input->post('jumlah');
		$jam_arr = $this->input->post('jam');
		$delete = $this->pengeluaran_driver->delete_by_tanggal($driver_id, $tanggal);
		if ($delete){
			foreach ($nama_pengeluaran_arr as $key => $nama_pengeluaran){
				$jumlah = $this->string_to_number_new($jumlah_arr[$key]);
				$jam = $jam_arr[$key];

				$tanggal_save = $tanggal.' '.$jam;
				$tanggl_new = $this->datetime_database_format($tanggal_save);

				$data_pengeluaran = array(
					'driver_id' => $driver_id,
					'nama_pengeluaran' => $nama_pengeluaran,
					'tanggal' => $tanggl_new,
					'jumlah' => $jumlah
				);

				$this->pengeluaran_driver->insert($data_pengeluaran);
			}
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		}


		echo json_encode($result);
	}

	function delete()
	{
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if ($id != "") {
			$delete = $this->pengeluaran_driver->delete_by_id("pengeluaran_driver_id", $id);
			if ($delete) {
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file ProdukController.php */
/* Location: ./application/controllers/ProdukController.php */
