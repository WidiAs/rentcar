<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require 'vendor/autoload.php';


class PenawaranController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('lokasi','',true);
		$this->load->model('penawaran','',true);
		$this->load->model('catatan_penawaran','',true);
		$this->load->model('penawaran_mobil','',true);
		$this->load->model('tipe_pembayaran','',true);
		$this->load->model('user','',true);
	}
	public function index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/penawaran.js");
		array_push($this->js, "script/app.js");


		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penawaran < Transaksi < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = 'penawaran';
		$data['lokasi'] = $this->lokasi->all_list();
		unset($_SESSION['pos']);
		$_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
		$data['no_penawaran'] = $this->penawaran->getFakturCode();
		$data['urutan'] = $this->penawaran->getUrutan();
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['transaksi']['penawaran'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/penawaran');
		$this->load->view('admin/static/footer');
	}

	function save(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data_penawaran = array();
		$data_penawaran['pelanggan_id'] = $this->input->post('guest_id') ? $this->input->post('guest_id') : null;
		$data_penawaran['pelanggan_nama'] = $this->input->post('guest_nama');
		$data_penawaran['pelanggan_alamat'] = $this->input->post('guest_alamat');
		$data_penawaran['pelanggan_kontak'] = $this->input->post('guest_telepon');
		$data_penawaran['tanggal_order'] = $this->date_database_format($this->input->post('tanggal_order'));
		$data_penawaran['no_penawaran'] = $this->input->post('no_penawaran');
		$data_penawaran['count'] = $this->input->post('urutan');
		$data_penawaran['grand_total'] = $this->input->post('grand_total');
		$data_penawaran['keterangan_tambahan'] = $this->input->post('keterangan_tambahan');
		$data_penawaran['status'] = 'order';
		$data_penawaran['user_id'] = $_SESSION["login"]["user_id"];

		$this->penawaran->insert($data_penawaran);
		$penawaran_id = $this->penawaran->last_id();

		$mobil_id_arr = $this->input->post('mobil_id');
		$jenis_mobil_nama_arr = $this->input->post('jenis_mobil_nama');
		$plat_mobil_arr = $this->input->post('plat_mobil');
		$harga_arr = $this->input->post('harga');
//		$hari_arr = $this->input->post('hari');
//		$driver_id_arr = $this->input->post('driver_id');
		$driver_harga_arr = $this->input->post('driver_harga');
		$bbm_arr = $this->input->post('bbm');
		$jenis_arr = $this->input->post('jenis_sewa');
//		$tanggal_ambil_arr = $this->input->post('tanggal_ambil');
		$sub_total_arr = $this->input->post('subtotal');
		$keterangan_arr = $this->input->post('keterangan');
		$catatan_arr = $this->input->post('catatan');

		foreach ($mobil_id_arr as $key => $mobil_id){
			$data_item = array();
			$data_item['mobil_id'] = $mobil_id;
			$data_item['jenis_mobil_nama'] = $jenis_mobil_nama_arr[$key];
			$data_item['plat_mobil'] = $plat_mobil_arr[$key];
			$data_item['harga'] = $this->string_to_number_new($harga_arr[$key]);
//			$data_item['hari'] = $this->string_to_number_new($hari_arr[$key]);
//			$data_item['driver_id'] = $driver_id_arr[$key] ? $driver_id_arr[$key] : null;
			$data_item['driver_harga'] = $this->string_to_number_new($driver_harga_arr[$key]);
			$data_item['bbm'] = $this->string_to_number_new($bbm_arr[$key]);
			$data_item['jenis'] = $jenis_arr[$key];
//			$data_item['tanggal_ambil'] = $this->date_database_format($tanggal_ambil_arr[$key]);
			$data_item['sub_total'] = $sub_total_arr[$key];
			$data_item['keterangan'] = $keterangan_arr[$key];
			$data_item['penawaran_id'] = $penawaran_id;

			$this->penawaran_mobil->insert($data_item);
		}
		if (!empty($catatan_arr)){
			foreach ($catatan_arr as $catatan){
				$this->catatan_penawaran->insert(array('penawaran_id' => $penawaran_id, 'catatan_text' => $catatan));
			}
		}

		$result['success'] = true;
		$result['message'] = "Transaksi tersimpan";
		$result["url"] = base_url()."penawaran/print/".$penawaran_id;
		echo json_encode($result);
	}
	function print(){
		$id = $this->uri->segment(3);
		$trans = $this->penawaran->row_by_id($id);
		$user_id = $trans->user_id ? $trans->user_id : $_SESSION["login"]["user_id"];
		$data['user'] = $this->user->user_by_id($user_id);
		$detail = $this->penawaran_mobil->result_by_condition(array('penawaran_id' => $id));
		$trans->catatan = $this->catatan_penawaran->result_by_condition(array('penawaran_id' => $id));
		foreach ($detail as $item) {
			$item->harga_lbl = $this->idr_currency($item->harga);
			$item->bbm_lbl = $this->idr_currency($item->bbm);
			$item->driver_harga_lbl = $this->idr_currency($item->driver_harga);
		}
		$trans->detail = $detail;
		$data["trans"] = $trans;

//		echo json_encode($data);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'P']);
		$html = $this->load->view('admin/transaksi/print_penawaran', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output('Penawaran nomor ' . $trans->no_penawaran, "I");
//		$this->load->view('admin/transaksi/print_penawaran', $data);
	}

	public function index_list()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");
//		array_push($this->js, "script/admin/list.js");
		array_push($this->js, "script/admin/penawaran_list.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Transaksi < Daftar Penawaran < Waisnawa";
		$data['parrent'] = "transaksi";
		$data['page'] = "penawaran";
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "no_penawaran"));
		array_push($column, array("data" => "pelanggan_nama"));
		array_push($column, array("data" => "pelanggan_kontak"));
		array_push($column, array("data" => "tanggal_order_lbl"));
		array_push($column, array("data" => "grand_total_lbl"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0, 5)));
		$data['sumColumn'] = json_encode(array());
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['transaksi']['penawaran'] as $key => $value) {
			if ($key != "akses_menu") {
				$action[$key] = $value;
			}
		}

		$action["print"] = true;
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/transaksi/penawaran_list');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
//		if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
//			$_GET['lokasi_id'] = $_GET["columns"][3]["search"]["value"];
//		} else if (isset($_SESSION['login']['lokasi_id'])) {
//			$_GET['lokasi_id'] = $_SESSION['login']['lokasi_id'];
//		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penawaran->penawaran_count_all();
		$result['iTotalDisplayRecords'] = $this->penawaran->penawaran_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->penawaran->penawaran_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$time = strtotime($key->tanggal_order);
			$key->tanggal_order_lbl = date('d-m-Y', $time);
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->grand_total_lbl = number_format($key->grand_total);
			$key->delete_url = base_url() . 'penawaran/delete/';
			$key->print_url = base_url() . "penawaran/print/" . $key->penawaran_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function detail()
	{
		$id = $this->uri->segment(3);
		$data = $this->penawaran_mobil->result_by_condition(array('penawaran_id' => $id));
		$result['data'] = $data;
		foreach ($result['data'] as $key) {
			$key->harga = number_format($key->harga);
			$key->sub_total = number_format($key->sub_total);
			$time = strtotime($key->tanggal_ambil);
			$key->tanggal_ambil_lbl = date('d-m-Y', $time);
		}
		echo json_encode($result);
	}
}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
