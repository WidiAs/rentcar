<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuplierController extends MY_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->model('suplier','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Satuan < Master Data < System Balioz Linen";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"suplier_kode"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"suplier_alamat"));
		array_push($column, array("data"=>"suplier_telepon"));
		array_push($column, array("data"=>"suplier_email"));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$data["action"] = json_encode(array("view"=>true,"edit"=>true,"delete"=>true));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['suplier'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/suplier');
		$this->load->view('admin/static/footer');
	}

	function options(){
		$data = array();
		$suplier = $this->suplier->all_list();
		foreach ($suplier as $key) {
			array_push($data, strtoupper($key->suplier_nama));
		}
		echo json_encode($data);
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->suplier->suplier_count_all();
		$result['iTotalDisplayRecords'] = $this->suplier->suplier_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->suplier->suplier_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'suplier/delete/';
			$key->row_id = $key->suplier_id;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
		$this->form_validation->set_rules('suplier_kode', '', 'required|is_unique[suplier.suplier_kode]');
		if ($this->form_validation->run() == TRUE) {
			$suplier_kode = $this->input->post('suplier_kode');
			$suplier_nama = $this->input->post('suplier_nama');
			$suplier_alamat = $this->input->post('suplier_alamat');
			$suplier_telepon = $this->input->post('suplier_telepon');
			$suplier_email = $this->input->post('suplier_email');
			$data = array("suplier_kode"=>$suplier_kode,"suplier_nama"=>$suplier_nama,"suplier_alamat"=>$suplier_alamat,"suplier_telepon"=>$suplier_telepon,"suplier_email"=>$suplier_email);
			$insert = $this->suplier->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["suplier_nama"] = $this->input->post('suplier_nama');
		$data["suplier_alamat"] = $this->input->post('suplier_alamat');
		$data["suplier_telepon"] = $this->input->post('suplier_telepon');
		$data["suplier_email"] = $this->input->post('suplier_email');
		$suplier_id = $this->input->post('suplier_id');

		if ($this->suplier->is_ready_kode($suplier_id,$this->input->post('suplier_kode'))){
			$data['suplier_kode'] = $this->input->post('suplier_kode');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->suplier->update_by_id('suplier_id',$suplier_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->suplier->delete_by_id("suplier_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file SuplierController.php */
/* Location: ./application/controllers/SuplierController.php */
