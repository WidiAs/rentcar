<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user', '', true);
		$this->load->model('lokasi', '', true);
		$this->load->library('mail');
		$this->load->helper('string');

//		require APPPATH . 'libraries/phpmailer/src/Exception.php';
//		require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
//		require APPPATH . 'libraries/phpmailer/src/SMTP.php';
	}

	public function index()
	{
		$data = array();
		if (isset($_SESSION['login'])) {
			redirect(base_url(), 'refresh');
		} else {
			$data['lokasi'] = $this->lokasi->all_toko();
			$this->load->view('admin/login', $data);
		}
	}

	function auth()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$lokasi = $this->input->post('lokasi');
		$auth = $this->user->auth_login($email);
		if ($auth != null) {
			if (password_verify($password, $auth->password)) {
				$result["success"] = true;
				$result["message"] = "Login success";
				$_SESSION["login"]["user_id"] = $auth->user_id;
				$_SESSION["login"]["user_name"] = $auth->staff_nama;
				$_SESSION["login"]["email"] = $auth->staff_email;
				$_SESSION["login"]["user_role_id"] = $auth->user_role_id;
				$_SESSION["login"]["level"] = $auth->level;
				$_SESSION["login"]["avatar"] = $auth->avatar;
				$_SESSION["login"]["staff_id"] = $auth->user_staff_id;
				if ($auth->lokasi_id != null) {
					$_SESSION["login"]["lokasi_id"] = $auth->lokasi_id;
					$_SESSION["login"]["lokasi_nama"] = $auth->lokasi_nama;
					if ($lokasi) {
						$lokasi_data = $this->lokasi->lokasi_by_id($lokasi);
						$_SESSION["login"]["lokasi_id"] = $lokasi_data->lokasi_id;
						$_SESSION["login"]["lokasi_nama"] = $lokasi_data->lokasi_nama;
					}
				}
			} else {
				$result["message"] = "Wrong password";
			}
		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}

	function multi()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$auth = $this->user->email_login($email);
		$result['multi'] = 0;
		if ($auth != null) {
			if (password_verify($password, $auth->password)) {
				$result["success"] = true;
				$result["message"] = "Login success";
				$result['multi'] = $auth->multi;
			} else {
				$result["message"] = "Wrong password";
			}
		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}

	function logout()
	{
		$result['success'] = false;
		$result['message'] = 'Gagal logout, silakan lakukan closing pada POS terlebih dahulu';
		if (!isset($_SESSION['log_pos']['log_kasir_id'])) {
			unset($_SESSION['login']);
			$result['success'] = true;
			$result['message'] = 'Berhasil logout';
		}
		echo json_encode($result);
	}

	function forget_old()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$auth = $this->user->auth_login($email);
		if ($auth != null) {
			$timeout = time() + (24 * 3600);
			$code = random_string('alnum', 25);
			$session = $this->user->forgot_request($email, $code, $timeout);
			if ($session) {
				$config = Array(
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'protocol' => 'smtp',
					'smtp_port' => 465,
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_user' => 'cvwaisnawatransgroup@gmail.com',
					'smtp_pass' => 'vgqshazaegszvtnw',
					'crlf' => "\r\n",
					'newline' => "\r\n"
				);
				$htmlContent = '<h3><strong>Reset Password</strong> </h3>';
				$htmlContent .= '<p>We have confirmed your request, please click <a href="' . base_url() . 'forget/' . $email . '/' . $code . '">here</a> to reset your password</p>';
				$this->load->library('email');
				$this->email->initialize($config);
				$this->email->from('cvwaisnawatransgroup@gmail.com', 'Waisnawa Trans Group');
				$this->email->to($email);
				$this->email->subject('Request forget password');
				$this->email->message($htmlContent);
				$send = $this->email->send();

				if ($send) {
					$result["success"] = true;
					$result["message"] = "Your request has been accepted please check your email";
				} else {
					$result["message"] = json_encode($send);
				}
			}

		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}

	function forget()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$auth = $this->user->auth_login($email);
		if ($auth != null) {
			$timeout = time() + (24 * 3600);
			$code = random_string('alnum', 25);
			$session = $this->user->forgot_request($email, $code, $timeout);
			if ($session) {

				$htmlContent = '<h3><strong>Reset Password</strong> </h3>';
				$htmlContent .= '<p>Permintaan anda telah dikonfirmasi, silahkan klik <a href="' . base_url() . 'forget/' . $email . '/' . $code . '">disini</a> untuk mengganti password anda.</p>';
				$send = $this->mail->mailer_auth('Request forget password', $email, $auth->staff_nama, $htmlContent);
				if ($send) {
					$result["success"] = true;
					$result["message"] = "Permintaan anda telah diproses, silahkan cek email anda";
				} else {
					$result["message"] = "Gagal mengirim pesan";
				}
			}
		} else {
			$result["message"] = "Email tidak terdaftar";
		}

		echo json_encode($result);
	}

	public function forget_index()
	{
		$email = $this->uri->segment(2);
		$code = $this->uri->segment(3);
		$check = $this->user->forget_session_check($email, $code);
		if (sizeof($check)) {
			$data["email"] = $email;
			$data["code"] = $code;
			$this->load->view('admin/reset_password');
		} else {
			redirect(base_url() . "404_override", 'refresh');
		}
	}

	function reset_password()
	{
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$code = $this->input->post('code');
		$new_password = $this->input->post('new_pass');
		$check = $this->user->forget_session_check($email, $code);
		if (sizeof($check)) {
			$password = password_hash($new_password, PASSWORD_BCRYPT);
			$change = $this->user->change_password($email, $password);
			if ($change) {
				$result["success"] = true;
				$result["message"] = "Your password successfully changed";
			} else {
				$result["message"] = "Failed to change password";
			}
		}
		echo json_encode($result);
	}

//	function mail_send($subject, $to_email, $to_name, $body)
//	{
//		$this->load->library('my_phpmailer');
//		$mail = new PHPMailer;
//
//		try {
//
//			$mail->IsSMTP();
//			$mail->SMTPSecure = "ssl";
//			$mail->Host = "cvwaisnawatransgroup.com"; //hostname masing-masing provider email
//			$mail->SMTPDebug = 2;
//			$mail->SMTPDeug = 2;
//			$mail->do_debug = 1 ;
//			$mail->Port = 465;
//			$mail->SMTPAuth = true;
//			$mail->Username = "info@cvwaisnawatransgroup.com"; //user email
//			$mail->Password = "waisnawa2022"; //password email
//			$mail->SetFrom("info@cvwaisnawatransgroup.com", "Waisnawa Trans Group"); //set email pengirim
//			$mail->Subject = $subject; //subyek email
//			$mail->AddAddress($to_email, $to_name); //tujuan email
//			$mail->MsgHTML($body);
//			$send = $mail->Send();
//
//			if ($send){
//				return true;
//			}
//		} catch (phpmailerException $e) {
//			echo $e->errorMessage(); //Pretty error messages from PHPMailer
//		} catch (Exception $e) {
//			echo $e->getMessage(); //Boring error messages from anything else!
//		}
//	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
