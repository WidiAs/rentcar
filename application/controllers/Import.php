<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk', '', true);
		$this->load->model('jenis_produk', '', true);
		$this->load->model('satuan', '', true);
		$this->load->model('harga_produk', '', true);
		$this->load->model('size', '', true);
		$this->load->model('konversi_bahan', '', true);
		$this->load->model('color', '', true);
		$this->load->model('patern', '', true);
		$this->load->model('fabric', '', true);
		$this->load->model('stock_produk', '', true);
		$this->load->model('lokasi', '', true);
	}

	public function index()
	{
		$jenis = 'Bedsheet';
		$id_jenis = null;
		$data_jenis = $this->jenis_produk->jenis_produk_by_name($jenis);
		if (empty($data_jenis)) {
			$id_jenis = $data_jenis->jenis_produk_id;
		} else {
			$insert = $this->jenis_produk->insert(array('jenis_produk_kode' => $jenis, 'jenis_produk_nama' => $jenis));
			$id_jenis = $insert->jenis_produk_id;
		}
		echo json_encode($data_jenis);
	}

	public function create()
	{
		$data['title'] = "Upload File Excel";
		$this->load->view('create', $data);
	}

	public function excel()
	{
		if (isset($_FILES["file"]["name"])) {
			// upload
			$file_tmp = $_FILES['file']['tmp_name'];
			$file_name = $_FILES['file']['name'];
			$file_size = $_FILES['file']['size'];
			$file_type = $_FILES['file']['type'];
			// move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads

			$object = PHPExcel_IOFactory::load($file_tmp);


			foreach ($object->getWorksheetIterator() as $worksheet) {

				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();


				for ($row = 2; $row <= $highestRow; $row++) {

					$kode = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$jenis = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$style = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$tipe = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$size = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$g_size = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$b_size = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$fabric = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
					$patern = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
					$pile = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
					$color = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
					$satuan = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
					$harga = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
					$pc = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
					$m2 = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
					$potong = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
					$qty = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
					$barcode = $worksheet->getCellByColumnAndRow(17, $row)->getValue();


					if ($kode) {
						$id_jenis = null;
						if ($jenis) {
							$data_jenis = $this->jenis_produk->jenis_produk_by_name($jenis);
							if (!empty($data_jenis)) {
								$id_jenis = $data_jenis->jenis_produk_id;
							} else {
								$insert = $this->jenis_produk->insert(array('jenis_produk_kode' => $jenis, 'jenis_produk_nama' => $jenis));
								$id_jenis = $this->jenis_produk->last_id();
							}
						}
						$satuan_id = null;
						if ($satuan) {
							$data_satuan = $this->satuan->satuan_by_name($satuan);
							if (!empty($data_satuan)) {
								$satuan_id = $data_satuan->satuan_id;
							} else {
								$insert = $this->satuan->insert(array('satuan_kode' => $satuan, 'satuan_nama' => $satuan));
								$satuan_id = $this->satuan->last_id();
							}
						}

						$size_id = null;
						if ($size) {
							$data_size = $this->size->size_by_name($size);
							if (!empty($data_size)) {
								$size_id = $data_size->size_id;
							} else {
								$insert_size = $this->size->insert(array('size_nama' => $size));
								$size_id = $this->size->last_id();
							}
						}

						$fabric_id = null;
						if ($fabric) {
							$data_fabric = $this->fabric->fabric_by_name($fabric);
							if (!empty($data_fabric)) {
								$fabric_id = $data_fabric->fabric_id;
							} else {
								$insert_fabric = $this->fabric->insert(array('fabric_nama' => $fabric));
								$fabric_id = $this->fabric->last_id();
							}
						}

						$patern_id = null;
						if ($patern) {
							$data_patern = $this->patern->patern_by_name($patern);
							if (!empty($data_patern)) {
								$patern_id = $data_patern->patern_id;
							} else {
								$insert_patern = $this->patern->insert(array('patern_nama' => $patern));
								$patern_id = $this->patern->last_id();
							}
						}

						$color_id = null;
						if ($color) {
							$data_color = $this->color->color_by_name($color);
							if (!empty($data_color)) {
								$color_id = $data_color->color_id;
							} else {
								$insert_color = $this->color->insert(array('color_nama' => $color));
								$color_id = $this->color->last_id();
							}
						}

						$g_size_id = null;
						if ($g_size) {
							$data_g_size = $this->size->size_by_name($g_size);
							if (!empty($data_g_size)) {
								$g_size_id = $data_g_size->size_id;
							} else {
								$insert_g_size = $this->size->insert(array('size_nama' => $g_size));
								$g_size_id = $this->size->last_id();
							}
						}

						$b_size_id = null;
						if ($b_size) {
							$data_b_size = $this->size->size_by_name($b_size);
							if (!empty($data_b_size)) {
								$b_size_id = $data_b_size->size_id;
							} else {
								$insert_b_size = $this->size->insert(array('size_nama' => $b_size));
								$b_size_id = $this->size->last_id();
							}
						}

						$data = array(
							'produk_kode' => $kode,
							'produk_jenis_id' => $id_jenis,
							'style' => $style,
							'tipe' => $tipe,
							'size_id' => $size_id,
							'good_size_id' => $g_size_id,
							'bed_size_id' => $b_size_id,
							'fabric_id' => $fabric_id,
							'patern_id' => $patern_id,
							'file' => $pile,
							'color_id' => $color_id,
							'produk_satuan_id' => $satuan_id,
							'harga_eceran' => $harga,
							'weight_pc' => $pc,
							'weight_m' => $m2,
						);

						$this->produk->insert($data);

						$id_produk = $this->produk->last_id();

						$produk = $this->produk->row_by_id($id_produk);


						$id_satuan = $produk->produk_satuan_id;
						$id_jenis = $produk->produk_jenis_id;
						$id_fabric = $produk->fabric_id;
						$id_patern = $produk->patern_id;
						$id_color = $produk->color_id;
						$id_size = $produk->size_id;
						$id_bed_size = $produk->bed_size_id;
						$id_good_size = $produk->good_size_id;
						$id_produk = $produk->produk_id;

						$id_satuan = $id_satuan ? $id_satuan : 0;
						$id_jenis = $id_jenis ? $id_jenis : 0;
						$id_fabric = $id_fabric ? $id_fabric : 0;
						$id_patern = $id_patern ? $id_patern : 0;
						$id_color = $id_color ? $id_color : 0;
						$id_size = $id_size ? $id_size : 0;
						$id_bed_size = $id_bed_size ? $id_bed_size : 0;
						$id_good_size = $id_good_size ? $id_good_size : 0;

						if (!$barcode) {

							$barcode = $id_satuan . $id_jenis . $id_fabric . $id_patern . $id_color . $id_size . $id_bed_size . $id_good_size . $id_produk;
						}


						$update = $this->produk->update_by_id('produk_id', $id_produk, array('barcode' => $barcode));

						if ($potong) {
							$jumlah = $potong / $qty;
							$data_bahan = $this->bahan->bahan_by_name($fabric);
							if (!empty($data_bahan)) {
								$bahan_id = $data_bahan->bahan_id;
							} else {
								$insert_bahan = $this->bahan->insert(array('bahan_nama' => $fabric, 'bahan_jenis_id' => 4, 'bahan_satuan_id' => 6, 'bahan_suplier_id' => 6));
								$bahan_id = $this->bahan->last_id();
							}
							if ($id_produk) {
								$data_konversi = array(
									'produk_id' => $id_produk,
									'bahan_id' => $bahan_id,
									'jumlah' => $jumlah,
									'created_at' => Date("Y-m-d H:i:s")
								);

								$this->konversi_bahan->insert($data_konversi);

							}
						}

					}


				}

			}
		}
		echo 'Sukses';
	}

	public function create_baku()
	{
		$data['title'] = "Upload File Excel Bahan Baku";
		$this->load->view('create_baku', $data);
	}


	public function excel_baku()
	{
		if (isset($_FILES["file"]["name"])) {
			// upload
			$file_tmp = $_FILES['file']['tmp_name'];
			$file_name = $_FILES['file']['name'];
			$file_size = $_FILES['file']['size'];
			$file_type = $_FILES['file']['type'];
			// move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads

			$object = PHPExcel_IOFactory::load($file_tmp);
			$data = array();


			foreach ($object->getWorksheetIterator() as $worksheet) {

				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();


				for ($row = 2; $row <= $highestRow; $row++) {

					$jenis = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$ukuran = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$patern = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$bahan = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$potong = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$qty = $worksheet->getCellByColumnAndRow(5, $row)->getValue();


					if ($jenis) {
						$id_produk = null;
						if ($jenis) {
							$data_produk = $this->produk->produk_by_custom($jenis, $ukuran, $patern);
							if (!empty($data_produk)) {
								$id_produk = $data_produk->produk_id;
							} else {
								$data_produk = $this->produk->produk_by_jenis($jenis);
								if (!empty($data_produk)) {
									$id_produk = $data_produk->produk_id;

								}
							}
						}

						$bahan_id = null;
						if ($bahan) {
							$data_bahan = $this->bahan->bahan_by_name($bahan);
							if (!empty($data_bahan)) {
								$bahan_id = $data_bahan->bahan_id;
							} else {
								$insert_bahan = $this->bahan->insert(array('bahan_nama' => $bahan, 'bahan_jenis_id' => 4, 'bahan_satuan_id' => 6, 'bahan_suplier_id' => 6));
								$bahan_id = $this->bahan->last_id();
							}
						}

						$jumlah = $potong / $qty;
						$data[$id_produk] = $data_produk;

						if ($id_produk) {
							$data_konversi = array(
								'produk_id' => $id_produk,
								'bahan_id' => $bahan_id,
								'jumlah' => $jumlah,
								'created_at' => Date("Y-m-d H:i:s")
							);

							$this->konversi_bahan->insert($data_konversi);

						}

					}


				}

			}
		}
		echo json_encode($data);
	}

	public function create_stok()
	{
		$data['title'] = "Upload File Excel";
		$this->load->view('create_stok', $data);
	}

	public function excel_stok()
	{
		if (isset($_FILES["file"]["name"])) {
			// upload
			$file_tmp = $_FILES['file']['tmp_name'];
			$file_name = $_FILES['file']['name'];
			$file_size = $_FILES['file']['size'];
			$file_type = $_FILES['file']['type'];
			// move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads

			$object = PHPExcel_IOFactory::load($file_tmp);

			$data_report = array();
			foreach ($object->getWorksheetIterator() as $worksheet) {

				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();


				for ($row = 2; $row <= $highestRow; $row++) {

					$jenis = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$size = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$color = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$fabric = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$patern = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$stok = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$lokasi = $worksheet->getCellByColumnAndRow(6, $row)->getValue();


					if ($jenis) {
						$id_jenis = null;
						if ($jenis) {
							$data_jenis = $this->jenis_produk->jenis_produk_by_name($jenis);
							if (!empty($data_jenis)) {
								$id_jenis = $data_jenis->jenis_produk_id;
							}
						}
						$size_id = null;
						if ($size) {
							$data_size = $this->size->size_by_name($size);
							if (!empty($data_size)) {
								$size_id = $data_size->size_id;
							}
						}

						$fabric_id = null;
						if ($fabric) {
							$data_fabric = $this->fabric->fabric_by_name($fabric);
							if (!empty($data_fabric)) {
								$fabric_id = $data_fabric->fabric_id;
							}
						}

						$patern_id = null;
						if ($patern) {
							$data_patern = $this->patern->patern_by_name($patern);
							if (!empty($data_patern)) {
								$patern_id = $data_patern->patern_id;
							}
						}

						$color_id = null;
						if ($color) {
							$data_color = $this->color->color_by_name($color);
							if (!empty($data_color)) {
								$color_id = $data_color->color_id;
							}
						}

						$where = array(
							'produk_jenis_id' => $id_jenis,
							'fabric_id' => $fabric_id,
							'patern_id' => $patern_id,
							'color_id' => $color_id,
							'size_id' => $size_id
						);

						$data_produk = $this->produk->produk_by_where($where);
						if ($data_produk) {

							$produk_id = $data_produk->produk_id;
							$data_lokasi = $this->lokasi->lokasi_by_name($lokasi);
							$lokasi_id = $data_lokasi->lokasi_id;

							$data_stock = $this->stock_produk->row_by_condition(array('produk_id' => $produk_id, 'stock_produk_lokasi_id' => $lokasi_id));

							if ($data_stock) {
								$data_report[$row] = 'ada';
								$where_stock = array(
									'produk_id' => $produk_id,
									'stock_produk_lokasi_id' => $lokasi_id
								);

								$update = $this->stock_produk->update_by_condition($where_stock, array('stock_produk_qty' => $stok));

							} else {
								$data_report[$row] = 'kosong';
								$year = date("y");
								$month = date("m");
								$lokasi_kode = $data_lokasi->lokasi_kode;
								$jenis_produk_kode = $this->produk->produk_by_id($produk_id)->jenis_produk_kode;
								$stock_produk_seri = $month . $year . $jenis_produk_kode . $lokasi_kode;
								$urutan = $this->stock_produk->urutan_seri($stock_produk_seri);
								$stock_produk_seri = $stock_produk_seri . $urutan;
								$this->stock_produk->insert(array(
									'produk_id' => $produk_id,
									'stock_produk_lokasi_id' => $lokasi_id,
									'month' => $month,
									'year' => $year,
									'urutan' => $urutan,
									'stock_produk_seri' => $stock_produk_seri,
									'stock_produk_qty' => $stok
								));
							}

						}
					}


				}

			}
		}
		echo json_encode($data_report);
	}

}
