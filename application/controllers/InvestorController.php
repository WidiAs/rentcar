<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvestorController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('investor', '', true);
		$this->load->model('mobil', '', true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Investor < Master Data < Waisnawa";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data" => "no"));
		array_push($column, array("data" => "investor_nama"));
		array_push($column, array("data" => "investor_kontak"));
		array_push($column, array("data" => "investor_keterangan"));

		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
		$akses_menu = json_decode($this->menu_akses, true);
		$action = array();
		foreach ($akses_menu['master_data']['investor'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/master_data/investor');
		$this->load->view('admin/static/footer');
	}

	function list()
	{
		$query = (isset($this->input->get('search')["value"]) ? $this->input->get('search')["value"] : null);
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->investor->investor_count_all();
		$result['iTotalDisplayRecords'] = $this->investor->investor_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data = $this->investor->investor_list($start, $length, $query);
		$i = $start + 1;
		foreach ($data as $key) {
			if ($key->created_at != null) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s', $time);
			}
			if ($key->updated_at != null) {
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s', $time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url() . 'investor/delete/';
			$key->row_id = $key->investor_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

	function options()
	{
		$data = array();
		$investor = $this->investor->all_list();
		foreach ($investor as $key) {
			array_push($data, strtoupper($key->investor_nama));
		}
		echo json_encode($data);
	}

	function add()
	{
		$result['success'] = false;
		$result['message'] = "Investor ini telah terdaftar";
		$this->form_validation->set_rules('investor_nama', '', 'required|is_unique[investor.investor_nama]');
		if ($this->form_validation->run() == TRUE) {
			$investor_nama = $this->input->post('investor_nama');
			$investor_kontak = $this->input->post('investor_kontak');
			$investor_keterangan = $this->input->post('investor_keterangan');
			$data = array(
				"investor_keterangan" => $investor_keterangan,
				"investor_kontak" => $investor_kontak,
				"investor_nama" => $investor_nama
			);
			$insert = $this->investor->insert($data);
			if ($insert) {
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}

	function edit()
	{
		$result['success'] = false;
		$result['message'] = "Investor sudah terdaftar";
		$data = array();
		$data['investor_merk'] = $this->input->post('investor_merk');
		$investor_id = $this->input->post('investor_id');

		if ($this->investor->is_ready_nama($investor_id, $this->input->post('investor_nama'))) {
			$data["investor_nama"] = $this->input->post('investor_nama');
			$data["investor_kontak"] = $this->input->post('investor_kontak');
			$data["investor_keterangan"] = $this->input->post('investor_keterangan');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->investor->update_by_id('investor_id', $investor_id, $data);
			if ($update) {
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}

	function delete()
	{
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$count = $this->mobil->count_by_investor($id);
		if ($id != "" && $count == 0) {
			$delete = $this->investor->delete_by_id("investor_id", $id);
			if ($delete) {
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		} elseif ($count > 0) {
			$result['message'] = "Masih ada data mobil milik investor, silahkan hapus data mobil terlebih dahulu";
		}
		echo json_encode($result);
	}
}
