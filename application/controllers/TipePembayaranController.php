<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipePembayaranController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tipe_pembayaran','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Tipe Pembayaran < Master Data < Waisnawa";
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"tipe_pembayaran_kode"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"no_akun"));
		array_push($column, array("data"=>"created_at"));
		array_push($column, array("data"=>"updated_at"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['tipe-pembayaran'] as $key => $value) {
			if($key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/tipe_pembayaran');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->tipe_pembayaran->tipe_pembayaran_count_all();
		$result['iTotalDisplayRecords'] = $this->tipe_pembayaran->tipe_pembayaran_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->tipe_pembayaran->tipe_pembayaran_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'tipe-pembayaran/delete/';
			$key->row_id = $key->tipe_pembayaran_id;
		}
		$result['aaData'] = $data;	
		echo json_encode($result);		
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
		
		$this->form_validation->set_rules('tipe_pembayaran_kode', '', 'required|is_unique[tipe_pembayaran.tipe_pembayaran_kode]');
		if ($this->form_validation->run() == TRUE) {
			$data['tipe_pembayaran_kode'] = $this->input->post('tipe_pembayaran_kode');
			$data['tipe_pembayaran_nama'] = $this->input->post('tipe_pembayaran_nama');
			$data['no_akun'] = $this->input->post('no_akun');
			$data['kembalian'] = 0;
			$data['additional'] = 0;
			if(isset($_POST['kembalian'])){
				$data['kembalian'] = 1;
			}
			if(isset($_POST['additional'])){
				$data['additional'] = 1;
			}
			$data['jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
			$insert = $this->tipe_pembayaran->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["tipe_pembayaran_nama"] = $this->input->post('tipe_pembayaran_nama');
		$tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
		if ($this->tipe_pembayaran->is_ready_kode($tipe_pembayaran_id,$this->input->post('tipe_pembayaran_kode'))){
			$data['tipe_pembayaran_kode'] = $this->input->post('tipe_pembayaran_kode');
			$data['no_akun'] = $this->input->post('no_akun');
			$data['jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
			$data['kembalian'] = 0;
			$data['additional'] = 0;
			if(isset($_POST['kembalian'])){
				$data['kembalian'] = 1;
			}
			if(isset($_POST['additional'])){
				$data['additional'] = 1;
			}			
			$update = $this->tipe_pembayaran->update_by_id('tipe_pembayaran_id',$tipe_pembayaran_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->tipe_pembayaran->delete_by_id("tipe_pembayaran_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file TipePembayaran.php */
/* Location: ./application/controllers/TipePembayaran.php */
