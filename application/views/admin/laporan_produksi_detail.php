<!-- begin:: Content -->
						<input type="hidden" id="current_page" value="<?=base_url()."laporan/laporan-produksi-detail/"?>" name="">
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Laporan Produksi</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>laporan/laporan-po-bahan-detail" class="kt-subheader__breadcrumbs-link">Laporan</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>laporan/laporan-po-bahan-detail" class="kt-subheader__breadcrumbs-link">Laporan Produksi</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(2)=='laporan-produksi') ? 'active' : '')?>" href="<?=base_url()?>laporan/laporan-produksi">
															<i class="fa fa-globe"></i> <span class="kt--visible-desktop-inline-block">Global</span>
														</a>
													</li>
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(2)=='laporan-produksi-detail') ? 'active' : '')?>" href="<?=base_url()?>laporan/laporan-produksi-detail">
															<i class="fa fa-grip-horizontal"></i> <span class="kt--visible-desktop-inline-block">Detail</span>
														</a>
													</li>
												</ul>
										</div>
									</div>
									<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
									<input type="hidden" id="list_url" value="<?=base_url()."laporan/laporan-produksi-detail/list"?>" name="">
									<div style="display: none;" id="table_column"><?=$column?></div>
									<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
									<?php if(isset($columnDef)) {  ?>
									<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
									<?php } ?>
									<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<div class="btn-group btn-group btn-pill btn-group-sm">
													<?php
													$uri = $this->uri->segment(1);
													$getUrl = "";
													foreach (array_keys($this->input->get()) as $key) {
														$getUrl .=$key."=".$this->input->get($key)."&";
													}
													$getUrl = rtrim($getUrl,"& ");
													?>
													<a href="<?=base_url()?>laporan/laporan-produksi-detail/pdf" class="btn btn-danger akses-pdf" id="akses-pdf">
														<i class="la la-file-pdf-o"></i> Print PDF
													</a>
													<a href="<?=base_url()?>laporan/laporan-produksi-detail/excel" class="btn btn-success akses-excel" id="akses-excel">
														<i class="la la-file-excel-o"></i> Print Excel
													</a>
												</div>

											</div>
										</div>
									</div>
																					
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-md-12 order-2 order-xl-1 searchForm">
												<div class="row align-items-center">
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Kode Produksi</label>
															</div>
															<div class="kt-form__control">
																<input type="text" class="form-control searchInput textSearch" data-col-index="1" data-field="produksi_kode" placeholder="Search...">
															</div>
														</div>
													</div>													
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Tanggal Produksi</label>
															</div>
															<div class="kt-form__control">
																<div class="input-daterange input-group" id="kt_datepicker">
																	<input type="text" class="form-control kt-input searchInput" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="2" value="" data-field="mulai_start" />
																	<div class="input-group-append">
																		<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
																	</div>
																	<input type="text" class="form-control kt-input searchInput" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="2" value="" data-field="mulai_end" />
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Produk</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-col-index="4" data-field="produk_id">
																	<option value="">All</option>
																	<?php foreach ($produk as $key) {
																		?>
																		<option value="<?=$key->produk_id?>"><?=$key->produk_nama?></option>
																		<?php
																	} ?>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Bahan</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-col-index="5" data-field="bahan_id">
																	<option value="">All</option>
																	<?php foreach ($bahan as $key) {
																		?>
																		<option value="<?=$key->bahan_id?>"><?=$key->bahan_nama?></option>
																		<?php
																	} ?>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
											<table class="datatable table table-striped- table-hover table-checkable" >
												<thead>
													<tr>
														<th width="30">No</th>
														<th>Kode Produksi</th>
														<th>Tanggal Produksi</th>
														<th>Status Produksi</th>
														<th>Produk</th>
														<th>Bahan</th>
														<th>Jumlah</th>
													</tr>
												</thead>
												<tbody id="child_data_ajax"></tbody>
												<tfoot >
													<tr>
														<td colspan="6" style="text-align: right"><strong>Total</strong> </td>
														<td ><strong></strong> </td>
													</tr>

												</tfoot>
													</table>
									<!--end: Datatable -->
								</div>
						</div>							
						</div>