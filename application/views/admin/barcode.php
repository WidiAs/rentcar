<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Guest</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>guest" class="kt-subheader__breadcrumbs-link">Master Data</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>guest" class="kt-subheader__breadcrumbs-link">Guest</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<input type="hidden" value="<?= base_url('barcode-print') ?>" id="link-print">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Guest
				</h3>
				<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Search Form -->
<!--			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">-->
<!--				<div class="row align-items-center">-->
<!--					<div class="col-xl-8 order-2 order-xl-1">-->
<!--						<div class="row align-items-center">-->
<!--							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">-->
<!--								<div class="kt-input-icon kt-input-icon--left">-->
<!--									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">-->
<!--									<span class="kt-input-icon__icon kt-input-icon__icon--left">-->
<!--																<span><i class="la la-search"></i></span>-->
<!--															</span>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
			<table class="datatable table table-striped- table-hover table-checkable">
				<thead>
				<tr>
					<th width="30">Number</th>
					<th width="200">Action</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($angka as $r) { ?>
					<tr>
					<td><?= $r ?></td>
						<td><button class="btn btn-warning btn-barcode" data-number="<?= $r ?>">Aksi</button></td>
					</tr>
				<?php } ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_barcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Guest</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
<!--			<form action="--><?//= base_url() ?><!--barcode-print" method="post" id="kt_add_staff_form">-->
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">

								<label class="form-control-label ">Nomor <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="number" class="form-control" value=""
									   required="" readonly>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah Print <b class="label--required">*</b></label>
								<input type="number" placeholder="" name="jumlah" class="form-control" value="1"
									   required="">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="cetak" type="button" class="btn btn-primary">Cetak</button>
				</div>
<!--			</form>-->
		</div>
	</div>
</div>

