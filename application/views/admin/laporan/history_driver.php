<!-- begin:: Content -->
<input type="hidden" id="current_page" value="<?= base_url() . "laporan/laporan-history-driver/" ?>" name="">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Laporan History Kegiatan Driver</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>laporan/laporan-history-driver" class="kt-subheader__breadcrumbs-link">Laporan</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>laporan/laporan-history-driver" class="kt-subheader__breadcrumbs-link">Laporan
				History Kegiatan Driver</a>
			<input type="hidden" id="action" value='<?= $action ?>'>
			<input type="hidden" id="home-url" value='<?= base_url() ?>'>
			<input type="hidden" id="url-edit-pengeluaran" value="<?=base_url()?>pengeluaran-driver/edit/">
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				<div class="kt-subheader__main">
				</div>
			</div>
			<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
			<input type="hidden" id="list_url" value="<?= base_url() . "laporan/laporan-history-driver/list" ?>"
				   name="">
			<div style="display: none;" id="table_column"><?= $column ?></div>
			<div style="display: none;" id="sumColumn"><?= (isset($sumColumn) ? $sumColumn : "") ?></div>
			<?php if (isset($columnDef)) { ?>
				<div style="display: none;" id="table_columnDef"><?= $columnDef ?></div>
			<?php } ?>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<div class="btn-group btn-group btn-pill btn-group-sm">
							<?php
							$uri = $this->uri->segment(1);
							$getUrl = "";
							foreach (array_keys($this->input->get()) as $key) {
								$getUrl .= $key . "=" . $this->input->get($key) . "&";
							}
							$getUrl = rtrim($getUrl, "& ");
							?>
							<a href="<?= base_url() ?>laporan/laporan-history-driver/pdf"
							   class="btn btn-danger akses-pdf" id="akses-pdf">
								<i class="la la-file-pdf-o"></i> Print PDF
							</a>
							<a href="<?= base_url() ?>laporan/laporan-history-driver/excel"
							   class="btn btn-success akses-excel" id="akses-excel">
								<i class="la la-file-excel-o"></i> Print Excel
							</a>
						</div>

					</div>
				</div>
			</div>

		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
								</div>
							</div>
							<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Nama Driver</label>
									</div>
									<div class="kt-form__control">
										<input type="text" class="form-control searchInput textSearch"
											   data-col-index="2" data-field="no_faktur" placeholder="Search...">
									</div>
								</div>
							</div>
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Tanggal</label>
									</div>
									<div class="kt-form__control">
										<div class="input-daterange input-group" id="kt_datepicker">
											<input type="text" class="form-control kt-input searchInput"
												   name="start_date" placeholder="Dari" autocomplete="off"
												   data-col-index="1" value="" data-field="tanggal_start"/>
											<div class="input-group-append">
												<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
											</div>
											<input type="text" class="form-control kt-input searchInput" name="end_date"
												   placeholder="Sampai" autocomplete="off" data-col-index="1" value=""
												   data-field="tanggal_end"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable table-kegiatan">
				<thead>
				<tr>
					<th width="30">No</th>
					<th>Nama Driver</th>
					<th>Tanggal</th>
					<th>Jenis Mobil</th>
					<th>Nomor Plat</th>
					<th>Jam Mulai</th>
					<th>Jam Selesai</th>
					<th>Waktu Kerja</th>
					<th>Status</th>
					<?php if ($_SESSION['login']['user_role_id'] == 1) { ?>
						<th>Aksi</th>
					<?php } ?>
				</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>

	<div class="modal" id="modal-kegiatan" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Pencatatan Aktifitas</h5>
					<button type="button btn" class="close" data-dismiss="modal" aria-label="Close"
							style="margin-top: -30px">
					</button>
				</div>
				<form action="<?= base_url() ?>kegiatan-driver/update" method="post" id="kt_add_staff_form"
					  enctype="multipart/form-data">
					<div class="modal-body">
						<input type="hidden" name="kegiatan_driver_id">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Nama Driver</label>
									<div class="col-9">
										<label name="driver_nama" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jenis Mobil</label>
									<div class="col-9">
										<label name="mobil_jenis" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Plat Mobil</label>
									<div class="col-9">
										<label name="mobil_plat" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
									<div class="col-9">
										<label name="tanggal_lbl" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jam Kerja</label>
									<div class="col-9">
										<label name="waktu_kerja" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Jam Mulai <b class="label--required">*</b></label>
									<input type="time" name="waktu_mulai" class="form-control" required="">
								</div>
								<div class="form-group">
									<label class="control-label">Jam Selesai <b class="label--required">*</b></label>
									<input type="time" name="waktu_selesai" class="form-control" required="">
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<button type="button" id="btn-manage-pengeluaran" class="btn btn-warning" style="float : right" data-id="">Edit</button>
									</div>
									<table class="table" style="max-height: 200px; overflow-y: scroll; display:inline-block;">
										<thead>
										<tr>
											<th width="100">Jam</th>
											<th width="250">Jenis Pengeluaran</th>
											<th width="100">Jumlah</th>
										</tr>
										</thead>
										<tbody id="item-pengeluaran">
										</tbody>
										<tfoot>
										<tr>
											<td colspan="2">Total</td>
											<td id="total-pengeluaran"></td>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal" id="modal-bayar" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Pembayaran Driver</h5>
					<button type="button btn" class="close" data-dismiss="modal" aria-label="Close"
							style="margin-top: -30px">
					</button>
				</div>
				<form action="<?= base_url() ?>kegiatan-driver/bayar" method="post" id="kt_add_staff_form"
					  enctype="multipart/form-data">
					<div class="modal-body">
						<input type="hidden" name="kegiatan_driver_id">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Nama Driver</label>
									<div class="col-9">
										<label name="driver_nama" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jenis Mobil</label>
									<div class="col-9">
										<label name="mobil_jenis" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Plat Mobil</label>
									<div class="col-9">
										<label name="mobil_plat" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
									<div class="col-9">
										<label name="tanggal_lbl" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jam Kerja</label>
									<div class="col-9">
										<label name="waktu_kerja" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jam Mulai</label>
									<div class="col-9">
										<label name="waktu_mulai" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Jam Selesai</label>
									<div class="col-9">
										<label name="waktu_selesai" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-3 col-form-label">Total Pengeluaran</label>
									<div class="col-9">
										<label name="total_pengeluaran" class="col-form-label"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Cabang <b class="label--required">*</b></label>
									<input type="hidden" name="lokasi_nama" class="form-control">
									<select class="form-control" name="lokasi_id" required="">
										<option value="">Pilih Cabang</option>
										<?php foreach ($lokasi as $l){ ?>
											<option value="<?=$l->lokasi_id?>"><?=$l->lokasi_nama?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label class="control-label">Metode Pembayaran <b class="label--required">*</b></label>
									<input type="hidden" name="tipe_pembayaran" class="form-control">
									<select class="form-control" name="tipe_pembayaran_id" required="">
										<option value="">Pilih Metode Pembayaran</option>
										<?php foreach ($tipe_pembayaran as $t){ ?>
											<option value="<?=$t->tipe_pembayaran_id?>"><?=$t->tipe_pembayaran_nama?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label class="control-label">Jumlah Bayaran <b class="label--required">*</b></label>
									<input type="text" name="jumlah_bayaran" class="form-control input-numeral" required="" value="">
								</div>
								<div class="form-group">
									<div class="form-group">
										<label>Bukti Pembayaran <b class="label--required">*</b></label>
										<div></div>
										<div class="custom-file">
											<input type="file" class="custom-file-input img-input"
												   data-display="displayImg1custom" name="bukti_pembayaran"
												   accept="image/x-png,image/gif,image/jpeg"
												   id="customFile1" required="">
											<label class="custom-file-label selected" for="customFile1"
												   id="labelCustomFile1"></label>
											<input type="hidden" name="input_bukti_pembayaran">
										</div>
									</div>
									<div class="fileinput-new thumbnail"
										 style="width: 200px; height: 150px;">
										<img src="<?= base_url() ?>assets/media/no_image.png" alt=""
											 id="displayImg1custom" width="200" height="150">
									</div>

								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="btn-bayar" type="button" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
