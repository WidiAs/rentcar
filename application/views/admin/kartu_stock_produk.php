<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Kartu Stok Produk</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>kartu-stock-produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>kartu-stock-produk" class="kt-subheader__breadcrumbs-link">Kartu Stok Produk</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Filter Data
										</h3>
									</div>
								</div>
								<?php
									$start_date = $this->input->get('start_date');
									if($start_date == ""){
										$start_date = $first_date;
									}
									$end_date = $this->input->get('end_date');
									if ($end_date == ""){
										$end_date = date("Y-m-d");
									}
								?>
								<div class="kt-portlet__body">
									<form method="get">
										<div class="kt-portlet__body">
					                        <div class="form-group m-form__group row">
					                            <label class="col-form-label col-lg-2 col-sm-12">Tanggal Progress</label>
					                            <div class="col-lg-4 col-md-9 col-sm-12">
					                                <div class="input-daterange input-group" id="kt_datepicker">
														<input type="text" class="form-control kt-input" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=$start_date?>" />
														<div class="input-group-append">
															<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
														</div>
														<input type="text" class="form-control kt-input" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="5" value="<?=$end_date?>" />
													</div>
					                            </div>
					                            <label class="col-form-label col-lg-1 col-sm-12">Produk</label>
					                            <div class="col-lg-2 col-md-9 col-sm-12">
					                                <select class="form-control" name="produk_id" data-select2-id="4" tabindex="-1" aria-hidden="true" value="">
					                                    <option value="all" <?=(($this->input->get("produk_id")=="all")? 'selected' : '' )?>>Semua Produk</option>
					                                     <?php foreach ($produk as $key) {
					                                     	?>
					                                     		<option value="<?=$key->produk_id?>" <?=(($this->input->get("produk_id")==$key->produk_id)? 'selected' : '' )?>><?=$key->produk_nama?></option>
					                                     	<?php
					                                     } ?>                                     
					                                 </select>
					                            </div>
					                            <label class="col-form-label col-lg-1 col-sm-12">Lokasi</label>
					                            <div class="col-lg-2 col-md-9 col-sm-12">
					                                <select class="form-control" name="lokasi_id" data-select2-id="4" tabindex="-1" aria-hidden="true" value="">
					                                    <option value="all" <?=(($this->input->get("lokasi_id")=="all")? 'selected' : '' )?>>Semua Lokasi</option>
					                                     <?php foreach ($lokasi as $key) {
					                                     	?>
					                                     		<option value="<?=$key->lokasi_id?>" <?=(($this->input->get("lokasi_id")==$key->lokasi_id)? 'selected' : '' )?>><?=$key->lokasi_nama?></option>
					                                     	<?php
					                                     } ?>                                     
					                                 </select>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="kt-portlet__foot text-center">
					                        <div class="btn-group btn-group btn-pill btn-group-sm">
					                            <button type="submit" class="btn btn-info akses-filter_data">
					                                <i class="la la-search"></i> Filter Data
					                            </button>
					                            <?php
					                            	$uri = $this->uri->segment(1);
					                            	$getUrl = "";
					                            	foreach (array_keys($this->input->get()) as $key) {
					                            		$getUrl .=$key."=".$this->input->get($key)."&";
					                            	}
					                            	$getUrl = rtrim($getUrl,"& ");
					                            ?>
					                            <a href="<?=base_url()?>kartu-stock-produk/pdf?<?=$getUrl?>" class="btn btn-danger akses-pdf">
					                                <i class="la la-file-pdf-o"></i> Print PDF
					                            </a>
					                            <a href="<?=base_url()?>kartu-stock-produk/excel?<?=$getUrl?>" class="btn btn-success akses-excel">
					                                <i class="la la-file-excel-o"></i> Print Excel
					                            </a>
					                        </div>
					                    </div>
				                    </form>
								</div>
							</div>
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Kartu Stok
										</h3>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url().(($this->uri->segment(1) == 'kartu-stock-produk') ? 'kartu-stock-produk' : 'kartu-stock-produk' )?>/list?<?=$getUrl?>" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									</div>
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-1 searchForm">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
											<table class="datatable table table-striped- table-bordered table-hover table-checkable" id="">
												<thead>
													<tr>
														<th colspan="5" style="text-align: center">Stok Masuk</th>
														<th rowspan="2" style="text-align: center;">Tanggal</th>
														<th colspan="5" style="text-align: center">Stok Keluar</th>
														<th rowspan="2" style="text-align: center;">Stok Akhir</th>
													</tr>
													<tr>
														<th>Produk</th>
														<th width="60">Seri Produk</th>	
														<th>Lokasi</th>
														<th width="60">Masuk</th>
														<th>Keterangan</th>
														<th>Produk</th>
														<th width="60">Seri Produk</th>	
														<th>Lokasi</th>
														<th width="60">Keluar</th>
														<th>Keterangan</th>
													</tr>
												</thead>
												<tbody></tbody>
												<tfoot >
													
												</tfoot>
											</table>
									<!--end: Datatable -->
								</div>
							</div>
						</div>
