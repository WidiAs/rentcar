<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body ">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-3"></div>
							<div class="col-md-2 kt-margin-b-20-tablet-and-mobile" style="display: <?=((isset($_SESSION["redpos_login"]['lokasi_id']))? "none":"block")?>">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Cabang</label>
									</div>
									<div class="kt-form__control">
										<select id="lokasi_id" class="form-control searchInput" data-col-index="3" data-field="lokasi_id">
											<?php if(!isset($_SESSION["login"]['lokasi_id'])) { ?>
												<option value="All" <?=($lokasi_id=='All'?'selected':'')?>>All</option>
												<?php foreach ($lokasi as $key) {
													?>
													<option value="<?=$key->lokasi_id?>" <?=($lokasi_id==$key->lokasi_id?'selected':'')?>><?=$key->lokasi_nama?></option>
													<?php
												}
											} else {
												?>
												<option value="<?=$_SESSION["login"]['lokasi_id']?>"></option>
												<?php
											} ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Tanggal</label>
									</div>
									<div class="kt-form__control">
										<div class="input-daterange input-group" id="kt_datepicker">
											<input type="text" class="form-control kt-input searchInput tanggal" id="start_date" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="1" value="<?=$min_tanggal?>" data-field="tanggal_start" />
											<div class="input-group-append">
												<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
											</div>
											<input type="text" class="form-control kt-input searchInput tanggal" id="end_date" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__foot text-center">
			<div class="btn-group btn-group btn-pill btn-group-sm">
				<button type="button" class="btn btn-info akses-filter_data">
					<i class="la la-search"></i> Filter Data
				</button>

			</div>
		</div>
	</div>
	<div class="col-md-12 row">
		<div class="col-md-4">
			<div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
				<div class="kt-portlet__head ">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Sales - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y",strtotime($max_tanggal))?>
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<h3></h3>
					</div>
				</div>
				<div class="kt-portlet__body" style="text-align: center;">
					<h2>Rp. <?=$sales?></h2>								</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-brand">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Hutang Keseluruhan
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body" style="text-align: center;">
					<h2>Rp. <?=$hutang?></h2>								</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Piutang Keseluruhan
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body" style="text-align: center;">
					<h2>Rp. <?=$piutang?></h2>								</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">

			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--tab">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
						<h3 class="kt-portlet__head-title">
							Sales Chart
							<textarea style="display: none;" id="chart-data"><?=json_encode($chartData)?></textarea>
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div id="kt_morris_1" style="height:500px;"></div>
				</div>
			</div>

			<!--end::Portlet-->
		</div>
	</div>
</div>


