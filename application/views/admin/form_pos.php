
<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penjualan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>pos" class="kt-subheader__breadcrumbs-link">POS</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>pos" class="kt-subheader__breadcrumbs-link">Penjualan</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='pos') ? 'active' : '')?>" href="<?=base_url()?>pos">
							<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penjualan</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='rekapitulasi-pos') ? 'active' : '')?>" href="<?=base_url()?>rekapitulasi-pos">
							<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Rekapitulasi POS</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<form action="<?=base_url()?>pos/save-preaty-cash" method="post" id="save-form">
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-5 col-form-label">Kas Sebelumnya</label>
							<div class="input-group col-7">
								<label for="example-text-input" class="col-12 col-form-label">Rp. 0</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Kas Saat Ini </label>
							<div class="input-group col-10">
								<input type="text" name="kas_awal" id="kas_awal" class="input-numeral form-control" value="0">
							</div>
						</div>
					</div>

				</div>
				<div class="kt-portlet__foot">
					<button type="button" class="btn btn-primary pull-right" id="btn-save-kas"><i class="flaticon2-safe"></i> Save</button>
				</div>

			</div>

		</form>
	</div>
</div>

