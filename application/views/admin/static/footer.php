</div>
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
	<div class="kt-footer__copyright">
		2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Balioz Linen</a>
	</div>
</div>
</div>
</div>
</div>
<div id="kt_scrolltop" class="kt-scrolltop">
	<i class="fa fa-arrow-up"></i>
</div>
<script>
	var KTAppOptions = {
		"colors": {
			"state": {
				"brand": "#5d78ff",
				"dark": "#282a3c",
				"light": "#ffffff",
				"primary": "#5867dd",
				"success": "#34bfa3",
				"info": "#36a3f7",
				"warning": "#ffb822",
				"danger": "#fd3995"
			},
			"base": {
				"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
				"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
			}
		}
	};
</script>
<script src="<?= base_url() ?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/custom/components/vendors/sweetalert2/init.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/jquery-form/dist/jquery.form.min.js"
		type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/cleave/cleave.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/script/admin/time.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/script/admin/reset.js" type="text/javascript"></script>


<?php if (isset($js)) {
	foreach ($js as $key) {
		?>
		<script src="<?= base_url() . 'assets/' . $key ?>" type="text/javascript"></script>
		<?php
	}
} ?>
<script src="<?= base_url() ?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>
<script>
	$(document).ready(function () {
		var base_url = $("#base_url").val();
		$("#logout").click(function (e) {
			$.ajax({
				type: "POST",
				url: base_url + 'logout',
				cache: false,
				success: function (response) {
					var data = jQuery.parseJSON(response)
					if (data.success) {
						window.location.href = base_url + 'login'
					} else {
						Swal.fire({
							type: 'error',
							title: 'Logout Error',
							text: 'Lakukan closing di POS terlebih dahulu'
						}).then(function () {
							window.location.href = base_url + 'pos'
						});
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		})
	})
</script>
</body>
</html>
