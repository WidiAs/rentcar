				<input type="hidden" id="page" value="<?=$page?>" name="" >
				<input type="hidden" id="last_open" value="<?php echo date("H:i:s")?>">
				<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-subheader--enabled kt-subheader--transparent kt-grid--hor kt-wrapper" id="kt_wrapper">
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
						</div>
						<div class="kt-header__topbar">
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user">
										<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span class="kt-header__topbar-username kt-hidden-mobile account_name"><?=$_SESSION['login']['user_name']?></span>
										<img class="kt-image account_avatar" alt="Pic" src="<?=base_url().$_SESSION['login']['avatar']?>" />
										<!-- <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">S</span> -->
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?=base_url()?>assets/media/misc/bg-1.jpg)">
										<div class="kt-user-card__avatar">
											<img class="account_avatar" alt="Pic" src="<?=base_url().$_SESSION['login']['avatar']?>" />
										</div>
										<div class="kt-user-card__name account_name">
											<?=$_SESSION['login']['user_name']?>
										</div>
									</div>
									<div class="kt-notification">
										<a href="<?=base_url()?>profile" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Profile
												</div>
												<div class="kt-notification__item-time">
													Account settings and more
												</div>
											</div>
										</a>
										
										<div class="kt-notification__custom">
											<div class="col-lg-4">
												<a href="#" id="logout" class="btn btn-label-brand btn-sm btn-bold">Sign Out</a>
											</div>
											<?php if(!isset($_SESSION['login']['lokasi_id'])) { ?>
											<div class="col-lg-8" style="display: none">
												<btn data-route="<?=base_url()?>reset-stok" class="btn btn-warning btn-sm btn-bold tombol-reset">Reset Stok</btn>
												<btn data-route="<?=base_url()?>reset-sistem" class="btn btn-danger btn-sm btn-bold tombol-reset">Reset Sistem</btn>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

