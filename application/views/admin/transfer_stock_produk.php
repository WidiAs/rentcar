<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title"><?=$produk->produk_nama.' ('.$produk->spec.')'?></h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>transfer-produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>transfer-produk" class="kt-subheader__breadcrumbs-link">Transfer Stock Produk</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url().'transfer-produk/stock/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Stok</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet">
								<div class="kt-portlet__body kt-portlet__body--fit">
									<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
										<div class="kt-grid__item">

											<!--begin: Form Wizard Nav -->
											<div class="kt-wizard-v3__nav">
												<div class="kt-wizard-v3__nav-items">
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>transfer-produk" data-ktwizard-type="step" data-ktwizard-state="current">
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>1&nbsp;</span><span><i class="flaticon-truck"></i></span> Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>konfirmasi-transfer-produk" data-ktwizard-type="step" >
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>2&nbsp;</span><span><i class="flaticon2-checking"></i></span> Konfirmasi Kiriman
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>history-transfer-produk" data-ktwizard-type="step">
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>3&nbsp;</span><span><i class="flaticon2-time"></i></span> Histori Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
												</div>
											</div>

											<!--end: Form Wizard Nav -->
										</div>
										<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
											<div class="kt-portlet kt-portlet--mobile">
												<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
												<input type="hidden" id="list_url" value="<?=base_url()?>stock-produk/list/<?=$id?>" name="">
												<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
												<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
												<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
												<div class="kt-portlet__body">

													<!--begin: Search Form -->
													<div class="">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="row align-items-center">
																	<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
																		<div class="kt-input-icon kt-input-icon--left">
																			<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																			<span class="kt-input-icon__icon kt-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<table class="datatable table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Lokasi</th>
																<th>No Seri Produk</th>
																<th>Jumlah</th>
																<th width="80">Action</th>
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
														<tfoot >
															<tr>
																<td colspan="3" style="text-align: right"><strong>Total</strong> </td>
																<td ><strong></strong> </td>
																<td></td>
															</tr>
															
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal" id="kt_modal_transfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Transfer Stok</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url().'transfer-produk/stock/transfer/'.$id?>" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
															<div class="col-9">
																<input type="hidden" name="stock_produk_id">
				                                            	<input type="hidden" name="produk_id">
				                                            	<input type="hidden" placeholder="" name="produk_kode" class="form-control" value="<?=$produk->produk_kode?>">
																<label name="suplier_kode" class="col-form-label"><?=$produk->produk_kode?></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama Produk</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="produk_nama" class="form-control" value="<?=$produk->produk_nama?>">
																<label name="suplier_kode" class="col-form-label"><?=$produk->produk_nama?></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">No Seri</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="stock_produk_seri" class="form-control">
																<label name="stock_produk_seri" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Lokasi Asal</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="stock_produk_lokasi_id" class="form-control" value="">
				                                            	<input type="hidden" placeholder="" name="lokasi_nama" class="form-control" value="">
																<label name="lokasi_nama" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Jumlah</label>
															<div class="col-9">
																<label name="stock_produk_qty" class="col-form-label"></label>
															</div>
														</div>														
														<div class="form-group">
				                                            	<label class="form-control-label ">lokasi tujuan<b class="label--required">*</b></label>
				                                            	<input type="hidden" name="produk_id" value="<?=$id?>">
				                                            	<input type="hidden" name="lokasi_tujuan_nama">
				                                            	<select class="form-control col-md-12" name="histori_lokasi_tujuan_id" required="">
				                                            		<option value="" style="display: none;">Pilih Lokasi</option>
				                                            		<?php
				                                            			foreach ($lokasi as $key) {
				                                            			?>
				                                            			<option class="lokasi_option" value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">jumlah <b class="label--required">*</b></label>
				                                            	<input type="hidden" name="stock_produk_qty" value="">
				                                            	<input type="text" placeholder="" name="qty" class="input-numeral form-control" value="" required="">
				                                        </div>
				                                        
				                                        
			                                        </div>
			                                        

	                                        	
											</div>
										</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary" data-page="transfer-stock">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
