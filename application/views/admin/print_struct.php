<!DOCTYPE html>

<html lang="en">


<head>

	<meta charset="utf-8">

	<title>Receipt</title>


	<!-- Normalize or reset CSS with your favorite library -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">


	<!-- Load paper.css for happy printing -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">


	<!-- Set page size here: A5, A4 or A3 -->

	<!-- Set also "landscape" if you need -->

	<style>

		@page {
			size: 58mm;
			margin: 0 0 0 0;
		}

		/* output size */

		body.receipt .sheet {
			width: 58mm;
			padding: 0px
		}

		/* sheet size */

		@media print {
			body.receipt .sheet {
				width: 58mm;
				padding: 0px;
				margin: 0 0 0 0;
			}

			@page {
				margin: 0 0 0 0
			}
		}

		/* fix for Chrome */

		p {
			font-size: 13px;
			margin: 0px;
			font-family: Tahoma, Verdana, Segoe, sans-serif;
		}


		@media screen {

			body {
				background: #e0e0e0
			}

		}

	</style>

</head>


<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


	<h5 style="text-align: center;margin-bottom: 10px">Balioz Linen</h5>
	<p style="margin:0px">Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $trans->no_faktur ?></p>


	<p style="margin:0px">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $trans->tanggal ?></p>
	<p style="margin:0px">Customer:&nbsp;<?= $trans->nama_pelanggan ?></p>
	<p style="margin:0px">
		Staff&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $_SESSION['login']['user_name'] ?></p>
	<p style="margin:0px">
		Lokasi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $trans->lokasi_nama ?></p>
	<div style="border-bottom:1px dashed #000000;">
	</div>

	<table style="width: 100%">

		<tbody>


		<?php
		$count = 0;
		foreach ($trans->detail_transaksi as $key) {
			$count += $key->qty;
			?>
			<tr>
				<td colspan="3"><p><?= $key->produk_nama ?></p></td>
			</tr>

			<tr>
				<td><p><?= number_format($key->qty) . " " . $key->satuan_nama ?></p></td>
				<td><p>x <?= number_format($key->harga) ?></p></td>
				<td><?php if ($key->diskon_nominal && $key->diskon_nominal > 0) { ?>
						<p>- <?= number_format($key->diskon_nominal) ?> </p>
					<?php } ?></td>
				<td style="text-align: right"><p><?= number_format($key->sub_total) ?></p></td>
			</tr>
			<?php
		}
		?>


		</tbody>


	</table>


	<div>
		<div style="border-bottom:1px dashed #000000;">
			<table style="width: 100%">

				<thead>
				<th></th>
				<th></th>
				<th></th>
				</thead>

				<tbody>

				<tr>
					<td colspan="2" style="text-align: right"><p>Total Qty :</p></td>
					<td style="text-align: right"><p><?= $count ?></p></td>
				</tr>

				<?php if ($trans->potongan_akhir > 0 || $trans->nominal_potongan_persen > 0 || $trans->biaya_tambahan > 0) { ?>

					<tr>
						<td colspan="2" style="text-align: right"><p>Total Price :</p></td>
						<td style="text-align: right"><p>Rp.<?= number_format($trans->total) ?></p></td>
					</tr>
					<?php if ($trans->biaya_tambahan > 0) { ?>
						<tr>
							<td colspan="2" style="text-align: right"><p>Additional Cost :</p></td>
							<td style="text-align: right"><p>Rp.<?= number_format($trans->biaya_tambahan) ?></p></td>
						</tr>
					<?php } ?>
					<?php if ($trans->nominal_potongan_persen > 0) { ?>
						<tr>
							<td colspan="2" style="text-align: right"><p>Discount(%) :</p></td>
							<td style="text-align: right"><p><?= number_format($trans->potongan_persen) ?>%</p></td>
						</tr>
					<?php } ?>
					<?php if ($trans->potongan_akhir > 0) { ?>
						<tr>
							<td colspan="2" style="text-align: right"><p>Additional Discount :</p></td>
							<td style="text-align: right"><p>Rp.<?= number_format($trans->potongan_akhir) ?></p></td>
						</tr>
					<?php } ?>
				<?php } ?>

				<tr>
					<td colspan="2" style="text-align: right"><p>Grand Total :</p></td>
					<td style="text-align: right"><p>Rp.<?= number_format($trans->grand_total) ?></p></td>
				</tr>


				<tr>
					<td colspan="2" style="text-align: right"><p>Amount received :</p></td>
					<td style="text-align: right"><p>Rp.<?= number_format($trans->terbayar) ?></p></td>
				</tr>

				<?php if ($trans->kembalian >= 0) { ?>

					<tr>
						<td colspan="2" style="text-align: right"><p>Changes :</p></td>
						<td style="text-align: right"><p>Rp.<?= number_format($trans->kembalian) ?></p></td>
					</tr>

				<?php } else { ?>

					<tr>
						<td colspan="2" style="text-align: right"><p>Remaining payment :</p></td>
						<td style="text-align: right"><p>Rp.<?= number_format($trans->sisa_pembayaran) ?></p></td>
					</tr>

				<?php } ?>

				</tbody>


			</table>
		</div>

		<p style="margin:0px;text-align: center">Thank you for visiting us</p>

		<br>

		<p></p>

		<p></p>

		<?php $time = time() + 3600;
		$date = date_create(date("Y-m-d H:i:s", $time), timezone_open('Asia/Kuala_Lumpur')); ?>

		<p>Log : <?= $_SESSION['login']['user_name'] . " " . date_format($date, 'Y-m-d H:i:s P') ?></p>

</section>

</body>

</html>
