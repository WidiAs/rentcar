<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penawaran</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pos" class="kt-subheader__breadcrumbs-link">Penawaran</a>
			<input type="hidden" id="action" value='<?=$action?>'>
		</div>

	</div>
</div>

<div id="konten-utama">
<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
	<form action="<?= base_url() ?>penawaran/save" method="post" id="save-form">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold"
						role="tablist">
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'penawaran') ? 'active' : '') ?>"
							   href="<?= base_url() ?>penawaran">
								<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penawaran</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'daftar-penawaran') ? 'active' : '') ?>"
							   href="<?= base_url() ?>daftar-penawaran">
								<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Daftar Penawaran</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pelanggan</label>
							<div class="input-group col-10">
								<input type="hidden" class="form-control" name="guest_id" value="">
								<input type="text" class="form-control" name="guest_nama" value="Guest">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" data-toggle="modal"
											data-target="#kt_modal_search_pelanggan"><i class="flaticon-search"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Alamat</label>
							<div class="col-10">
								<input class="form-control" type="text" value="" name="guest_alamat">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Telepon</label>
							<div class="col-10">
								<input class="form-control" type="text" value="" name="guest_telepon">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Penawaran </label>
							<div class="input-group col-10">
								<label for="example-text-input" class="col-form-label"> <?= $no_penawaran ?> </label>
							</div>
							<input type="hidden" name="no_penawaran" value="<?= $no_penawaran ?>">
							<input type="hidden" name="urutan" value="<?= $urutan ?>">
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Tanggal Order </label>
							<div class="input-group col-10">
								<input type="hidden" id="today" value="<?= date('d-m-Y') ?>">
								<input type="text" name="tanggal_order" class="form-control tanggal"
									   value="<?= date('d-m-Y') ?>" readonly>
							</div>
						</div>
						<?php if (!isset($_SESSION['login']['lokasi_id'])) { ?>
							<div class="form-group row" style="display: none">
								<label for="example-text-input" class="col-2 col-form-label">lokasi<b
										class="label--required">*</b></label>
								<div class="col-10">
									<select class="form-control col-md-12" name="lokasi_id" id="lokasi_id" required="">
										<?php
										foreach ($lokasi as $key) {
											?>
											<option value="<?= $key->lokasi_id ?>"><?= $key->lokasi_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						<?php } ?>
					</div>

				</div>
			</div>
		</div>
		<div class="kt-portlet kt-portlet--mobile" style="overflow-x: auto">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal"
									data-target="#kt_modal_search_mobil" id="button_add_trans" data-count="0">
								<i class="flaticon2-plus"></i> Tambah Data
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-hover table-checkable">
					<thead>
					<tr>
						<th width="250">Plat Mobil</th>
						<th>Jenis Mobil</th>
						<th>Tipe Sewa</th>
						<!--						<th width="150">Tanggal Ambil</th>-->
						<!--						<th width="80">Hari</th>-->
						<th width="200">Harga</th>
						<th width="150">Driver</th>
						<th width="150">BBM</th>
						<th>Subtotal</th>
						<th>Keterangan</th>
						<th width="30">Aksi</th>
					</tr>
					</thead>
					<tbody id="item_child"></tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-lg-5 offset-lg-7">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__body row">
						<div class="col-lg-12">
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Total</label>
								<div class="col-8 col-form-label">
									<input type="hidden" name="grand_total" value="0">
									<strong><span class="total" id="total-item">0</span></strong>
								</div>
							</div>
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Redaksi</label>
								<div class="col-8">
									<textarea type="text" id="keterangan_tambahan" class="form-control"
											  name="keterangan_tambahan"
											  style="height : 100px"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="kt-portlet kt-portlet--mobile" style="overflow-x: auto">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-brand btn-icon-sm" id="add-catatan" data-count="0">
								<i class="flaticon2-plus"></i> Tambah Catatan
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-hover table-checkable">
					<thead>
					<tr>
						<th>Catatan</th>
						<th width="30">Aksi</th>
					</tr>
					</thead>
					<tbody id="catatan_child"></tbody>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="modal" id="kt_modal_search_pelanggan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="guest_url" value="<?= base_url() ?>list/guest" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="guest-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Pelanggan</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="guest_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_driver" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="driver_url" value="<?= base_url() ?>list/driver" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="driver-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Driver</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="driver_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_mobil" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="mobil_url" value="<?= base_url() ?>list/mobil" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
						</div>
					</div>
				</div>
				<table class="table table-striped- table-hover table-checkable" id="mobil-table">
					<thead>
					<tr>
						<th>No</th>
						<th>Jenis Mobil</th>
						<th>Warna</th>
						<th>Plat Mobil</th>
						<th>Aksi</th>

					</tr>
					</thead>
					<tbody id="mobil_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="pos-floating-button">
	<button class="btn btn-success btn-pill btn-lg" id="btn-save"><i class="fa fa-arrow-right"></i>&nbsp; Simpan
	</button>
</div>
</div>
