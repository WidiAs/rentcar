<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penyewaan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pos" class="kt-subheader__breadcrumbs-link">Penyewaan</a>
		</div>

	</div>
</div>

<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
	<input type="hidden" id="home-url" value="<?= base_url() ?>penyewaan">
	<form action="<?= base_url() ?>edit-penyewaan/update" method="post" id="save-form">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold"
						role="tablist">
						<li class="nav-item">
							<a class="nav-link active"
							   href="<?= base_url() ?>penyewaan">
								<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penyewaan</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link"
							   href="<?= base_url() ?>daftar-penyewaan">
								<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Daftar Penyewaan</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Penawaran</label>
							<div class="input-group col-10">
								<input type="hidden" class="form-control" name="penawaran_id_old"
									   value="<?= $penyewaan->penawaran_id ?>">
								<input type="hidden" class="form-control" name="penyewaan_id"
									   value="<?= $penyewaan->penyewaan_id ?>">
								<input type="hidden" class="form-control" name="penawaran_id"
									   value="<?= $penyewaan->penawaran_id ?>">
								<input type="text" class="form-control" name="no_penawaran"
									   value="<?= $penyewaan->no_penawaran ?>" readonly>
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" data-toggle="modal"
											data-target="#kt_modal_search_penawaran"><i class="flaticon-search"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Penyewaan </label>
							<div class="input-group col-10">
								<label for="example-text-input"
									   class="col-form-label"> <?= $penyewaan->no_invoice ?> </label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Tanggal Order </label>
							<div class="input-group col-10">
								<input type="hidden" id="today" value="<?= $penyewaan->tanggal_lbl ?>">
								<input type="text" name="tanggal_order" class="form-control tanggal"
									   value="<?= $penyewaan->tanggal_lbl ?>" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Jenis Sewa </label>
							<div class="input-group col-10">
								<select class="form-control" name="jenis_penyewaan">
									<option value="Reguler">Reguler</option>
									<option value="Endors">Endors</option>
								</select>
							</div>
						</div>
						<?php if (!isset($_SESSION['login']['lokasi_id'])) { ?>
							<div class="form-group row" style="display: none">
								<label for="example-text-input" class="col-2 col-form-label">lokasi<b
										class="label--required">*</b></label>
								<div class="col-10">
									<select class="form-control col-md-12" name="lokasi_id" id="lokasi_id" required="">
										<?php
										foreach ($lokasi as $key) {
											?>
											<option value="<?= $key->lokasi_id ?>"><?= $key->lokasi_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pelanggan</label>
							<div class="input-group col-10">
								<input type="hidden" class="form-control" name="guest_id"
									   value="<?= $penyewaan->pelanggan_id ?>">
								<input type="text" class="form-control" name="guest_nama"
									   value="<?= $penyewaan->pelanggan_nama ?>">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" data-toggle="modal"
											data-target="#kt_modal_search_pelanggan"><i class="flaticon-search"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Alamat</label>
							<div class="col-10">
								<input class="form-control" type="text" value="<?= $penyewaan->pelanggan_alamat ?>"
									   name="guest_alamat">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Telepon</label>
							<div class="col-10">
								<input class="form-control" type="text" value="<?= $penyewaan->pelanggan_kontak ?>"
									   name="guest_telepon">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="kt-portlet kt-portlet--mobile" style="overflow-x: auto">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal"
									data-target="#kt_modal_search_mobil" id="button_add_trans" data-count="<?=count($item)?>">
								<i class="flaticon2-plus"></i> Tambah Data
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-hover table-checkable">
					<thead>
					<tr>
						<th width="185">Plat Mobil</th>
						<th width="150">Jenis Mobil</th>
						<th width="150">Tipe Sewa</th>
						<th width="120">Tanggal Ambil</th>
						<th width="120">Tanggal Selesai</th>
						<th width="140">Harga</th>
						<th width="250">Driver</th>
						<th width="100">Biaya Tambahan</th>
						<th width="100">Subtotal</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody id="item_child">
					<?php foreach ($item as $key => $i) { ?>
						<tr id="row_<?=$key?>">
							<td style="display:none">
								<input type="hidden" name="mobil_id[<?=$key?>]" value="<?=$i->mobil_id?>">
								<input type="hidden" name="driver_id[<?=$key?>]" value="<?=$i->driver_id?>">
								<input type="hidden" class="subtotal" name="subtotal[<?=$key?>]" value="<?=$i->sub_total?>">
								<input type="hidden" name="jenis_mobil_nama[<?=$key?>]" value="<?=$i->jenis_mobil_nama?>">
								<input type="hidden" name="biaya_tambahan[<?=$key?>]" value="<?=$i->biaya_tambahan?>">
								<input type="hidden" name="hari[<?=$key?>]" value="<?=$i->hari?>">
								<textarea type="hidden" name="biaya_array[<?=$key?>]"><?=$i->biaya_array?></textarea>
							</td>
							<td>
								<div class="input-group col-12">
									<input type="text" class="form-control" id="plat_<?=$key?>" readonly="" value="<?=$i->plat_mobil?>" name="plat_mobil[<?=$key?>]">
									<div class="input-group-append">
										<button class="btn btn-primary change-mobil" data-toggle="modal" data-target="#kt_modal_search_mobil" type="button" data-no="<?=$key?>">
											<i class="flaticon-search"></i>
										</button>
									</div>
								</div>
							</td>
							<td><span id="jenis_mobil_<?=$key?>"><?=$i->jenis_mobil_nama?></span></td>
							<td><select class="form-control tipe-sewa" name="jenis_sewa[<?=$key?>]" data-no="<?=$key?>">
									<option value="Lepas Kunci" <?=$i->jenis == 'Lepas Kunci' ? 'selected' : ''?>>Lepas Kunci</option>
									<option value="All In" <?=$i->jenis == 'All In' ? 'selected' : ''?>>All In</option>
								</select></td>
							<td>
								<input type="datetime-local" class="form-control tanggal-mobil" name="tanggal_ambil[<?=$key?>]" value="<?=$i->tanggal_ambil_lbl?>" data-no="<?=$key?>">
							</td>
							<td>
								<input type="datetime-local" class="form-control tanggal-mobil" name="tanggal_selesai[<?=$key?>]" value="<?=$i->tanggal_selesai_lbl?>" data-no="<?=$key?>">
							</td>
							<td>
								<input type="text" class="form-control input-numeral-harga harga" value="<?=$i->harga_lbl?>" name="harga[<?=$key?>]" data-no="<?=$key?>">
							</td>
							<td>
								<div class="input-group col-12">
									<input type="text" class="form-control" name="driver_nama[<?=$key?>]" id="driver_<?=$key?>" value="<?=$i->driver_nama?>" readonly="">
									<div class="input-group-append">
										<button class="btn btn-primary driver-search" type="button" id="btn_driver_<?=$key?>" data-no="<?=$key?>" <?=$i->jenis == 'Lepas Kunci' ? 'disabled' : ''?>>
											<i class="flaticon-search"></i>
										</button>
									</div>
								</div>
							</td>
							<td><span id="biaya_tambahan_<?=$key?>"><?=$i->biaya_tambahan_lbl?></span></td>
							<td><span id="subtotal_<?=$key?>"><?=$i->sub_total_lbl?></span></td>
							<td><textarea class="form-control" name="keterangan[<?=$key?>]"><?=$i->keterangan?></textarea></td>
							<td>
								<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group"
									 aria-label="First group">
									<button class="btn btn-warning biaya-btn" type="button" data-no="<?=$key?>">
										<i class="fas fa-dollar-sign"></i>
									</button>
									<button class="btn btn-danger delete-btn" data-no="<?=$key?>">
										<i class="flaticon2-trash"></i>
									</button>
								</div>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-lg-5 offset-lg-7">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__body row">
						<div class="col-lg-12">
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Total</label>
								<div class="col-8 col-form-label">
									<input type="hidden" name="grand_total" value="<?=$penyewaan->grand_total?>">
									<strong><span class="total" id="total-item"><?=$penyewaan->grand_total_lbl?></span></strong>
								</div>
							</div>
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Keterangan</label>
								<div class="col-8">
									<textarea type="text" id="keterangan_tambahan" class="form-control"
											  name="keterangan_tambahan"
											  style="height : 100px"><?=$penyewaan->keterangan?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="modal" id="kt_modal_search_penawaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="penawaran_url" value="<?= base_url() ?>list/penawaran" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Penawaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="penawaran-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>No Penawaran</th>
						<th>Nama Pelanggan</th>
						<th>Tanggal Order</th>
						<th>Grand Total</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="penawaran_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_pelanggan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="guest_url" value="<?= base_url() ?>list/guest" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="guest-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Pelanggan</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="guest_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_driver" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="driver_url" value="<?= base_url() ?>list/driver" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="driver-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Driver</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="driver_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_mobil" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="mobil_url" value="<?= base_url() ?>list/mobil" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Mobil</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>
			<div class="modal-body">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
						</div>
					</div>
				</div>
				<table class="table table-striped- table-hover table-checkable" id="mobil-table">
					<thead>
					<tr>
						<th>No</th>
						<th>Jenis Mobil</th>
						<th>Warna</th>
						<th>Plat Mobil</th>
						<th>Aksi</th>

					</tr>
					</thead>
					<tbody id="mobil_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal show" id="modal-biaya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Biaya Lainnya</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="nomor-biaya">
				<input type="hidden" id="biaya-count">
				<div align="right">
					<button id="tambah-biaya-list" type="button" class="btn btn-primary">Tambah Biaya</button>
				</div>
				<div class="row">
					<table border="0" class="col-md-12">
						<thead>
						<tr>
							<th>Jenis Biaya</th>
							<th>Jumlah</th>
							<th>Aksi</th>

						</tr>
						</thead>
						<tbody id="biaya-list">

						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-lg-8">
					<div class="form-group kt-form__group row">
						<label for="example-text-input" class="col-2 col-form-label">Total </label>
						<div class="col-6 col-form-label">
							<strong><span id="total-biaya">0</span></strong>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="simpan-biaya">Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="pos-floating-button">
	<button class="btn btn-success btn-pill btn-lg" id="btn-save"><i class="fa fa-arrow-right"></i>&nbsp; Simpan
	</button>
</div>
