<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penyewaan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pos" class="kt-subheader__breadcrumbs-link">Penyewaan</a>
		</div>

	</div>
</div>

<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
	<form action="<?= base_url() ?>invoice-penyewaan/save/<?=$penyewaan->penyewaan_id?>" method="post" id="save-form">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold"
						role="tablist">
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'penyewaan') ? 'active' : '') ?>"
							   href="<?= base_url() ?>penyewaan">
								<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penyewaan</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'daftar-penyewaan') ? 'active' : '') ?>"
							   href="<?= base_url() ?>daftar-penyewaan">
								<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Daftar Penyewaan</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Penyewaan </label>
							<div class="input-group col-10">
								<label for="example-text-input"
									   class="col-form-label"> <?= $penyewaan->no_invoice ?> </label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Tanggal Order </label>
							<div class="input-group col-10">
								<input type="hidden" id="today" value="<?= date('d-m-Y') ?>">
								<input type="text" name="tanggal_order" class="form-control"
									   value="<?= $penyewaan->tanggal_order ?>" readonly>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pelanggan</label>
							<div class="input-group col-10">
								<input type="text" class="form-control" name="guest_nama"
									   value="<?= $penyewaan->pelanggan_nama ?>" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Alamat</label>
							<div class="col-10">
								<input class="form-control" type="text" value="<?= $penyewaan->pelanggan_alamat ?>"
									   name="guest_alamat" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Telepon</label>
							<div class="col-10">
								<input class="form-control" type="text" value="<?= $penyewaan->pelanggan_kontak ?>"
									   name="guest_telepon" readonly>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="kt-portlet kt-portlet--mobile" style="overflow-x: auto">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal"
									data-target="#kt_modal_search_mobil" id="button_add_trans" data-count="<?=count($penyewaan_mobil) + 1?>">
								<i class="flaticon2-plus"></i> Tambah Data
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-hover table-checkable">
					<thead>
					<tr>
						<th width="185">Plat Mobil</th>
						<th width="130">Jenis Mobil</th>
						<th width="160">Tipe Sewa</th>
						<th width="140">Tanggal Ambil</th>
						<th width="50">Hari</th>
						<th width="130">Harga</th>
						<th width="130">Luar Kota</th>
						<th width="200">Driver</th>
						<th width="100">Biaya Tambahan</th>
						<th width="100">Subtotal</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody id="item_child">
					<?php foreach ($penyewaan_mobil as $key => $value){ ?>
						<tr id="row_<?=$key?>">
							<td style="display:none">
								<input type="hidden" name="mobil_id[<?=$key?>]" value="<?=$value->mobil_id?>">
								<input type="hidden" name="driver_id[<?=$key?>]" value="<?=$value->driver_id?>">
								<input type="hidden" class="hari" name="hari[<?=$key?>]" value="<?=$value->hari?>">
								<input type="hidden" name="hari_luar_kota[<?=$key?>]" value="<?=$value->hari_luar_kota?>">
								<input type="hidden" name="total_jadwal[<?=$key?>]" value="<?=$value->hari * $value->harga?>">
								<input type="hidden" name="total_overtime[<?=$key?>]" value="<?=$value->total_overtime?>">
								<input type="hidden" class="subtotal" name="subtotal[<?=$key?>]" value="<?=$value->sub_total?>">
								<input type="hidden" name="jenis_mobil_nama[<?=$key?>]" value="<?=$value->jenis_mobil_nama?>">
								<input type="hidden" name="biaya_tambahan[<?=$key?>]" value="<?=$value->biaya_tambahan?>">
								<input type="hidden" name="biaya_array[<?=$key?>]" value='<?=json_encode($value->biaya_tambahan_list)?>'>
								<input type="hidden" name="jadwal_mobil[<?=$key?>]" value='<?=json_encode($value->jadwal_mobil)?>'>
							</td>
							<td>
								<div class="input-group col-12">
									<input type="text" class="form-control" id="plat_<?=$key?>" readonly="" value="<?=$value->plat_mobil?>" name="plat_mobil[<?=$key?>]">
									<div class="input-group-append">
										<button class="btn btn-primary change-mobil" data-toggle="modal" data-target="#kt_modal_search_mobil" type="button" data-no="<?=$key?>">
											<i class="flaticon-search"></i>
										</button>
									</div>
								</div>
							</td>
							<td><span id="jenis_mobil_<?=$key?>"><?=$value->jenis_mobil_nama?></span></td>
							<td>
								<select class="form-control tipe-sewa" name="jenis_sewa[<?=$key?>]" data-no="<?=$key?>">
									<option value="Lepas Kunci" <?=$value->jenis == 'Lepas Kunci' ? 'selected' : '' ?>>Lepas Kunci</option>
									<option value="All In" <?=$value->jenis == 'All In' ? 'selected' : '' ?> >All In</option>
								</select>
							</td>
							<td>
								<input type="text" class="form-control tanggal" name="tanggal_ambil[<?=$key?>]" value="<?=$value->tanggal_ambil?>" readonly="">
							</td>
							<td>
								<span id="hari-label-<?=$key?>"><?=$value->hari?></span>
							</td>
							<td>
								<input type="text" class="form-control input-numeral harga" value="<?=$value->harga?>" name="harga[<?=$key?>]" data-no="<?=$key?>">
							</td>
							<td>
								<input type="text" class="form-control input-numeral harga-luar-kota" value="0" name="harga_luar_kota[<?=$key?>]" data-no="<?=$key?>">
							</td>
							<td>
								<div class="input-group col-12">
									<input type="text" class="form-control" name="driver_nama[<?=$key?>]" id="driver_<?=$key?>" value="<?=$value->driver_nama?>" <?=$value->jenis == 'Lepas Kunci' ? 'readonly' : '' ?>>
									<div class="input-group-append">
										<button class="btn btn-primary driver-search" type="button" id="btn_driver_<?=$key?>" data-no="<?=$key?>" <?=$value->jenis == 'Lepas Kunci' ? 'disabled' : '' ?>><i class="flaticon-search"></i></button>
									</div>
								</div>
							</td>
							<td><span id="biaya_tambahan_<?=$key?>"><?=$value->biaya_tambahan?></span></td>
							<td><span id="subtotal_<?=$key?>"><?=$value->sub_total?></span></td>
							<td><textarea class="form-control" name="keterangan[<?=$key?>]"></textarea></td>
							<td>
								<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group">
									<button class="btn btn-success jadwal-mobil" type="button" data-no="<?=$key?>">
										<i class="fa fa-calendar-check"></i></button>
									<button class="btn btn-warning biaya-btn" type="button" data-no="<?=$key?>">
										<i class="fas fa-dollar-sign"></i></button>
									<button class="btn btn-danger delete-btn" data-no="<?=$key?>">
										<i class="flaticon2-trash"></i>
									</button>
								</div>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-lg-5 offset-lg-7">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__body row">
						<div class="col-lg-12">
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Total</label>
								<div class="col-8 col-form-label">
									<input type="hidden" name="grand_total" value="<?=$penyewaan->grand_total?>">
									<strong><span class="total" id="total-item"><?=$penyewaan->grand_total?></span></strong>
								</div>
							</div>
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-4 col-form-label">Keterangan</label>
								<div class="col-8">
									<textarea type="text" id="keterangan_tambahan" class="form-control"
											  style="height : 100px" readonly></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="modal" id="kt_modal_search_driver" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="driver_url" value="<?= base_url() ?>list/driver" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="driver-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Driver</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="driver_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_mobil" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="mobil_url" value="<?= base_url() ?>list/mobil" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Mobil</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
						</div>
					</div>
				</div>
				<table class="table table-striped- table-hover table-checkable" id="mobil-table">
					<thead>
					<tr>
						<th>No</th>
						<th>Jenis Mobil</th>
						<th>Warna</th>
						<th>Plat Mobil</th>
						<th>Aksi</th>

					</tr>
					</thead>
					<tbody id="mobil_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal show" id="modal-biaya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Biaya Lainnya</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="nomor-biaya">
				<input type="hidden" id="biaya-count">
				<div align="right">
					<button id="tambah-biaya-list" type="button" class="btn btn-primary">Tambah Biaya</button>
				</div>
				<div class="row">
					<table border="0" class="col-md-12">
						<thead>
						<tr>
							<th>Jenis Biaya</th>
							<th>Jumlah</th>
							<th>Aksi</th>

						</tr>
						</thead>
						<tbody id="biaya-list">

						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-lg-8">
					<div class="form-group kt-form__group row">
						<label for="example-text-input" class="col-2 col-form-label">Total </label>
						<div class="col-6 col-form-label">
							<strong><span id="total-biaya">0</span></strong>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="simpan-biaya">Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="modal show" id="modal-jadwal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Kegiatan Mobil</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="nomor-jadwal">
				<input type="hidden" id="jadwal-count">
				<input type="hidden" id="area-jadwal">
				<input type="hidden" id="harga-normal">
				<input type="hidden" id="harga-luar">
				<input type="hidden" id="total-hari">
				<input type="hidden" id="total-hari-luar">
				<input type="hidden" id="total-hari-global">
				<input type="hidden" id="total-nominal-jadwal">
				<input type="hidden" id="total-nominal-over">
				<div align="right">
					<button id="tambah-jadwal-list" type="button" class="btn btn-primary">Tambah Kegiatan</button>
				</div>
				<div class="row">
					<table border="0" class="col-md-12">
						<thead>
						<tr>
							<th width="120">Tanggal</th>
							<th>Area</th>
							<th width="150">Overtime</th>
							<th width="50">Aksi</th>

						</tr>
						</thead>
						<tbody id="jadwal-list">

						</tbody>
					</table>
				</div>
				<div class="col-lg-12">
					<div class="form-group kt-form__group row" style="margin-bottom : 0px !important;">
						<label for="example-text-input" class="col-4 col-form-label">Total Overtime</label>
						<div class="col-6 col-form-label">
							<strong><span id="total-over">0</span></strong>
						</div>
					</div>
					<div class="form-group kt-form__group row" style="margin-bottom : 0px !important;">
						<label for="example-text-input" class="col-4 col-form-label">Total </label>
						<div class="col-6 col-form-label">
							<strong><span id="total-jadwal">0</span></strong>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="simpan-jadwal">Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="pos-floating-button">
	<button class="btn btn-success btn-pill btn-lg" id="btn-save"><i class="fa fa-arrow-right"></i>&nbsp; Simpan
	</button>
</div>
