<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Penawaran</title>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<style type="text/css">
		.table {
			width: 100%;
			margin-bottom: 1rem;
			color: #212529;
			background-color: transparent;
			border-collapse: collapse;
		}

		.table th,
		.table td {
			padding: 0.75rem;
			vertical-align: top;
			border-top: 1px solid #ebedf2;
			margin-top: 0px;
			margin-bottom: 0px;
		}

		.table thead th {
			vertical-align: bottom;
			border-bottom: 2px solid #ebedf2;
		}

		.table tbody + tbody {
			border-top: 1px solid #ebedf2;
		}

		.table-sm th,
		.table-sm td {
			padding: 0.3rem;
		}

		.table-bordered {
			border: 1px solid #ebedf2;
		}

		.table-bordered th,
		.table-bordered td {
			border: 1px solid #ebedf2;
		}

		.table-bordered thead th,
		.table-bordered thead td {
			border-bottom-width: 1px;
		}

		.table-borderless th,
		.table-borderless td,
		.table-borderless thead th,
		.table-borderless tbody + tbody {
			border: 0;
		}

		.table-striped tbody tr:nth-of-type(odd) {
			background-color: #f7f8fa;
		}

		.table-hover tbody tr:hover {
			color: #212529;
			background-color: #fafbfc;
		}

		body {
			font-family: 'Poppins';
			font-size: 12px;
		}
	</style>
</head>
<body>
<header>
	<table class="datatable table table-borderless">
		<thead>
		<tr>
			<td width="20%"><img src="<?= base_url() ?>assets/media/logos/waisnawa.png" width="150px" height="auto">
			</td>
			<td width="90%" style="text-align: center; font-size: 17px"><strong>CV. Waisnawa Transport Group</strong>
				<br>Office: Jl. Penyaringan Gg. Tiying No. 3 Sanur Kauh - Denpasar Selatan
				<br>0811 393 1234/081 353 996 698/0811 395 7887/0361 4748201
				<br>cwaisnawatransgroup@gmail.comm
			</td>
		</tr>
		</thead>
	</table>
	<hr>
</header>

<div id="container">
	<table class="datatable table table-borderless" style="margin-bottom: 0px">
		<thead>
		<tr>
			<td width="10%">Nomor</td>
			<td width="1%">:</td>
			<td width="89%"><?= $trans->no_penawaran ?></td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Hal</td>
			<td>:</td>
			<td>Penawaran Harga</td>
		</tr>
		<tr>
			<td>Kepada</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Yth.</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?= $trans->pelanggan_nama ?></td>
		</tr>
		<tr>
			<td>Di-</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?= $trans->pelanggan_alamat ?></td>
		</tr>
		</tbody>
	</table>
	<table class="datatable table table-borderless" style="margin-bottom: 0px">
		<tbody>
		<tr>
			<td>Yang bertanda tangan dibawah ini :</td>
		</tr>
		</tbody>
	</table>
	<table class="datatable table table-borderless" style="margin-bottom: 0px">
		<thead>
		<tr>
			<td width="10%">Nama</td>
			<td width="1%">:</td>
			<td width="89%"><?= $user->staff_nama ?></td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Jabatan</td>
			<td>:</td>
			<td><?= $user->staff_status ?></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>:</td>
			<td><?= $user->staff_alamat ?></td>
		</tr>
		</tbody>
	</table>

	<table class="datatable table table-borderless" style="margin-bottom: 0px">
		<tbody>
		<tr>
			<td><?= $trans->keterangan_tambahan ?></td>
		</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
		<thead>
		<tr>
			<th>No</th>
			<th>Jenis Kendaraan</th>
			<th>Tipe Sewa</th>
			<th>Sewa Harian</th>
			<th>Driver</th>
			<th>BBM</th>
			<th>Keterangan</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$no = 1;
		foreach ($trans->detail as $key) {
			?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $key->jenis_mobil_nama ?></td>
				<td><?= $key->jenis ?></td>
				<td style="text-align: right;"><?= $key->harga_lbl ?></td>
				<td style="text-align: right;"><?= $key->driver_harga_lbl ?></td>
				<td style="text-align: right;"><?= $key->bbm_lbl ?></td>
				<td><?= $key->keterangan ?></td>
			</tr>
			<?php
		}
		?>
		</tbody>
		<tfoot></tfoot>
	</table>
	<p>Demikian penawaran kami dan bersama ini pula telah kami lampirkan surat-surat sesuai dengan persyaratan yang
		telah ditentukan <br>
		<?= !empty($trans->catatan) ? 'Catatan :' : '' ?></p>
	<ul style="margin-left: 0px">
		<?php foreach ($trans->catatan as $catatan) { ?>
			<li><?=$catatan->catatan_text?></li>
		<?php } ?>
	</ul>
</div>

</body>
</html>
