<html>
<head>
	<meta charset="utf-8"/>
	<title>Print Barcode</title>
	<meta name="description" content="Child datatable from remote data">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">


	<!-- Load paper.css for happy printing -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	<script>
		WebFont.load({
			google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<style>

		@page {
			size: 80mm;
			margin: 0 0 0 0;
		}

		/* output size */

		body.receipt .sheet {
			width: 109mm;
			height: 100%;
			padding: 0px
		}

		hr {
			border: 1px solid black;
		}

		/* sheet size */

		@media print {
			body.receipt .sheet {
				width: 109mm;
				padding: 0px;
				margin: 0 0 0 0;
			}

			@page {
				margin: 0 0 0 0
			}
		}

		/* fix for Chrome */

		p {
			font-size: 10px;
			margin: 0px;
			font-family: Tahoma, Verdana, Segoe, sans-serif;
		}


		@media screen {

			body {
				background: #e0e0e0
			}

		}

	</style>
	<link href="<?= base_url() ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css"/>

</head>
<body class="receipt" onload="window.print()">
<!--<body class="receipt">-->
<section class="sheet" style="padding: 3mm 0mm 0mm 2mm">
	<div class="row" style="margin-left: 2mm;margin-right: 0mm;">

		<?php
		$jumlah_cetak = $this->uri->segment(3);
		for ($i = 0; $i < $jumlah_cetak; $i++) {
			?>

			<div
				style="padding: 1mm 0mm 1mm 0mm;background-color: #FFF;width: 100mm;height: 55mm;display:block; margin-bottom:-9mm;">
				<hr style="border: 1px solid black">
				<p style="text-align:center;font-weight: bold;font-family:Tahoma, Verdana, Segoe, sans-serif;color:#000000;font-size:13px;margin-bottom:0px"><?= $produk->produk_kode ?></p>
				<img src="<?= base_url() . 'stock-produk/barcode/' . $this->uri->segment(2) ?>"
					 style="width: 80mm;height: 45%;display:block; margin:auto;">
				<p style="text-align:center;font-weight: bold;font-family:Tahoma, Verdana, Segoe, sans-serif;color:#000000;font-size:13px"><?= $produk->produk_nama ?></p>
				<p style="text-align:center;font-weight: bold;font-family:Tahoma, Verdana, Segoe, sans-serif;color:#000000;font-size:13px; margin-top:-18px !important;">Harga : Rp.<?= $produk->harga_eceran ?></p>
				<hr style="border: 1px solid black">
			</div>

			<?php

		}
		?>
	</div>
</section>
</body>
</html>
