<!-- begin:: Content -->
                        <form action="<?=base_url().'user-role/access/edit/'.$id?>" method="post" id="form-send">
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">User Role</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user_role" class="kt-subheader__breadcrumbs-link">Master Data</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user_role" class="kt-subheader__breadcrumbs-link">User Role</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Data Akses Role
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
                                <table class="table table-striped table-bordered table-hover datatable-no-pagination dataTable no-footer dtr-inline" id="role_table">
                            <thead>
                                <tr >
                                    <th width="30">No</th>
                                    <th width="250">Nama Menu</th>
                                    <th width="250">Hak Akses</th>
                                    <th>Akses Tombol Aksi</th>
                                    <th width="250">Data</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                   <?php $akses_role=json_decode($akses_role,true);$no=1;$parent='';$sub_parent_no=100; 
                                   foreach ($menu as $key) {
                                       $text_no = $no;
                                       $parent_name = '<strong>'.$key->menu_nama.'</strong>';                                     
                                       $input_name_general = 'role['.$key->menu_kode.'][akses_menu]';
                                       $globalValue = $akses_role[''.$key->menu_kode]['akses_menu'];
                                       $class = 'check-akses parent-super';
                                       $action = '';
                                       $displayData = '';
                                       $data = 'data-no="'.$no.'"';
                                       if($key->action != null){
                                            $temp = '';
                                            foreach ($key->action as $item) {
                                                $title = str_replace('_', ' ',$item);
                                                $title = ucwords($title);
                                                $value = $akses_role[''.$key->menu_kode][''.$item];
                                                $childClass = "check-akses child-super";
                                                $temp .='<label class="m-checkbox m-checkbox--success">
                                                            <input type="checkbox" class="'.$childClass.'" '.$data.' name="role['.$key->menu_kode.']['.$item.']" value="true"'.(($value) ? "checked=''" : "").' >
                                                            '.$title.'
                                                            <span></span>
                                                        </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                            }
                                            $action = $temp;
                                       }                                     
                                       if($parent != $key->menu_nama){
                                            if($key->sub_menu_kode != null){                                              
                                                ?>
                                                    <tr>
                                                        <td><?=$text_no?></td>
                                                        <td><?=$parent_name?></td>
                                                        <td>
                                                            <label class="m-checkbox m-checkbox--success">
                                                            <input type="checkbox" class="<?=$class?>" <?=$data?> name="<?=$input_name_general?>" value="true" <?=(($globalValue) ? "checked=''" : "")?>>
                                                                Akses Menu
                                                            <span></span>
                                                            </label>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>                                                
                                                <?php
                                                $input_name_general = 'role['.$key->menu_kode.']['.$key->sub_menu_kode.'][akses_menu]';
                                                $text_no = '';
                                                $parent_name = '<i class="la la-arrow-circle-o-right"></i>&nbsp;'.$key->sub_menu_nama;
                                                $globalValue = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode]['akses_menu'];
                                                $class = 'check-akses parent';
                                                $sub_parent_no ++;
                                                $data = 'data-no="'.$sub_parent_no.'" data-no-parent="'.$no.'"';
                                               if($key->sub_action != null){
                                                    $temp = '';
                                                    foreach ($key->sub_action as $item) {
                                                        $title = str_replace('_', ' ',$item);
                                                        $title = ucwords($title);
                                                        $value = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode][''.$item];
                                                        $childClass = "check-akses child";
                                                        $temp .='<label class="m-checkbox m-checkbox--success">
                                                                    <input type="checkbox" class="'.$childClass.'" '.$data.' name="role['.$key->menu_kode.']['.$key->sub_menu_kode.']['.$item.']" value="true" '.(($value) ? "checked=''" : "").'>
                                                                    '.$title.'
                                                                    <span></span>
                                                                </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    }
                                                    $action = $temp;
                                               }
                                               if($key->data != null){
                                                    $temp = '';
                                                    foreach ($key->data as $item) {
                                                        $title = str_replace('_', ' ',$item);
                                                        $title = ucwords($title);
                                                        $value = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode]["data"][''.$item];
                                                        $childClass = "check-akses child-display";
                                                        $temp .='<label class="m-checkbox m-checkbox--success">
                                                                    <input type="checkbox" class="'.$childClass.'" '.$data.' name="role['.$key->menu_kode.']['.$key->sub_menu_kode.'][data]['.$item.']" value="true" '.(($value) ? "checked=''" : "").'>
                                                                    '.$title.'
                                                                    <span></span>
                                                                </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    }
                                                    $displayData = $temp;
                                                }                                                 
                                            }
                                            $parent = $key->menu_nama;                                           
                                            $no++;
                                       } else {
                                            $input_name_general = 'role['.$key->menu_kode.']['.$key->sub_menu_kode.'][akses_menu]';
                                            $text_no = '';
                                            $parent_name = '<i class="la la-arrow-circle-o-right"></i>&nbsp;'.$key->sub_menu_nama;
                                            $globalValue = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode]['akses_menu'];
                                            $class = 'check-akses parent';
                                            $sub_parent_no ++;
                                            $data = 'data-no="'.$sub_parent_no.'" data-no-parent="'.($no-1).'"';
                                               if($key->sub_action != null){
                                                    $temp = '';
                                                    foreach ($key->sub_action as $item) {
                                                        $title = str_replace('_', ' ',$item);
                                                        $title = ucwords($title);
                                                        $value = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode][''.$item];
                                                        $childClass = "check-akses child";
                                                        $temp .='<label class="m-checkbox m-checkbox--success">
                                                                    <input type="checkbox" class="'.$childClass.'" '.$data.' name="role['.$key->menu_kode.']['.$key->sub_menu_kode.']['.$item.']" value="true" '.(($value) ? "checked=''" : "").'>
                                                                    '.$title.'
                                                                    <span></span>
                                                                </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    }
                                                    $action = $temp;
                                               } 
                                                if($key->data != null){
                                                    $temp = '';
                                                    foreach ($key->data as $item) {
                                                        $title = str_replace('_', ' ',$item);
                                                        $title = ucwords($title);
                                                        $value = $akses_role[''.$key->menu_kode][''.$key->sub_menu_kode]["data"][''.$item];
                                                        $childClass = "check-akses child";
                                                        $temp .='<label class="m-checkbox m-checkbox--success">
                                                                    <input type="checkbox" class="'.$childClass.'" '.$data.' name="role['.$key->menu_kode.']['.$key->sub_menu_kode.'][data]['.$item.']" value="true" '.(($value) ? "checked=''" : "").'>
                                                                    '.$title.'
                                                                    <span></span>
                                                                </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    }
                                                    $displayData = $temp;
                                               }                                             
                                       }
                                       ?>
                                        <tr>
                                            <td><?=$text_no?></td>
                                            <td><?=$parent_name?></td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" class="<?=$class?>" <?=$data?> name="<?=$input_name_general?>" value="true" <?=(($globalValue) ? "checked=''" : "")?>>
                                                    Akses Menu
                                                <span></span>
                                                </label>
                                            </td>
                                            <td><?=$action?></td>
                                            <td><?=$displayData?></td>
                                        </tr>
                                       <?php
                                   } ?>
                                    
                                    
                            </tbody>
                        </table>									
								</div>
								
							</div>
						</div>
					<div class="pos-floating-button" style="left: 40%">
                                <button type="button" class="btn btn-primary btn-lg m-btn btn--custom btn--pill btn--icon btn--air" id="submit-btn">
                                            <span>
                                                <i class="la la-check"></i>
                                                <span>Perbarui Role User</span>
                                            </span>
                                </button>

                                <a href="<?=base_url()?>user-role" class="btn-produk-add btn btn-warning btn-lg btn btn--custom btn--pill btn--icon btn--air">
                                            <span>
                                                <i class="la la-angle-double-left"></i>
                                                <span>Kembali ke Daftar</span>
                                            </span>
                                </a>
                            </div>
                        </form>