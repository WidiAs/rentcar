<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Staff</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user" class="kt-subheader__breadcrumbs-link">Master Data</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user" class="kt-subheader__breadcrumbs-link">Staff</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Data Staff
										</h3>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url()?>staff/list" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									</div>
									<?php $akses = json_decode($action,true);if($akses["add"]){ ?>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
													<i class="flaticon2-plus"></i> Tambah Data
												</button>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Search Form -->
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-1">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<table class="datatable table table-striped- table-hover table-checkable" >
										<thead>
											<tr>
												<th width="30">No</th>
												<th>Nama</th>
												<th>Alamat</th>
												<th>Email</th>
												<th>No telepon</th>
												<th>Status</th>
												<th>Lama Bekerja</th>
												<th width="200">Action</th>
											</tr>
										</thead>
										<tbody id="child_data_ajax"></tbody>
									</table>
								</div>
								
							</div>
						</div>
						<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Tambah Staf</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
										<form action="<?=base_url()?>staff/add" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
			                                            	<label class="form-control-label required">NIK <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="1115051039" name="nik" class="form-control" value=""  id="nik">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Nama <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="John Lennon" name="staff_nama" class="form-control" value=""  id="staff_nama" >
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Tempat Lahir <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="Gianyar" name="tempat_lahir" class="form-control" value=""  id="tempat_tanggal_lahir">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Tanggal Lahir <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="21-01-1987" name="tanggal_lahir" class="form-control tanggal" value=""  id="tanggal_lahir">
			                                        	</div>			                                        	
			                                        	<div class="form-group">
			                                            	<label class="control-label">Alamat <b class="label--required">*</b></label>
			                                            	<textarea class="form-control" id="staff_alamat" rows="3" name="staff_alamat"></textarea>
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Status <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="" name="staff_status" class="form-control" value=""  id="staff_status">
			                                        	</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Jenis Kelamin <b class="label--required">*</b></label>
															<div class="kt-radio-inline">
																<label class="kt-radio">
																	<input type="radio" name="staff_kelamin" id="staff_kelamin1" value="Laki-laki" checked=""> Laki-laki
																	<span></span>
																</label>
																<label class="kt-radio">
																	<input type="radio" name="staff_kelamin" id="staff_kelamin2" value="Perempuan"> Perempuan
																	<span></span>
																</label>
															</div>	
														</div>
														<div class="form-group">
			                                            	<label class="control-label">Mulai Bekerja <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="2011-02-20" readonly name="mulai_bekerja" class="form-control" value=""  id="mulai_bekerja">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Email <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="wayan@gmail.com" name="staff_email" class="form-control" value=""  id="staff_email">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Telepon <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="0857944627688" name="staff_phone_number" class="form-control" value=""  id="staff_phone_number">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Keterangan</label>
			                                            	<textarea class="form-control" id="staff_keterangan" rows="3" name="staff_keterangan"></textarea>
			                                        	</div>
													</div>
												</div>

	                                        	
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Detail Staf</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">NIK</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="nik" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_nama" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Tempat Lahir</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="tempat_lahir" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Tanggal Lahir</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="tanggal_lahir" class="col-form-label"></label>
															</div>
														</div>														
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Alamat</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_alamat" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Status</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_status" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Jenis Kelamin</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_kelamin" class="col-form-label"></label>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">NIK</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="nik" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Mulai Bekerja</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="mulai_bekerja" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Email</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_email" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Telepon</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_phone_number" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="staff_keterangan" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Dibuat Pada</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="created_at" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Diubah Pada</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="updated_at" class="col-form-label"></label>
															</div>
														</div>
													</div>
												</div>

	                                        	
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Edit Staf</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
										<form action="<?=base_url()?>staff/edit" method="post" id="kt_edit_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<input type="hidden" name="staff_id">
														<div class="form-group">
			                                            	<label class="form-control-label required">NIK <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="1115051039" name="nik" class="form-control" value=""  id="nik">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Nama <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="John Lennon" name="staff_nama" class="form-control" value=""  id="staff_nama" >
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Tempat Lahir <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="Gianyar" name="tempat_lahir" class="form-control" value=""  id="tempat_tanggal_lahir">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Tanggal Lahir <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="21-01-1987" name="tanggal_lahir" class="form-control tanggal" value=""  id="tanggal_lahir">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Alamat <b class="label--required">*</b></label>
			                                            	<textarea class="form-control" id="staff_alamat" rows="3" name="staff_alamat"></textarea>
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Status <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="" name="staff_status" class="form-control" value=""  id="staff_status">
			                                        	</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Jenis Kelamin <b class="label--required">*</b></label>
															<div class="kt-radio-inline">
																<label class="kt-radio">
																	<input type="radio" name="staff_kelamin" id="staff_kelamin1" value="Laki-laki" checked=""> Laki-laki
																	<span></span>
																</label>
																<label class="kt-radio">
																	<input type="radio" name="staff_kelamin" id="staff_kelamin2" value="Perempuan"> Perempuan
																	<span></span>
																</label>
															</div>	
														</div>
														<div class="form-group">
			                                            	<label class="control-label">Mulai Bekerja <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="2011-02-20" readonly name="mulai_bekerja" class="form-control" value=""  id="mulai_bekerja">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Email <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="wayan@gmail.com" name="staff_email" class="form-control" value=""  id="staff_email">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Telepon <b class="label--required">*</b></label>
			                                            	<input type="text" placeholder="0857944627688" name="staff_phone_number" class="form-control" value=""  id="staff_phone_number">
			                                        	</div>
			                                        	<div class="form-group">
			                                            	<label class="control-label">Keterangan</label>
			                                            	<textarea class="form-control" id="staff_keterangan" rows="3" name="staff_keterangan"></textarea>
			                                        	</div>
													</div>
												</div>

	                                        	
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
