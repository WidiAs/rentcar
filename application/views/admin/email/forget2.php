<table style="border-spacing:0;border-collapse:collapse;vertical-align:top;height:100%;width:100%;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;background:#fff" width="100%" cellspacing="0" cellpadding="0" align="left">
	<tbody>
	<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
		<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
			<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;background:#ffffff;width:640px;margin:0 auto;float:none;text-align:center" width="640" cellspacing="0" cellpadding="0" align="center">
				<tbody>
				<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
					<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
						<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
							<tbody>
							<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
								<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px;border-collapse:collapse" align="left" valign="top" height="24px">&nbsp;</td>
							</tr>
							</tbody>
						</table>
						<table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;width:100%;display:table" width="100%" cellspacing="0" cellpadding="0" align="left">
							<tbody>
							<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
								<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:1.3;margin:0 auto;padding-bottom:16px;width:564px;padding-left:52px;padding-right:52px" align="left">
									<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
										<tbody>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left"><center><img style="outline:none;text-decoration:none;width:auto;max-width:100%;clear:both;display:block;margin:0 auto;float:none;text-align:center" src="https://ci6.googleusercontent.com/proxy/z5uHKD_J6jV_ypzfv_cpLnLSAet8i__VyhNCV6nTGZXRaJVbL0pPgQjGwbMzHmYRt4RMjubZa_drWY9jWj3gSxrzd9XptA=s0-d-e1-ft#https://dzqu55nkc9yyb.cloudfront.net/panel/logo.png" alt="YouGov" class="CToWUd"></center></th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
										</tr>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left"><center><img style="outline:none;text-decoration:none;width:auto;max-width:100%;clear:both;display:block;margin:0 auto;float:none;text-align:center" src="https://ci4.googleusercontent.com/proxy/tmybQDcGN9yrX2DNKpstwMi9-62au8zQx_tRIRgi4aT1pceSk6bouEB9TsSD_hBpJGAN3gWCjhPi08Nv_eZMtHEkRzFu6PrUykntvQpKHLz9fzMq=s0-d-e1-ft#https://dzqu55nkc9yyb.cloudfront.net/panel/new_survey_welcome.gif" alt="YouGov" class="CToWUd"></center></th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
										</tr>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left">
												<h1 style="padding:0;margin:0;font-size:24px;color:#332c41;word-wrap:normal;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.25;letter-spacing:-0.012em;margin-bottom:26px;text-align:center">You have been selected for a special survey</h1>
												<p style="letter-spacing:-0.019em;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;font-size:16px;line-height:1.3;margin-bottom:10px;opacity:0.7;text-align:center">This survey will take 5 minutes and it is worth extra points!</p>
											</th>
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px" align="left" width="18px">&nbsp;</th>
										</tr>
										</tbody>
									</table>
								</th>
							</tr>
							</tbody>
						</table>
						<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;margin:0 auto;float:none;text-align:center;width:auto" width="100%" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
								<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
									<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left" width="100%" cellspacing="0" cellpadding="0" align="left">
										<tbody>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<td style="vertical-align:top;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;color:#ffffff;width:290px;background-color:#fff;background:linear-gradient(to bottom,white 0%,white 50%,#f8f7fb 50%,#f8f7fb 50%,#f8f7fb 100%);border-radius:0;font-size:16px;line-height:16px;border-collapse:collapse" align="left" valign="top" bgcolor="#fff" width="290">&nbsp;</td>
											<td style="vertical-align:top;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;color:#ffffff;width:290px;border-radius:2px;background-color:#f34a3e;border-collapse:collapse" align="left" valign="top" bgcolor="#f34a3e" width="290">  <a style="padding:0;margin:0;line-height:1.3;text-align:center;width:290px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;display:inline-block;border-radius:3px;padding-top:18px;padding-bottom:18px;font-size:20px;font-weight:bold;color:#ffffff" href="https://start.yougov.com/a/v9Ck8QszmsSkTg" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://start.yougov.com/a/v9Ck8QszmsSkTg&amp;source=gmail&amp;ust=1649979569394000&amp;usg=AOvVaw279UJOC9ESYjmlGu_FeMiA">Start survey</a> </td>
											<td style="vertical-align:top;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;color:#ffffff;width:290px;background-color:#fff;background:linear-gradient(to bottom,white 0%,white 50%,#f8f7fb 50%,#f8f7fb 50%,#f8f7fb 100%);border-radius:0;font-size:16px;line-height:16px;border-collapse:collapse" align="left" valign="top" bgcolor="#fff" width="290">&nbsp;</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>

						<table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;background-color:#f8f7fb;padding:0;width:100%;display:table" width="100%" cellspacing="0" cellpadding="0" align="left" bgcolor="#f8f7fb">
							<tbody>
							<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
								<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;text-align:left;font-size:16px;line-height:1.3;margin:0 auto;padding-bottom:16px;width:564px;padding-left:52px;padding-right:52px" align="left">
									<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
										<tbody>
										<tr style="padding:0;vertical-align:top;text-align:left;border-bottom:1px solid #f2f0f7" align="left" valign="top">
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left">
												<p style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin-bottom:18px;color:#aba3bd;text-align:center;margin:26px 60px 12px 60px;font-size:14px;line-height:1.71;letter-spacing:-0.014em">If you can't see or click the button above, please copy and paste this link into your browser:</p>
												<p style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;color:#aba3bd;text-align:center;margin:26px 60px 12px 60px;font-size:14px;line-height:1.71;letter-spacing:-0.014em;margin-top:0;margin-bottom:28px"><a style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.3;text-decoration:none;color:#f5827d" href="https://start.yougov.com/a/v9Ck8QszmsSkTg" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://start.yougov.com/a/v9Ck8QszmsSkTg&amp;source=gmail&amp;ust=1649979569394000&amp;usg=AOvVaw279UJOC9ESYjmlGu_FeMiA">https://start.yougov.com/a/<wbr>v9Ck8QszmsSkTg</a></p>
											</th>
										</tr>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left">
												<p style="letter-spacing:-0.019em;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;margin-bottom:18px;font-size:12px;line-height:1.5;color:#aba3bd;margin-top:24px">This email was intended for <a href="mailto:iwayanwidiastika@gmail.com" target="_blank">iwayanwidiastika@gmail.com</a>. You received this email because you signed up to receive surveys from YouGov. Do not reply to this email - to contact us please select 'Contact Us' below.</p>
												<p style="letter-spacing:-0.019em;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:12px;line-height:1.5;color:#aba3bd;margin-bottom:0">67 Tanjong Pagar Road,</p>
												<p style="letter-spacing:-0.019em;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;margin-bottom:18px;font-size:12px;line-height:1.5;color:#aba3bd">#02-01 Singapore, 088488</p>
											</th>
										</tr>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
												<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
													<tbody>
													<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
														<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
															<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
																<tbody>
																<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
																	<th style="color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3" align="left"><a style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;margin-right:10px;font-size:12px;line-height:1.5;text-decoration:underline;color:#aba3bd;margin-left:0" href="https://ap.yougov.com/account/unsubscribe/vpHFMN5PkNFyjv/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://ap.yougov.com/account/unsubscribe/vpHFMN5PkNFyjv/&amp;source=gmail&amp;ust=1649979569394000&amp;usg=AOvVaw2EEx1m-MlSFm1T5jQ_TVHq">Unsubscribe</a> <span style="font-size:12px;color:#aba3bd">|</span> <a style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;margin-right:10px;margin-left:10px;font-size:12px;line-height:1.5;text-decoration:underline;color:#aba3bd" href="https://sg.yougov.com/en-sg/about/privacy/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://sg.yougov.com/en-sg/about/privacy/&amp;source=gmail&amp;ust=1649979569394000&amp;usg=AOvVaw35HLdHfQuu3kPGPBVJ0Mzq">Privacy</a> <span style="font-size:12px;color:#aba3bd">|</span> <a style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;margin-right:10px;margin-left:10px;font-size:12px;line-height:1.5;text-decoration:underline;color:#aba3bd" href="https://yougov.zendesk.com/hc/en-au" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://yougov.zendesk.com/hc/en-au&amp;source=gmail&amp;ust=1649979569394000&amp;usg=AOvVaw2FSnExz_kYzLXIwXoXtqXC">Contact us</a></th>
																</tr>
																</tbody>
															</table>
														</td>
													</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
											<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:1.3;border-collapse:collapse" align="left" valign="top">
												<table style="border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;width:100%" width="100%" cellspacing="0" cellpadding="0" align="left">
													<tbody>
													<tr style="padding:0;vertical-align:top;text-align:left" align="left" valign="top">
														<td style="vertical-align:top;color:#332c41;font-family:Arial,Helvetica,sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;font-size:16px;line-height:16px;border-collapse:collapse" align="left" valign="top" height="24px">&nbsp;</td>
													</tr>
													</tbody>
												</table>
											</td>
										</tr>
										</tbody>
									</table>
								</th>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
