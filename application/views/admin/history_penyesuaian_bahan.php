<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title"><?=((isset($bahan)) ? $bahan->bahan_nama : 'Penyesuaian Stock Bahan' )?></h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>penyesuaian-bahan" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>penyesuaian-bahan" class="kt-subheader__breadcrumbs-link">Penyesuaian Barang</a>
						                <?php if(isset($bahan)){
						                	?>
						                	<span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url().'penyesuaian-bahan/stock/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Stock</a>
						                	<?php
						                } ?>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
								<?php
									$start_date = $this->input->get('start_date');
									if($start_date == ""){
										$start_date = $first_date;
									}
									$end_date = $this->input->get('end_date');
									if ($end_date == ""){
										$end_date = date("Y-m-d");
									}
								?>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="row">
								<div class="col-lg-12">
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-toolbar">
												<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(1)=='penyesuaian-bahan') ? 'active' : '')?>" href="<?=base_url()?>penyesuaian-bahan">
															<i class="flaticon-truck"></i> <span class="kt--visible-desktop-inline-block">Penyesuaian Stock</span>
														</a>
													</li>
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(1)=='history-penyesuaian-bahan') ? 'active' : '')?>" href="<?=base_url()?>history-penyesuaian-bahan">
															<i class="flaticon2-time"></i> <span class="kt--visible-desktop-inline-block">History Penyesuaian</span>
														</a>
													</li>
												</ul>
											</div>
										</div>										
										<form method="get">
										<div class="kt-portlet__body">
											<div class="tab-content">
												<div class="form-group m-form__group row">
							                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal History</label>
							                            <div class="col-lg-4 col-md-9 col-sm-12">
							                                <div class="input-daterange input-group" id="kt_datepicker">
																<input type="text" class="form-control kt-input" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=$start_date?>" />
																<div class="input-group-append">
																	<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
																</div>
																<input type="text" class="form-control kt-input" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="5" value="<?=$end_date?>" />
															</div>
							                            </div>
							                            
							                        </div>
											</div>
										</div>
										<div class="kt-portlet__foot text-center">
							                        <div class="btn-group btn-group btn-pill btn-group-sm">
							                            <button type="submit" class="btn btn-info akses-filter_data">
							                                <i class="la la-search"></i> Filter Data
							                            </button>
							                            <?php
							                            	$uri = $this->uri->segment(1);
							                            	$getUrl = "";
							                            	foreach (array_keys($this->input->get()) as $key) {
							                            		$getUrl .=$key."=".$this->input->get($key)."&";
							                            	}
							                            	$getUrl = rtrim($getUrl,"& ");
							                            ?>
							                            <a href="<?=base_url()?>history-penyesuaian-bahan/pdf?<?=$getUrl?>" class="btn btn-danger akses-pdf">
							                                <i class="la la-file-pdf-o"></i> Print PDF
							                            </a>
							                            <a href="<?=base_url()?>history-penyesuaian-bahan/excel?<?=$getUrl?>" class="btn btn-success akses-excel">
							                                <i class="la la-file-excel-o"></i> Print Excel
							                            </a>
							                        </div>
							                    </div>
							            </form>
									</div>
									<div class="kt-portlet">
										<div class="kt-portlet__body">
																						<div class="kt-portlet kt-portlet--mobile">
												<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
														<input type="hidden" id="list_url" value="<?=$list_url.'?'.$getUrl?>" name="">
														<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
														<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
														<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
												<div class="kt-portlet__body">

													<!--begin: Search Form -->
													<div class="">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="row align-items-center">
																	<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
																		<div class="kt-input-icon kt-input-icon--left">
																			<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																			<span class="kt-input-icon__icon kt-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<table class="datatable table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Tanggal</th>
																<th>Kode Bahan</th>
																<th>Nama Bahan</th>
																<th>Jenis Bahan</th>
																<th>Jumlah Awal</th>
																<th>Jumlah Akhir</th>
																<th>Keterangan</th>
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						