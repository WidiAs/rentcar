<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title"><?=$piutang->no_invoice?></h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>piutang" class="kt-subheader__breadcrumbs-link">Hutang/Piutang</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>piutang" class="kt-subheader__breadcrumbs-link">Piutang</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url().'piutang/pay/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Pembayaran Piutang</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Pembayaran
				</h3>
				<input type="hidden" id="general_data_url" value="<?=base_url()?>piutang/detail/<?=$id?>" name="">
				<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
				<input type="hidden" id="list_url" value="<?=base_url()?>piutang/pay/list/<?=$id?>" name="">
				<div style="display: none;" id="table_column"><?=$column?></div>
				<?php if(isset($columnDef)) {  ?>
				<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
				<?php } ?>
				<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
				<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<a href="<?=base_url()?>piutang" class="btn btn-brand btn-icon-sm btn-warning"><i class="la la-angle-double-left"></i> Kembali</a>
						<button type="button" class="btn btn-brand btn-icon-sm" id="add_btn">
							<i class="flaticon2-plus"></i> Tambah Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
					<tr>
						<th width="30">No</th>
						<th>Tipe Pembayaran</th>
						<th>Jumlah</th>
						<th>Keterangan</th>
						<th width="150">Action</th>
					</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
				<tfoot >
					<tr>
						<td colspan="2" style="text-align: right"><strong>Total</strong> </td>
						<td ><strong></strong> </td>
						<td></td>
						<td></td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Pembayaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>
			<form action="<?=base_url().'piutang/pay/add/'.$id?>" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
								<div class="col-9">
									<label name="no_invoice" class="col-form-label"><?=$piutang->no_invoice?></label>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Nama Pelanggan</label>
								<div class="col-9">
									<label name="pelanggan_nama" class="col-form-label"><?=$piutang->pelanggan_nama?></label>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Jumlah Piutang</label>
								<div class="col-9">
									<label name="grand_total" class="col-form-label"><?=number_format($piutang->grand_total)?></label>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Terbayar</label>
								<div class="col-9">
									<label name="terbayar" class="col-form-label"><?=number_format($piutang->terbayar)?></label>

								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Sisa</label>
								<div class="col-9">
									<label name="sisa" class="col-form-label"><?=number_format($piutang->sisa)?></label>
									<input type="hidden" name="sisa">
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran<b class="label--required">*</b></label>
								<div class="col-9">
									<select class="form-control col-md-12" name="tipe_pembayaran_id" id="tipe_pembayaran_id" required="">
										<?php
										foreach ($tipe_pembayaran as $key) {
											?>
											<option value="<?=$key->tipe_pembayaran_id?>"><?=$key->tipe_pembayaran_nama." ".$key->no_akun?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" placeholder="" name="jumlah" class="input-numeral form-control" value="" required="">
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" placeholder="" name="tanggal" class="form-control tanggal" value="" required="" autocomplete="off">
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
								<div class="col-9">
									<textarea class="form-control" rows="3" name="keterangan"></textarea>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_edit_hp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Stok</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>
			<form action="<?=base_url().'piutang/pay/edit/'.$id?>" method="post" id="kt_edit_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
								<div class="col-9">
									<label name="no_invoice" class="col-form-label"><?=$piutang->no_invoice?></label>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Nama Pelanggan</label>
								<div class="col-9">
									<label name="pelanggan_nama" class="col-form-label"><?=$piutang->pelanggan_nama?></label>
									<input type="hidden" name="pembayaran_piutang_id">
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Jumlah Piutang</label>
								<div class="col-9">
									<label name="grand_total" class="col-form-label"><?=number_format($piutang->grand_total)?></label>

									<input type="hidden" name="grand_total">
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Terbayar</label>
								<div class="col-9">
									<label name="terbayar" class="col-form-label"><?=number_format($piutang->terbayar)?></label>
									<input type="hidden" name="terbayar">
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Sisa</label>
								<div class="col-9">
									<label name="sisa" class="col-form-label"><?=number_format($piutang->sisa)?></label>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran<b class="label--required">*</b></label>
								<div class="col-9">
									<select class="form-control col-md-12" name="tipe_pembayaran_id" id="tipe_pembayaran_id" required="">
										<?php
										foreach ($tipe_pembayaran as $key) {
											?>
											<option value="<?=$key->tipe_pembayaran_id?>"><?=$key->tipe_pembayaran_nama." ".$key->no_akun?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<input type="hidden" name="piutang_id" value="<?=$id?>">
								<label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" placeholder="" name="jumlah" class="input-numeral form-control" value="" required="">
									<input type="hidden" name="old_jumlah">
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" placeholder="" name="tanggal" class="form-control tanggal" value="" required="" autocomplete="off">
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
								<div class="col-9">
									<textarea class="form-control" rows="3" name="keterangan"></textarea>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
