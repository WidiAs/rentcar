<!-- begin:: Content -->
<style type="text/css">
	.select2-container .select2-selection--single {
		height: auto;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		position: fixed;
	}
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Pencatatan Kendaraan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pencatatan-kendaraan" class="kt-subheader__breadcrumbs-link">Inventory</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pencatatan-kendaraan" class="kt-subheader__breadcrumbs-link">Pencatatan Kendaraan</a>

			<input type="hidden" id="action" value='<?=$action?>'>
			<input type="hidden" id="home-url" value="<?= base_url() ?>">
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold"
						role="tablist">
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'pencatatan-kendaraan') ? 'active' : '') ?>"
							   href="<?= base_url() ?>pencatatan-kendaraan">
								<span class="kt--visible-desktop-inline-block">Jadwal Hari Ini</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?= (($this->uri->segment(1) == 'pencatatan-kendaraan-besok') ? 'active' : '') ?>"
							   href="<?= base_url() ?>pencatatan-kendaraan-besok">
								<span class="kt--visible-desktop-inline-block">Jadwal Besok</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Daftar Mobil Hari Ini
				</h3>
				<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
				<input type="hidden" id="list_url" value="<?= base_url() ?>list/jadwal-today" name="">
				<div style="display: none;" id="table_column"><?= $column ?></div>
				<?php if (isset($columnDef)) { ?>
					<div style="display: none;" id="table_columnDef"><?= $columnDef ?></div>
				<?php } ?>
<!--				<div style="display: none;" id="table_action">--><?//= (isset($action) ? $action : "") ?><!--</div>-->
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable table-jadwal">
				<thead>
				<tr>
					<th width="30">No</th>
					<th>Tanggal</th>
					<th>Jenis Mobil</th>
					<th>Plat Mobil</th>
					<th>Status</th>
					<th>Customer</th>
					<th>Driver</th>
					<th>Jam Ambil</th>
					<th width="200">Action</th>
				</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal" id="modal-pencatatan" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Pencatatan Aktifitas</h5>
				<button type="button btn" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
				</button>
			</div>
			<form action="<?= base_url() ?>arus-kendaraan/add" method="post" id="kt_add_staff_form" enctype="multipart/form-data">
				<div class="modal-body">
					<input type="hidden" name="mobil_id">
					<input type="hidden" name="penyewaan_id">
					<input type="hidden" name="penyewaan_rekanan_id">
					<input type="hidden" name="mobil_jenis_nama">
					<input type="hidden" name="mobil_plat">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Jenis Mobil</label>
								<div class="col-9">
									<label name="mobil_jenis_nama_lbl" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Plat Mobil</label>
								<div class="col-9">
									<label name="mobil_plat_lbl" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Jenis Sewa</label>
								<div class="col-9">
									<label name="jenis" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group user_lokasi_container">
								<div class="form-group">
									<label class="form-control-label required">Status <b
											class="label--required">*</b></label>
									<select class="form-control col-md-12 lokasi-select" name="status" required="">
										<option value="Stand By">Masuk</option>
										<option value="Keluar">Keluar</option>
										<option value="Dikembalikan">Dikembalikan</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="form-group">
									<label>Foto Kendaraan <b class="label--required">*</b></label>
									<div></div>
									<div class="custom-file">
										<input type="file" class="custom-file-input img-input"
											   data-display="displayImg1custom" name="foto_mobil"
											   accept="image/x-png,image/gif,image/jpeg"
											   id="customFile1" required="">
										<label class="custom-file-label selected" for="customFile1"
											   id="labelCustomFile1"></label>
										<input type="hidden" name="input_foto_mobil">
									</div>
								</div>
								<div class="fileinput-new thumbnail"
									 style="width: 200px; height: 150px;">
									<img src="<?= base_url() ?>assets/media/no_image.png" alt=""
										 id="displayImg1custom" width="200" height="150">
								</div>

							</div>
							<div class="form-group">
								<div class="form-group">
									<label>Foto Bensin <b class="label--required">*</b></label>
									<div></div>
									<div class="custom-file">
										<input type="file" class="custom-file-input img-input"
											   data-display="displayImg2custom" name="foto_bensin"
											   accept="image/x-png,image/gif,image/jpeg"
											   id="customFile2" required="">
										<label class="custom-file-label selected" for="customFile2"
											   id="labelCustomFile2"></label>
										<input type="hidden" name="input_foto_bensin">
									</div>
								</div>
								<div class="fileinput-new thumbnail"
									 style="width: 200px; height: 150px;">
									<img src="<?= base_url() ?>assets/media/no_image.png" alt=""
										 id="displayImg2custom" width="200" height="150">
								</div>
							</div>
							<div class="form-group" id="foto-ktp">
								<div class="form-group">
									<label>Foto KTP <b class="label--required">*</b></label>
									<div></div>
									<div class="custom-file">
										<input type="file" class="custom-file-input img-input"
											   data-display="displayImg3custom" name="foto_ktp"
											   accept="image/x-png,image/gif,image/jpeg"
											   id="customFile3">
										<label class="custom-file-label selected" for="customFile3"
											   id="labelCustomFile3"></label>
										<input type="hidden" name="input_foto_ktp">
									</div>
								</div>
								<div class="fileinput-new thumbnail"
									 style="width: 200px; height: 150px;">
									<img src="<?= base_url() ?>assets/media/no_image.png" alt=""
										 id="displayImg3custom" width="200" height="150">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Dibawa Oleh <b class="label--required">*</b></label>
								<input type="text" name="driver" class="form-control" required="">
							</div>
							<div class="form-group">
								<label class="control-label">Kontak Pembawa <b class="label--required">*</b></label>
								<input type="text" name="kontak_driver" class="form-control" required="">
							</div>
							<div class="form-group">
								<label class="control-label">Keterangan <b class="label--required">*</b></label>
								<input type="text" name="keterangan" class="form-control" required="">
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="submit" class="btn btn-primary" data-reload="true">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
