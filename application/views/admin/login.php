<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Login | Waisnawa Transport Group</title>
		<meta name="description" content="Login page">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
		<link href="<?=base_url()?>assets/app/custom/login/login-v4.default.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<link href="<?=base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?=base_url()?>assets/media/logos/logo.ico" />
	</head>
	<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?=base_url()?>assets/media/bg/bg-2.jpg);">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__logo">
								<a href="#">
									<img src="<?=base_url()?>assets/media/logos/waisnawa.png" style="width : 300px">
								</a>
							</div>
							<div class="kt-login__signin">
								<input type="hidden" id="login-auth" value="<?=base_url()?>login/auth">
								<div class="kt-login__head">
									<h3 class="kt-login__title">Sign In To Admin</h3>
								</div>
								<form class="kt-form" action="<?=base_url()?>login/multi_check" method="post" id="login-form">
									<div class="input-group">
										<input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
									</div>
									<div class="input-group">
										<input class="form-control" type="password" placeholder="Password" name="password">
										<input type="hidden" name="lokasi" value="">
									</div>
									<div class="row kt-login__extra">
										<div class="col" style="display : none">
											<label class="kt-checkbox">
												<input type="checkbox" name="remember"> Remember me
												<span></span>
											</label>
										</div>
										<div class="col kt-align-right">
											<a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
										</div>
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_signin_submit" class="btn btn-brand btn-pill kt-login__btn-primary">Sign In</button>
									</div>
								</form>
							</div>
							<div class="kt-login__forgot">
								<div class="kt-login__head">
									<h3 class="kt-login__title">Forgotten Password ?</h3>
									<div class="kt-login__desc">Enter your email to reset your password:</div>
								</div>
								<form class="kt-form" action="<?=base_url()?>forget" method="post">
									<div class="input-group">
										<input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_forgot_submit" class="btn btn-brand btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
										<button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel</button>
									</div>
								</form>
							</div>

							<div class="modal" id="modal-lokasi"  role="dialog" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Pilih Lokasi</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group user_lokasi_container">
															<label class="form-control-label required">Lokasi <b class="label--required">*</b></label>
															<select class="form-control col-md-12 lokasi-select" id="lokasi_multi" required="">
																<option value="">Pilih Lokasi</option>
																<?php
																foreach ($lokasi as $key) {
																	?>
																	<option value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
																	<?php
																}
																?>
															</select>
														</div>
													</div>

												</div>
											</div>
											<div class="modal-footer">
												<button id="kt_login_multi_signin_submit" type="submit" class="btn btn-primary">Pilih</button>
											</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="<?=base_url()?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js"
				type="text/javascript"></script>
		<script src="<?= base_url() ?>assets/vendors/custom/components/vendors/sweetalert2/init.js"
				type="text/javascript"></script>
		<script src="<?=base_url()?>assets/script/admin/login.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>
	</body>
</html>
