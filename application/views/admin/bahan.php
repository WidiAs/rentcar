<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Bahan</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Bahan</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Data Bahan
										</h3>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url().(($this->uri->segment(1) == 'bahan') ? 'bahan' : 'low-stock-bahan' )?>/list" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
										<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									</div>
									<?php  if(isset($action)) $akses = json_decode($action,true); if($this->uri->segment(1)=="bahan" && ($akses["add"])){ ?>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												
												<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
													<i class="flaticon2-plus"></i> Tambah Data
												</button>

											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-1 searchForm">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<table class="datatable table table-striped- table-hover table-checkable" >
										<?php if($this->uri->segment(1)=="bahan") {
											?>
										<thead>
											<tr>
												<th width="30">No</th>
												<th>Kode Bahan</th>
												<th>Nama Bahan</th>
												<th>Jenis Bahan</th>
												<th>Satuan</th>
												<th>Global Stok</th>
												<?=((isset($_SESSION['login']['lokasi_id']))? '<td>Jumlah</td>' : '')?>
												<th>Suplier</th>
												<th width="250">Action</th>
											</tr>
										</thead>
										<tbody id="child_data_ajax"></tbody>
										<tfoot >
											<tr>
												<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
												<td ><strong></strong> </td>
												<td></td>
												<td></td>
												<?=((isset($_SESSION['login']['lokasi_id']))? '<td></td>' : '')?>
											</tr>
											
										</tfoot>
											<?php
										} else {
											?>
											<thead>
												<tr>
													<th width="30">No</th>
													<th>Kode Bahan</th>
													<th>Nama Bahan</th>
													<th>Jenis Bahan</th>
													<th>Satuan</th>
													<th>Global Stok</th>
													<td>Jumlah</td>
													<td>Lokasi</td>
													<th>Suplier</th>
												</tr>
											</thead>
											<tbody id="child_data_ajax"></tbody>
											<tfoot >
												<tr>
													<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
													<td ><strong></strong> </td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												
											</tfoot>											
											<?php
										} ?>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
						</div>
						<?php if($this->uri->segment(1)=="bahan"){ ?>
						<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Tambah Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
										<form action="<?=base_url()?>bahan/add" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
				                                            	<label class="form-control-label ">Kode Bahan<b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="bahan_kode" class="form-control" value="" required="" >
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Nama Bahan<b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="bahan_nama" class="form-control" value="" required="">
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Jenis Bahan <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12" name="bahan_jenis_id" required="">
				                                            		<option value="">Pilih Jenis</option>
				                                            		<?php
				                                            			foreach ($jenis_bahan as $key) {
				                                            			?>
				                                            			<option value="<?=$key->jenis_bahan_id?>"><?=$key->jenis_bahan_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                         <div class="form-group">
				                                            	<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12" name="bahan_satuan_id" required="">
				                                            		<option value="">Pilih Satuan</option>
				                                            		<?php
				                                            			foreach ($satuan as $key) {
				                                            			?>
				                                            			<option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                        
			                                        </div>
			                                        <div class="col-md-6">
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Suplier <b class="label--required">*</b></label>
				                                            	<div class="typeahead col-md-12">
				                                            		<input class="form-control" id="kt_typeahead_1" name="suplier_nama" type="text" dir="ltr">
				                                            	</div>
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Minimal Stok <b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="bahan_minimal_stock" class="input-numeral form-control" value="0" required="">
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Keterangan</label>
				                                            	<textarea class="form-control" id="bahan_keterangan" rows="3" name="bahan_keterangan" ></textarea>
				                                        </div>
			                                        </div>
												</div>

	                                        	
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Detail Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
											<div class="modal-body">
												<div class="row">
													 <div class="col-md-6">
													 	<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Kode Bahan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="bahan_kode" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama Bahan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="bahan_nama" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Jenis Bahan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="jenis_bahan_nama" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Satuan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="satuan_nama" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Suplier</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="suplier_nama" class="col-form-label"></label>
															</div>
														</div>
			                                        </div>
			                                        <div class="col-md-6">
			                                        	<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Global Stok</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="stock" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Minimal Stok</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="bahan_minimal_stock" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="bahan_keterangan" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Dibuat Pada</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="created_at" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Diubah Pada</label>
															<label for="example-text-input" class="col-1 col-form-label">:</label>
															<div class="col-8">
																<label name="updated_at" class="col-form-label"></label>
															</div>
														</div>
			                                        </div>
												</div>

	                                        	
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Edit Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
											</button>
										</div>
										<form action="<?=base_url()?>bahan/edit" method="post" id="kt_edit_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
				                                            	<label class="form-control-label ">Kode Bahan<b class="label--required">*</b></label>
				                                            	<input type="hidden" name="bahan_id">
				                                            	<input type="text" placeholder="" name="bahan_kode" class="form-control" value="" required="" >
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Nama Bahan<b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="bahan_nama" class="form-control" value="" required="">
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Jenis Bahan <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12" name="bahan_jenis_id" required="">
				                                            		<option value="">Pilih Jenis</option>
				                                            		<?php
				                                            			foreach ($jenis_bahan as $key) {
				                                            			?>
				                                            			<option value="<?=$key->jenis_bahan_id?>"><?=$key->jenis_bahan_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                         <div class="form-group">
				                                            	<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12" name="bahan_satuan_id" required="">
				                                            		<option value="">Pilih Satuan</option>
				                                            		<?php
				                                            			foreach ($satuan as $key) {
				                                            			?>
				                                            			<option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                        
			                                        </div>
			                                        <div class="col-md-6">
														<div class="form-group">
				                                            	<label class="form-control-label ">Suplier <b class="label--required">*</b></label>
				                                            	<div class="typeahead col-md-12">
				                                            		<input class="form-control" id="kt_typeahead_2" name="suplier_nama" type="text" dir="ltr">
				                                            	</div>
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Minimal Stok <b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="bahan_minimal_stock" class="input-numeral form-control" value="" required="">
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Keterangan</label>
				                                            	<textarea class="form-control" id="bahan_keterangan" rows="3" name="bahan_keterangan" ></textarea>
				                                        </div>
			                                        </div>
												</div>
												</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
					<?php } ?>
