
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Kartu Stok Produk</title>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }

    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/logo-laporan.png" width="65px" height="auto"></td>
				<td><strong>PT.Balioz Sadajiwa</strong><br>Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak<br>+6285858424039</td>
				<td style="text-align: right"><strong>Kartu Stok Produk</strong><br><?=((isset($start_date)) ? $start_date." s/d ". $end_date:$first_date. " s/d ".date("d-m-Y") ) ?><br><?=((isset($produk_nama)) ? $produk_nama : " Semua Produk " ) ?><br><?=((isset($lokasi_nama)) ? $lokasi_nama : " Semua lokasi " ) ?></td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th colspan="5" style="text-align: center">Stok Masuk</th>
					<th rowspan="2" style="text-align: center;">Tanggal</th>
					<th colspan="5" style="text-align: center">Stok Keluar</th>
					<th rowspan="2" style="text-align: center;">Stok Akhir</th>
				</tr>
				<tr>
					<th>Produk</th>
					<th width="60">Seri Produk</th>	
					<th>Lokasi</th>
					<th width="60">Masuk</th>
					<th>Keterangan</th>
					<th>Produk</th>
					<th width="60">Seri Produk</th>	
					<th>Lokasi</th>
					<th width="60">Keluar</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($list as $key) {
						?>
						<tr>
							<td><?=$key["produk_nama_in"]?></td>
							<td><?=$key["stock_produk_seri_in"]?></td>
							<td><?=$key["lokasi_nama_in"]?></td>
							<td style="text-align: right;"><?=$key["stock_in"]?></td>
							<td><?=$key["keterangan_in"]?></td>
							<td><?=$key["tanggal"]?></td>
							<td><?=$key["produk_nama_out"]?></td>
							<td><?=$key["stock_produk_seri_out"]?></td>
							<td><?=$key["lokasi_nama_out"]?></td>
							<td style="text-align: right;"><?=$key["stock_out"]?></td>
							<td><?=$key["keterangan_out"]?></td>
							<td style="text-align: right;"><?=$key["total_stock"]?></td>
						</tr>
						<?php
					}
				?>
			</tbody>
			<tfoot ></tfoot>
	</table> 
</div>
 
</body>
</html>