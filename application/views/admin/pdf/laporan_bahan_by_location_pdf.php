
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Bahan per Lokasi</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/logo-laporan.png" width="65px" height="auto"></td>
				<td><strong>PT.Balioz Sadajiwa</strong><br>Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak<br>+6285858424039</td>
				<td style="text-align: right"><strong>Laporan Bahan per Lokasi</strong>
					<br><span>Bahan : <?=$bahan?></span>
					<br><span>Jenis Bahan : <?=$jenis_bahan?></span> 
					<br><span>Satuan : <?=$satuan?></span> 
					<br><span>Lokasi : <?=$lokasi?></span>
					<br><span>Pencarian : <?=$cari?></span> 
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Kode Bahan</th>
					<th>Nama Bahan</th>
					<th>Jenis Bahan</th>
					<th>Satuan</th>
					<th>lokasi</th>
					<th>Stok</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sum = 0;
				foreach ($list as $key) {
				$sum +=str_replace(",","",$key->stock);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->bahan_kode?></td>
					<td><?=$key->bahan_nama?></td>
					<td><?=$key->jenis_bahan_nama?></td>
					<td><?=$key->satuan_nama?></td>
					<td><?=$key->lokasi_nama?></td>
					<td style="text-align: right;"><?=$key->stock?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot >
				<tr>
					<td colspan="6" style="text-align: right"><strong>Total</strong> </td>
					<td ><strong><?=number_format($sum)?></strong></td>
				</tr>

			</tfoot>
	</table> 
</div>
 
</body>
</html>