
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Piutang</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/logo-laporan.png" width="65px" height="auto"></td>
				<td><strong>PT.Balioz Sadajiwa</strong><br>Jalan Merthanadi no 62A Kerobokan Kelod - Seminyak<br>+6285858424039</td>
				<td style="text-align: right"><strong>Laporan Piutang</strong>
					<br><span>Tenggat Pelunasan : <?=$tenggat_start." s/d ".$tenggat_end?></span>
					<br><span>No Faktur : <?=$no_faktur?></span> 
					<br><span>Status Pembayaran : <?=$status_pembayaran?></span> 
					<br><span>Nama Pelanggan : <?=$nama_pelanggan?></span> 
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Pelanggan</th>
					<th>No Faktur</th>
					<th>Status Pembayaran</th>
					<th>Tenggat Pembayaran</th>
					<th>Grand Total</th>
					<th>Terbayar</th>
					<th>Sisa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$gt = 0;
				$terbayar = 0;
				$sisa = 0;
				foreach ($list as $key) {
				$gt +=str_replace(",","",$key->grand_total);
				$terbayar +=str_replace(",","",$key->terbayar);
				$sisa +=str_replace(",","",$key->sisa);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->nama_pelanggan?></td>
					<td><?=$key->no_faktur?></td>
					<td><?=$key->status_pembayaran?></td>
					<td><?=$key->tenggat_pelunasan?></td>
					<td style="text-align: right;"><?=$key->grand_total?></td>
					<td style="text-align: right;"><?=$key->terbayar?></td>
					<td style="text-align: right;"><?=$key->sisa?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot >
				<tr>
					<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
					<td ><strong><?=number_format($gt)?></strong></td>
					<td ><strong><?=number_format($terbayar)?></strong></td>
					<td ><strong><?=number_format($sisa)?></strong></td>
				</tr>

			</tfoot>
	</table> 
</div>
 
</body>
</html>