<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penjualan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pos" class="kt-subheader__breadcrumbs-link">POS</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>pos" class="kt-subheader__breadcrumbs-link">Penjualan</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			<input type="hidden" id="url" value="<?= base_url() ?>surat-jalan">
		</div>

	</div>
</div>

<form action="<?= base_url() ?>surat-jalan/save" method="post" id="save-form">
	<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Surat Jalan </label>
							<div class="input-group col-10">
								<label for="example-text-input"
									   class="col-form-label"> <?= $no_surat_jalan ?> </label>
								<input type="hidden" name="no_surat_jalan" value="<?= $no_surat_jalan ?>">
								<input type="hidden" name="urutan" value="<?= $urutan ?>">
								<input type="hidden" name="penjualan_id" value="<?= $penjualan->penjualan_id ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pelanggan</label>
							<div class="input-group col-10">
								<label for="example-text-input"
									   class="col-form-label"> <?= $penjualan->nama_pelanggan ?> </label>
								<input type="hidden" name="pelanggan" value="<?= $penjualan->nama_pelanggan ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Tanggal Kirim</label>
							<div class="input-group col-4">
								<input type="text" name="tanggal_kirim" class="form-control tanggal" required=""
									   autocomplete="off" value="<?= date('d-m-Y') ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Faktur </label>
							<div class="input-group col-10">
								<label for="example-text-input"
									   class="col-form-label"> <?= $penjualan->no_faktur ?> </label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Telepon</label>
							<div class="col-10">
								<input class="form-control" type="text" value="<?= $penjualan->telepon_pelanggan ?>"
									   name="kontak" required="">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Alamat</label>
							<div class="col-10">
								<textarea class="form-control" name="alamat" required=""><?= $penjualan->alamat_pelanggan ?></textarea>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-hover table-checkable">
					<thead>
					<tr>
						<th>Kode Produk</th>
						<th>Nama Produk</th>
						<th>QTY Pesan</th>
						<th>QTY Kirim</th>
					</tr>
					</thead>
					<tbody id="item_child">
					<?php foreach ($penjualan_produk as $key => $item) { ?>
						<tr id="row_<?= $key ?>">
							<td style="display:none">
								<input type="hidden" name="penjualan_produk_id[<?= $key ?>]" value="<?= $item->penjualan_produk_id ?>">
								<input type="hidden" name="produk_id[<?= $key ?>]" value="<?= $item->produk_id ?>">
								<input type="hidden" name="produk_custom_id[<?= $key ?>]" value="<?= $item->produk_custom_id ?>">
								<input type="hidden" name="max_kirim[<?= $key ?>]" value="<?= $item->sisa_qty ?>">
								<input type="hidden" name="stock_produk_id[<?= $key ?>]" value="<?= $item->stock_produk_id ?>">
							</td>
							<td>
								<span id="produk_nama_<?= $key ?>"><?= $item->produk_kode ? $item->produk_kode : 'custom' ?></span>
							</td>
							<td><span id="produk_nama_<?= $key ?>"><?= $item->produk_nama ? $item->produk_nama : $item->deskripsi ?></span></td>
							<td><span><?= $item->sisa_qty ?></span></td>
							<td><input type="text" class="form-control input-numeral" name="qty_kirim[<?= $key ?>]"
									   value="0" data-id="<?= $key ?>"></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="pos-floating-button">
		<button class="btn btn-success btn-pill btn-lg" id="btn-save"><i class="fa fa-arrow-right"></i>&nbsp;
			Proses
		</button>
	</div>

</form>
