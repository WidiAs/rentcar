<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_profile">

	<div class="row">
		<div class="col-md-4">
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">

				<div class="kt-portlet__body">
					<div class="kt-profile-pic">
						<img src="<?= base_url() . $_SESSION['login']['avatar'] ?>" style='height:auto;width: 40%;'
							 class="account_avatar">
					</div>
					<h5 style="text-align: center;margin-top: 20px"><b class="account_name"><?= $user->staff_nama ?></b>
					</h5>
					<span style="text-align: center;"><?= $user->user_role_name ?></span>
					<span style="text-align: center;"><?= $user->staff_email ?></span>
					<span style="text-align: center;"><?= $user->staff_phone_number ?></span>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<ul class="nav nav-tabs  nav-tabs-line" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_1" role="tab">Jadwal Hari
								Ini</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2" role="tab">Jadwal Kedepan</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab">History Driver</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_4" role="tab">History Pengeluaran</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel" style="padding: 10px">

							<?php if (!empty($kegiatan)) { ?>
								<!--								<div class="kt-portlet__head-toolbar" style="float : right">-->
								<!--									<div class="kt-portlet__head-wrapper" >-->
								<div class="dropdown dropdown-inline col-md-12">
									<button type="button" class="btn btn-brand btn-icon-sm"
											data-target="#kt_modal_add"
											data-toggle="modal" style="float : right">
										<i class="flaticon2-plus"></i> Tambah Pengeluaran
									</button>
								</div>
								<!--									</div>-->
								<!--								</div>-->
								<form role="form" action="<?= base_url() ?>kegiatan-driver/post" id="save-kegiatan"
									  method="post">
									<input type="hidden" name="kegiatan_driver_id"
										   value="<?= $kegiatan->kegiatan_driver_id ?>">
									<input type="hidden" name="waktu_mulai" value="<?= $kegiatan->waktu_mulai ?>">
									<input type="hidden" name="waktu_selesai" value="<?= $kegiatan->waktu_selesai ?>">

									<input type="hidden" name="mobil_id" value="<?= $kegiatan->mobil_id ?>">
									<input type="hidden" name="mobil_jenis" value="<?= $kegiatan->mobil_jenis ?>">
									<input type="hidden" name="mobil_plat" value="<?= $kegiatan->mobil_plat ?>">
									<input type="hidden" name="driver_id" value="<?= $kegiatan->driver_id ?>">
									<input type="hidden" name="driver_nama" value="<?= $kegiatan->driver_nama ?>">
									<input type="hidden" name="tanggal" value="<?= $kegiatan->tanggal ?>">
									<input type="hidden" name="penyewaan_id" value="<?= $kegiatan->penyewaan_id ?>">
									<div class="form-group row">
										<label for="example-text-input" class="col-4 col-form-label">Jenis Mobil</label>
										<div class="col-8">
											<label class="col-form-label"><?= $kegiatan->mobil_jenis ?></label>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-text-input" class="col-4 col-form-label">Plat Mobil</label>
										<div class="col-8">
											<label class="col-form-label"><?= $kegiatan->mobil_plat ?></label>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-text-input" class="col-4 col-form-label">Jenis
											Pekerjaan</label>
										<div class="col-8">
											<?php if ($kegiatan->jenis_pekerjaan){ ?>
												<label class="col-form-label"><?= $kegiatan->jenis_pekerjaan ?></label>
												<input type="hidden" name="jenis_pekerjaan" value="<?= $kegiatan->jenis_pekerjaan ?>">
											<?php }else{ ?>
												<select class="form-control" name="jenis_pekerjaan" required>
													<option value="">Pilih Jenis Pekerjaan</option>
													<option value="Check In/Out">Check In/Out</option>
													<option value="Half Day">Half Day</option>
													<option value="10 Jam">10 Jam</option>
													<option value="12 Jam">12 Jam</option>
													<option value="Borongan">Borongan</option>
												</select>
											<?php } ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-text-input" class="col-4 col-form-label">Jam Mulai</label>
										<div class="col-8">
											<label class="col-form-label"><?= $kegiatan->waktu_mulai ?></label>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-text-input" class="col-4 col-form-label">Jam Selesai</label>
										<div class="col-8">
											<label class="col-form-label"><?= $kegiatan->waktu_selesai ?></label>
										</div>
									</div>
									<?php if (!$kegiatan->waktu_selesai) { ?>
										<div class="form-group" class="" id="personal_cancel_group">
											<button type="button" class="btn btn-success pull-right" style="margin: 5px"
													id="submit-kegiatan"><?= $kegiatan->waktu_mulai ? 'Selesai' : 'Mulai' ?></button>
										</div>
									<?php } ?>
								</form>
							<?php } else { ?>
								<div class="form-group">
									<label class="control-label">Tidak Ada Jadwal Hari Ini</label>
								</div>

							<?php } ?>
						</div>
						<div class="tab-pane" id="kt_tabs_1_2" role="tabpanel" style="padding: 10px">
							<input type="hidden" id="kedepan_url" value="<?= base_url() ?>list/jadwal-kedepan" name="">
							<div class="modal-body">
								<table class="table table-striped- table-hover table-checkable" id="kedepan-table">
									<thead>
									<tr>
										<th width="30">No</th>
										<th>Jenis Mobil</th>
										<th>Plat Mobil</th>
										<th>Tanggal</th>
									</tr>
									</thead>
									<tbody id="kedepan_child"></tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="kt_tabs_1_3" role="tabpanel" style="padding: 10px">
							<input type="hidden" id="history_url" value="<?= base_url() ?>list/history-driver" name="">
							<div class="modal-body">
								<table class="table table-striped- table-hover table-checkable" id="history-table">
									<thead>
									<tr>
										<th width="30">No</th>
										<th>Jenis Mobil</th>
										<th>Plat Mobil</th>
										<th>Tanggal</th>
										<th>Jam Mulai</th>
										<th>Jam Selesai</th>
										<th>Jenis Pekerjaan</th>
										<th>Status Pembayaran</th>
										<th>Bukti</th>
									</tr>
									</thead>
									<tbody id="history_child"></tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="kt_tabs_1_4" role="tabpanel" style="padding: 10px">
							<input type="hidden" id="history_pengeluaran_url"
								   value="<?= base_url() ?>list/history-pengeluaran-driver" name="">
							<div class="modal-body">
								<table class="table table-striped- table-hover table-checkable"
									   id="history-pengeluaran-table">
									<thead>
									<tr>
										<th width="30">No</th>
										<th>Nama Pengeluaran</th>
										<th>Jumlah</th>
										<th>Tanggal</th>
										<th>Jam</th>
									</tr>
									</thead>
									<tbody id="history_pengeluaran_child"></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="kt_modal_add" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Tambah Pengeluaran</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"
								style="margin-top: -30px">
						</button>
					</div>
					<form action="<?= base_url() ?>pengeluaran-driver/add" method="post" id="kt_add_staff_form"
						  enctype="multipart/form-data">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">Nama Pengeluaran <b
												class="label--required">*</b></label>
										<input type="text" name="nama_pengeluaran" class="form-control" required="">
									</div>
									<div class="form-group">
										<label class="control-label">Jumlah Pengeluaran <b class="label--required">*</b></label>
										<input type="text" name="jumlah" class="form-control input-numeral" required=""
											   value="0">
									</div>
								</div>

							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal-image">
	<span class="close">&times;</span>
	<img class="modal-image-content" id="img01">
	<div id="caption"></div>
</div>


