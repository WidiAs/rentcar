<!-- begin:: Content -->
<style type="text/css">
	.select2-container .select2-selection--single {
		height: auto;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		position: fixed;
	}
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Produk</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>produk" class="kt-subheader__breadcrumbs-link">Produk</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Produk
				</h3>
				<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
				<input type="hidden" id="list_url"
					   value="<?= base_url() . (($this->uri->segment(1) == 'produk') ? 'produk' : 'low-stock-produk') ?>/list"
					   name="">
				<div style="display: none;" id="table_column"><?= $column ?></div>
				<?php if (isset($columnDef)) { ?>
					<div style="display: none;" id="table_columnDef"><?= $columnDef ?></div>
				<?php } ?>
				<div style="display: none;" id="sumColumn"><?= (isset($sumColumn) ? $sumColumn : "") ?></div>
				<div style="display: none;" data-width="150" data-style="dropdown"
					 id="table_action"><?= (isset($action) ? $action : "") ?></div>
			</div>
			<?php if (isset($action)) $akses = json_decode($action, true);
			if ($this->uri->segment(1) == "produk" && ($akses["add"])) { ?>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="dropdown dropdown-inline">
							<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add"
									data-toggle="modal">
								<i class="flaticon2-plus"></i> Tambah Data
							</button>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>

							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile"
								 style="display: <?=((!isset($_SESSION['login']))?"none" : "block")?>">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Sort By</label>
									</div>
									<div class="kt-form__control">
										<select class="form-control searchInput" data-field="order_by" data-col-index="2">
											<option value="">None</option>
												<?php foreach ($sort as $key => $label) {
													?>
													<option value="<?=$key?>"><?=$label?></option>
													<?php
												} ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile"
								 style="display: <?=((!isset($_SESSION['login']))?"none" : "block")?>">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Order By</label>
									</div>
									<div class="kt-form__control">
										<select class="form-control searchInput" data-field="sort_by" data-col-index="3">
											<option value="asc">A-Z</option>
											<option value="desc">Z-A</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable">
				<?php if ($this->uri->segment(1) == "produk") {
					?>
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Kode Produk</th>
						<th>Nama Produk</th>
						<th>Spec</th>
						<th>Barcode</th>
<!--						<th>Note</th>-->
						<th>Jenis Produk</th>
						<th>Minimal Stok</th>
						<th>Harga</th>
						<th>Global Stok</th>
						<?= ((isset($_SESSION['login']['lokasi_id'])) ? '<td>Jumlah</td>' : '') ?>
						<th>Diskon %</th>
						<th width="160">Action</th>
					</tr>
					</thead>
					<tbody id="child_data_ajax"></tbody>
					<tfoot>
					<tr>
						<td colspan="8" style="text-align: right"><strong>Total</strong></td>
						<td><strong></strong></td>
						<td></td>
						<td></td>
						<?= ((isset($_SESSION['login']['lokasi_id'])) ? '<td></td>' : '') ?>
					</tr>

					</tfoot>
					<?php
				} else {
					?>
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Kode Produk</th>
						<th>Nama Produk</th>
						<th>Barcode</th>
						<th>Jenis Produk</th>
						<th>Minimal Stok</th>
						<th>Harga Ecerean</th>
						<th>Harga Grosir</th>
						<th>Global Stok</th>
						<th>Jumlah</th>
						<th>Lokasi</th>
						<th>Diskon %</th>
					</tr>
					</thead>
					<tbody id="child_data_ajax"></tbody>
					<tfoot>
					<tr>
						<td colspan="8" style="text-align: right"><strong>Total</strong></td>
						<td><strong></strong></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					</tfoot>
					<?php
				} ?>

			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>
<?php if ($this->uri->segment(1) == "produk") { ?>
	<div class="modal" id="kt_modal_add" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Tambah Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form action="<?= base_url() ?>produk/add" method="post" id="kt_add_staff_form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-label ">Kode Produk<b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="produk_kode" class="form-control" value=""
										   required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Jenis Produk <b
											class="label--required">*</b></label>
									<select class="form-control col-md-12" name="produk_jenis_id" required="">
										<option value="">Pilih Jenis</option>
										<?php
										foreach ($jenis_produk as $key) {
											?>
											<option
												value="<?= $key->jenis_produk_id ?>"><?= $key->jenis_produk_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Size <b class="label--required">*</b></label>
									<select class="form-control col-md-12 kt_select_2" name="size_id" required="">
										<option value="">Pilih Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Good Size </label>
									<select class="form-control col-md-12 kt_select_2" name="good_size_id">
										<option value="">Pilih Good Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Bed Size </label>
									<select class="form-control col-md-12 kt_select_2 m-select2" name="bed_size_id">
										<option value="">Pilih Bed Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Color <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="color_id" required="">
										<option value="">Pilih Color</option>
										<?php
										foreach ($color as $key) {
											?>
											<option value="<?= $key->color_id ?>"><?= $key->color_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Weight gram/pc <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="weight_pc"
										   class="form-control input-numeral" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Weight gram/m2 <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="weight_m" class="form-control input-numeral"
										   value="" required="">
								</div>


							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-label ">Barcode <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="barcode" class="form-control" value=""
										   required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Fabric <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="fabric_id" required="">
										<option value="">Pilih Fabric</option>
										<?php
										foreach ($fabric as $key) {
											?>
											<option value="<?= $key->fabric_id ?>"><?= $key->fabric_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Patern <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="patern_id" required="">
										<option value="">Pilih Patern</option>
										<?php
										foreach ($patern as $key) {
											?>
											<option value="<?= $key->patern_id ?>"><?= $key->patern_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Style</label>
									<input type="text" placeholder="" name="style" class="form-control" value="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Pile</label>
									<input type="text" placeholder="" name="file" class="form-control" value="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="produk_satuan_id" required="">
										<option value="">Pilih Satuan</option>
										<?php
										foreach ($satuan as $key) {
											?>
											<option value="<?= $key->satuan_id ?>"><?= $key->satuan_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Harga <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="harga_eceran"
										   class="input-numeral form-control" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Minimal Stok <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="produk_minimal_stock"
										   class="input-numeral form-control" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Diskon % <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="disc_persen"
										   class="input-numeral form-control" value="" required="">
								</div>

							</div>
						</div>


					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal" id="kt_modal_edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Edit Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form action="<?= base_url() ?>produk/edit" method="post" id="kt_edit_staff_form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-label ">Kode Produk<b
											class="label--required">*</b></label>
									<input type="hidden" name="produk_id">
									<input type="text" placeholder="" name="produk_kode" class="form-control" value=""
										   required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Jenis Produk <b
											class="label--required">*</b></label>
									<select class="form-control col-md-12" name="produk_jenis_id" required="">
										<option value="">Pilih Jenis</option>
										<?php
										foreach ($jenis_produk as $key) {
											?>
											<option
												value="<?= $key->jenis_produk_id ?>"><?= $key->jenis_produk_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Size <b class="label--required">*</b></label>
									<select class="form-control col-md-12 kt_select_2" name="size_id" required="">
										<option value="">Pilih Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Good Size</label>
									<select class="form-control col-md-12 kt_select_2" name="good_size_id">
										<option value="">Pilih Good Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Bed Size</label>
									<select class="form-control col-md-12 kt_select_2" name="bed_size_id">
										<option value="">Pilih Bed Size</option>
										<?php
										foreach ($size as $key) {
											?>
											<option value="<?= $key->size_id ?>"><?= $key->size_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Color <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="color_id" required="">
										<option value="">Pilih Color</option>
										<?php
										foreach ($color as $key) {
											?>
											<option value="<?= $key->color_id ?>"><?= $key->color_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Weight gram/pc <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="weight_pc"
										   class="form-control input-numeral" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Weight gram/m2 <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="weight_m" class="form-control input-numeral"
										   value="" required="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-label ">Barcode <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="barcode" class="form-control" value=""
										   required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Fabric <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="fabric_id" required="">
										<option value="">Pilih Fabric</option>
										<?php
										foreach ($fabric as $key) {
											?>
											<option value="<?= $key->fabric_id ?>"><?= $key->fabric_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Patern <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="patern_id" required="">
										<option value="">Pilih Patern</option>
										<?php
										foreach ($patern as $key) {
											?>
											<option value="<?= $key->patern_id ?>"><?= $key->patern_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Style</label>
									<input type="text" placeholder="" name="style" class="form-control" value="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Pile</label>
									<input type="text" placeholder="" name="file" class="form-control" value="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="produk_satuan_id" required="">
										<option value="">Pilih Satuan</option>
										<?php
										foreach ($satuan as $key) {
											?>
											<option value="<?= $key->satuan_id ?>"><?= $key->satuan_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="form-control-label ">Harga <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="harga_eceran"
										   class="input-numeral form-control" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Minimal Stok <b
											class="label--required">*</b></label>
									<input type="text" placeholder="" name="produk_minimal_stock"
										   class="input-numeral form-control" value="" required="">
								</div>
								<div class="form-group">
									<label class="form-control-label ">Diskon % <b class="label--required">*</b></label>
									<input type="text" placeholder="" name="disc_persen"
										   class="input-numeral form-control" value="" required="">
								</div>

							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal" id="kt_modal_detail_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Detail Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
								<div class="col-9">
									<label name="produk_kode" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Barcode</label>
								<div class="col-9">
									<label name="barcode" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Jenis Produk</label>
								<div class="col-9">
									<label name="jenis_produk_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Size</label>
								<div class="col-9">
									<label name="size_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Color</label>
								<div class="col-9">
									<label name="color_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Weight gram/pc</label>
								<div class="col-9">
									<label name="weight_pc" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Weight gram/m2</label>
								<div class="col-9">
									<label name="weight_m" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Fabric</label>
								<div class="col-9">
									<label name="fabric_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Patern</label>
								<div class="col-9">
									<label name="patern_nama" class="col-form-label"></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Style</label>
								<div class="col-9">
									<label name="style" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Pile</label>
								<div class="col-9">
									<label name="file" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Satuan</label>
								<div class="col-9">
									<label name="satuan_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Harga Normal</label>
								<div class="col-9">
									<label name="harga_eceran" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Minimal Stok</label>
								<div class="col-9">
									<label name="produk_minimal_stock" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Diskon %</label>
								<div class="col-9">
									<label name="disc_persen" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Dibuat Pada</label>
								<div class="col-9">
									<label name="created_at" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Dirubah Pada</label>
								<div class="col-9">
									<label name="updated_at" class="col-form-label"></label>
								</div>
							</div>
						</div>
						<h5>List Harga</h5>
						<div class="col-md-12">
							<table class="table table-bordered table-hover table-checkable">
								<thead>
								<tr>
									<th>No</th>
									<th>Minimal Jumlah</th>
									<th>Harga</th>
								</tr>
								</thead>
								<tbody id="view_child_data">
								<tr>

								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="kt_modal_barcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
		 aria-hidden="true">
		<input type="hidden" value="<?= base_url('barcode-print') ?>" id="link-print">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Print Barcode</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">

								<label class="form-control-label ">Barcode <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="number" class="form-control" value=""
									   required="" readonly>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah Print <b class="label--required">*</b></label>
								<input type="number" placeholder="" name="jumlah" class="form-control" value="1"
									   required="">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="cetak" type="button" class="btn btn-primary">Cetak</button>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
