<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Jadwal Mobil Besok</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>arus-kas" class="kt-subheader__breadcrumbs-link">List Mobil</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Arus Kas
				</h3>
				<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
				<input type="hidden" id="list_url" value="<?= base_url() ?>list/jadwal-besok" name="">
				<div style="display: none;" id="table_column"><?= $column ?></div>
				<?php if (isset($columnDef)) { ?>
					<div style="display: none;" id="table_columnDef"><?= $columnDef ?></div>
				<?php } ?>

				<div style="display: none;" id="table_action"
					 data-style="dropdown"><?= (isset($action) ? $action : "") ?></div>
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-12 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable">
				<thead>
				<tr>
					<th width="30">No</th>
					<th>Tanggal</th>
					<th>Jenis Mobil</th>
					<th>Plat Mobil</th>
					<th>Status</th>
					<th>Customer</th>
					<th>Driver</th>
					<th>Jam Ambil</th>
					<th>Keterangan</th>
					<th width="160">Action</th>
				</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Jadwal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?= base_url() ?>jadwal-mobil/update" method="post" id="kt_edit_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="jadwal_id">
							<div class="form-group">
								<label class="form-control-label ">Jenis Mobil <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="mobil_jenis_nama" class="form-control"
									   value="" required="" readonly>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Plat <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="mobil_plat" class="form-control"
									   value="" required="" readonly>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Tanggal <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="tanggal" class="form-control"
									   value="" required="" readonly>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jam Ambil <b class="label--required">*</b></label>
								<input type="time" placeholder="" name="jam_ambil" class="form-control"
									   value="" required="">
							</div>

						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

