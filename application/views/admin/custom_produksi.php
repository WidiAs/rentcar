<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Produksi</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>produksi" class="kt-subheader__breadcrumbs-link">Produksi Custom</a>

		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
						<li class="nav-item">
							<a class="nav-link <?=(($this->uri->segment(1)=='custom-produksi') ? 'active' : '')?>" href="<?=base_url()?>custom-produksi">
								<i class="fa fa-toolbox"></i> <span class="kt--visible-desktop-inline-block">Produksi</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?=(($this->uri->segment(1)=='custom-history-produksi') ? 'active' : '')?>" href="<?=base_url()?>custom-history-produksi">
								<i class="flaticon2-time"></i> <span class="kt--visible-desktop-inline-block">History Produksi</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<input type="hidden" id="produksi_kode" value="<?=$produksi_kode?>" name="">
			<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
			<input type="hidden" id="list_url" value="<?=base_url()?>custom-produksi/list" name="">
			<div style="display: none;" id="table_column"><?=$column?></div>
			<?php if(isset($columnDef)) {  ?>
				<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
			<?php } ?>

			<div style="display: none;" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
				<tr>
					<th width="30">No</th>
					<th>Kode Produksi</th>
					<th>Deskripsi Item</th>
					<th>Tanggal Mulai</th>
					<th>Estimasi Selesai</th>
					<th>Status Produksi</th>
					<th>Tanggal Selesai</th>
					<th>Keterangan</th>
					<th>Status Penerimaan</th>
					<th>Tanggal Pengiriman</th>
					<th width="200">Action</th>
				</tr>
				</thead>
				<tbody id="child_data_ajax">

				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_detail_produksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Detail Produksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Kode Produksi</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="produksi_kode" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tanggal Mulai</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="tanggal_mulai" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Estimasi Selesai</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="estimasi_selesai" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Jumlah Item</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="jumlah_pesan" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Status Produksi</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="status_produksi" class="col-form-label"></label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tanggal Selesai</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="tanggal_selesai" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Status Penerimaan</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="status_penerimaan" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tanggal Penerimaan</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="tanggal_penerimaan" class="col-form-label"></label>
							</div>
						</div>
<!--						<div class="form-group row">-->
<!--							<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>-->
<!--							<label for="example-text-input" class="col-1 col-form-label">:</label>-->
<!--							<div class="col-8">-->
<!--								<label name="keterangan" class="col-form-label"></label>-->
<!--							</div>-->
<!--						</div>-->
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<h5 style="text-align: center"><strong>Data Bahan</strong></h5>
					</div>
					<div class="col-12">
						<table class="table table-bordered table-hover table-checkable" >
							<thead>
							<tr>
								<th>Nama Bahan</th>
								<th>Jumlah</th>
							</tr>
							</thead>
							<tbody id="view_child_data">

							</tbody>
						</table>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_status_produksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?=base_url()?>custom-produksi/selesai-produksi" method="post" id="kt_selesai_produksi">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Status Produksi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal Selesai</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<input type="hidden" id="produksi_id" name="produk_custom_id" value="">
									<input type="hidden" id="lokasi_bahan_id" name="lokasi_bahan_id" value="">
									<input type="text" class="form-control kt-input tanggal" name="tanggal_selesai" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=date("Y-m-d")?>" required="" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="kt_produksi_status_submit" type="button" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="modal" id="kt_penerimaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?=base_url()?>custom-produksi/penerimaan-produksi" method="post" id="kt_penerimaan_produksi">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Penerimaan Produksi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal Penerimaan</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<input type="hidden" id="produksi_id_penerimaan" name="produk_custom_id" value="">
									<input type="text" class="form-control kt-input tanggal" name="tanggal_penerimaan" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=date("Y-m-d")?>" required="" />
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Lokasi Penerimaan</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<select class="form-control col-md-12" name="lokasi_penerimaan_id" required="">
										<option value="">Pilih Lokasi</option>
										<?php
										foreach ($lokasi as $key) {
											?>
											<option class="lokasi_option" value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Harga Pokok</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<input type="text" class="form-control input-numeral" name="hpp" value="0">
								</div>
							</div>
							<div id="hpp_container">

							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="kt_produksi_penerimaan_submit" type="button" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>



