<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Produksi</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>custom-produksi" class="kt-subheader__breadcrumbs-link">Produksi Custom</a>
			<input type="hidden" id="base_url" name="" value="<?=base_url()?>">
			<input type="hidden" name="" id="no_produk" value="<?=$no_produk?>">
			<input type="hidden" name="" id="no_bahan" value="<?=$no_bahan?>">
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Edit Produksi
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<form action="<?=base_url()?>custom-produksi/save-start" method="post" id="kt_add">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Kode Produksi</label>
								<div class="col-9">
									<input type="hidden" name="produksi_kode" value="<?="PC-".$produk_custom->produk_custom_id?>">
									<input type="hidden" name="produksi_id" value="<?=$produk_custom->produk_custom_id?>">
									<label name="produksi_kode" class="col-form-label"><?="PC-".$produk_custom->produk_custom_id?></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal Mulai <b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" class="form-control kt-input tanggal" name="tanggal_mulai" placeholder="Dari" autocomplete="off" data-col-index="5"
										   value="<?=$produk_custom->tanggal_mulai?>"
										   required="" />
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Estimasi Selesai <b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" class="form-control kt-input tanggal" name="estimasi_selesai" placeholder="Dari" autocomplete="off" data-col-index="5"
										   value="<?=$produk_custom->estimasi_selesai?>"
										   required="" />
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Lokasi Asal Bahan</label>
								<div class="col-9">
									<select class="form-control col-md-12 lokasi-bahan" name="lokasi_bahan_id" required=""

									>
										<?="okok".$lokasi_bahan_id?>
										<option value="">Pilih Lokasi</option>
										<?php
										foreach ($lokasi as $key) {
											?>
											<option class="lokasi_option" value="<?=$key->lokasi_id?>" <?=($key->lokasi_id == $lokasi_bahan_id ? 'selected' : '')?>><?=$key->lokasi_nama?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Deskripsi Produk</label>
								<div class="col-9">
									<label name="deskripsi" class="col-form-label"><?=$produk_custom->deskripsi?></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Jumlah Pemesanan</label>
								<div class="col-9">
									<label name="deskripsi" class="col-form-label"><?=number_format($produk_custom->jumlah_pesan)?></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
								<div class="col-9">
									<textarea class="form-control" rows="3" name="keterangan">
										<?=$produk_custom->note?>
									</textarea>
								</div>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-8"><h5><strong>List Bahan</strong></h5></div>
						<div class="col-md-4">
							<button type="button" class="btn btn-primary pull-right add-bahan" id="add_bahan"  style="margin:5px;"><i class="flaticon2-plus"></i>&nbsp;Tambah</button>
						</div>
						<div class="bahan-container col-12" id="bahan_container">
							<?php
								$i = 1;
								foreach ($item as $key => $value){
									?>
									<div class="row" id="bahan_item_<?=$i?>">
										<div class="col-md-5">
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Bahan <b class="label--required">*</b></label>
												<div class="col-9">
													<div class="input-group col-12">
														<input type="text" class="form-control readonly" name="item_bahan_nama_<?=$i?>" id="item_bahan_nama_<?=$i?>" required="" autocomplete="off" value="<?=$value['nama']?>">
														<input type="hidden" class="form-control" id="item_bahan_id_<?=$i?>" name="item[<?=$key?>][bahan_id]" value="<?=$value['bahan_id']?>"><div class="input-group-append">
															<button class="btn btn-primary bahan-search" type="button" data-toggle="modal" data-no="<?=$i?>"><i class="flaticon-search"></i></button>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Jumlah <b class="label--required">*</b></label>
												<div class="col-6">
													<input type="text" class="form-control input-numeral jumlah-bahan" autocomplete="off" data-no="<?=$i?>" "="" value="<?=$value['jumlah']?>" id="item_bahan_jumlah_<?=$i?>" name="item[<?=$key?>][jumlah]">
												</div>
												<label class="col-3 col-form-label" id="item_bahan_satuan_<?=$i?>"></label>
											</div>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-danger btn-sm delete-bahan" data-no="<?=$i?>" data-item-bahan="bahan_item_<?=$i?>"><i class="flaticon2-trash"></i></button>
										</div>
									</div>
									<?php
									$i++;
								}
							?>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="<?=base_url()?>custom-produksi" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
													<span>
														<i class="la la-angle-double-left"></i>
														<span>Kembali ke Daftar</span>
													</span>
					</a>
					<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Data Bahan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="list_bahan" value="<?=base_url()?>custom-produksi/utility/list-bahan">
					<div class="col-md-12">
						<table class="table table-striped- table-hover table-checkable" id="bahan-table">
							<thead>
							<tr>
								<th>Kode Bahan</th>
								<th>Nama Bahan</th>
								<th>Stok</th>
								<th width="60">Aksi</th>
							</tr>
							</thead>
							<tbody id="bahan_child"></tbody>
						</table>
					</div>
				</div>


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

