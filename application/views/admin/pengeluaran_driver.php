<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Pengeluaran</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<input type="hidden" id="redirect" value="<?=base_url()?>laporan/laporan-history-driver">

	</div>
</div>

<div id="konten-utama">
	<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
		<form action="<?= base_url() ?>pengeluaran-driver/update" method="post" id="save-form">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body">
					<div class="row" id="guest-form">
						<div class="col-md-6">
							<div class="form-group row">
								<input type="hidden" name="driver_id" value="<?=$kegiatan_driver->driver_id?>">
								<input type="hidden" name="tanggal" value="<?=$kegiatan_driver->tanggal?>">
								<label for="example-text-input" class="col-2 col-form-label">Driver</label>
								<div class="input-group col-10">
									<label class="col-2 col-form-label"><?= $kegiatan_driver->driver_nama ?></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-2 col-form-label">Mobil</label>
								<div class="col-10">
									<label class="col-2 col-form-label"><?= $kegiatan_driver->mobil_jenis ?></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-2 col-form-label">Tanggal</label>
								<div class="col-10">
									<label
										class="col-2 col-form-label"><?= Date('d-m-Y', strtotime($kegiatan_driver->tanggal)) ?></label>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="kt-portlet kt-portlet--mobile" style="overflow-x: auto">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="dropdown dropdown-inline">
								<button type="button" class="btn btn-brand btn-icon-sm" id="button_add_pengeluaran" data-count="0">
									<i class="flaticon2-plus"></i> Tambah Data
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<table class="table table-striped- table-hover table-checkable">
						<thead>
						<tr>
							<th>Jam</th>
							<th>Jenis Pengeluaran</th>
							<th>Jumlah</th>
							<th width="30">Aksi</th>
						</tr>
						</thead>
						<tbody id="item_child">
						<?php foreach ($pengeluaran_driver as $key => $p) { ?>
							<tr id="row_<?=$key?>">
								<td><input type="time" class="form-control" name="jam[<?=$key?>]" value="<?=Date('H:i', strtotime($p->tanggal))?>" required></td>
								<td><input type="text" class="form-control" name="nama_pengeluaran[<?=$key?>]" value="<?=$p->nama_pengeluaran?>" required></td>
								<td><input type="text" class="form-control input-angka" name="jumlah[<?=$key?>]" value="<?=number_format($p->jumlah)?>" required></td>
								<td><button class="btn btn-danger delete-btn" data-no="<?=$key?>"><i class="flaticon2-trash"></i></button></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-lg-5 offset-lg-7">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body row">
							<div class="col-lg-12">
								<div class="form-group kt-form__group row">
									<label for="example-text-input" class="col-4 col-form-label">Total</label>
									<div class="col-8 col-form-label">
										<input type="hidden" name="grand_total" value="0">
										<strong><span class="total" id="total-item">0</span></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	<div class="pos-floating-button">
		<button class="btn btn-success btn-pill btn-lg" id="btn-simpan"><i class="fa fa-arrow-right"></i>&nbsp; Simpan
		</button>
	</div>
</div>
