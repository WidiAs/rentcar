<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Arus Kas</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?= base_url() ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?= base_url() ?>arus-kas" class="kt-subheader__breadcrumbs-link">Arus Kas List</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Arus Kas
				</h3>
				<input type="hidden" id="base_url" value="<?= base_url() ?>" name="">
				<input type="hidden" id="list_url" value="<?= base_url() ?>arus-kas/list" name="">
				<div style="display: none;" id="table_column"><?= $column ?></div>
				<?php if (isset($columnDef)) { ?>
					<div style="display: none;" id="table_columnDef"><?= $columnDef ?></div>
				<?php } ?>

				<div style="display: none;" id="table_action"
					 data-style="dropdown"><?= (isset($action) ? $action : "") ?></div>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add"
								data-toggle="modal">
							<i class="flaticon2-plus"></i> Tambah Data
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-12 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
								</div>
							</div>
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Tanggal</label>
									</div>
									<div class="kt-form__control">
										<div class="input-daterange input-group" id="kt_datepicker">
											<input type="text" class="form-control kt-input searchInput"
												   name="start_date" placeholder="Dari" autocomplete="off"
												   data-col-index="1" value="" data-field="tanggal_start"/>
											<div class="input-group-append">
												<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
											</div>
											<input type="text" class="form-control kt-input searchInput" name="end_date"
												   placeholder="Sampai" autocomplete="off" data-col-index="1" value=""
												   data-field="tanggal_end"/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-form__group kt-form__group--inline">
									<div class="kt-form__label">
										<label>Tipe Pembayaran</label>
									</div>
									<div class="kt-form__control">
										<select class="form-control searchInput" data-col-index="8"
												data-field="suplier_id">
											<option value="">All</option>
											<?php
											foreach ($tipe_pembayaran as $key) {
												?>
												<option value="<?=$key->tipe_pembayaran_id?>"><?=$key->tipe_pembayaran_nama." ".$key->no_akun?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
								<div class="btn-group btn-group btn-pill btn-group-sm" style="float : right !important;">
									<a href="<?= base_url() ?>arus-kas/pdf"
									   class="btn btn-danger akses-pdf" id="akses-pdf">
										<i class="la la-file-pdf-o"></i> Print PDF
									</a>
									<a href="<?= base_url() ?>arus-kas/excel"
									   class="btn btn-success akses-excel" id="akses-excel">
										<i class="la la-file-excel-o"></i> Print Excel
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable">
				<thead>
				<tr>
					<th width="30">No</th>
					<th width="90">Tanggal</th>
					<th>Jenis</th>
					<th>Jumlah</th>
					<th>Tipe Pembayaran</th>
					<th>Keterangan</th>
					<th>Staff</th>
					<?php if (!isset($_SESSION["redpos_login"]['lokasi_id'])){ ?>
						<th>Cabang</th>
					<?php } ?>
					<th width="160">Action</th>
				</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Arus Kas</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?= base_url() ?>arus-kas/insert" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">

							<div class="form-group">
								<label class="control-label">Tanggal <b class="label--required">*</b></label>
								<input type="text" readonly placeholder="" name="tanggal"
									   class="form-control tanggal-indonesia" value="<?= date('d-m-Y') ?>" required=""
									   autocomplete="off">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jenis <b
										class="label--required">*</b></label>
								<select class="form-control col-md-12" name="jenis" required="">
									<option value="pemasukan">Pemasukan</option>
									<option value="pengeluaran">Pengeluaran</option>
								</select>
							</div>
							<?php if(!isset($_SESSION["redpos_login"]["lokasi_id"])) {?>
								<div class="form-group">
									<label class="form-control-label ">Cabang <b
											class="label--required">*</b></label>
									<select class="form-control col-md-12" name="lokasi_id" required="">
										<option value="">Pilih Cabang</option>
										<?php
										foreach ($lokasi as $key) {
											?>
											<option value="<?= $key->lokasi_id ?>"><?= $key->lokasi_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
							<?php } ?>
							<div class="form-group">
								<label class="form-control-label ">Tipe Pembayaran <b
										class="label--required">*</b></label>
								<select class="form-control col-md-12" name="tipe_pembayaran_id" required="">
									<option value="">Pilih Tipe Pembayaran</option>
									<?php
									foreach ($tipe_pembayaran as $key) {
										?>
										<option value="<?= $key->tipe_pembayaran_id ?>"><?= $key->tipe_pembayaran_nama ?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="jumlah" class="input-numeral form-control"
									   value="" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
								<textarea class="form-control" name="keterangan" required=""></textarea>
							</div>

						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
	 aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Arus Kas</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?= base_url() ?>arus-kas/update" method="post" id="kt_edit_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="arus_kas_id">
							<div class="form-group">
								<label class="control-label">Tanggal <b class="label--required">*</b></label>
								<input type="text" readonly placeholder="" name="tanggal"
									   class="form-control tanggal-indonesia" value="<?= date('d-m-Y') ?>" required=""
									   autocomplete="off">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jenis <b
										class="label--required">*</b></label>
								<select class="form-control col-md-12" name="jenis" required="">
									<option value="pemasukan">Pemasukan</option>
									<option value="pengeluaran">Pengeluaran</option>
								</select>
							</div>
							<?php if(!isset($_SESSION["redpos_login"]["lokasi_id"])) {?>
								<div class="form-group">
									<label class="form-control-label ">Cabang <b
											class="label--required">*</b></label>
									<select class="form-control col-md-12" name="lokasi_id" required="">
										<option value="">Pilih Cabang</option>
										<?php
										foreach ($lokasi as $key) {
											?>
											<option value="<?= $key->lokasi_id ?>"><?= $key->lokasi_nama ?></option>
											<?php
										}
										?>
									</select>
								</div>
							<?php } ?>
							<div class="form-group">
								<label class="form-control-label ">Tipe Pembayaran <b
										class="label--required">*</b></label>
								<select class="form-control col-md-12" name="tipe_pembayaran_id" required="">
									<option value="">Pilih Tipe Pembayaran</option>
									<?php
									foreach ($tipe_pembayaran as $key) {
										?>
										<option value="<?= $key->tipe_pembayaran_id ?>"><?= $key->tipe_pembayaran_nama ?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="jumlah" class="input-numeral form-control"
									   value="" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
								<textarea class="form-control" name="keterangan" required=""></textarea>
							</div>

						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

