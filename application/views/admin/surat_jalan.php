<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Surat Jalan</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
<!--			<a href="--><?//=base_url()?><!--rekapitulasi-pos" class="kt-subheader__breadcrumbs-link">POS</a>-->
<!--			<span class="kt-subheader__breadcrumbs-separator"></span>-->
			<a href="<?=base_url()?>surat-jalan" class="kt-subheader__breadcrumbs-link">Surat Jalan</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='surat-jalan') ? 'active' : '')?>" href="<?=base_url()?>surat-jalan">
							<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Surat Jalan</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='history-sj') ? 'active' : '')?>" href="<?=base_url()?>history-sj">
							<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">History Surat Jalan</span>
						</a>
					</li>
				</ul>
			</div>
			<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
			<input type="hidden" id="list_url" value="<?=base_url()?>surat-jalan/list" name="">
			<div style="display: none;" id="table_column"><?=$column?></div>
			<?php if(isset($columnDef)) {  ?>
				<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
			<?php } ?>
			<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
			<div style="display: none;" data-width="150" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
				<tr>
					<th width="30">No</th>
					<th>Tanggal</th>
					<th>No Faktur</th>
					<th>lokasi</th>
					<th>Pelanggan</th>
					<th>Total</th>
					<th>Tambahan</th>
					<th>Potongan</th>
					<th>Grand Total</th>
					<th>Tipe Pembayaran</th>
					<th width="160">Action</th>
				</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
				<tfoot >
				<tr>
					<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
					<td><strong></strong></td>
					<td><strong></strong></td>
					<td><strong></strong></td>
					<td><strong></strong></td>
					<td><strong></strong></td>
					<td><strong></strong></td>
				</tr>

				</tfoot>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>
