<!DOCTYPE html>

<html lang="en">


<head>

	<meta charset="utf-8">

	<title>Receipt</title>


	<!-- Normalize or reset CSS with your favorite library -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">


	<!-- Load paper.css for happy printing -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">


	<!-- Set page size here: A5, A4 or A3 -->

	<!-- Set also "landscape" if you need -->

	<style>
		/*@font-face {*/
		/*	font-family: Saxmono;*/
		/*	font-style: normal;*/
		/*	font-weight: normal;*/
		/*	src: url(*/<?//=base_url('assets/saxmono.ttf')?>/*) format('truetype');*/
		/**/
		/*}*/

		@page {
			size: 58mm;
			margin: 0 0 0 0;
		}

		/* output size */

		body.receipt .sheet {
			width: 210mm;
			padding: 0px
		}

		/* sheet size */

		@media print {
			body.receipt .sheet {
				width: 210mm;
				padding: 0px;
				margin: 0 0 0 0;
			}

			@page {
				margin: 0 0 0 0
			}
		}

		/* fix for Chrome */

		p {
			margin: 0px;
		}


		@media screen {

			body {
				background: #e0e0e0
			}

		}



		.table-data {
			width: 100%;
		}

		.table-data td,
		.table-data th {
			padding: 2px 4px;
			text-align: center;
			font-size: 10px;
			font-family: Arial;
		}

		.table-data th {
			border-top: 1px solid !important;
			border-bottom: 1px solid !important;
			font-family: Arial;
		!Important;

		}

		.table-total {
			width: 100%;
		}

		.table-total td,
		.table-total th {
			padding: 2px 4px;
			text-align: center;
			font-size: 10px;
			font-family: Arial;
		}

		.table-total th {
			border-top: 1px solid !important;
			border-bottom: 0;
			font-family: Arial;
		!Important;

		}

		.table-foot {
			width: 100%;
		}

		.table-foot td,
		.table-foot th {
			padding: 2px 4px;
			text-align: center;
			font-size: 10px;
			font-family: Arial;
		}

		footer {
			position: fixed;
			left: 20px;
			right: 20px;
			bottom: 20px;
			text-align: center;
			background-color: #FFFFFF;
			height: 100px;
			bottom: 25px !important;
		}

		@page {
			margin: 20px;
		}

		body {
			margin: 0px;
		}

		br {
			/*display: block;*/
			margin: 10px 0;
		}

	</style>

</head>


<body class="receipt" onload="window.print()">
<!--<body class="receipt">-->

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


	<table width="100%">
		<tbody>
		<tr>
			<td style="font-family: Arial; font-size: 11px">Balioz Linen</td>
		</tr>
		</tbody>
	</table>
	<table width="100%">
		<tr>
			<td width="80%" style="font-family: Arial; font-size: 11px">SURAT JALAN
				<br>No. Surat Jalan :<span><?= $sj->no_surat_jalan ?></span>
				<br>Tanggal Order : <span><?= $sj->tanggal_kirim ?></span>
				<br>
			</td>
			<td style="font-family: Arial; font-size: 11px">
				<span>KIRIM KE :</span> <span><?= $sj->pelanggan ?></span>
				<br><span></span><span><?= $sj->alamat ?></span>
				<br><span></span><span>Kontak : <?= $sj->kontak ?></span>
				<br>
			</td>
		</tr>
	</table>
	<table
		style="margin-top: 0px; margin-bottom: 10px; font-size: 10px; font-weight: normal !important; border-bottom: 0px !important;"
		width="100%"
		class="table-data">
		<thead>
		<tr>
			<th width="3%">No</th>
			<th width="20%">Kode Produk</th>
			<th width="20%">Nama Produk</th>
			<th width="10%">Qty Kirim</th>
		</tr>
		</thead>
		<tbody>
		<?php $no = 1 ?>
		<?php foreach ($item as $i) { ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $i->produk_kode ?></td>
				<td><?= $i->produk_nama ?></td>
				<td style="text-align: right; padding-right: 30px"><?= $i->qty_kirim_label ?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php
	$sisa = 10 - $no;
	for ($x = 0; $x <= $sisa; $x++) {
		echo '<br>';
	}
	?>
	<table width="100%" style="position: relative;">
		<tbody>
		<tr>
			<td align="center" width="50%" style="font-family: Arial; font-size: 10px">Supir
				<br>
				<br>
				<br>
				<br>
				<br>
				<p>(................................)</p> <br></td>

			<td align="center" width="50%" style="font-family: Arial; font-size: 10px">Penerima
				<br>
				<br>
				<br>
				<br>
				<br>
				<p>(................................)</p> <br></td>
		</tr>
		</tbody>
	</table>

</section>

</body>

</html>
