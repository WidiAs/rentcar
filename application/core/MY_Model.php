<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	var $table_name = '';
	public function __construct()
	{
		parent::__construct();
		
	}
	function delete_by_id($field_name,$field_id){
		$this->db->where($field_name, $field_id);
		return $this->db->delete($this->table_name);
	}
	function delete_all(){
		return $this->db->delete($this->table_name);
	}
	function insert($data){
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table_name, $data);
	}
	function last_id(){
		return $this->db->insert_id();
	}
	function update_by_id($field_name,$field_id,$data){
		$data['updated_at'] = date('Y-m-d H:i:s');
		$this->db->where($field_name, $field_id);
		return $this->db->update($this->table_name, $data);
	}
	function update_by_condition($where,$data){
		$this->db->where($where);
		return $this->db->update($this->table_name, $data);
	}
	function detail($field_name,$field_id){
		$this->db->where($field_name, $field_id);
		return $this->db->get($this->table_name);		
	}
	function all_list(){
		return $this->db->get($this->table_name)->result();
	}
	function row_by_id($id){
		$this->db->where($this->table_name."_id", $id);
		return $this->db->get($this->table_name)->row();
	}
	function row_by_condition($where, $field = null, $order = 'asc'){
		$this->db->where($where);
		if ($field != null){
			$this->db->order_by($field, $order);
		}
		return $this->db->get($this->table_name)->row();
	}
	function result_by_condition($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result();
	}
	function result_by_field($column, $condition){
		$this->db->where($column, $condition);
		return $this->db->get($this->table_name)->result();
	}
	function string_to_number($string){
		$temp = str_replace(".", "", $string);
		$temp = str_replace(",", "", $temp);
		return $temp;
	}
	function string_to_decimal($string){
		$temp = str_replace(",", "", $string);
		return $temp;
	}
	function start_trans(){
		$this->db->trans_begin();		
	}
	function result_trans(){
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;		
	}
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
