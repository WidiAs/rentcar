<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "penjualan";
	}

	function save()
	{
		$this->load->model('stock_produk', '', true);
		$this->db->trans_begin();
		$data["lokasi_id"] = $this->input->post('lokasi_id');
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if (isset($_SESSION["login"]["lokasi_id"])) {
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$potongan_persen = $transaksi->potongan_persen;
		$nominal_potongan_persen = $transaksi->nominal_potongan_persen;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;
		$sisa = $grand_total - $terbayar;
		if ($sisa < 0) {
			$sisa = 0;
		}
		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["potongan_persen"] = $potongan_persen;
		$data["nominal_potongan_persen"] = $nominal_potongan_persen;
		$data["grand_total"] = $grand_total;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d", strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["keterangan_tambahan"] = $this->input->post('keterangan_tambahan');
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = $sisa;
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id'] : null;
		$data["kembalian"] = $kembalian;
		$data["created_at"] = date("Y-m-d H:i:s");
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if ($jenis_pembayaran == "kredit") {
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		foreach ($transaksi->item as $key) {
			$harga_net = $key->harga_eceran;
			$sisa_item = 0;
			if ($key->tipe_penjualan == "grosir") {
				$harga_net = $key->harga_grosir;
			}

			if ($this->input->post('pengiriman') == 'ya') {
				$sisa_item = $this->string_to_number($key->jumlah);
			}

			$old_data = $this->stock_produk->row_by_id($key->stock_produk_id);
			$data = array();
			$data['stock_produk_qty'] = $old_data->stock_produk_qty - $key->jumlah;
			$this->db->where('stock_produk_id', $key->stock_produk_id);
			$this->db->update('stock_produk', $data);
			$data = array();
			$data["penjualan_id"] = $penjualan_id;
			$data["stock_produk_id"] = $key->stock_produk_id;
			$data["produk_id"] = $key->produk_id;
			$data["qty"] = $this->string_to_number($key->jumlah);
			$data["sisa_qty"] = $sisa_item;
			$data["diskon"] = $this->string_to_number($key->diskon);
			$data["diskon_nominal"] = $this->string_to_number($key->diskon_nominal);
			$data["harga"] = $this->string_to_number($harga_net);
			$data["ppn_persen"] = "0.1";
			$data["hpp"] = $this->string_to_number($key->hpp);
			$data["sub_total"] = $this->string_to_number($key->subtotal);
			$data["created_at"] = date("Y-m-d H:i:s");
			$this->db->insert('penjualan_produk', $data);

			if ($this->input->post('pengiriman') == 'ya') {
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_produk";
				$data["stock_produk_id"] = $key->stock_produk_id;
				$data["produk_id"] = $key->produk_id;
				$data["stock_out"] = $key->jumlah;
				$data["stock_in"] = 0;
				$data["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
				$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
				$data["keterangan"] = "Penjualan produk";
				$data["method"] = "delete";
				$this->stock_produk->arus_stock_produk($data);
			}

		}
		if ($jenis_pembayaran == "kredit") {
			$data = array();
			$data_piutang = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d", $temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if ($terbayar > 0) {
				$data_piutang['piutang_id'] = $piutang_id;
				$data_piutang['log_kasir_id'] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id'] : null;
				$data_piutang['tipe_pembayaran_id'] = 6;
				if ($this->input->post('jenis_piutang') != 'tunai'){
					$data_piutang['tipe_pembayaran_id'] = 14;
				}
				$data_piutang['jumlah'] = $terbayar;
				$temp = strtotime($this->input->post('tanggal'));
				$data_piutang['tanggal'] = date("Y-m-d", $temp);
				$data_piutang['keterangan'] = 'Pembayaran DP';
				$this->db->insert('pembayaran_piutang', $data_piutang);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}

	function custom_save()
	{
		$this->load->model('stock_produk', '', true);
		$this->db->trans_begin();
		$data["lokasi_id"] = $this->input->post('lokasi_id');
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if (isset($_SESSION["login"]["lokasi_id"])) {
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$potongan_persen = $transaksi->potongan_persen;
		$nominal_potongan_persen = $transaksi->nominal_potongan_persen;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;
		$sisa = $grand_total - $terbayar;
		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["potongan_persen"] = $potongan_persen;
		$data["nominal_potongan_persen"] = $nominal_potongan_persen;
		$data["grand_total"] = $grand_total;
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = $sisa;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d", strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id'] : null;
		$data["kembalian"] = $kembalian;
		$data["keterangan_tambahan"] = $this->input->post('keterangan_tambahan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if ($jenis_pembayaran == "kredit") {
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		foreach ($transaksi->item as $key) {
			$harga_net = $key->harga_eceran;
			if ($key->tipe_penjualan == "grosir") {
				$harga_net = $key->harga_grosir;
			}
			$sisa_qty = $this->string_to_number($key->jumlah);
			$data = array();
			$data['deskripsi'] = $key->des_produk;
			$data['note'] = $key->note;
			$data['satuan_id'] = $key->satuan;
			$data['harga_satuan'] = $this->string_to_number($key->harga_eceran);
			$data['jumlah_pesan'] = $this->string_to_number($key->jumlah);
			$data['created_at'] = Date("Y-m-d");
			$this->db->insert('produk_custom', $data);
			$produk_custom_id = $this->db->insert_id();
			$data = array();
			$data["penjualan_id"] = $penjualan_id;
			$data["sisa_qty"] = $sisa_qty;
			$data["produk_custom_id"] = $produk_custom_id;
			$data["qty"] = $this->string_to_number($key->jumlah);
			$data["harga"] = $this->string_to_number($harga_net);
			$data["diskon"] = $this->string_to_number($key->diskon);
			$data["diskon_nominal"] = $this->string_to_number($key->diskon_nominal);
			$data["ppn_persen"] = "0.1";
			$data["hpp"] = $this->string_to_number(0);
			$data["sub_total"] = $this->string_to_number($key->subtotal);
			$this->db->insert('penjualan_produk', $data);
		}
		if ($jenis_pembayaran == "kredit") {
			$data = array();
			$data_piutang = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d", $temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if ($terbayar > 0) {
				$data_piutang['piutang_id'] = $piutang_id;
				$data_piutang['tipe_pembayaran_id'] = 6;
				if ($this->input->post('jenis_piutang') != 'tunai'){
					$data_piutang['tipe_pembayaran_id'] = 14;
				}
				$data_piutang['jumlah'] = $terbayar;
				$temp = strtotime($this->input->post('tanggal'));
				$data_piutang['tanggal'] = date("Y-m-d", $temp);
				$data_piutang['keterangan'] = 'Pembayaran DP';
				$this->db->insert('pembayaran_piutang', $data_piutang);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}

	function getUrutan()
	{
		$date = date("Y-m-d");
		$this->db->select('max(urutan) as urutan');
		$this->db->where('tanggal', $date);
		$urutan = $this->db->get('penjualan')->row()->urutan;
		if ($urutan == null) {
			$urutan = 1;
		} else {
			$urutan++;
		}
		return $urutan;
	}

	function getFakturCode()
	{
		$date = date("ymd");
		$urutan = $this->getUrutan();
		$lok = "00";
		if (isset($_SESSION['login']['lokasi_id'])) {
			$lok = $_SESSION['login']['lokasi_id'];
		}
		$tipe = "PL";
		$faktur = $tipe . '-' . $date . '/' . $lok . '/' . $urutan;
		return $faktur;
	}

	function transaksi_count_all()
	{
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id');
		return $this->db->get('penjualan')->num_rows();
	}

	function transaksi_count_filter($query)
	{
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
		$this->db->join('user', 'log_kasir.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['tipe_pembayaran_id']) && $this->input->get('tipe_pembayaran_id') != "") {
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
			$this->db->where('staff.staff_id', $this->input->get('staff_id'));
		}
		$this->db->order_by('penjualan_id', 'desc');
		//echo $this->db->_compile_select();		
		return $this->db->get('penjualan')->num_rows();
	}

	function transaksi_list($start, $length, $query)
	{
		$this->db->select('penjualan.*,lokasi.lokasi_nama,tipe_pembayaran_nama');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id');
		$this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
		$this->db->join('user', 'log_kasir.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['tipe_pembayaran_id']) && $this->input->get('tipe_pembayaran_id') != "") {
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
			$this->db->where('staff.staff_id', $this->input->get('staff_id'));
		}
		$this->db->order_by('penjualan_id', 'desc');
		return $this->db->get('penjualan', $length, $start)->result();

	}

	function transaksi_by_log($log_kasir_id)
	{
		$this->db->select("if(sum(penjualan.grand_total) is null, 0, sum(penjualan.grand_total)) as total");
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran on penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('penjualan');
		$this->db->where('tipe_pembayaran.kembalian', '1');
		$this->db->where('penjualan.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}

	function transaksi_nontunai_by_log($log_kasir_id)
	{
		$this->db->select("if(sum(penjualan.grand_total) is null, 0, sum(penjualan.grand_total)) as total");
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran on penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('penjualan');
		$this->db->where('tipe_pembayaran.kembalian', '0');
		$this->db->where('tipe_pembayaran.jenis_pembayaran', 'kas');
		$this->db->where('penjualan.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}

	function detailTransaksi($id)
	{
		$this->db->select("penjualan_produk.*,concat_ws(' ',jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS 'produk_nama',produk.*,satuan.satuan_nama,produk_custom.deskripsi, b.satuan_nama as 'satuan_custom' ");
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id', 'left');
		$this->db->join('produk_custom', 'produk_custom.produk_custom_id = penjualan_produk.produk_custom_id', 'left');
		$this->db->join('satuan as b', 'b.satuan_id = produk_custom.satuan_id', 'left');
		$this->db->where('penjualan_produk.penjualan_id', $id);
		return $this->db->get('penjualan_produk')->result();
	}

	function laporan_detail_transaksi_count_all()
	{
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
		return $this->db->get('penjualan')->num_rows();
	}

	function laporan_detail_transaksi_count_filter($query)
	{
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
		$this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
		$this->db->join('user', 'log_kasir.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['produk_id']) && $this->input->get('produk_id') != "") {
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
			$this->db->where('staff.staff_id', $this->input->get('staff_id'));
		}
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->get('penjualan')->num_rows();
	}

	function laporan_detail_transaksi_list($start, $length, $query)
	{
		$this->db->select("penjualan.*,lokasi.lokasi_nama,produk.produk_kode,concat_ws(' ',jenis_produk.jenis_produk_nama,color.color_nama,fabric.fabric_nama,size.size_nama,patern.patern_nama) AS 'produk_nama',penjualan_produk.qty,penjualan_produk.harga,penjualan_produk.sub_total");
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
		$this->db->join('user', 'log_kasir.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->group_start();
		$this->db->or_like('size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['produk_id']) && $this->input->get('produk_id') != "") {
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
			$this->db->where('staff.staff_id', $this->input->get('staff_id'));
		}
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->get('penjualan', $length, $start)->result();

	}

	function mostPenjualanDashboardList()
	{
		$this->db->select('produk.produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,fabric.fabric_nama,size.size_nama,patern.patern_nama ) as produk_nama, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
		$this->db->group_by('penjualan_produk.produk_id');
		$this->db->group_by('penjualan.lokasi_id');
		$this->db->order_by('qty', 'desc');
		return $this->db->get('penjualan_produk', 5, 0)->result();
	}

	function mostPenjualanDashboardCount()
	{
		$this->db->select('produk.produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,fabric.fabric_nama,size.size_nama,patern.patern_nama ) as produk_nama, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
		$this->db->group_by('penjualan_produk.produk_id');
		$this->db->group_by('penjualan.lokasi_id');
		$this->db->order_by('qty', 'desc');
		return $this->db->get('penjualan_produk')->num_rows();
	}

	function leastPenjualanDashboardList()
	{
		$this->db->select('produk.produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama ) as produk_nama, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
		$this->db->group_by('penjualan_produk.produk_id');
		$this->db->group_by('penjualan.lokasi_id');
		$this->db->order_by('qty', 'asc');
		return $this->db->get('penjualan_produk', 5, 0)->result();
	}

	function leastPenjualanDashboardCount()
	{
		$this->db->select('produk.produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama ) as produk_nama, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
		$this->db->group_by('penjualan_produk.produk_id');
		$this->db->group_by('penjualan.lokasi_id');
		$this->db->order_by('qty', 'asc');
		return $this->db->get('penjualan_produk')->num_rows();
	}

	function minTanggalPenjualan()
	{
		$this->db->select('min(tanggal) as tanggal');
		return $this->db->get('penjualan')->row()->tanggal;
	}

	function totalPenjualan(){
		$this->db->select('sum(penyewaan.grand_total) as sales');
		$this->db->from('penyewaan');
		if($this->input->get('start_date')!=""){
			$this->db->where('tanggal_order >=',$this->input->get('start_date'));
		}else{
			$this->db->where('tanggal_order >=',date('Y-m-01'));
		}
		if($this->input->get('end_date')!=""){
			$this->db->where('tanggal_order <=',$this->input->get('end_date'));
		}
		if(!isset($_SESSION["login"]['lokasi_id'])) {
			if($this->input->get('lokasi_id')!="All" && $this->input->get('lokasi_id')){
				$this->db->where('lokasi_id =',$this->input->get('lokasi_id'));
			}
		}else{
			$this->db->where('lokasi_id =',$_SESSION["login"]['lokasi_id']);
		}

		return $this->db->get()->row()->sales;
	}

	function penjualanByMonth(){
		$this->db->select('sum(penyewaan.grand_total) as sales, concat_ws(" ",MONTH(penyewaan.tanggal_order),YEAR(penyewaan.tanggal_order)) as "label"');
		$this->db->from('penyewaan');
		$this->db->group_by('MONTH(penyewaan.tanggal_order)');
		$this->db->group_by('YEAR(penyewaan.tanggal_order)');
		return $this->db->get('')->result();
	}

	function penjualan_by_id($id)
	{
		$this->db->select('penjualan.*, lokasi.lokasi_nama');
		$this->db->from('penjualan');
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->where("penjualan_id", $id);
		return $this->db->get()->row();
	}

	function penjualan_produk_by_id($id)
	{
		$this->db->from('penjualan_produk');
		$this->db->where("penjualan_id", $id);
		return $this->db->get()->result();
	}

	function detailTransaksiSJ($id)
	{
		$this->db->select("penjualan_produk.*,concat_ws(' ',jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS 'produk_nama',produk.*,satuan.satuan_nama,produk_custom.deskripsi, b.satuan_nama as 'satuan_custom' ");
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id', 'left');
		$this->db->join('produk_custom', 'produk_custom.produk_custom_id = penjualan_produk.produk_custom_id', 'left');
		$this->db->join('satuan as b', 'b.satuan_id = produk_custom.satuan_id', 'left');
		$this->db->where('penjualan_produk.penjualan_id', $id);
		$this->db->where('penjualan_produk.sisa_qty > ', 0);
		return $this->db->get('penjualan_produk')->result();
	}

	function bayar_piutang_by_log($log_kasir_id)
	{
		$this->db->select("if(sum(pembayaran_piutang.jumlah) is null, 0, sum(pembayaran_piutang.jumlah)) as total");
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran on pembayaran_piutang.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('pembayaran_piutang');
		$this->db->where('tipe_pembayaran.kembalian', '1');
		$this->db->where('pembayaran_piutang.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}

	function bayar_piutang_nontunai_by_log($log_kasir_id)
	{
		$this->db->select("if(sum(pembayaran_piutang.jumlah) is null, 0, sum(pembayaran_piutang.jumlah)) as total");
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran on pembayaran_piutang.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('pembayaran_piutang');
		$this->db->where('tipe_pembayaran.kembalian', '0');
		$this->db->where('tipe_pembayaran.jenis_pembayaran', 'kas');
		$this->db->where('pembayaran_piutang.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}

	function jumlah_transaksi_by_log($log_kasir_id)
	{
		$this->db->where('penjualan.log_kasir_id', $log_kasir_id);
		return $this->db->get('penjualan')->num_rows();
	}

	function log_kasir_info($log_kasir_id)
	{
		$this->db->select('log_kasir.*,staff.staff_nama');
		$this->db->join('user', 'log_kasir.user_id=user.user_id');
		$this->db->join('staff', 'user.user_staff_id=staff.staff_id');
		$this->db->where('log_kasir_id', $log_kasir_id);
		return $this->db->get('log_kasir')->row();
	}
	// mendapatkan staff mana saja yang telah melakukan penjualan
	function staff_penjualan(){
		$this->db->select('staff.staff_id,staff.staff_nama');
		$this->db->distinct();
		$this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
		$this->db->join('user', 'log_kasir.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		return $this->db->get('penjualan')->result();
	}
}

/* End of file Pos.php */
/* Location: ./application/models/Pos.php */
