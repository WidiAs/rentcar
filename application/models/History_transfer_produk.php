<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_transfer_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "history_transfer_produk";
	}	
	function need_confirm_list($start,$length,$query){
		$this->db->where('status', 'Menunggu Konfirmasi');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
		}		
		$this->db->order_by('history_transfer_produk_id', 'desc');
		return $this->db->get('history_transfer_produk', $length, $start)->result();
	}
	function need_confirm_list_sort($start,$length,$query,$sort,$order){
		$sql = 'history_transfer_produk.*,
		concat_ws(" "
		,if(produk.style is null, "", concat("style : ",produk.style))
		,if(produk.tipe is null, "", concat("tipe : ",produk.tipe))
		,if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec"';

		$this->db->select($sql);
		$this->db->join('produk', 'history_transfer_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->where('status', 'Menunggu Konfirmasi');
		$this->db->group_start();
		$this->db->like('tanggal', $query, 'BOTH');
		$this->db->or_like('history_transfer_produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('history_transfer_produk.produk_nama', $query, 'BOTH');
		$this->db->or_like('history_transfer_produk.dari', $query, 'BOTH');
		$this->db->or_like('history_transfer_produk.tujuan', $query, 'BOTH');
		$this->db->or_like('history_transfer_produk.status', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
		}
		if ($sort == 'none'){
			$this->db->order_by('history_transfer_produk_id', 'desc');
		}else{
			$this->db->order_by($sort, $order);
//			$this->db->query("order by case when $sort is null then 1 else 0 end, $sort DESC", $order);
		}
		return $this->db->get('history_transfer_produk', $length, $start)->result();
	}
	function need_confirm_count_filter($query){
		$sql = 'history_transfer_produk.*,
		concat_ws(" "
		,if(produk.style is null, "", concat("style : ",produk.style))
		,if(produk.tipe is null, "", concat("tipe : ",produk.tipe))
		,if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec"';

		$this->db->select($sql);
		$this->db->join('produk', 'history_transfer_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->where('status', 'Menunggu Konfirmasi');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('history_transfer_produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('history_transfer_produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('history_transfer_produk.dari', $query, 'BOTH');
			$this->db->or_like('history_transfer_produk.tujuan', $query, 'BOTH');
			$this->db->or_like('history_transfer_produk.status', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
		}		
		return $this->db->get('history_transfer_produk')->num_rows();
	}
	function need_confirm_count_all(){
		$this->db->select('history_transfer_produk.*');
		$this->db->join('produk', 'history_transfer_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
		}		
		$this->db->where('status', 'Menunggu Konfirmasi');
		return $this->db->get('history_transfer_produk')->num_rows();
	}	
	function history_list($start,$length,$query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}	
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["login"]["lokasi_nama"]);
			$this->db->group_end();
		}				
		$this->db->order_by('history_transfer_produk_id', 'desc');
		return $this->db->get('history_transfer_produk', $length, $start)->result();
	}
	function history_count_filter($query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["login"]["lokasi_nama"]);
			$this->db->group_end();
		}		
		return $this->db->get('history_transfer_produk')->num_rows();
	}
	function history_count_all(){
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["login"]["lokasi_nama"]);
			$this->db->group_end();
		}		
		return $this->db->get('history_transfer_produk')->num_rows();
	}
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('history_transfer_produk');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}
}

/* End of file Histori_transfer_produk.php */
/* Location: ./application/models/Histori_transfer_produk.php */
