<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "po_produk";
	}
	function get_kode_po_produk(){
		$year = date("y");
		$month = date("m");
		$prefix = "POP/".$month.$year."/";
		$this->db->like('po_produk_no', $prefix, 'BOTH');
		$this->db->select('(max(urutan)+1) as kode');
		$this->db->from('po_produk');
		$result = $this->db->get()->row()->kode;
		if ($result != null){
			return $prefix.($result);
		} else {
			return $prefix."1";
		}
	}
	function po_produk_count_all(){
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_by('po_produk.po_produk_id');
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}

		return $this->db->get('po_produk')->num_rows();
	}
	function po_produk_count_filter($query){
		$this->db->select('po_produk.*,suplier.suplier_nama');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};
		$this->db->group_by('po_produk.po_produk_id');
		return $this->db->get('po_produk')->num_rows();
	}
	function po_produk_list($start,$length,$query){
		$this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->order_by('po_produk.po_produk_id', 'desc');
		return $this->db->get('po_produk', $length, $start)->result();
	}
	function po_produk_count_all_history(){
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->where('status_penerimaan', "Diterima");
		return $this->db->get('po_produk')->num_rows();
	}
	function po_produk_count_filter_history($query){
		$this->db->select('po_produk.*,suplier.suplier_nama');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->group_by('po_produk.po_produk_id');
		return $this->db->get('po_produk')->num_rows();
	}
	function po_produk_list_history($start,$length,$query){
		$this->db->select('po_produk.*,suplier.suplier_nama');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->order_by('po_produk.po_produk_id', 'desc');
		return $this->db->get('po_produk', $length, $start)->result();
	}
	function insert_po_produk(){
		$this->db->trans_begin();
		$po_no = $this->input->post('po_produk_no');
		$temp = explode("/", $po_no);
		$urutan = $temp[sizeof($temp)-1];
		$data['po_produk_no'] = $po_no;
		$data['urutan'] = $urutan;
		$data['suplier_id'] = $this->input->post('suplier_id');
		$temp = strtotime($this->input->post('tanggal_pemesanan'));
		$tanggal_pemesanan = date("Y-m-d",$temp);
		$data['tanggal_pemesanan'] = $tanggal_pemesanan;
		$data['keterangan'] = $this->input->post('keterangan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$data['jenis_pembayaran'] = $jenis_pembayaran;
		if($jenis_pembayaran == "kas"){
			$data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
			$data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
			$data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
			$data['status_pembayaran'] = "Lunas";
		} else {
			$data['tipe_pembayaran'] = null;
			$data['tipe_pembayaran_nama'] = "Kredit";
			$data['status_pembayaran'] = "Hutang";
		}
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["status_penerimaan"] = "Belum Diterima";
		$this->db->insert('po_produk', $data);
		$po_produk_id = $this->db->insert_id();
		$item = $this->input->post('item_produk');
		foreach ($item as $key) {
			$data = array();
			$data["po_produk_id"] = $po_produk_id;
			$data["produk_id"] = $key["produk_id"];
			$data["harga"] = $this->string_to_number($key["harga"]);
			$data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$this->db->insert('po_produk_detail', $data);
		}
		if($jenis_pembayaran != "kas"){
			$data = array();
			$data['po_produk_id'] = $po_produk_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
			$tenggat_pelunasan = date("Y-m-d",$temp);
			$data['tenggat_pelunasan'] = $tenggat_pelunasan;
			$this->db->insert('hutang', $data);
		}
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;
	}
	function edit_po_produk(){
		$this->db->trans_begin();
		$po_produk_id = $this->input->post('po_produk_id');
		$this->db->where('po_produk_id', $po_produk_id);
		$this->db->delete('po_produk_detail');
		$po_no = $this->input->post('po_produk_no');
		$temp = explode("/", $po_no);
		$urutan = $temp[sizeof($temp)-1];
		$data['po_produk_no'] = $po_no;
		$data['urutan'] = $urutan;
		$data['suplier_id'] = $this->input->post('suplier_id');
		$temp = strtotime($this->input->post('tanggal_pemesanan'));
		$tanggal_pemesanan = date("Y-m-d",$temp);
		$data['tanggal_pemesanan'] = $tanggal_pemesanan;
		$data['keterangan'] = $this->input->post('keterangan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$data['jenis_pembayaran'] = $jenis_pembayaran;
		if($jenis_pembayaran == "kas"){
			$data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
			$data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
			$data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
			$data['status_pembayaran'] = "Lunas";
		} else {
			$data['tipe_pembayaran'] = null;
			$data['tipe_pembayaran_nama'] = "Kredit";
			$data['status_pembayaran'] = "Hutang";
		}
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["status_penerimaan"] = "Belum Diterima";
		$this->db->where('po_produk_id', $po_produk_id);
		$this->db->update('po_produk', $data);
		$item = $this->input->post('item_produk');
		foreach ($item as $key) {
			$data = array();
			$data["po_produk_id"] = $po_produk_id;
			$data["produk_id"] = $key["produk_id"];
			$data["harga"] = $this->string_to_number($key["harga"]);
			$data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$this->db->insert('po_produk_detail', $data);
		}
		$data = array();
		$this->db->where('po_produk_id', $po_produk_id);
		$hutang = $this->db->get('hutang')->row();
		if($jenis_pembayaran != "kas"){
			if($hutang != null){
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->where('hutang_id', $hutang->hutang_id);
				$this->db->update('hutang', $data);
			} else {
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['po_produk_id'] = $po_produk_id;
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->insert('hutang', $data);
			}
		} else {
			$this->db->where('po_produk_id', $po_produk_id);
			$this->db->delete('hutang');
		}

		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;
	}
	function po_produk_detail_by_id($po_produk_id){
		$this->db->where('po_produk_detail.po_produk_id', $po_produk_id);
		$this->db->select('po_produk_detail.*,suplier.suplier_nama,
		concat_ws(" ",jenis_produk.jenis_produk_nama,fabric.fabric_nama) AS "produk_nama",
		concat_ws(" ",if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec"');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('po_produk', 'po_produk.po_produk_id = po_produk_detail.po_produk_id');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		return $this->db->get('po_produk_detail')->result();
	}
	function po_produk_by_id($po_produk_id){
		$this->db->where('po_produk.po_produk_id', $po_produk_id);
		$this->db->select('po_produk.*,suplier.suplier_nama,hutang.tenggat_pelunasan');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('hutang', 'hutang.po_produk_id = po_produk.po_produk_id', 'left');
		return $this->db->get('po_produk')->row();
	}
	function laporan_po_produk_count_all(){
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
		return $this->db->get('po_produk')->num_rows();

	}
	function laporan_po_produk_count_filter($query){
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
		$this->db->group_start();
		$this->db->like('po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('produk.produk_nama', $query, 'BOTH');
		$temp = str_replace(",", "", $query);
		$temp = str_replace(".", "", $temp);
		$this->db->or_like('po_produk_detail.jumlah', $temp, 'BOTH');
		$this->db->or_like('po_produk_detail.harga', $temp, 'BOTH');
		$this->db->or_like('po_produk_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_produk_no']) && $this->input->get('po_produk_no')!=""){
			$this->db->like('po_produk.po_produk_no', $this->input->get('po_produk_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_produk.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_produk.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_produk.tipe_pembayaran is null', null, false);
			}

		}
		return $this->db->get('po_produk')->num_rows();

	}
	function laporan_po_produk_list($start,$length,$query){
		$this->db->select('po_produk.jenis_pembayaran,po_produk.po_produk_id,po_produk.po_produk_no,po_produk.tanggal_pemesanan,suplier.suplier_nama,produk.produk_kode,produk.produk_nama,po_produk_detail.jumlah,po_produk_detail.harga,po_produk_detail.sub_total,tipe_pembayaran.tipe_pembayaran_nama as "kas_nama" ,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('produk.produk_nama', $query, 'BOTH');
		$temp = str_replace(",", "", $query);
		$temp = str_replace(".", "", $temp);
		$this->db->or_like('po_produk_detail.jumlah', $temp, 'BOTH');
		$this->db->or_like('po_produk_detail.harga', $temp, 'BOTH');
		$this->db->or_like('po_produk_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_produk_no']) && $this->input->get('po_produk_no')!=""){
			$this->db->like('po_produk.po_produk_no', $this->input->get('po_produk_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_produk.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_produk.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_produk.tipe_pembayaran is null', null, false);
			}

		}
		$this->db->order_by('po_produk.po_produk_id', 'desc');
		return $this->db->get('po_produk', $length, $start)->result();
	}
}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */
