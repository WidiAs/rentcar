<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penawaran extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'penawaran';
	}


	function getUrutan()
	{
		$date = date("Y-m-d");
		$this->db->select('max(count) as count');
		$this->db->where('tanggal_order', $date);
		$count = $this->db->get('penawaran')->row()->count;
		if ($count == null) {
			$count = 1;
		} else {
			$count++;
		}
		return $count;
	}

	function getFakturCode()
	{
		$date = date("ymd");
		$count = $this->getUrutan();
		$lok = "00";
		if (isset($_SESSION['login']['lokasi_id'])) {
			$lok = $_SESSION['login']['lokasi_id'];
		}
		$tipe = "PN";
		$faktur = $tipe . '-' . $date . '/' . $lok . '/' . $count;
		return $faktur;
	}

	function penawaran_list($start,$length,$query){
		$this->db->group_start();
		$this->db->like('no_penawaran', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('penawaran_id', 'desc');
		return $this->db->get('penawaran', $length, $start)->result();
	}
	function penawaran_count_filter($query){
		$this->db->group_start();
		$this->db->like('no_penawaran', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->get('penawaran')->num_rows();
	}
	function penawaran_count_all(){
		return $this->db->get('penawaran')->num_rows();
	}

	function penawaran_list_order($start,$length,$query){
		$this->db->group_start();
		$this->db->like('no_penawaran', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status', 'order');
		$this->db->order_by('penawaran_id', 'desc');
		return $this->db->get('penawaran', $length, $start)->result();
	}
	function penawaran_count_filter_order($query){
		$this->db->group_start();
		$this->db->like('no_penawaran', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status', 'order');
		return $this->db->get('penawaran')->num_rows();
	}
	function penawaran_count_all_order(){
		$this->db->where('status', 'order');
		return $this->db->get('penawaran')->num_rows();
	}

}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
