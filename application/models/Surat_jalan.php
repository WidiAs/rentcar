<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_jalan extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "surat_jalan";
	}

	function penjualan_count_all($lokasi_id)
	{
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan_sj.tipe_pembayaran_id', 'left');
		$this->db->where('sisa > ', 0);
		if ($lokasi_id) {
			$this->db->where('penjualan_sj.lokasi_id =', $lokasi_id);
		}
		return $this->db->get('penjualan_sj')->num_rows();
	}

	function penjualan_count_filter($query, $lokasi_id)
	{
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan_sj.tipe_pembayaran_id', 'left');
		$this->db->where('sisa > ', 0);
		if ($lokasi_id) {
			$this->db->where('penjualan_sj.lokasi_id =', $lokasi_id);
		}
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan_sj.lokasi_id = lokasi.lokasi_id', 'left');
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan_sj.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['tipe_pembayaran_id']) && $this->input->get('tipe_pembayaran_id') != "") {
			$this->db->where('penjualan_sj.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		$this->db->order_by('penjualan_id', 'desc');
		//echo $this->db->_compile_select();
		return $this->db->get('penjualan_sj')->num_rows();
	}

	function penjualan_list($start, $length, $query, $lokasi_id)
	{
		$this->db->select('penjualan_sj.*,lokasi.lokasi_nama,tipe_pembayaran_nama', 'left');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan_sj.tipe_pembayaran_id', 'left');
		$this->db->where('sisa > ', 0);
		if ($lokasi_id) {
			$this->db->where('penjualan_sj.lokasi_id =', $lokasi_id);
		}
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan_sj.lokasi_id = lokasi.lokasi_id', 'left');
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if (isset($_GET['lokasi_id']) && $this->input->get('lokasi_id') != "") {
			$this->db->where('penjualan_sj.lokasi_id', $this->input->get('lokasi_id'));
		}
		if (isset($_GET['tipe_pembayaran_id']) && $this->input->get('tipe_pembayaran_id') != "") {
			$this->db->where('penjualan_sj.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		$this->db->order_by('penjualan_id', 'desc');
		return $this->db->get('penjualan_sj', $length, $start)->result();

	}

	function getUrutan()
	{
		$date = date("Y-m-d");
		$this->db->select('max(urutan) as urutan');
		$this->db->where('tanggal_kirim', $date);
		$urutan = $this->db->get('surat_jalan')->row()->urutan;
		if ($urutan == null) {
			$urutan = 1;
		} else {
			$urutan++;
		}
		return $urutan;
	}

	function getSJCode()
	{
		$date = date("ymd");
		$urutan = $this->getUrutan();
		$lok = "00";
		if (isset($_SESSION['login']['lokasi_id'])) {
			$lok = $_SESSION['login']['lokasi_id'];
		}
		$tipe = "SJ";
		$faktur = $tipe . '-' . $date . '/' . $lok . '/' . $urutan;
		return $faktur;
	}

	function sj_by_id($id)
	{
		$this->db->select('surat_jalan.*, penjualan.no_faktur');
		$this->db->join('penjualan', 'surat_jalan.penjualan_id = penjualan.penjualan_id', 'left');
		$this->db->where("surat_jalan_id", $id);
		return $this->db->get('surat_jalan')->row();
	}

	function detailItem($id)
	{
		$this->db->select("surat_jalan_produk.qty_kirim, penjualan_produk.*,concat_ws(' ',jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS 'produk_nama',produk.*,satuan.satuan_nama,produk_custom.deskripsi, b.satuan_nama as 'satuan_custom' ");
		$this->db->join('penjualan_produk', 'surat_jalan_produk.penjualan_produk_id = penjualan_produk.penjualan_produk_id', 'left');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id', 'left');
		$this->db->join('produk_custom', 'produk_custom.produk_custom_id = penjualan_produk.produk_custom_id', 'left');
		$this->db->join('satuan as b', 'b.satuan_id = produk_custom.satuan_id', 'left');
		$this->db->where('surat_jalan_produk.surat_jalan_id', $id);
		return $this->db->get('surat_jalan_produk')->result();
	}

	function sj_count_all($lokasi_id)
	{
		$this->db->join('penjualan', 'penjualan.penjualan_id = surat_jalan.penjualan_id');
		if ($lokasi_id) {
			$this->db->where('penjualan.lokasi_id =', $lokasi_id);
		}
		return $this->db->get('surat_jalan')->num_rows();
	}

	function sj_count_filter($query, $lokasi_id)
	{

		$this->db->group_start();
		$this->db->like('pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal_kirim', $query, 'BOTH');
		$this->db->or_like('no_surat_jalan', $query, 'BOTH');
		$this->db->or_like('kontak', $query, 'BOTH');
		$this->db->or_like('alamat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('penjualan', 'surat_jalan.penjualan_id = penjualan.penjualan_id', 'left');
		if ($lokasi_id) {
			$this->db->where('penjualan.lokasi_id =', $lokasi_id);
		}
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal_kirim >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal_kirim <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		$this->db->order_by('surat_jalan_id', 'desc');
		return $this->db->get('surat_jalan')->num_rows();
	}

	function sj_list($start, $length, $query, $lokasi_id)
	{
		$this->db->select('surat_jalan.*,penjualan.no_faktur');
		$this->db->group_start();
		$this->db->like('pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal_kirim', $query, 'BOTH');
		$this->db->or_like('no_surat_jalan', $query, 'BOTH');
		$this->db->or_like('kontak', $query, 'BOTH');
		$this->db->or_like('alamat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('penjualan', 'surat_jalan.penjualan_id = penjualan.penjualan_id', 'left');
		if ($lokasi_id) {
			$this->db->where('penjualan.lokasi_id =', $lokasi_id);
		}
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal_kirim >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal_kirim <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['no_faktur']) && $this->input->get('no_faktur') != "") {
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		$this->db->order_by('surat_jalan_id', 'desc');
		return $this->db->get('surat_jalan', $length, $start)->result();
	}

}
