<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekanan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "rekanan";
	}
	function rekanan_list($start,$length,$query){
		$this->db->like('rekanan_id', $query, 'BOTH');
		$this->db->or_like('rekanan_nama', $query, 'BOTH');
		$this->db->order_by('rekanan_id', 'desc');
		return $this->db->get('rekanan', $length, $start)->result();
	}
	function rekanan_count_filter($query){
		$this->db->like('rekanan_id', $query, 'BOTH');
		$this->db->or_like('rekanan_nama', $query, 'BOTH');
		return $this->db->get('rekanan')->num_rows();
	}
	function rekanan_by_name($query){
		$this->db->like('rekanan_nama', $query, 'BOTH');
		return $this->db->get('rekanan')->row();
	}
	function rekanan_count_all(){
		return $this->db->get('rekanan')->num_rows();
	}
	function is_ready_nama($rekanan_id,$nama){
		$this->db->where('rekanan_nama', $nama);
		$data = $this->db->get('rekanan')->row();
		if($data != null){
			if($data->rekanan_id == $rekanan_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


}

/* End of file Jenis_mobil.php */
/* Location: ./application/models/Jenis_mobil.php */
