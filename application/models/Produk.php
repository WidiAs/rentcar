<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'produk';
	}

	function produk_list($start, $length, $query)
	{

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama , 
		concat_ws(" ",jenis_produk.jenis_produk_nama,fabric.fabric_nama) AS "produk_nama",
		concat_ws(" "
		,if(produk.style is null, "", concat("style : ",produk.style))
		,if(produk.tipe is null, "", concat("tipe : ",produk.tipe))
		,if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec",
		normal_size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		}
		$this->db->select($sql);
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('barcode', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function produk_list_sort($start, $length, $query, $sort, $order)
	{

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama , 
		concat_ws(" ",jenis_produk.jenis_produk_nama,fabric.fabric_nama) AS "produk_nama",
		concat_ws(" "
		,if(produk.style is null, "", concat("style : ",produk.style))
		,if(produk.tipe is null, "", concat("tipe : ",produk.tipe))
		,if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec",
		normal_size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		}
		$this->db->select($sql);
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('barcode', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		if ($sort == 'none'){
			$this->db->order_by('produk_id', 'desc');
		}else{
			$this->db->order_by($sort, $order);
//			$this->db->query("order by case when $sort is null then 1 else 0 end, $sort DESC", $order);
		}
		return $this->db->get('produk', $length, $start)->result();
	}

	function all_list()
	{
		$this->db->select('produk.*,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS "produk_nama"');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		return $this->db->get('produk')->result();
	}

	function konversi_produk_list()
	{
		$this->db->select('produk.produk_id,produk.produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama,produk.style) as produk_nama');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'size.size_id = produk.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		return $this->db->get('produk')->result();
	}

	function row_by_id($id)
	{
		$this->db->select('produk.*,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS "produk_nama"');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->where('produk_id', $id);
		return $this->db->get('produk')->row();
	}

	function produk_count_all()
	{
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		if (isset($_SESSION['login']['lokasi_id'])) {
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function produk_count_filter($query)
	{
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		if (isset($_SESSION['login']['lokasi_id'])) {
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		}
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('barcode', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function low_produk_list($start, $length, $query)
	{

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS "produk_nama"';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		} else {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->select($sql);
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_start();
		$this->db->where('a.jumlah <= produk.produk_minimal_stock', '', false);
		$this->db->or_where('a.jumlah is null', '', false);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk.produk_id', 'desc');

		return $this->db->get('produk', $length, $start)->result();
	}

	function low_produk_count_filter($query)
	{
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		if (isset($_SESSION['login']['lokasi_id'])) {
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		} else {
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_start();
		$this->db->where('a.jumlah <= produk.produk_minimal_stock', '', false);
		$this->db->or_where('a.jumlah is null', '', false);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk')->num_rows();
	}

	function low_produk_count_all()
	{
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		if (isset($_SESSION['login']['lokasi_id'])) {
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		} else {
			$this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->group_start();
		$this->db->where('a.jumlah <= produk.produk_minimal_stock', '', false);
		$this->db->or_where('a.jumlah is null', '', false);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk')->num_rows();
	}

	function produk_by_id($produk_id)
	{
		$this->db->select('produk.*,jenis_produk.jenis_produk_nama,jenis_produk.jenis_produk_kode,concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS "produk_nama"');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->where('produk.produk_id', $produk_id);
		return $this->db->get('produk')->row();
	}

	function produk_by_barcode($barcode)
	{
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size', 'produk.size_id = size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama , concat_ws(" ",jenis_produk.jenis_produk_nama,color.color_nama,size.size_nama,fabric.fabric_nama,patern.patern_nama) AS "produk_nama",size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama';
		$this->db->select($sql);
		$this->db->where('produk.barcode', $barcode);
		return $this->db->get('produk')->row();
	}

	function laporan_produk_list($start, $length, $query)
	{

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama,
		concat_ws(" ",jenis_produk.jenis_produk_nama,fabric.fabric_nama) AS "produk_nama",
		concat_ws(" ",if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec"';
		$this->db->select($sql);
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');

		$this->db->group_end();
		if (isset($_GET['jenis_produk_id'])) {
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if (isset($_GET['satuan_id'])) {
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}

	function laporan_produk_count_all()
	{
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'stock_produk.produk_id = produk.produk_id');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function laporan_produk_count_filter($query)
	{
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'stock_produk.produk_id = produk.produk_id');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->group_start();
		$this->db->like('produk.produk_id', $query, 'BOTH');
		$this->db->or_like('produk.produk_kode', $query, 'BOTH');
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('patern_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query, 'BOTH');
		$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['jenis_produk_id'])) {
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if (isset($_GET['satuan_id'])) {
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function produk_produksi_count_all()
	{
		$this->db->join('konversi_bahan', 'produk.produk_id = konversi_bahan.produk_id', 'left');
		$this->db->join('bahan', 'konversi_bahan.bahan_id = bahan.bahan_id', 'left');
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function produk_produksi_count_filter($query)
	{
		$this->db->join('konversi_bahan', 'produk.produk_id = konversi_bahan.produk_id', 'left');
		$this->db->join('bahan', 'konversi_bahan.bahan_id = bahan.bahan_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->group_start();
		$this->db->or_like('produk_kode', $query);
		$this->db->or_like('jenis_produk_nama', $query);
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query);
		$this->db->or_like('fabric_nama', $query);
		$this->db->or_like('patern_nama', $query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}

	function produk_produksi_list($start, $length, $query)
	{
		$this->db->select('produk.produk_id,produk.produk_kode,
		concat_ws(" ",jenis_produk_nama,fabric_nama) as produk_nama,
		display_stock_produk.jumlah,group_concat(konversi_bahan.jumlah separator "|") as potong,
		group_concat(bahan.bahan_nama separator "|") as bahan_nama, group_concat(konversi_bahan.bahan_id separator "|") as bahan_id,
		concat_ws(" ",if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec"');
		$this->db->join('konversi_bahan', 'produk.produk_id = konversi_bahan.produk_id', 'left');
		$this->db->join('bahan', 'konversi_bahan.bahan_id = bahan.bahan_id', 'left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->group_start();
		$this->db->or_like('produk_kode', $query);
		$this->db->or_like('jenis_produk_nama', $query);
		$this->db->or_like('normal_size.size_nama', $query, 'BOTH');
		$this->db->or_like('bed_size.size_nama', $query, 'BOTH');
		$this->db->or_like('good_size.size_nama', $query, 'BOTH');
		$this->db->or_like('color_nama', $query);
		$this->db->or_like('fabric_nama', $query);
		$this->db->or_like('patern_nama', $query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('display_stock_produk.jumlah');
		return $this->db->get('produk', $length, $start)->result();
	}

	function get_exist_with_id($id, $kode_barang, $barcode)
	{
		$this->db->from('produk');
		$this->db->where('produk_id !=', $id);
		$this->db->group_start();
		$this->db->or_where('produk_kode', $kode_barang);
		$this->db->or_where('barcode', $barcode);
		$this->db->group_end();
		$query = $this->db->count_all_results();
		if ($query > 0) {
			$exist = 1;
		} else {
			$exist = 0;
		}
		return $exist;
	}


	function produk_by_custom($jenis, $ukuran, $patern)
	{
		$this->db->select('produk.*');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'size.size_id = produk.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->join('fabric', 'fabric.fabric_id = produk.fabric_id', 'left');
		$this->db->join('patern', 'patern.patern_id = produk.patern_id', 'left');
		$this->db->where('jenis_produk.jenis_produk_nama', $jenis);
		$this->db->where('size.size_nama', $ukuran);
		if ($patern) {
			$this->db->where('patern.patern_nama', $patern);
		}
		return $this->db->get('produk')->row();
	}

	function produk_by_jenis($jenis)
	{
		$this->db->select('produk.*');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('size', 'size.size_id = produk.size_id', 'left');
		$this->db->join('color', 'color.color_id = produk.color_id', 'left');
		$this->db->where('jenis_produk.jenis_produk_nama like ', $jenis);
		return $this->db->get('produk')->row();
	}

	function produk_by_where($where)
	{
		$this->db->select('produk.*');
		$this->db->where($where);
		return $this->db->get('produk')->row();
	}

	function get_data_by_id($id)
	{

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id', 'left');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id', 'left');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id', 'left');
		$this->db->join('size as normal_size', 'produk.size_id = normal_size.size_id', 'left');
		$this->db->join('size as bed_size', 'produk.bed_size_id = bed_size.size_id', 'left');
		$this->db->join('size as good_size', 'produk.good_size_id = good_size.size_id', 'left');
		$this->db->join('color', 'produk.color_id = color.color_id', 'left');
		$this->db->join('patern', 'produk.patern_id = patern.patern_id', 'left');
		$this->db->join('fabric', 'produk.fabric_id = fabric.fabric_id', 'left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama , 
		concat_ws(" ",jenis_produk.jenis_produk_nama,fabric.fabric_nama) AS "produk_nama",
		concat_ws(" "
		,if(produk.style is null, "", concat("style : ",produk.style))
		,if(produk.tipe is null, "", concat("tipe : ",produk.tipe))
		,if(color.color_nama is null, "", concat("color : ",color.color_nama))
		,if(normal_size.size_nama is null, "", concat("size : ", normal_size.size_nama))
		,if(bed_size.size_nama is null, "", concat("bed size : ", bed_size.size_nama))
		,if(good_size.size_nama is null, "", concat("good size : ", good_size.size_nama))
		,if(patern.patern_nama is null, "", concat("patern : ", patern.patern_nama))) AS "spec",
		normal_size.size_nama,color.color_nama,fabric.fabric_nama,patern.patern_nama';
		if (isset($_SESSION['login']['lokasi_id'])) {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = ' . $_SESSION['login']['lokasi_id'] . ') a', 'a.produk_id = produk.produk_id', 'left');

		}
		$this->db->select($sql);
		$this->db->where('produk.produk_id', $id);
//		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->row();
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
