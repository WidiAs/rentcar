<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran_driver extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "pengeluaran_driver";
	}

	function pengeluaran_list($start,$length,$query){
		$this->db->select('pengeluaran_driver.*');
		$this->db->group_start();
		$this->db->like('nama_pengeluaran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('tanggal', 'desc');
		return $this->db->get('pengeluaran_driver', $length, $start)->result();
	}
	function pengeluaran_count_filter($query){
		$this->db->group_start();
		$this->db->like('nama_pengeluaran', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->get('pengeluaran_driver')->num_rows();
	}
	function pengeluaran_count_all(){
		return $this->db->get('pengeluaran_driver')->num_rows();
	}

	function pengeluaran_driver_list($start,$length,$query,$driver_id){
		$this->db->select('pengeluaran_driver.*');
		$this->db->group_start();
		$this->db->like('nama_pengeluaran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('pengeluaran_driver.driver_id', $driver_id);
		$this->db->order_by('tanggal', 'desc');
		return $this->db->get('pengeluaran_driver', $length, $start)->result();
	}
	function pengeluaran_driver_count_filter($query,$driver_id){
		$this->db->group_start();
		$this->db->like('nama_pengeluaran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('pengeluaran_driver.driver_id', $driver_id);
		return $this->db->get('pengeluaran_driver')->num_rows();
	}
	function pengeluaran_driver_count_all($driver_id){
		$this->db->where('pengeluaran_driver.driver_id', $driver_id);
		return $this->db->get('pengeluaran_driver')->num_rows();
	}
	function get_pengeluaran($driver_id, $tanggal){
		$this->db->where('pengeluaran_driver.driver_id', $driver_id);
		$this->db->where('pengeluaran_driver.tanggal > ', $tanggal.' 00:01:00');
		$this->db->where('pengeluaran_driver.tanggal < ', $tanggal.' 23:59:00');
		return $this->db->get('pengeluaran_driver')->result();
	}
	function delete_by_tanggal($driver_id, $tanggal){
		$this->db->where('pengeluaran_driver.driver_id', $driver_id);
		$this->db->where('pengeluaran_driver.tanggal > ', $tanggal.' 00:01:00');
		$this->db->where('pengeluaran_driver.tanggal < ', $tanggal.' 23:59:00');
		return $this->db->delete('pengeluaran_driver');
	}

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
