<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mobil extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'mobil';
	}

	function mobil_list($start, $length, $query)
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil.nomor_stnk', $query, 'BOTH');
		$this->db->or_like('mobil.plat_mobil', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_nama', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_merk', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status != ', 'Dikembalikan');
		$this->db->where('tipe_mobil != ', 'Rekanan');
		$this->db->group_by('mobil.mobil_id');
		$this->db->order_by('mobil_id', 'desc');
		return $this->db->get('mobil', $length, $start)->result();
	}

	function mobil_list_sort($start, $length, $query, $sort, $order)
	{

		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil.nomor_stnk', $query, 'BOTH');
		$this->db->or_like('mobil.plat_mobil', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_nama', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_merk', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status != ', 'Dikembalikan');
		$this->db->where('tipe_mobil != ', 'Rekanan');
		$this->db->group_by('mobil.mobil_id');
		if ($sort == 'none') {
			$this->db->order_by('mobil_id', 'desc');
		} else {
			$this->db->order_by($sort, $order);
//			$this->db->query("order by case when $sort is null then 1 else 0 end, $sort DESC", $order);
		}
		return $this->db->get('mobil', $length, $start)->result();
	}

	function all_list()
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->where('status != ', 'Dikembalikan');
		$this->db->where('tipe_mobil != ', 'Rekanan');
		$this->db->group_by('mobil.mobil_id');
		$this->db->order_by('mobil_id', 'desc');
		return $this->db->get('mobil')->result();
	}

	function row_by_id($id)
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->where('mobil_id', $id);
		return $this->db->get('mobil')->row();
	}

	function mobil_count_all()
	{
		$this->db->select('mobil.*');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->where('status != ', 'Dikembalikan');
		$this->db->where('tipe_mobil != ', 'Rekanan');
		$this->db->group_by('mobil.mobil_id');
		return $this->db->get('mobil')->num_rows();
	}

	function mobil_count_filter($query)
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil.nomor_stnk', $query, 'BOTH');
		$this->db->or_like('mobil.plat_mobil', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_nama', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_merk', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status != ', 'Dikembalikan');
		$this->db->where('tipe_mobil != ', 'Rekanan');
		$this->db->group_by('mobil.mobil_id');
		return $this->db->get('mobil')->num_rows();
	}

	function get_exist_with_id($plat)
	{
		$this->db->from('mobil');
		$this->db->where('plat_mobil !=', $plat);
		$query = $this->db->count_all_results();
		if ($query > 0) {
			$exist = 1;
		} else {
			$exist = 0;
		}
		return $exist;
	}

	function mobil_by_jenis($jenis_id)
	{
		$this->db->select('mobil.*, jenis_mobil_nama, jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id');
		$this->db->where('jenis_mobil.jenis_mobil_id ', $jenis_id);
		return $this->db->get('mobil')->row();
	}

	function mobil_by_investor($investor_id)
	{
		$this->db->select('mobil.*, investor_nama');
		$this->db->join('investor', 'mobil.investor_id = investor.investor_id');
		$this->db->where('investor_mobil.investor_mobil_id ', $investor_id);
		return $this->db->get('mobil')->row();
	}

	function mobil_by_rekanan($rekanan_id)
	{
		$this->db->select('mobil.*, rekanan_nama');
		$this->db->join('rekanan', 'mobil.rekanan_id = rekanan.rekanan_id');
		$this->db->where('rekanan_mobil.rekanan_mobil_id ', $rekanan_id);
		return $this->db->get('mobil')->row();
	}

	function count_by_jenis($jenis_id)
	{
		$this->db->select('mobil.*');
		$this->db->where('jenis_mobil_id ', $jenis_id);
		return $this->db->get('mobil')->num_rows();
	}

	function count_by_investor($investor_id)
	{
		$this->db->select('mobil.*');
		$this->db->where('investor_id ', $investor_id);
		return $this->db->get('mobil')->num_rows();
	}

	function count_by_rekanan($rekanan_id)
	{
		$this->db->select('mobil.*');
		$this->db->where('rekanan_id ', $rekanan_id);
		return $this->db->get('mobil')->num_rows();
	}

	function mobil_by_where($where)
	{
		$this->db->select('mobil.*, jenis_mobil_nama, jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id');
		$this->db->where($where);
		return $this->db->get('mobil')->row();
	}

	function mobil_all_list($start, $length, $query, $mobil_id = array())
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil.nomor_stnk', $query, 'BOTH');
		$this->db->or_like('mobil.plat_mobil', $query, 'BOTH');
		$this->db->or_like('mobil.warna', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_nama', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_merk', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status != ', 'Dikembalikan');
		if (!empty($mobil_id)){
			$this->db->where_not_in('mobil_id', $mobil_id);
		}
		$this->db->group_by('mobil.mobil_id');
		$this->db->order_by('mobil_id', 'desc');
		return $this->db->get('mobil', $length, $start)->result();
	}


	function mobil_all_count($mobil_id = array())
	{
		$this->db->select('mobil.*');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->where('status != ', 'Dikembalikan');
		if (!empty($mobil_id)){
			$this->db->where_not_in('mobil_id', $mobil_id);
		}
		$this->db->group_by('mobil.mobil_id');
		return $this->db->get('mobil')->num_rows();
	}

	function mobil_all_filter($query, $mobil_id = array())
	{
		$this->db->select('mobil.*,jenis_mobil_nama,jenis_mobil_merk');
		$this->db->join('jenis_mobil', 'mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil.nomor_stnk', $query, 'BOTH');
		$this->db->or_like('mobil.plat_mobil', $query, 'BOTH');
		$this->db->or_like('mobil.warna', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_nama', $query, 'BOTH');
		$this->db->or_like('jenis_mobil.jenis_mobil_merk', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status != ', 'Dikembalikan');
		if (!empty($mobil_id)){
			$this->db->where_not_in('mobil_id', $mobil_id);
		}
		$this->db->group_by('mobil.mobil_id');
		return $this->db->get('mobil')->num_rows();
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
