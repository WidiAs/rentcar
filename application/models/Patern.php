<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patern extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "patern";
	}	
	function patern_list($start,$length,$query){
		$this->db->like('patern_id', $query, 'BOTH'); 
		$this->db->or_like('patern_nama', $query, 'BOTH'); 
		$this->db->order_by('patern_id', 'desc');
		return $this->db->get('patern', $length, $start)->result();
	}
	function patern_count_all(){
		return $this->db->get('patern')->num_rows();
	}
	function patern_count_filter($query){
		$this->db->like('patern_id', $query, 'BOTH'); 
		$this->db->or_like('patern_nama', $query, 'BOTH'); 
		return $this->db->get('patern')->num_rows();
	}
	function patern_by_name($query){
		$this->db->like('patern_nama', $query, 'BOTH');
		return $this->db->get('patern')->row();
	}
	function is_ready_kode($patern_id,$kode){
		$this->db->where('patern_kode', $kode);
		$data = $this->db->get('patern')->row();
		if($data != null){
			if($data->patern_id == $patern_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */
