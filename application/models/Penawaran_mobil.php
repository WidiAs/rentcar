<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penawaran_mobil extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'penawaran_mobil';
	}

	function penawaran_detail($id)
	{
		$this->db->select('penawaran_mobil.*');
		$this->db->where('penawaran_id', $id);
		return $this->db->get('penawaran_mobil')->result();
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
