<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan_penawaran extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'catatan_penawaran';
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
