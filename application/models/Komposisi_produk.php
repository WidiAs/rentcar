<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komposisi_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'komposisi_produk';
	}
	function komposisi_list($start,$length,$query,$produk_id){
		$this->db->select('komposisi_produk.*,bahan_nama,satuan_nama');
		$this->db->join('bahan', 'bahan.bahan_id = komposisi_produk.bahan_id');
		$this->db->join('satuan', 'satuan.satuan_id = komposisi_produk.satuan_id');
		$this->db->group_start();
			$this->db->like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('komposisi_produk.produk_id', $produk_id); 
		$this->db->order_by('komposisi_produk.komposisi_id', 'desc');
		return $this->db->get('komposisi_produk', $length, $start)->result();
	}
	function komposisi_count_all($produk_id){
		$this->db->join('bahan', 'bahan.bahan_id = komposisi_produk.bahan_id');
		$this->db->join('satuan', 'satuan.satuan_id = komposisi_produk.satuan_id');
		$this->db->where('komposisi_produk.produk_id', $produk_id); 
		return $this->db->get('komposisi_produk')->num_rows();
	}
	function komposisi_count_filter($query,$produk_id){
		$this->db->join('bahan', 'bahan.bahan_id = komposisi_produk.bahan_id');
		$this->db->join('satuan', 'satuan.satuan_id = komposisi_produk.satuan_id');
		$this->db->where('komposisi_produk.produk_id', $produk_id); 
		$this->db->group_start();
			$this->db->like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();		
		return $this->db->get('komposisi_produk')->num_rows();		
	}

}

/* End of file Komposisi_produk.php */
/* Location: ./application/models/Komposisi_produk.php */