<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_mobil extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "jadwal_mobil";
	}

	function range_tanggal_sewa($penyewaan_id){
		$this->db->distinct('tanggal');
		$this->db->select('tanggal');
		$this->db->where('penyewaan_id', $penyewaan_id);

		return $this->db->get('jadwal_mobil')->result();
	}

	function jadwal_list($start,$length,$query){
		$this->db->select('jadwal_mobil.*, mobil.status');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('jadwal_mobil', $length, $start)->result();
	}
	function jadwal_count_filter($query){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->get('jadwal_mobil')->num_rows();
	}
	function jadwal_count_all(){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		return $this->db->get('jadwal_mobil')->num_rows();
	}

	function jadwal_today_list($start,$length,$query){
		$this->db->select('jadwal_mobil.*, mobil.status, penyewaan_mobil.jenis, penyewaan_mobil.driver_nama, penyewaan.pelanggan_nama, penyewaan_mobil.jenis');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal', date('Y-m-d'));
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('jadwal_mobil', $length, $start)->result();
	}
	function jadwal_today_count_filter($query){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal', date('Y-m-d'));
		return $this->db->get('jadwal_mobil')->num_rows();
	}
	function jadwal_today_count_all(){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->where('jadwal_mobil.tanggal', date('Y-m-d'));
		return $this->db->get('jadwal_mobil')->num_rows();
	}

	function jadwal_besok_list($start,$length,$query){
		$datetime = new DateTime('tomorrow');
		$date =  $datetime->format('Y-m-d');
		$this->db->select('jadwal_mobil.*, mobil.status, penyewaan_mobil.jenis, penyewaan_mobil.driver_nama, penyewaan.pelanggan_nama, penyewaan_mobil.keterangan as keterangan_sewa');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal', $date);
		$this->db->where('jadwal_mobil.status', 'ambil');
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('jadwal_mobil', $length, $start)->result();
	}
	function jadwal_besok_count_filter($query){
		$datetime = new DateTime('tomorrow');
		$date =  $datetime->format('Y-m-d');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal', $date);
		$this->db->where('jadwal_mobil.status', 'ambil');
		return $this->db->get('jadwal_mobil')->num_rows();
	}
	function jadwal_besok_count_all(){
		$datetime = new DateTime('tomorrow');
		$date =  $datetime->format('Y-m-d');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan', 'jadwal_mobil.penyewaan_id = penyewaan.penyewaan_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->where('jadwal_mobil.tanggal', $date);
		$this->db->where('jadwal_mobil.status', 'ambil');
		return $this->db->get('jadwal_mobil')->num_rows();
	}

	function jadwal_kedepan_list($start,$length,$query,$driver_id){
		$this->db->select('jadwal_mobil.*, mobil.status');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id', $driver_id);
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('jadwal_mobil', $length, $start)->result();
	}
	function jadwal_kedepan_count_filter($query,$driver_id){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id', $driver_id);
		return $this->db->get('jadwal_mobil')->num_rows();
	}
	function jadwal_kedepan_count_all($driver_id){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id', $driver_id);
		return $this->db->get('jadwal_mobil')->num_rows();
	}

function jadwal_kedepan_all_list($start,$length,$query){
		$this->db->select('jadwal_mobil.*, mobil.status, penyewaan_mobil.driver_nama');
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->or_like('penyewaan_mobil.driver_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id !=', null);
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('jadwal_mobil', $length, $start)->result();
	}
	function jadwal_kedepan_all_count_filter($query){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis_nama', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->or_like('penyewaan_mobil.driver_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id !=', null);
		return $this->db->get('jadwal_mobil')->num_rows();
	}
	function jadwal_kedepan_all_count_all(){
		$this->db->join('mobil', 'jadwal_mobil.mobil_id = mobil.mobil_id', 'left');
		$this->db->join('penyewaan_mobil', 'jadwal_mobil.penyewaan_mobil_id = penyewaan_mobil.penyewaan_mobil_id', 'left');
		$this->db->where('jadwal_mobil.tanggal > ', date('Y-m-d'));
		$this->db->where('penyewaan_mobil.driver_id !=', null);
		return $this->db->get('jadwal_mobil')->num_rows();
	}



	function jadwal_range(){
		$this->db->select('jadwal_mobil.*');
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('jadwal_mobil.tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('jadwal_mobil.tanggal <=', $this->input->get('tanggal_end'));
		}
		$this->db->group_by('mobil_id');
		return $this->db->get('jadwal_mobil')->result();
	}

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
