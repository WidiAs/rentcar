<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Investor extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "investor";
	}
	function investor_list($start,$length,$query){
		$this->db->like('investor_id', $query, 'BOTH');
		$this->db->or_like('investor_nama', $query, 'BOTH');
		$this->db->order_by('investor_id', 'desc');
		return $this->db->get('investor', $length, $start)->result();
	}
	function investor_count_filter($query){
		$this->db->like('investor_id', $query, 'BOTH');
		$this->db->or_like('investor_nama', $query, 'BOTH');
		return $this->db->get('investor')->num_rows();
	}
	function investor_by_name($query){
		$this->db->like('investor_nama', $query, 'BOTH');
		return $this->db->get('investor')->row();
	}
	function investor_count_all(){
		return $this->db->get('investor')->num_rows();
	}
	function is_ready_nama($investor_id,$nama){
		$this->db->where('investor_nama', $nama);
		$data = $this->db->get('investor')->row();
		if($data != null){
			if($data->investor_id == $investor_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


}

/* End of file Jenis_mobil.php */
/* Location: ./application/models/Jenis_mobil.php */
