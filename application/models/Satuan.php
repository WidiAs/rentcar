<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "satuan";
	}	
	function satuan_list($start,$length,$query){
		$this->db->like('satuan_id', $query, 'BOTH'); 
		$this->db->or_like('satuan_kode', $query, 'BOTH'); 
		$this->db->or_like('satuan_nama', $query, 'BOTH');
		$this->db->order_by('satuan_id', 'desc'); 
		return $this->db->get('satuan', $length, $start)->result();
	}
	function satuan_count_filter($query){
		$this->db->like('satuan_id', $query, 'BOTH'); 
		$this->db->or_like('satuan_kode', $query, 'BOTH'); 
		$this->db->or_like('satuan_nama', $query, 'BOTH'); 
		return $this->db->get('satuan')->num_rows();
	}
	function satuan_count_all(){ 
		return $this->db->get('satuan')->num_rows();
	}
	function satuan_by_name($query){
		$this->db->where('satuan_nama = ', $query);
		return $this->db->get('satuan')->row();
	}
	function is_ready_kode($satuan_id,$kode){
		$this->db->where('satuan_kode', $kode);
		$data = $this->db->get('satuan')->row();
		if($data != null){
			if($data->satuan_id == $satuan_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */
