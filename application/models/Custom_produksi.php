<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_produksi extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'produk_custom';
	}

	function produksi_custom_count_all()
	{
		$this->db->where('status_produksi !=', 'cancel');
		$this->db->where('status_penerimaan !=', 'Diterima');
		return $this->db->get('produk_custom')->num_rows();
	}

	function produksi_custom_count_filter($query)
	{
		$this->db->where('status_produksi !=', 'cancel');
		$this->db->where('status_penerimaan !=', 'Diterima');
		$this->db->group_start();
		$this->db->or_like('deskripsi', $query);
		$this->db->or_like('note', $query);
		$temp = $this->string_to_number($query);
		$this->db->or_like('harga_satuan', $temp);
		$this->db->or_like('jumlah_pesan', $temp);
		$this->db->or_like('status_produksi', $query);
		$this->db->or_like('status_penerimaan', $query);
		$this->db->or_like('tanggal_mulai', $query);
		$this->db->or_like('tanggal_selesai', $query);
		$this->db->or_like('tanggal_penerimaan', $query);
		$this->db->group_end();
		return $this->db->get('produk_custom')->num_rows();
	}

	function produksi_custom_list($start, $length, $query)
	{
		$this->db->where('status_produksi !=', 'cancel');
		$this->db->where('status_penerimaan !=', 'Diterima');
		$this->db->group_start();
		$this->db->or_like('deskripsi', $query);
		$this->db->or_like('note', $query);
		$temp = $this->string_to_number($query);
		$this->db->or_like('harga_satuan', $temp);
		$this->db->or_like('jumlah_pesan', $temp);
		$this->db->or_like('status_produksi', $query);
		$this->db->or_like('status_penerimaan', $query);
		$this->db->or_like('tanggal_mulai', $query);
		$this->db->or_like('tanggal_selesai', $query);
		$this->db->or_like('tanggal_penerimaan', $query);
		$this->db->group_end();
		return $this->db->get('produk_custom', $length, $start)->result();
	}

	function produksi_custom_save()
	{
		$this->db->trans_begin();
		$data['status_produksi'] = 'progress';
		$data['tanggal_mulai'] = $this->input->post('tanggal_mulai');
		$data['lokasi_bahan_id'] = $this->input->post('lokasi_bahan_id');
		$data['estimasi_selesai'] = $this->input->post('estimasi_selesai');
		$data['note'] = $this->input->post('keterangan');
		$produk_custom_id = $this->input->post('produksi_id');
		$this->db->where('produk_custom_id', $produk_custom_id);
		$update = $this->db->update('produk_custom', $data);
		$item = $this->input->post('item');
		$this->db->where('produk_custom_id', $produk_custom_id);
		$this->db->delete('produk_custom_bahan');
		foreach ($item as $key) {
			$data = array();
			$data['produk_custom_id'] = $produk_custom_id;
			$data['bahan_id'] = $key['bahan_id'];
			$data['jumlah'] = $this->string_to_decimal($key['jumlah']);
			$data['created_at'] = Date('Y-m-d H:i:s');
			$this->db->insert('produk_custom_bahan', $data);
		}
		if ($this->db->trans_status() === FALSE) {
			return false;
		}
		$this->db->trans_commit();

		return true;
	}

	function produksi_custom_bahan_by_id($produk_custom_id)
	{
		$this->db->select('produk_custom_bahan.*,bahan.bahan_nama');
		$this->db->join('bahan', 'bahan.bahan_id = produk_custom_bahan.bahan_id');
		$this->db->where('produk_custom_id', $produk_custom_id);
		return $this->db->get('produk_custom_bahan')->result();
	}

	function history_produksi_list($start, $length, $query)
	{
		$this->db->where('status_produksi', 'finish');
		$this->db->where('status_penerimaan', 'Diterima');
		$this->db->group_start();
		$this->db->or_like('deskripsi', $query);
		$this->db->or_like('note', $query);
		$temp = $this->string_to_number($query);
		$this->db->or_like('harga_satuan', $temp);
		$this->db->or_like('jumlah_pesan', $temp);
		$this->db->or_like('status_produksi', $query);
		$this->db->or_like('status_penerimaan', $query);
		$this->db->or_like('tanggal_mulai', $query);
		$this->db->or_like('tanggal_selesai', $query);
		$this->db->or_like('tanggal_penerimaan', $query);
		$this->db->group_end();
		return $this->db->get('produk_custom', $length, $start)->result();
	}

	function history_produksi_count_filter($query)
	{
		$this->db->where('status_produksi', 'finish');
		$this->db->where('status_penerimaan', 'Diterima');
		$this->db->group_start();
		$this->db->or_like('deskripsi', $query);
		$this->db->or_like('note', $query);
		$temp = $this->string_to_number($query);
		$this->db->or_like('harga_satuan', $temp);
		$this->db->or_like('jumlah_pesan', $temp);
		$this->db->or_like('status_produksi', $query);
		$this->db->or_like('status_penerimaan', $query);
		$this->db->or_like('tanggal_mulai', $query);
		$this->db->or_like('tanggal_selesai', $query);
		$this->db->or_like('tanggal_penerimaan', $query);
		$this->db->group_end();
		return $this->db->get('produk_custom')->num_rows();
	}

	function history_produksi_count_all()
	{
		$this->db->where('status_produksi', 'finish');
		$this->db->where('status_penerimaan', 'Diterima');
		return $this->db->get('produk_custom')->num_rows();
	}

	function produksi_custom_by_id($id)
	{
		$this->db->select('`produk_custom`.*,
			`produk_custom`.`deskripsi` AS `produk_nama`');
		$this->db->where('produk_custom.produk_custom_id', $id);
		return $this->db->get('produk_custom')->row();
	}
}
