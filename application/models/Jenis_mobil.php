<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_mobil extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "jenis_mobil";
	}
	function jenis_mobil_list($start,$length,$query){
		$this->db->like('jenis_mobil_id', $query, 'BOTH');
		$this->db->or_like('jenis_mobil_merk', $query, 'BOTH');
		$this->db->or_like('jenis_mobil_nama', $query, 'BOTH');
		$this->db->order_by('jenis_mobil_id', 'desc');
		return $this->db->get('jenis_mobil', $length, $start)->result();
	}
	function jenis_mobil_count_filter($query){
		$this->db->like('jenis_mobil_id', $query, 'BOTH');
		$this->db->or_like('jenis_mobil_merk', $query, 'BOTH');
		$this->db->or_like('jenis_mobil_nama', $query, 'BOTH');
		return $this->db->get('jenis_mobil')->num_rows();
	}
	function jenis_mobil_by_name($query){
		$this->db->like('jenis_mobil_nama', $query, 'BOTH');
		return $this->db->get('jenis_mobil')->row();
	}
	function jenis_mobil_count_all(){
		return $this->db->get('jenis_mobil')->num_rows();
	}
	function is_ready_nama($jenis_mobil_id,$nama){
		$this->db->where('jenis_mobil_nama', $nama);
		$data = $this->db->get('jenis_mobil')->row();
		if($data != null){
			if($data->jenis_mobil_id == $jenis_mobil_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


}

/* End of file Jenis_mobil.php */
/* Location: ./application/models/Jenis_mobil.php */
