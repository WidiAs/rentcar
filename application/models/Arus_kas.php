<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arus_kas extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'arus_kas';
	}
	function arus_list($start,$length,$query){
		$level = $_SESSION['login']['level'];
		$user_id = $_SESSION['login']['user_id'];
		$this->db->select('arus_kas.*, staff.staff_nama, lokasi.lokasi_nama as lokasi_lbl');
		$this->db->join('lokasi', 'arus_kas.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->join('user', 'arus_kas.user_id = user.user_id', 'left');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id', 'left');
		$this->db->group_start();
		$this->db->like('staff.staff_nama', $query, 'BOTH');
		$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
		$this->db->or_like('arus_kas.keterangan', $query, 'BOTH');
		$this->db->or_like('arus_kas.tanggal', $query, 'BOTH');
		$this->db->or_like('arus_kas.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$this->db->where('arus_kas.lokasi_id', $_SESSION["login"]['lokasi_id']);
		}
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('arus_kas.tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('arus_kas.tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['tipe_pembayaran']) && $this->input->get('tipe_pembayaran') != "") {
			$this->db->where('arus_kas.tipe_pembayaran_id', $this->input->get('tipe_pembayaran'));
		}
		if ($level != 4){
			$this->db->where('arus_kas.user_id', $user_id);
		}
		$this->db->order_by('arus_kas.tanggal', 'desc');
		return $this->db->get('arus_kas', $length, $start)->result();
	}
	function arus_count_all(){
		$level = $_SESSION['login']['level'];
		$user_id = $_SESSION['login']['user_id'];
		$this->db->select('arus_kas.*');
		$this->db->join('lokasi', 'arus_kas.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->join('user', 'arus_kas.user_id = user.user_id', 'left');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id', 'left');
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$this->db->where('arus_kas.lokasi_id', $_SESSION["login"]['lokasi_id']);
		}
		if ($level != 4){
			$this->db->where('arus_kas.user_id', $user_id);
		}
		return $this->db->count_all_results('arus_kas');
	}
	function arus_count_filter($query){
		$level = $_SESSION['login']['level'];
		$user_id = $_SESSION['login']['user_id'];
		$this->db->select('arus_kas.*');
		$this->db->join('lokasi', 'arus_kas.lokasi_id = lokasi.lokasi_id', 'left');
		$this->db->join('user', 'arus_kas.user_id = user.user_id', 'left');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id', 'left');
		$this->db->group_start();
		$this->db->like('staff.staff_nama', $query, 'BOTH');
		$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
		$this->db->or_like('arus_kas.keterangan', $query, 'BOTH');
		$this->db->or_like('arus_kas.tanggal', $query, 'BOTH');
		$this->db->or_like('arus_kas.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_SESSION["login"]['lokasi_id'])) {
			$this->db->where('arus_kas.lokasi_id', $_SESSION["login"]['lokasi_id']);
		}
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('arus_kas.tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('arus_kas.tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['tipe_pembayaran']) && $this->input->get('tipe_pembayaran') != "") {
			$this->db->where('arus_kas.tipe_pembayaran_id', $this->input->get('tipe_pembayaran'));
		}
		if ($level != 4){
			$this->db->where('arus_kas.user_id', $user_id);
		}
		return $this->db->count_all_results('arus_kas');
	}

	function pemasukan_manual_total($log_pos_id)
	{
		$this->db->select('sum(jumlah) as qty');
		$this->db->where('user_id', $log_pos_id);
		$this->db->where('jenis', 'pemasukan');
		$this->db->where('insert', 'manual');
		return $this->db->get('arus_kas')->row()->qty;
	}

	function pengeluaran_manual_total($log_pos_id)
	{
		$this->db->select('sum(jumlah) as qty');
		$this->db->where('user_id', $log_pos_id);
		$this->db->where('jenis', 'pengeluaran');
		$this->db->where('insert', 'manual');
		return $this->db->get('arus_kas')->row()->qty;
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
