<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang extends My_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "hutang";
	}

	function hutang_count_all()
	{
		$this->db->join('sewa_rekanan', 'hutang.sewa_rekanan_id = sewa_rekanan.sewa_rekanan_id', 'left');
		$this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
		$this->db->group_by('hutang.hutang_id');
		return $this->db->get('hutang')->num_rows();
	}

	function hutang_count_filter($query)
	{
		$this->db->join('sewa_rekanan', 'hutang.sewa_rekanan_id = sewa_rekanan.sewa_rekanan_id', 'left');
		$this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
		$this->db->group_start();
		$this->db->or_like('sewa_rekanan.no_order', $query, 'BOTH');
		$temp = strtotime($query);
		$date = date("Y-m-d", $temp);
		$this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
		$this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
		$this->db->group_end();
		if ($this->input->get('no_order') != "") {
			$this->db->like('no_order', $this->input->get('no_order'), 'BOTH');
		}
		if ($this->input->get('tenggat_mulai') != "") {
			$this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
		}
		if ($this->input->get('tenggat_end') != "") {
			$this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
		}
		if ($this->input->get('status_pembayaran') != "") {
			$this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
		}
		$this->db->group_by('hutang.hutang_id');
		return $this->db->get('hutang')->num_rows();
	}

	function hutang_list($start, $length, $query)
	{
		$this->db->select('hutang.*,
		sewa_rekanan.no_order,sewa_rekanan.grand_total,sewa_rekanan.rekanan_nama,
		if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, 
		(sewa_rekanan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))) as sisa, sewa_rekanan.status_pembayaran');
		$this->db->join('sewa_rekanan', 'hutang.sewa_rekanan_id = sewa_rekanan.sewa_rekanan_id', 'left');
		$this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
		$this->db->group_start();
		$this->db->or_like('sewa_rekanan.no_order', $query, 'BOTH');
		$temp = strtotime($query);
		$date = date("Y-m-d", $temp);
		$this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
		$this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
		$this->db->group_end();
		if ($this->input->get('no_order') != "") {
			$this->db->like('no_order', $this->input->get('no_order'), 'BOTH');
		}
		if ($this->input->get('tenggat_mulai') != "") {
			$this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
		}
		if ($this->input->get('tenggat_end') != "") {
			$this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
		}
		if ($this->input->get('status_pembayaran') != "") {
			$this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
		}
		$this->db->group_by('hutang.hutang_id');
		$this->db->order_by('hutang.hutang_id', 'desc');
		return $this->db->get('hutang', $length, $start)->result();
	}

	function hutang_by_id($hutang_id)
	{
		$this->db->select('hutang.*,
		sewa_rekanan.no_order,sewa_rekanan.grand_total,sewa_rekanan.rekanan_nama,
		if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, 
		(sewa_rekanan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))) as sisa, sewa_rekanan.status_pembayaran');
		$this->db->join('sewa_rekanan', 'hutang.sewa_rekanan_id = sewa_rekanan.sewa_rekanan_id', 'left');
		$this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
		$this->db->group_by('hutang.hutang_id');
		$this->db->where('hutang.hutang_id', $hutang_id);
		return $this->db->get('hutang')->row();
	}

	function pembayaran_hutang_count($hutang_id)
	{
		$this->db->select('pembayaran_hutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_hutang.hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang')->num_rows();
	}

	function pembayaran_hutang_filter($hutang_id, $query)
	{
		$this->db->select('pembayaran_hutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_hutang.hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang')->num_rows();
	}

	function pembayaran_hutang_list($start, $length, $query, $hutang_id)
	{
		$this->db->select('pembayaran_hutang.*,pembayaran_hutang.jumlah as "old_jumlah",tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->order_by('pembayaran_hutang.pembayaran_hutang_id', 'desc');
		$this->db->where('hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang', $length, $start)->result();
	}

	function insert_pembayaran($data)
	{
		return $this->db->insert('pembayaran_hutang', $data);
	}

	function edit_pembayaran($data, $pembayaran_hutang_id)
	{
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		return $this->db->update('pembayaran_hutang', $data);
	}

	function delete_pembayaran($pembayaran_hutang_id)
	{
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		return $this->db->delete('pembayaran_hutang');
	}

	function detail_pembayaran($pembayaran_hutang_id)
	{
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		$this->db->from('pembayaran_hutang');
		return $this->db->get()->row();
	}

	function laporan_pembayaran_hutang_all()
	{
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
		$this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id');
		return $this->db->get('pembayaran_hutang')->num_rows();
	}

//	function laporan_pembayaran_hutang_filter($query)
//	{
//		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
//		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
//		$this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id');
//		$this->db->group_start();
//		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
//		$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
//		$this->db->group_end();
//		if ($this->input->get('po_bahan_no') != "") {
//			$this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
//		}
//		if ($this->input->get('tipe_pembayaran_id') != "") {
//			$this->db->where('pembayaran_hutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
//		}
//		if ($this->input->get('tanggal_start') != "") {
//			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
//		}
//		if ($this->input->get('tanggal_end') != "") {
//			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
//		}
//		$this->db->order_by('pembayaran_hutang.pembayaran_hutang_id', 'desc');
//		return $this->db->get('pembayaran_hutang')->num_rows();
//	}

//	function laporan_pembayaran_hutang_list($start, $length, $query)
//	{
//		$this->db->select('pembayaran_hutang.*,po_bahan.po_bahan_no,tipe_pembayaran.tipe_pembayaran_nama');
//		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
//		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
//		$this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id');
//		$this->db->group_start();
//		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
//		$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
//		$this->db->group_end();
//		if ($this->input->get('po_bahan_no') != "") {
//			$this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
//		}
//		if ($this->input->get('tipe_pembayaran_id') != "") {
//			$this->db->where('pembayaran_hutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
//		}
//		if ($this->input->get('tanggal_start') != "") {
//			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
//		}
//		if ($this->input->get('tanggal_end') != "") {
//			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
//		}
//		return $this->db->get('pembayaran_hutang', $length, $start)->result();
//	}

	function totalHutang()
	{
		$sql = 'SELECT (sum(sewa_rekanan.grand_total) - sum(pembayaran_hutang.jumlah)) as hutang FROM hutang INNER JOIN sewa_rekanan on hutang.sewa_rekanan_id = sewa_rekanan.sewa_rekanan_id LEFT JOIN pembayaran_hutang on hutang.hutang_id = pembayaran_hutang.hutang_id';
		$res = $this->db->query($sql);
		return $res->row()->hutang;
	}
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */
