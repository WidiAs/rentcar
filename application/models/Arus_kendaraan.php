<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arus_kendaraan extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'arus_kendaraan';
	}

	function arus_list($start, $length, $query)
	{
		$this->db->select('arus_kendaraan.*, staff.staff_nama');
		$this->db->join('user', 'arus_kendaraan.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->group_start();
		$this->db->like('staff.staff_nama', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.keterangan', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.tanggal', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.mobil_jenis', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('arus_kendaraan.tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('arus_kendaraan.tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['mobil_jenis']) && $this->input->get('mobil_jenis') != "") {
			$this->db->where('arus_kendaraan.mobil_jenis', $this->input->get('mobil_jenis'));
		}
		$this->db->order_by('arus_kendaraan.tanggal', 'desc');
		return $this->db->get('arus_kendaraan', $length, $start)->result();
	}

	function arus_count_all()
	{
		$this->db->select('arus_kendaraan.*');
		$this->db->join('user', 'arus_kendaraan.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		return $this->db->count_all_results('arus_kendaraan');
	}

	function arus_count_filter($query)
	{
		$this->db->select('arus_kendaraan.*');
		$this->db->join('user', 'arus_kendaraan.user_id = user.user_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->group_start();
		$this->db->like('staff.staff_nama', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.keterangan', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.tanggal', $query, 'BOTH');
		$this->db->or_like('arus_kendaraan.mobil_jenis', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('arus_kendaraan.tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('arus_kendaraan.tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['mobil_jenis']) && $this->input->get('mobil_jenis') != "") {
			$this->db->where('arus_kendaraan.mobil_jenis', $this->input->get('mobil_jenis'));
		}
		return $this->db->count_all_results('arus_kendaraan');
	}

	function mobil_jenis_arus(){
		$this->db->select('arus_kendaraan.mobil_jenis');
		$this->db->group_by('mobil_jenis');

		return $this->db->get('arus_kendaraan')->result();
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
