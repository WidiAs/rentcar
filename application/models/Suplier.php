<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suplier extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "suplier";
	}	
	function suplier_list($start,$length,$query){
		$this->db->like('suplier_id', $query, 'BOTH'); 
		$this->db->or_like('suplier_kode', $query, 'BOTH'); 
		$this->db->or_like('suplier_nama', $query, 'BOTH'); 
		$this->db->or_like('suplier_alamat', $query, 'BOTH'); 
		$this->db->or_like('suplier_telepon', $query, 'BOTH'); 
		$this->db->or_like('suplier_email', $query, 'BOTH'); 
		$this->db->order_by('suplier_id', 'desc');
		return $this->db->get('suplier', $length, $start)->result();
	}
	function suplier_count_filter($query){
		$this->db->like('suplier_id', $query, 'BOTH'); 
		$this->db->or_like('suplier_kode', $query, 'BOTH'); 
		$this->db->or_like('suplier_nama', $query, 'BOTH'); 
		$this->db->or_like('suplier_alamat', $query, 'BOTH'); 
		$this->db->or_like('suplier_telepon', $query, 'BOTH'); 
		$this->db->or_like('suplier_email', $query, 'BOTH'); 
		return $this->db->get('suplier')->num_rows();
	}
	function suplier_count_all(){
		return $this->db->get('suplier')->num_rows();
	}
	function find_suplier_by_name($name){
		$this->db->where('upper(suplier_nama)', $name);
		$get = $this->db->get('suplier');
		if($get->num_rows() > 0){
			return $get->row()->suplier_id;
		} else {
			$data["suplier_nama"] = $name;
			$this->db->insert('suplier', $data);
			return $this->db->insert_id();
		}
	}	
	function is_ready_kode($suplier_id,$kode){
		$this->db->where('suplier_kode', $kode);
		$data = $this->db->get('suplier')->row();
		if($data != null){
			if($data->suplier_id == $suplier_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Suplier.php */
/* Location: ./application/models/Suplier.php */