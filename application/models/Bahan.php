<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "bahan";
	}	
	function bahan_list($start,$length,$query){

		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		$sql = 'bahan.*,suplier_nama,satuan_nama,jenis_bahan_nama, display_stock_bahan.jumlah as stock';
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"||$this->uri->segment(1)=="custom-produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_bahan_lokasi.* from display_stock_bahan_lokasi where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$condition.') a', 'a.bahan_id = bahan.bahan_id', 'left');
			
		}
		$this->db->select($sql);
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan', $length, $start)->result();
	}
	function bahan_count_all(){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"||$this->uri->segment(1)=="custom-produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();
	}
	function bahan_count_filter($query){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"||$this->uri->segment(1)=="custom-produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();		
	}
	function custom_bahan_list($start,$length,$query){

		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		$sql = 'bahan.*,suplier_nama,satuan_nama,jenis_bahan_nama, display_stock_bahan.jumlah as stock';
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = $_SESSION['custom-produksi']['lokasi'];
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_bahan_lokasi.* from display_stock_bahan_lokasi where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$condition.') a', 'a.bahan_id = bahan.bahan_id', 'left');

		}
		$this->db->select($sql);
		$this->db->group_start();
		$this->db->like('bahan.bahan_id', $query, 'BOTH');
		$this->db->or_like('bahan.bahan_kode', $query, 'BOTH');
		$this->db->or_like('bahan.bahan_nama', $query, 'BOTH');
		$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH');
		$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan', $length, $start)->result();
	}
	function custom_bahan_count_all(){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = $_SESSION['custom-produksi']['lokasi'];
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();
	}
	function custom_bahan_count_filter($query){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")||($this->uri->segment(1)=="custom-produksi")){
			$condition = $_SESSION['custom-produksi']['lokasi'];
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_start();
		$this->db->like('bahan.bahan_id', $query, 'BOTH');
		$this->db->or_like('bahan.bahan_kode', $query, 'BOTH');
		$this->db->or_like('bahan.bahan_nama', $query, 'BOTH');
		$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH');
		$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();
	}
	function low_bahan_list($start,$length,$query){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		$sql = 'bahan.*,suplier_nama,satuan_nama,jenis_bahan_nama, display_stock_bahan.jumlah as stock';
		if(isset($_SESSION['login']['lokasi_id'])){
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id  where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$_SESSION['login']['lokasi_id'].') as a', 'a.bahan_id = bahan.bahan_id', 'left');
			
		} else {
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id) as a', 'a.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->select($sql);
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		$this->db->where('a.jumlah <= bahan.bahan_minimal_stock', '',false);
		return $this->db->get('bahan', $length, $start)->result();
	}
	function low_bahan_count_filter($query){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])){
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id  where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$_SESSION['login']['lokasi_id'].') as a', 'a.bahan_id = bahan.bahan_id', 'left');
			
		} else {
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id) as a', 'a.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		$this->db->where('a.jumlah <= bahan.bahan_minimal_stock', '',false);
		return $this->db->get('bahan')->num_rows();
	}
	function low_bahan_count_all(){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])){
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id  where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$_SESSION['login']['lokasi_id'].') as a', 'a.bahan_id = bahan.bahan_id', 'left');
			
		} else {
			$this->db->join('(select display_stock_bahan_lokasi.*,lokasi.lokasi_nama from display_stock_bahan_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_bahan_lokasi.stock_bahan_lokasi_id) as a', 'a.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		$this->db->where('a.jumlah <= bahan.bahan_minimal_stock', '',false);
		return $this->db->get('bahan')->num_rows();
	}	
	function bahan_by_id($bahan_id){
		$this->db->select('bahan.*,jenis_bahan.jenis_bahan_nama');
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->where('bahan_id', $bahan_id);
		return $this->db->get('bahan')->row();
	}
	function laporan_bahan_list($start,$length,$query){

		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		$sql = 'bahan.*,suplier_nama,satuan_nama,jenis_bahan_nama, display_stock_bahan.jumlah as stock';
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_bahan_lokasi.* from display_stock_bahan_lokasi where display_stock_bahan_lokasi.stock_bahan_lokasi_id = '.$condition.') a', 'a.bahan_id = bahan.bahan_id', 'left');
			
		}
		$this->db->select($sql);
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['jenis_bahan_id'])){
			$this->db->where('jenis_bahan_id', $this->input->get('jenis_bahan_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan', $length, $start)->result();
	}
	function laporan_bahan_count_all(){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();
	}
	function laporan_bahan_count_filter($query){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('suplier', 'bahan.bahan_suplier_id = suplier.suplier_id');
		$this->db->join('display_stock_bahan', 'display_stock_bahan.bahan_id = bahan.bahan_id','left');
		if(isset($_SESSION['login']['lokasi_id'])||($this->uri->segment(1)=="produksi")){
			$condition = "";
			if($this->uri->segment(1)=="produksi"){
				$condition = $_SESSION['produksi']['lokasi'];
			} else {
				$condition = $_SESSION['login']['lokasi_id'];
			}
			$this->db->join('display_stock_bahan_lokasi', 'display_stock_bahan_lokasi.bahan_id = bahan.bahan_id', 'left');
		}
		$this->db->group_start();
			$this->db->like('bahan.bahan_id', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['jenis_bahan_id'])){
			$this->db->where('jenis_bahan_id', $this->input->get('jenis_bahan_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}		
		$this->db->group_by('bahan.bahan_id');
		$this->db->order_by('bahan.bahan_id', 'desc');
		return $this->db->get('bahan')->num_rows();		
	}


	function bahan_by_name($query){
		$this->db->where('bahan_nama = ', $query);
		return $this->db->get('bahan')->row();
	}
}

/* End of file Bahan.php */
/* Location: ./application/models/Bahan.php */
