<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends My_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "piutang";
	}
	function piutang_count_all($lokasi_id){
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->join('pembayaran_piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id', 'left');
		if ($lokasi_id) {
			$this->db->where('penyewaan.lokasi_id =', $lokasi_id);
		}
		$this->db->group_by('piutang.piutang_id');	
		return $this->db->get('piutang')->num_rows();
	}
	function piutang_count_filter($query,$lokasi_id){
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->join('pembayaran_piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id', 'left');
		$this->db->group_start();
			$this->db->or_like('no_invoice', $query, 'BOTH');
			$temp = strtotime($query);
			$date = date("Y-m-d",$temp);
			$this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
			$this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
		$this->db->group_end();
		if ($lokasi_id) {
			$this->db->where('penyewaan.lokasi_id =', $lokasi_id);
		}
		if($this->input->get('no_invoice')!=""){
			$this->db->like('no_invoice', $this->input->get('no_invoice'), 'BOTH');
		}
		if($this->input->get('tenggat_mulai')!=""){
			$this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
		}
		if($this->input->get('tenggat_end')!=""){
			$this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
		}
		if($this->input->get('status_pembayaran')!=""){
			$this->db->where('piutang.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if($this->input->get('pelanggan_nama')!=""){
			$this->db->like('pelanggan_nama', $this->input->get('pelanggan_nama'), 'BOTH');
		}				
		$this->db->group_by('piutang.piutang_id');	
		return $this->db->get('piutang')->num_rows();
	}
	function piutang_list($start,$length,$query,$lokasi_id){
		$this->db->select('piutang.*,penyewaan.pelanggan_nama,penyewaan.no_invoice,penyewaan.grand_total,if(sum(pembayaran_piutang.jumlah)is null,0,sum(pembayaran_piutang.jumlah)) as terbayar, (penyewaan.grand_total-(if(sum(pembayaran_piutang.jumlah)is null,0,sum(pembayaran_piutang.jumlah)))) as sisa');
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->join('pembayaran_piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id', 'left');
		$this->db->group_start();
			$this->db->or_like('no_invoice', $query, 'BOTH');
			$temp = strtotime($query);
			$date = date("Y-m-d",$temp);
			$this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
			$this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
		$this->db->group_end();
		if ($lokasi_id) {
			$this->db->where('penyewaan.lokasi_id =', $lokasi_id);
		}
		if($this->input->get('no_invoice')!=""){
			$this->db->like('no_invoice', $this->input->get('no_invoice'), 'BOTH');
		}
		if($this->input->get('tenggat_mulai')!=""){
			$this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
		}
		if($this->input->get('tenggat_end')!=""){
			$this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
		}
		if($this->input->get('status_pembayaran')!=""){
			$this->db->where('piutang.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if($this->input->get('pelanggan_nama')!=""){
			$this->db->like('pelanggan_nama', $this->input->get('pelanggan_nama'), 'BOTH');
		}		
		$this->db->group_by('piutang.piutang_id');	
		$this->db->order_by('piutang.piutang_id', 'desc');
		return $this->db->get('piutang',$length,$start)->result();		
	}
	function piutang_by_id($piutang_id){
		$this->db->select('piutang.*,penyewaan.pelanggan_nama,penyewaan.no_invoice,penyewaan.grand_total,if(sum(pembayaran_piutang.jumlah)is null,0,sum(pembayaran_piutang.jumlah)) as terbayar, (penyewaan.grand_total-(if(sum(pembayaran_piutang.jumlah)is null,0,sum(pembayaran_piutang.jumlah)))) as sisa');
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->join('pembayaran_piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id', 'left');	
		$this->db->group_by('piutang.piutang_id');
		$this->db->where('piutang.piutang_id', $piutang_id);
		return $this->db->get('piutang')->row();
	}
	function pembayaran_piutang_count($piutang_id){
		$this->db->select('pembayaran_piutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_piutang.piutang_id', $piutang_id);
		return $this->db->get('pembayaran_piutang')->num_rows();
	}
	function pembayaran_piutang_filter($piutang_id,$query){
		$this->db->select('pembayaran_piutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_piutang.piutang_id', $piutang_id);
		return $this->db->get('pembayaran_piutang')->num_rows();
	}
	function pembayaran_piutang_list($start,$length,$query,$piutang_id){
		$this->db->select('pembayaran_piutang.*,pembayaran_piutang.jumlah as "old_jumlah",tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->where('piutang_id', $piutang_id);
		$this->db->order_by('pembayaran_piutang.pembayaran_piutang_id', 'desc');
		return $this->db->get('pembayaran_piutang',$length,$start)->result();		
	}
	function insert_pembayaran($data){
		return $this->db->insert('pembayaran_piutang', $data);
	}
	function edit_pembayaran($data,$pembayaran_piutang_id)	{
		$this->db->where('pembayaran_piutang_id', $pembayaran_piutang_id);
		return $this->db->update('pembayaran_piutang', $data);
	}
	function delete_pembayaran($pembayaran_piutang_id){
		$this->db->where('pembayaran_piutang_id', $pembayaran_piutang_id);
		return $this->db->delete('pembayaran_piutang');
	}
	function detail_pembayaran($pembayaran_piutang_id){
		$this->db->where('pembayaran_piutang_id', $pembayaran_piutang_id);
		$this->db->from('pembayaran_piutang');
		return $this->db->get()->row();
	}
	function laporan_pembayaran_piutang_all(){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->join('piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id');
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		return $this->db->get('pembayaran_piutang')->num_rows();
	}
	function laporan_pembayaran_piutang_filter($query){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->join('piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id');
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->group_start();
			$this->db->like('penyewaan.no_invoice', $query, 'BOTH');
			$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		if($this->input->get('no_invoice')!=""){
			$this->db->like('no_invoice', $this->input->get('no_invoice'), 'BOTH');
		}
		if($this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('pembayaran_piutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if($this->input->get('tanggal_start')!=""){
			$this->db->where('pembayaran_piutang.tanggal >=', $this->input->get('tanggal_start'));
		}
		if($this->input->get('tanggal_end')!=""){
			$this->db->where('pembayaran_piutang.tanggal <=', $this->input->get('tanggal_end'));
		}
		return $this->db->get('pembayaran_piutang')->num_rows();
	}
	function laporan_pembayaran_piutang_list($start,$length,$query){
		$this->db->select('pembayaran_piutang.*,penyewaan.no_invoice,tipe_pembayaran.tipe_pembayaran_nama');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_piutang.tipe_pembayaran_id');
		$this->db->join('piutang', 'pembayaran_piutang.piutang_id = piutang.piutang_id');
		$this->db->join('penyewaan', 'piutang.penyewaan_id = penyewaan.penyewaan_id');
		$this->db->group_start();
			$this->db->like('penyewaan.no_invoice', $query, 'BOTH');
			$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		if($this->input->get('no_invoice')!=""){
			$this->db->like('no_invoice', $this->input->get('no_invoice'), 'BOTH');
		}
		if($this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('pembayaran_piutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if($this->input->get('tanggal_start')!=""){
			$this->db->where('pembayaran_piutang.tanggal >=', $this->input->get('tanggal_start'));
		}
		if($this->input->get('tanggal_end')!=""){
			$this->db->where('pembayaran_piutang.tanggal <=', $this->input->get('tanggal_end'));
		}
		$this->db->order_by('pembayaran_piutang.pembayaran_piutang_id', 'desc');
		return $this->db->get('pembayaran_piutang',$length,$start)->result();		
	}
	function totalPiutang(){
		$sql = 'SELECT (sum(penyewaan.grand_total) - sum(pembayaran_piutang.jumlah)) as piutang FROM piutang INNER JOIN penyewaan on piutang.penyewaan_id = penyewaan.penyewaan_id LEFT JOIN pembayaran_piutang on piutang.piutang_id = pembayaran_piutang.piutang_id' ;
		$res = $this->db->query($sql);
		return $res->row()->piutang;
	}	
}

/* End of file Piutang.php */
/* Location: ./application/models/Piutang.php */
