<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan_driver extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "kegiatan_driver";
	}

	function range_tanggal_sewa($penyewaan_id){
		$this->db->distinct('tanggal');
		$this->db->select('tanggal');
		$this->db->where('penyewaan_id', $penyewaan_id);

		return $this->db->get('kegiatan_driver')->result();
	}

	function jadwal_list($start,$length,$query){
		$this->db->select('kegiatan_driver.*, mobil.status');
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('jadwal_id', 'desc');
		return $this->db->get('kegiatan_driver', $length, $start)->result();
	}
	function jadwal_count_filter($query){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->get('kegiatan_driver')->num_rows();
	}
	function jadwal_count_all(){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		return $this->db->get('kegiatan_driver')->num_rows();
	}

	function history_driver_list($start,$length,$query,$driver_id){
		$this->db->select('kegiatan_driver.*, mobil.status');
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		$this->db->where('kegiatan_driver.driver_id', $driver_id);
		$this->db->order_by('kegiatan_driver.tanggal', 'desc');
		return $this->db->get('kegiatan_driver', $length, $start)->result();
	}
	function history_driver_count_filter($query,$driver_id){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		$this->db->where('kegiatan_driver.driver_id', $driver_id);
		return $this->db->get('kegiatan_driver')->num_rows();
	}
	function history_driver_count_all($driver_id){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		$this->db->where('kegiatan_driver.driver_id', $driver_id);
		return $this->db->get('kegiatan_driver')->num_rows();
	}



	function history_all_list($start,$length,$query){
		$this->db->select('kegiatan_driver.*, mobil.status');
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['driver_nama']) && $this->input->get('driver_nama') != "") {
			$this->db->like('driver_nama', $this->input->get('driver_nama'));
		}
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		$this->db->order_by('kegiatan_driver.tanggal', 'desc');
		return $this->db->get('kegiatan_driver', $length, $start)->result();
	}
	function history_all_count_filter($query){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['driver_nama']) && $this->input->get('driver_nama') != "") {
			$this->db->like('driver_nama', $this->input->get('driver_nama'));
		}
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		return $this->db->get('kegiatan_driver')->num_rows();
	}
	function history_all_count_all(){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->where('kegiatan_driver.tanggal < ', date('Y-m-d'));
		return $this->db->get('kegiatan_driver')->num_rows();
	}

	function jadwal_all_list($start,$length,$query){
		$this->db->select('kegiatan_driver.*, mobil.status');
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['driver_nama']) && $this->input->get('driver_nama') != "") {
			$this->db->like('driver_nama', $this->input->get('driver_nama'));
		}
		$this->db->where('kegiatan_driver.status !=', 'Selesai');
		$this->db->order_by('kegiatan_driver.tanggal', 'desc');
		return $this->db->get('kegiatan_driver', $length, $start)->result();
	}
	function jadwal_all_count_filter($query){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->group_start();
		$this->db->like('mobil_jenis', $query, 'BOTH');
		$this->db->or_like('mobil_plat', $query, 'BOTH');
		$this->db->group_end();
		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['driver_nama']) && $this->input->get('driver_nama') != "") {
			$this->db->like('driver_nama', $this->input->get('driver_nama'));
		}
		$this->db->where('kegiatan_driver.status !=', 'Selesai');
		return $this->db->get('kegiatan_driver')->num_rows();
	}
	function jadwal_all_count_all(){
		$this->db->join('mobil', 'kegiatan_driver.mobil_id = mobil.mobil_id', 'left');
		$this->db->where('kegiatan_driver.status !=', 'Selesai');
		return $this->db->get('kegiatan_driver')->num_rows();
	}

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
