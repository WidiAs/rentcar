<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Size extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "size";
	}	
	function size_list($start,$length,$query){
		$this->db->like('size_id', $query, 'BOTH'); 
		$this->db->or_like('size_nama', $query, 'BOTH'); 
		$this->db->order_by('size_id', 'desc');
		return $this->db->get('size', $length, $start)->result();
	}
	function size_count_all(){
		return $this->db->get('size')->num_rows();
	}
	function size_count_filter($query){
		$this->db->like('size_id', $query, 'BOTH'); 
		$this->db->or_like('size_nama', $query, 'BOTH'); 
		return $this->db->get('size')->num_rows();
	}
	function size_by_name($query){
		$this->db->like('size_nama', $query, 'BOTH');
		return $this->db->get('size')->row();
	}
	function is_ready_kode($size_id,$kode){
		$this->db->where('size_kode', $kode);
		$data = $this->db->get('size')->row();
		if($data != null){
			if($data->size_id == $size_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Size.php */
/* Location: ./application/models/Size.php */
