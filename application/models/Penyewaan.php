<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penyewaan extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'penyewaan';
	}


	function getUrutan()
	{
		$date = date("Y-m-d");
		$this->db->select('max(count) as count');
		$this->db->where('tanggal_order', $date);
		$count = $this->db->get('penyewaan')->row()->count;
		if ($count == null) {
			$count = 1;
		} else {
			$count++;
		}
		return $count;
	}

	function getFakturCode()
	{
		$date = date("ymd");
		$count = $this->getUrutan();
		$lok = "00";
		if (isset($_SESSION['login']['lokasi_id'])) {
			$lok = $_SESSION['login']['lokasi_id'];
		}
		$tipe = "INV";
		$faktur = $tipe . '-' . $date . '/' . $lok . '/' . $count;
		return $faktur;
	}

	function penyewaan_list($start,$length,$query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();

		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('penyewaan.tanggal_order >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('penyewaan.tanggal_order <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['pelanggan_nama']) && $this->input->get('pelanggan_nama') != "") {
			$this->db->where('penyewaan.pelanggan_nama', $this->input->get('pelanggan_nama'));
		}
		$this->db->order_by('penyewaan_id', 'desc');
		return $this->db->get('penyewaan', $length, $start)->result();
	}
	function penyewaan_count_filter($query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();

		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('penyewaan.tanggal_order >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('penyewaan.tanggal_order <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['pelanggan_nama']) && $this->input->get('pelanggan_nama') != "") {
			$this->db->where('penyewaan.pelanggan_nama', $this->input->get('pelanggan_nama'));
		}
		return $this->db->get('penyewaan')->num_rows();
	}
	function penyewaan_count_all(){
		return $this->db->get('penyewaan')->num_rows();
	}

	function penyewaan_list_order($start,$length,$query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status', 'order');
		$this->db->order_by('penyewaan_id', 'desc');
		return $this->db->get('penyewaan', $length, $start)->result();
	}
	function penyewaan_count_filter_order($query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('status', 'order');
		return $this->db->get('penyewaan')->num_rows();
	}
	function penyewaan_count_all_order(){
		$this->db->where('status', 'order');
		return $this->db->get('penyewaan')->num_rows();
	}


	function penyewaan_by_id($penyewaan_id){
		$this->db->select('penyewaan.*, penawaran.no_penawaran');
		$this->db->join('penawaran', 'penawaran.penawaran_id = penyewaan.penawaran_id', 'left');
		$this->db->where('penyewaan_id', $penyewaan_id);
		$this->db->order_by('penyewaan_id', 'desc');
		return $this->db->get('penyewaan')->row();
	}


	function get_pelanggan(){
		$this->db->select('penyewaan.pelanggan_nama');
		$this->db->order_by('pelanggan_nama', 'desc');
		$this->db->group_by('pelanggan_nama');
		return $this->db->get('penyewaan')->result();
	}



	function penyewaan_list_selesai($start,$length,$query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();

		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('penyewaan.tanggal_order >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('penyewaan.tanggal_order <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['pelanggan_nama']) && $this->input->get('pelanggan_nama') != "") {
			$this->db->where('penyewaan.pelanggan_nama', $this->input->get('pelanggan_nama'));
		}
		$this->db->where('status', 'selesai');
		$this->db->order_by('penyewaan_id', 'desc');
		return $this->db->get('penyewaan', $length, $start)->result();
	}
	function penyewaan_count_filter_selesai($query){
		$this->db->group_start();
		$this->db->like('no_invoice', $query, 'BOTH');
		$this->db->or_like('pelanggan_nama', $query, 'BOTH');
		$this->db->group_end();

		if (isset($_GET['tanggal_start']) && $this->input->get('tanggal_start') != "") {
			$this->db->where('penyewaan.tanggal_order >=', $this->input->get('tanggal_start'));
		}
		if (isset($_GET['tanggal_end']) && $this->input->get('tanggal_end') != "") {
			$this->db->where('penyewaan.tanggal_order <=', $this->input->get('tanggal_end'));
		}
		if (isset($_GET['pelanggan_nama']) && $this->input->get('pelanggan_nama') != "") {
			$this->db->where('penyewaan.pelanggan_nama', $this->input->get('pelanggan_nama'));
		}
		$this->db->where('status', 'selesai');
		return $this->db->get('penyewaan')->num_rows();
	}
	function penyewaan_count_all_selesai(){
		$this->db->where('status', 'selesai');
		return $this->db->get('penyewaan')->num_rows();
	}

}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
