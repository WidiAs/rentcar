<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_bahan extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "jenis_bahan";
	}	
	function jenis_bahan_list($start,$length,$query){
		$this->db->like('jenis_bahan_id', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan_kode', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan_nama', $query, 'BOTH'); 
		$this->db->order_by('jenis_bahan_id', 'desc');
		return $this->db->get('jenis_bahan', $length, $start)->result();
	}
	function jenis_bahan_count_all(){
		return $this->db->get('jenis_bahan')->num_rows();
	}
	function jenis_bahan_count_filter($query){
		$this->db->like('jenis_bahan_id', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan_kode', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan_nama', $query, 'BOTH'); 
		return $this->db->get('jenis_bahan')->num_rows();
	}	
	function is_ready_kode($jenis_bahan_id,$kode){
		$this->db->where('jenis_bahan_kode', $kode);
		$data = $this->db->get('jenis_bahan')->row();
		if($data != null){
			if($data->jenis_bahan_id == $jenis_bahan_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Jenis_bahan.php */
/* Location: ./application/models/Jenis_bahan.php */