<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penyewaan_mobil extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'penyewaan_mobil';
	}

	function penyewaan_detail($id)
	{
		$this->db->select('penyewaan_mobil.*');
		$this->db->where('penyewaan_id', $id);
		return $this->db->get('penyewaan_mobil')->result();
	}

	function list_by_penawaran($id)
	{
		$this->db->select('penyewaan_mobil.*, jenis_mobil.jenis_mobil_nama, mobil.plat_mobil');
		$this->db->join('mobil', 'mobil on penyewaan_mobil.mobil_id = mobil.mobil_id');
		$this->db->join('jenis_mobil', 'jenis_mobil on mobil.jenis_mobil_id = jenis_mobil.jenis_mobil_id');
		$this->db->where('penyewaan_id', $id);
		return $this->db->get('penyewaan_mobil')->result();
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
