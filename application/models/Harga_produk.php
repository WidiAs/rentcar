<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "harga_produk";
	}
	function harga_produk_count($produk_id){
		$this->db->where('produk_id', $produk_id);
		return $this->db->get('harga_produk')->num_rows();
	}
	function harga_produk_count_filter($query,$produk_id){
		$this->db->where('produk_id', $produk_id);
		$this->db->group_start();
			$this->db->like('minimal_pembelian', $query, 'BOTH');
			$this->db->or_like('harga', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->get('harga_produk')->num_rows();
	}
	function harga_produk_list($start,$length,$query,$produk_id){
		$this->db->where('produk_id', $produk_id);
		$this->db->group_start();
			$this->db->like('minimal_pembelian', $query, 'BOTH');
			$this->db->or_like('harga', $query, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="pos"){
			$this->db->order_by('minimal_pembelian', 'desc');
		}
		return $this->db->get('harga_produk', $length, $start)->result();
	}

}

/* End of file Harga_produk.php */
/* Location: ./application/models/Harga_produk.php */